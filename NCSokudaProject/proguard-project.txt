# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-ignorewarnings
-keepattributes Signature

-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

# 添加第三方jar包

-libraryjars libs/android-support-v13.jar
-libraryjars libs/android-support-v7-recyclerview.jar
-libraryjars libs/universal-image-loader-1.9.3.jar
-libraryjars libs/MiPush_SDK_Client_2_2_16.jar

#1-libraryjars ../JsonPatch/libs/guava-18.0.jar
#1-libraryjars ../JsonPatch/libs/jackson-annotations-2.4.0.jar
#1-libraryjars ../JsonPatch/libs/jackson-core-2.4.2.jar
#1-libraryjars ../JsonPatch/libs/jackson-databind-2.4.2.jar
#1-libraryjars ../JsonPatch/libs/jsr305-1.3.9.jar
#1-libraryjars ../JsonPatch/libs/msg-simple-1.1.jar

#1-libraryjars ../NC_SDK_Lib/libs/android-async-http-1.4.6.jar
#1-libraryjars ../NC_SDK_Lib/libs/eventbus-2.4.0.jar
#1-libraryjars ../NC_SDK_Lib/libs/httpcore-4.3.3.jar
#1-libraryjars ../NC_SDK_Lib/libs/httpmime-4.3.6.jar

#1-libraryjars ../OpenSourceLibrary/libs/android-support-v13.jar

#1-libraryjars ../SipClientLibrary/libs/armeabi/libsipclient-jni.so
#1-libraryjars ../SipClientLibrary/libs/armeabi-v7a/libsipclient-jni.so
#1-libraryjars ../SipClientLibrary/libs/arm64-v8a/libsipclient-jni.so

#1-libraryjars ../UmengLibrary/libs/umeng-analytics-v5.5.3.jar
#1-libraryjars ../UmengLibrary/libs/umeng-update-v2.6.0.1.jar

-keep public class * extends android.app.Fragment    
-keep public class * extends android.app.Activity  
-keep public class * extends android.app.Application  
-keep public class * extends android.app.Service  
-keep public class * extends android.content.BroadcastReceiver  
-keep public class * extends android.content.ContentProvider  
-keep public class * extends android.app.backup.BackupAgentHelper  
-keep public class * extends android.preference.Preference  
-keep public class * extends android.support.**  
-keep public class com.android.vending.licensing.ILicensingService

-keep class android.support.** { *; }  
-keep interface android.support.** { *; }

-keep public class org.apache.http.** { *; }
-keep public class com.google.common.** { *; }


-keep class com.fasterxml.jackson.** { *; }
-keep class com.alimama.** { *; }
#-keep class com.loopj.android.** { *; }

-keepclassmembers class * {
   public <init>(org.json.JSONObject);
}

-keep public class com.micyun.R$*{
	public static final int *;
}

#这里com.xiaomi.mipushdemo.DemoMessageRreceiver改成app中定义的完整类名
-keep class com.xiaomi.mipush.sdk.DemoMessageReceiver {*;}

# 保持自定义控件类不被混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

# 保持自定义控件类不被混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# 保持 native 方法不被混淆
-keepclasseswithmembernames class * {
	native <methods>;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
 
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

