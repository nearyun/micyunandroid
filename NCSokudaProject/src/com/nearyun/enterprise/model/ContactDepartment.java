package com.nearyun.enterprise.model;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import com.nearyun.contact.ui.util.PinyinComparator;

public class ContactDepartment {
	private static final String LABEL_ID = "id";
	private static final String LABEL_NAME = "name";
	private static final String LABEL_MEMEBERS = "members";

	private String name;
	private int id;
	private final ArrayList<ContactMember> members = new ArrayList<ContactMember>();

	public static ContactDepartment create(JSONObject json) {
		int id = json.optInt(LABEL_ID);
		String name = json.optString(LABEL_NAME);
		ContactDepartment department = new ContactDepartment(id, name);
		JSONArray array = json.optJSONArray(LABEL_MEMEBERS);
		for (int i = 0; i < array.length(); i++) {
			department.add(ContactMember.create(array.optJSONObject(i), id, name));
		}
		Collections.sort(department.getMembers(), new PinyinComparator());// 根据a-z进行排序
		return department;
	}

	protected ContactDepartment(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public void add(ContactMember member) {
		members.add(member);
	}

	public ArrayList<ContactMember> getMembers() {
		return members;
	}

	public ContactMember getMembersItem(int position) {
		return members.get(position);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
