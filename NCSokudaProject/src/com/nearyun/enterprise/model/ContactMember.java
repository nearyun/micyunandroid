package com.nearyun.enterprise.model;

import org.json.JSONObject;

import com.nearyun.contact.model.Contact;
import com.nearyun.contact.model.ContactEx;

public class ContactMember extends ContactEx {

	private static final long serialVersionUID = -3364929488234963487L;

	// "contact_id": "25",
	// "contact_avatar": "/images/it_people.png",
	// "contact_name": "小红刚刚",
	// "contact_sex": "女",
	// "contact_department": "研发部门",
	// "contact_job": "设计师",
	// "contact_mobile": "15948573647",
	// "contact_mail": "xiaohong@nearyun.com"

	private static final String LABEL_ID = "id";
	private static final String LABEL_AVATAR = "avatar";
	private static final String LABEL_NAME = "name";
	private static final String LABEL_SEX = "sex";
	private static final String LABEL_JOB = "job";
	private static final String LABEL_MOBILE = "mobile";
	private static final String LABEL_MAIL = "mail";

	private int dId;// 部门id
	private String department;
	private int mId;// 个人id
	private String avatar;
	private String sex;
	private String job;
	private String mail;

	public static ContactMember create(JSONObject json, int did, String department) {
		String name = json.optString(LABEL_NAME);
		String phone = json.optString(LABEL_MOBILE);
		Contact contact = new Contact(name, phone);
		ContactMember epc = new ContactMember(contact);
		epc.setId(json.optInt(LABEL_ID));
		epc.setJob(json.optString(LABEL_JOB));
		epc.setMail(json.optString(LABEL_MAIL));
		epc.setSex(json.optString(LABEL_SEX));
		epc.setAvatar(json.optString(LABEL_AVATAR));
		epc.setDId(did);
		epc.setDepartment(department);
		return epc;
	}

	protected ContactMember(Contact contact) {
		super(contact);
	}

	public int getDId() {
		return dId;
	}

	public void setDId(int id) {
		this.dId = id;
	}

	public int getMId() {
		return mId;
	}

	public void setId(int id) {
		this.mId = id;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

}
