package com.nearyun.enterprise.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.adapter.AbsBaseAdapter;
import com.micyun.util.ImageLoaderUtil;
import com.nearyun.enterprise.model.ContactMember;
import com.tornado.util.ViewHolderUtil;

public class EnterpriseContactAdapter extends AbsBaseAdapter<ContactMember> {

	public EnterpriseContactAdapter(Context cxt) {
		super(cxt, new ArrayList<ContactMember>());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_enterprise_contact_layout, parent, false);
		}

		TextView itemAlpha = ViewHolderUtil.get(convertView, R.id.item_alpha);
		ImageView itemAvatar = ViewHolderUtil.get(convertView, R.id.item_avatar_imageview);
		TextView itemName = ViewHolderUtil.get(convertView, R.id.item_name);
		TextView itemPhone = ViewHolderUtil.get(convertView, R.id.item_phone);
		View itemSelected = ViewHolderUtil.get(convertView, R.id.item_selected);

		final ContactMember contact = mDataList.get(position);
		final String name = contact.getName();
		final String phone = contact.getPhone();
		itemName.setText(name);
		itemPhone.setText(phone);

		// 根据position获取分类的首字母的Char ascii值
		int did = getDepartmentId(position);
		int lastDid = position == 0 ? -100 : getDepartmentId(position - 1);
		// 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
		if (position == 0 || did != lastDid) {
			itemAlpha.setVisibility(View.VISIBLE);
			itemAlpha.setText(contact.getDepartment());
		} else {
			itemAlpha.setVisibility(View.GONE);
		}

		if (contact.isSelected()) {
			itemSelected.setBackgroundResource(R.drawable.item_check_on);
		} else {
			itemSelected.setBackgroundResource(R.drawable.item_check_off);
		}
		ImageLoaderUtil.refreshAvatar(mContext, name, phone, itemAvatar);

		return convertView;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 */
	public void updateListViewData(ArrayList<ContactMember> list) {
		updateListViewDataOnly(list);
		notifyDataSetChanged();
	}

	public void updateListViewDataOnly(ArrayList<ContactMember> list) {
		mDataList = list;
	}

	private int getDepartmentId(int position) {
		return mDataList.get(position).getDId();
	}

}
