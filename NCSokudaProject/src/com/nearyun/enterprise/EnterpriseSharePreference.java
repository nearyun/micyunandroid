package com.nearyun.enterprise;

import android.content.Context;
import android.content.SharedPreferences;

public class EnterpriseSharePreference {

	private static final String FILE_NAME = "enterprise.io_";

	private static final String KEY_VERSION = "version";
	private static final String KEY_CONTENT = "content";

	public static final void saveVersion(Context context, String userId, int version) {
		SharedPreferences preferences = context.getSharedPreferences(FILE_NAME + userId, Context.MODE_PRIVATE);
		preferences.edit().putInt(KEY_VERSION, version).commit();
	}

	public static final int readVersion(Context context, String userId) {
		SharedPreferences preferences = context.getSharedPreferences(FILE_NAME + userId, Context.MODE_PRIVATE);
		return preferences.getInt(KEY_VERSION, -1);
	}

	public static final void saveContent(Context context, String userId, String data) {
		SharedPreferences preferences = context.getSharedPreferences(FILE_NAME + userId, Context.MODE_PRIVATE);
		preferences.edit().putString(KEY_CONTENT, data).commit();
	}

	public static final String readContent(Context context, String userId) {
		SharedPreferences preferences = context.getSharedPreferences(FILE_NAME + userId, Context.MODE_PRIVATE);
		return preferences.getString(KEY_CONTENT, null);
	}

	public static final void clean(Context context, String userId) {
		SharedPreferences preferences = context.getSharedPreferences(FILE_NAME + userId, Context.MODE_PRIVATE);
		preferences.edit().remove(KEY_VERSION).remove(KEY_CONTENT).commit();
	}
}
