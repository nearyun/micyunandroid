package com.nearyun.contact.ui;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.view.ClearEditText;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.nearyun.contact.adapter.ContactAdapter;
import com.nearyun.contact.adapter.ContactSelectedAdapter;
import com.nearyun.contact.adapter.ContactSelectedAdapter.OnRecyclerViewListener;
import com.nearyun.contact.model.Contact;
import com.nearyun.contact.model.ContactEx;
import com.nearyun.contact.ui.util.ContactsHelper;
import com.nearyun.contact.ui.util.PinyinComparator;
import com.nearyun.contact.ui.view.AlphabetView;
import com.nearyun.contact.ui.view.AlphabetView.OnTouchingLetterChangedListener;
import com.tornado.util.ValidateUtils;

/**
 * 通讯录界面
 * 
 * @author xiaohua
 * 
 */
public class ContactActivity extends BaseActivity {
	public static final String KEY_DATA = "KEY_DATA";

	public static final void newInstance(Activity activity, ArrayList<String> data, int requestCode) {
		Intent intent = new Intent(activity, ContactActivity.class);
		intent.putExtra(KEY_DATA, data);
		activity.startActivityForResult(intent, requestCode);
	}

	private final int MAX_SELECTED = 10;

	private ClearEditText msearchEditText;
	private TextView characterDialog;
	private AlphabetView rightLetterView;

	private TextView extraTipView;
	private Button extraButton;
	private View extraRootView;

	private ListView contactListView;
	private ContactAdapter mContactAdapter;// 联系人表
	private final ArrayList<ContactEx> contactData = new ArrayList<ContactEx>();

	private InputMethodManager inputMethodManager;

	/** 根据拼音来排列ListView里面的数据类 */
	private final PinyinComparator pinyinComparator = new PinyinComparator();

	/** 已选中数据的存放地 */
	private final ArrayList<ContactEx> selectedData = new ArrayList<ContactEx>();
	private RecyclerView mHorizontalListView;
	private ContactSelectedAdapter mContactSelectedAdapter;
	private Button confirmButton;

	/** 从外部传入的数据 */
	private ArrayList<String> phoneList = null;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		phoneList = (ArrayList<String>) intent.getSerializableExtra(KEY_DATA);
		if (phoneList == null) {
			phoneList = new ArrayList<String>();
		}

		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		init();
	}

	private void init() {
		initListView();
		initHorizontalListView();
		initRightLetterView();
		initEditText();
		new ObtainContactDataTask().execute();
	}

	private void refreshUI(int size) {
		if (size == 0) {
			confirmButton.setText("确定");
			confirmButton.setEnabled(false);
		} else {
			confirmButton.setText("确定(" + size + ")");
			confirmButton.setEnabled(true);
		}
		mContactSelectedAdapter.notifyDataSetChanged();
		mContactAdapter.notifyDataSetChanged();
	}

	private void initHorizontalListView() {
		confirmButton = (Button) findViewById(R.id.confirmButton);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (selectedData.size() == 0)
					return;
				Intent intent = new Intent();
				intent.putExtra(KEY_DATA, selectedData);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
		mHorizontalListView = (RecyclerView) findViewById(R.id.horizontalRecyclerView);
		mContactSelectedAdapter = new ContactSelectedAdapter(mActivity, selectedData);
		mHorizontalListView.setHasFixedSize(true);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
		mHorizontalListView.setLayoutManager(layoutManager);
		mContactSelectedAdapter.setOnRecyclerViewListener(new OnRecyclerViewListener() {

			@Override
			public void onItemClick(int position) {
				ContactEx item = selectedData.get(position);
				item.setSelected(false);
				selectedData.remove(item);
				refreshUI(selectedData.size());
			}
		});
		mHorizontalListView.setAdapter(mContactSelectedAdapter);

	}

	private void initEditText() {
		extraRootView = findViewById(R.id.extraRootView);
		extraTipView = (TextView) findViewById(R.id.extraTipView);
		extraButton = (Button) findViewById(R.id.extraButton);
		extraButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String phone = msearchEditText.getText().toString();
				if (selectedData.size() == MAX_SELECTED) {
					showToast("一次只能邀请10个号码");
					return;
				} else {
					msearchEditText.setText("");
					selectedData.add(new ContactEx(new Contact(phone, phone), true));
					refreshUI(selectedData.size());
				}
				hideSoftInput();
			}
		});
		msearchEditText = (ClearEditText) findViewById(R.id.searchEditText);
		// 根据输入框输入值的改变来过滤搜索
		msearchEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// 当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
				filterData(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String content = s.toString().trim();
				boolean flag = mContactAdapter.getCount() == 0 && content.length() > 0;
				if (flag) {
					boolean isNumeric = ValidateUtils.isNumeric(content);
					if (isNumeric) {
						extraRootView.setVisibility(View.VISIBLE);
						extraTipView.setText(content);
						// boolean isMobile = ValidateUtils.isMobile(content);
						boolean isMobile = content.length() > 3;
						extraButton.setEnabled(isMobile);
					} else {
						extraRootView.setVisibility(View.GONE);
					}
				} else {
					extraRootView.setVisibility(View.GONE);
				}
			}
		});
	}

	/**
	 * 根据输入框中的值来过滤数据并更新ListView
	 * 
	 * @param filterStr
	 */
	private void filterData(String filterStr) {
		ArrayList<ContactEx> filterDateList = new ArrayList<ContactEx>();
		if (TextUtils.isEmpty(filterStr)) {
			filterDateList = contactData;
		} else {
			for (ContactEx sortModel : contactData) {
				String name = sortModel.getName();
				String fullPinYin = sortModel.getFullPinYin();
				String phone = sortModel.getPhone();
				if (TextUtils.isEmpty(name))
					continue;
				if (name.indexOf(filterStr) != -1 || fullPinYin.indexOf(filterStr) != -1
						|| phone.indexOf(filterStr) != -1) {
					filterDateList.add(sortModel);
				}
			}
		}
		mContactAdapter.updateListViewData(filterDateList);
	}

	private class ObtainContactDataTask extends AsyncTask<Void, Void, Void> {
		LoadingDialog dialog;

		public ObtainContactDataTask() {
			dialog = new LoadingDialog(mActivity);
		}

		@Override
		protected void onPreExecute() {
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			filledData();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			refreshUI(selectedData.size());
			dialog.dismiss();
		}

	}

	/**
	 * 为ListView填充数据
	 * 
	 * @param date
	 * @return
	 */
	private void filledData() {
		ArrayList<Contact> datas = ContactsHelper.getPhoneContacts(mActivity);
		int total = datas.size();
		for (int i = 0; i < total; i++) {
			Contact contact = datas.get(i);
			contactData.add(new ContactEx(contact));
		}

		Collections.sort(contactData, pinyinComparator);// 根据a-z进行排序

		int len = phoneList.size();
		for (int i = 0; i < len; i++) {
			boolean exist = false;
			String phone = phoneList.get(i);
			for (int j = 0; j < total; j++) {
				ContactEx item = contactData.get(j);
				if (TextUtils.equals(phone, item.getPhone())) {
					item.setSelected(true);
					selectedData.add(item);
					exist = true;
					break;
				}
			}
			if (!exist) {
				selectedData.add(new ContactEx(new Contact(phone, phone), true));
			}
		}
		mContactAdapter.updateListViewDataOnly(contactData);
	}

	private void initListView() {
		contactListView = (ListView) findViewById(R.id.contactListView);
		View emptyView = findViewById(R.id.emptyView);
		contactListView.setEmptyView(emptyView);
		mContactAdapter = new ContactAdapter(this);
		contactListView.setAdapter(mContactAdapter);
		contactListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				ContactEx item = (ContactEx) mContactAdapter.getItem(position);
				if (item.isSelected()) {
					item.setSelected(false);
					selectedData.remove(item);
				} else {
					if (selectedData.size() == MAX_SELECTED) {
						showToast("一次只能邀请10个号码");
						return;
					}

					item.setSelected(true);
					selectedData.add(item);
				}
				refreshUI(selectedData.size());
			}
		});
		contactListView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// 隐藏软键盘
				hideSoftInput();
				return false;
			}
		});

	}

	private void hideSoftInput() {
		if (mActivity.getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (mActivity.getCurrentFocus() != null)
				inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	private void initRightLetterView() {
		rightLetterView = (AlphabetView) findViewById(R.id.rightCharacterLetter);
		characterDialog = (TextView) findViewById(R.id.characterDialog);
		rightLetterView.setTextView(characterDialog);
		rightLetterView.setOnTouchingLetterChangedListener(new OnTouchingLetterChangedListener() {

			@Override
			public void onTouchingLetterChanged(String s) {
				// 该字母首次出现的位置
				int position = mContactAdapter.getPositionForSection(s.charAt(0));
				if (position != -1) {
					contactListView.setSelection(position);
				}
			}
		});
	}

}
