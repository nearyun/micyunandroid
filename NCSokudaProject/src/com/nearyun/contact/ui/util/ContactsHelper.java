package com.nearyun.contact.ui.util;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Contacts.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;

import com.nearyun.contact.model.Contact;

/**
 * 从数据库中获取通讯录数据
 * 
 * @author xiaohua
 * 
 */
public class ContactsHelper {
	private static final String TAG = ContactsHelper.class.getSimpleName();

	private static final String[] PHONES_PROJECTION = new String[] { Phone.DISPLAY_NAME, Phone.NUMBER };

	private static final String[] SELECTION_ARGS = new String[] { "工作宝", "第一企信", "会议宝", "来电", "微会", "一刻", "易信", "钉钉",
			"触宝" };

	private static final boolean includeArgs(String name) {
		if (TextUtils.isEmpty(name))
			return false;
		for (int i = 0; i < SELECTION_ARGS.length; i++) {
			boolean flag = name.contains(SELECTION_ARGS[i]);
			if (flag)
				return true;
		}
		return false;
	}

	// 取本机通讯录
	public static ArrayList<Contact> getPhoneContacts(Context mContext) {
		ArrayList<Contact> contactsList = new ArrayList<Contact>();
		ContentResolver resolver = mContext.getContentResolver();
		// 获取手机联系人
		Cursor phoneCursor = resolver.query(Phone.CONTENT_URI, PHONES_PROJECTION, null, null, PHONES_PROJECTION[0]
				+ " ASC");
		if (phoneCursor != null) {
			try {
				while (phoneCursor.moveToNext()) {
					String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(Phone.NUMBER)); // 获取联系人number
					if (TextUtils.isEmpty(phoneNumber)) {
						continue;
					}
					phoneNumber = phoneNumber.replace(" ", "");
					String name = phoneCursor.getString(phoneCursor.getColumnIndex(Phone.DISPLAY_NAME));// 获取联系人name
					if (!includeArgs(name)) {
						Contact contact = new Contact(name, phoneNumber);
						contactsList.add(contact);
					}
				}
			} finally {
				phoneCursor.close();
			}

		}
		return contactsList;
	}

	public static void insertDefaultData(Context context, String givenName, String givenPhone, String givenWebsite) {
		if (context == null || TextUtils.isEmpty(givenName) || TextUtils.isEmpty(givenPhone)
				|| TextUtils.isEmpty(givenWebsite))
			return;
		if (existContact(context, givenName))
			return;
		insert2LocalContacts(context, givenName, givenPhone, givenWebsite);
	}

	/**
	 * 获取通讯录中联系人
	 */
	public static boolean existContact(Context context, String givenName) {
		if (context == null || TextUtils.isEmpty(givenName))
			return false;
		ContentResolver contentResolver = context.getContentResolver();
		// Uri uri = Uri.parse("content://com.android.contacts/contacts");
		Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		if (cursor == null)
			return false;
		try {
			while (cursor.moveToNext()) {
				// String contactId =
				// cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (TextUtils.equals(givenName, name))
					return true;
				// Cursor phones =
				// contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				// null,
				// ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " +
				// contactId, null, null);
				// while (phones.moveToNext()) {
				// String phone =
				// phones.getString(phones.getColumnIndex("data1"));
				// }
			}
			return false;
		} finally {
			cursor.close();
		}
	}

	/**
	 * 首先向RawContacts.CONTENT_URI执行一个空值插入，目的是获取系统返回的rawContactId
	 * 
	 * 这是后面插入data表的数据，只有执行空值插入，才能使插入的联系人在通讯录里可见
	 */
	public static void insert2LocalContacts(Context context, String name, String phone, String website) {
		if (context == null || TextUtils.isEmpty(name) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(website))
			return;
		ContentValues values = new ContentValues();
		ContentResolver contentResolver = context.getContentResolver();
		// 首先向RawContacts.CONTENT_URI执行一个空值插入，目的是获取系统返回的rawContactId
		Uri rawContactUri = contentResolver.insert(RawContacts.CONTENT_URI, values);
		long rawContactId = ContentUris.parseId(rawContactUri);

		// 往data表入姓名数据
		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
		values.put(StructuredName.GIVEN_NAME, name);
		contentResolver.insert(android.provider.ContactsContract.Data.CONTENT_URI, values);

		// 往data表入电话数据
		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
		values.put(Phone.NUMBER, phone);
		values.put(Phone.TYPE, Phone.TYPE_COMPANY_MAIN);
		contentResolver.insert(android.provider.ContactsContract.Data.CONTENT_URI, values);

		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE);
		values.put(Website.TYPE, Website.TYPE_HOMEPAGE);
		values.put(Website.URL, website);
		contentResolver.insert(android.provider.ContactsContract.Data.CONTENT_URI, values);

	}
}
