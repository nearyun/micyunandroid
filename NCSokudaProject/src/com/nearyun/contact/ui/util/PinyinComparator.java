package com.nearyun.contact.ui.util;

import java.util.Comparator;

import com.nearyun.contact.model.ContactEx;

public class PinyinComparator implements Comparator<ContactEx> {

	public int compare(ContactEx o1, ContactEx o2) {
		if (o1.getSortLetters().equals("@")
				|| o2.getSortLetters().equals("#")) {
			return -1;
		} else if (o1.getSortLetters().equals("#")
				|| o2.getSortLetters().equals("@")) {
			return 1;
		} else {
			return o1.getSortLetters().compareTo(o2.getSortLetters());
		}
	}

}
