package com.nearyun.contact.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.adapter.BaseRecyclerViewAdapter;
import com.micyun.util.ImageLoaderUtil;
import com.nearyun.contact.model.ContactEx;

public class ContactSelectedAdapter extends
		BaseRecyclerViewAdapter<ContactSelectedAdapter.ContactViewHolder, ContactEx> {

	public ContactSelectedAdapter(Context context, ArrayList<ContactEx> dataList) {
		super(context, dataList);
	}

	class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		public View rootView;
		public ImageView avatarImageView;
		public TextView nameTextView;
		public int position;

		public ContactViewHolder(View itemView) {
			super(itemView);
			rootView = itemView.findViewById(R.id.item_root_view);
			rootView.setOnClickListener(this);
			avatarImageView = (ImageView) itemView.findViewById(R.id.item_avatar);
			nameTextView = (TextView) itemView.findViewById(R.id.item_name);
		}

		@Override
		public void onClick(View v) {
			executeOnItemClick(position);
		}
	}

	@Override
	public void onBindViewHolder(ContactViewHolder viewHolder, int position) {
		final ContactEx contact = mDataList.get(position);
		final String name = contact.getName();
		final String phone = contact.getPhone();
		viewHolder.position = position;
		viewHolder.nameTextView.setText(name);

		ImageLoaderUtil.refreshAvatar(mContext, name, phone, viewHolder.avatarImageView);
	}

	@Override
	public ContactViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View view = mInflater.inflate(R.layout.item_contact_selected_layout, parent, false);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		view.setLayoutParams(lp);
		return new ContactViewHolder(view);
	}

	public interface OnRecyclerViewListener {
		void onItemClick(int position);
	}

	public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
		this.onRecyclerViewListener = onRecyclerViewListener;
	}

	private OnRecyclerViewListener onRecyclerViewListener;

	protected void executeOnItemClick(int position) {
		if (onRecyclerViewListener != null) {
			onRecyclerViewListener.onItemClick(position);
		}
	}
}
