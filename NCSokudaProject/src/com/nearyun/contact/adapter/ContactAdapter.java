package com.nearyun.contact.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.adapter.AbsBaseAdapter;
import com.micyun.util.ImageLoaderUtil;
import com.nearyun.contact.model.ContactEx;
import com.tornado.util.ViewHolderUtil;

public class ContactAdapter extends AbsBaseAdapter<ContactEx> {

	public ContactAdapter(Context cxt) {
		super(cxt, new ArrayList<ContactEx>());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_contact_layout, parent, false);
		}

		TextView itemAlpha = ViewHolderUtil.get(convertView, R.id.item_alpha);
		ImageView itemAvatar = ViewHolderUtil.get(convertView, R.id.item_avatar_imageview);
		TextView itemName = ViewHolderUtil.get(convertView, R.id.item_name);
		TextView itemPhone = ViewHolderUtil.get(convertView, R.id.item_phone);
		View itemSelected = ViewHolderUtil.get(convertView, R.id.item_selected);

		final ContactEx contact = mDataList.get(position);
		final String name = contact.getName();
		final String phone = contact.getPhone();
		itemName.setText(name);
		itemPhone.setText(phone);

		// 根据position获取分类的首字母的Char ascii值
		int section = getSectionForPosition(position);
		// 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
		if (position == getPositionForSection(section)) {
			itemAlpha.setVisibility(View.VISIBLE);
			itemAlpha.setText(contact.getSortLetters());
		} else {
			itemAlpha.setVisibility(View.GONE);
		}

		if (contact.isSelected()) {
			itemSelected.setBackgroundResource(R.drawable.item_check_on);
		} else {
			itemSelected.setBackgroundResource(R.drawable.item_check_off);
		}
		ImageLoaderUtil.refreshAvatar(mContext, name, phone, itemAvatar);

		return convertView;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 */
	public void updateListViewData(ArrayList<ContactEx> list) {
		updateListViewDataOnly(list);
		notifyDataSetChanged();
	}

	public void updateListViewDataOnly(ArrayList<ContactEx> list) {
		mDataList = list;
	}

	/**
	 * 根据ListView的当前位置获取分类的首字母的Char ascii值
	 */
	private int getSectionForPosition(int position) {
		return mDataList.get(position).getSortLetters().charAt(0);
	}

	/**
	 * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
	 */
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = mDataList.get(i).getSortLetters();
			char firstChar = sortStr.toUpperCase(Locale.getDefault()).charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}
}
