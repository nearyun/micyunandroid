package com.nearyun.contact.model;

/**
 * 联系人扩展
 * 
 * @author xiaohua
 * 
 */
public class ContactEx extends ContactPinyin {
	private static final long serialVersionUID = 3890498425914874415L;

	private boolean isSelected = false;

	/** 是否手动输入的 */
	private boolean isManual = false;

	public ContactEx(Contact contact) {
		this(contact, false);
	}

	public ContactEx(Contact contact, boolean isManual) {
		super(contact);
		this.isManual = isManual;
	}

	public boolean isManual() {
		return isManual;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean flag) {
		this.isSelected = flag;
	}

}
