package com.nearyun.contact.model;

import java.io.Serializable;
import java.util.Locale;
import java.util.regex.Pattern;

import com.tornado.util.pinyin.PinYin;

/**
 * 带有拼音字符的联系人
 * 
 * @author xiaohua
 * 
 */
public class ContactPinyin implements Serializable {

	private static final long serialVersionUID = -6945880741896132028L;
	private Contact mContact;
	// 显示数据拼音
	private String pinYinFullName;
	// 数据拼音首字母
	private String sortLetters;

	public ContactPinyin(Contact contact) {
		this.mContact = contact;
		pinYinFullName = PinYin.getPinYin(contact.getName());
		sortLetters = getHeadChar(pinYinFullName);
	}

	/**
	 * 获取首字母
	 * 
	 * @param c
	 * @return
	 */
	private static String getHeadChar(String c) {
		if (c == null || c.trim().length() == 0)
			return "#";
		c = c.charAt(0) + "";
		Pattern pattern = Pattern.compile("^[A-Za-z]+$");
		if (pattern.matcher(c).matches()) {
			return c.toUpperCase(Locale.getDefault()); // 将小写字母转换为大写
		} else {
			return "#";
		}
	}

	public String getName() {
		return mContact.getName();
	}

	public String getPhone() {
		return mContact.getPhone();
	}

	public String getSortLetters() {
		return sortLetters;
	}

	public String getFullPinYin() {
		return pinYinFullName;
	}
}
