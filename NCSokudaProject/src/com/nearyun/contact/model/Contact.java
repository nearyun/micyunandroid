package com.nearyun.contact.model;

import java.io.Serializable;

public class Contact implements Serializable {

	private static final long serialVersionUID = -8496307402949345445L;

	private String name;
	private String phone;

	public Contact(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

}
