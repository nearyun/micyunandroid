package com.micyun.sip;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.ConferenceArgument;
import com.micyun.sip.media.RingToneMedia;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.nearyun.sip.SipClient;
import com.nearyun.sip.model.SipPhoneIntentFilter;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;
import com.nearyun.sip.receiver.AbsSipBroadcastReceiver;

/**
 * 呼叫振铃界面
 * 
 * @author xiaohua
 * 
 */
public class CallRingActivity extends AbstractCallRingActivity implements View.OnClickListener,
		OnServiceConnectionListener {

	private static final String KEY_CALLER_ID = "KEY_CALLER_ID";
	private static final String KEY_CALLER_URI = "KEY_CALLER_URI";
	private static final String KEY_CALLER_NAME = "KEY_CALLER_NAME";

	public static final void newInstance(Context context, String callerUri, String callerId, String callerName) {
		Intent intent = new Intent(context, CallRingActivity.class);
		intent.putExtra(KEY_CALLER_ID, callerId);
		intent.putExtra(KEY_CALLER_URI, callerUri);
		intent.putExtra(KEY_CALLER_NAME, callerName);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private String callerId;
	private String callerUri;
	private String callerName;
	private SipPhoneService mSipPhoneService;

	private View rejectButton, answerButton;
	private TextView callerNameTextView;
	private TextView subjectTextview;
	private TextView connectingTextView;
	private ImageView avatarImageView;

	private String inviterId;
	private String confId;
	private String subject;
	private String inviteAvatar;

	private boolean hasBindService = false;
	private boolean hasObtainData = false;

	private final Handler mHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call_ring);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		registerReceiver(sipBroadcastReceiver, SipPhoneIntentFilter.getFilter(mActivity));

		callerId = intent.getStringExtra(KEY_CALLER_ID);
		callerUri = intent.getStringExtra(KEY_CALLER_URI);
		callerName = intent.getStringExtra(KEY_CALLER_NAME);

		avatarImageView = (ImageView) findViewById(R.id.avatarImageView);

		callerNameTextView = (TextView) findViewById(R.id.callerName);
		callerNameTextView.setText(callerName);

		subjectTextview = (TextView) findViewById(R.id.subjectTextview);
		subjectTextview.setText("");

		connectingTextView = (TextView) findViewById(R.id.connectingTextView);
		connectingTextView.setText("");

		rejectButton = findViewById(R.id.rejectButton);
		rejectButton.setOnClickListener(this);
		answerButton = findViewById(R.id.answerButton);
		answerButton.setOnClickListener(this);

		disableOperationButton();

		mSipPhoneService = new SipPhoneService(this);
		mSipPhoneService.setOnServiceConnectionListener(this);
		mSipPhoneService.doBindService();

		Client.getInstance().obtainInvitationInfo(callerId, new OnSampleCallback() {

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					inviterId = json.optString("inviter_id");
					confId = json.optString("confid");
					inviteAvatar = json.optString("inviter_avatar");
					subject = json.optString("subject");
					if (TextUtils.isEmpty(inviterId) || TextUtils.isEmpty(confId)) {
						LogUtil.w(TAG, "inviterId:" + inviterId + ", confId:" + confId);
						mSipPhoneService.reject(SipClient.CODE_REJECT_603);
						RingToneMedia.stopTone(mActivity);
						finish();
						return;
					}
					subjectTextview.setText(subject);
					ImageLoaderUtil.refreshAvatar(inviteAvatar, avatarImageView);
					hasObtainData = true;
					enableOperationButton();
				} catch (JSONException e) {
					LogUtil.printStackTrace(e);
					mSipPhoneService.reject(SipClient.CODE_REJECT_603);
					RingToneMedia.stopTone(mActivity);
					finish();
				}
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, errorCode + reason);
				mSipPhoneService.reject(SipClient.CODE_REJECT_603);
				RingToneMedia.stopTone(mActivity);
				finish();
			}
		});

	}

	@Override
	protected void onStop() {
		super.onStop();
		showNotification(callerName);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(sipBroadcastReceiver);
		mSipPhoneService.doUnbindService();
		mHandler.removeCallbacks(finishRunnable);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rejectButton:
			doReject();
			break;
		case R.id.answerButton:
			doAnswer();
			break;
		default:
			break;
		}
	}

	@Override
	protected void doAnswer() {
		disableOperationButton();

		connectingTextView.setText("正在连接……");
		Client.getInstance().agreeInvite(confId, Client.getInstance().getUser().getUserId(), inviterId,
				new OnCallback() {

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, errorCode + reason);
						enableOperationButton();
					}

					@Override
					public void onSuccess() {
						mHandler.postDelayed(finishRunnable, 5000);
						connectingTextView.setText("正在接通语音……");
						mSipPhoneService.answer();
						RingToneMedia.stopTone(mActivity);
					}
				});
	}

	@Override
	protected void doReject() {
		disableOperationButton();
		Client.getInstance().rejectInvite(confId, Client.getInstance().getUser().getUserId(), inviterId, null);

		mSipPhoneService.reject(SipClient.CODE_REJECT_603);
		RingToneMedia.stopTone(mActivity);
		finish();
	}

	private void enableOperationButton() {
		if (hasBindService && hasObtainData) {
			rejectButton.setEnabled(true);
			answerButton.setEnabled(true);
		}
	}

	private void disableOperationButton() {
		rejectButton.setEnabled(false);
		answerButton.setEnabled(false);
	}

	@Override
	public void onConnected() {
		hasBindService = true;
		enableOperationButton();
		RingToneMedia.playTone(mActivity);
	}

	@Override
	public void onDisconnected() {
		finish();
	}

	private Runnable finishRunnable = new Runnable() {

		@Override
		public void run() {
			finish();
		}

	};

	private BroadcastReceiver sipBroadcastReceiver = new AbsSipBroadcastReceiver() {

		@Override
		protected void onSipMediaError(Context context, int index) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipRedirected(Context context, int index, String target) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipProceeding(Context context, int index, int proceedingStatus) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipIdle(Context context, int index) {
			disableOperationButton();
			RingToneMedia.stopTone(mActivity);
			finish();
		}

		@Override
		protected void onSipFailure(Context context, int index, int failureStatus, String reason) {
			disableOperationButton();
			RingToneMedia.stopTone(mActivity);
			finish();
		}

		@Override
		protected void onSipDisconnected(Context context, int index) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipConnected(Context context, int index) {
			ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(confId, Client.getInstance()
					.getUser().getUserId(), Client.getInstance().getUser().getNickname()));
			RingToneMedia.stopTone(mActivity);
			finish();
		}

		@Override
		protected void onSipAnswer(Context context, int index) {
			disableOperationButton();
		}

		@Override
		protected void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipUnregistered(Context context) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onSipRegistered(Context context) {
			// TODO Auto-generated method stub

		}

	};

}
