package com.micyun.sip.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.micyun.R;

public class RingToneMedia {
	public static void stopTone(Context context) {
		if (player != null && player.isPlaying()) {
			player.stop();
			player.release();
			player = null;
		}
		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.cancel();
	}

	private static MediaPlayer player = null;

	public static void playTone(Context context) {
		try {
			if (player != null)
				stopTone(context);
			player = MediaPlayer.create(context, R.raw.meetcalling);
			if (player != null) {
				player.setAudioStreamType(AudioManager.STREAM_MUSIC);
				player.setLooping(true);
				player.start();
			}

			Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(new long[] { 0, 1000, 800 }, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
