package com.micyun.sip.receiver;

import android.content.Context;

import com.micyun.sip.CallRingActivity;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.nearyun.sip.receiver.AbsSipBroadcastReceiver;


public class SipBroadcastReceiver extends AbsSipBroadcastReceiver {

	@Override
	protected void onSipMediaError(Context context, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipRedirected(Context context, int index, String target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipProceeding(Context context, int index, int proceedingStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipIdle(Context context, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipFailure(Context context, int index, int failureStatus, String reason) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipDisconnected(Context context, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipConnected(Context context, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipAnswer(Context context, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName) {
		if (ConferenceMainRoomActivity.isRunning())
			return;
		CallRingActivity.newInstance(context, callerUri, callerId, callerName);
	}

	@Override
	protected void onSipUnregistered(Context context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onSipRegistered(Context context) {
		// TODO Auto-generated method stub
		
	}


	
}
