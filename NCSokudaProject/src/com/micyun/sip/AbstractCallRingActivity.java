package com.micyun.sip;

import android.os.Bundle;

import com.micyun.BaseActivity;
import com.micyun.sip.media.RingToneMedia;
import com.micyun.ui.NotificationUtil;
import com.tornado.util.ScreenManager;

public abstract class AbstractCallRingActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ScreenManager.setUnlocked(mActivity);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		dismissNotification();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissNotification();
		ScreenManager.setLocked(mActivity);
		RingToneMedia.stopTone(mActivity);
	}

	protected abstract void doReject();

	protected abstract void doAnswer();

	protected void showNotification(String inviterName) {
		NotificationUtil.showNotification(mActivity, getClass(), inviterName + " 邀请你参加会议", inviterName + " 邀请你参加会议",
				"点击返回");
	}

	protected void dismissNotification() {
		NotificationUtil.dismissNotification(mActivity);
	}

	@Override
	public void onBackPressed() {
	}
}
