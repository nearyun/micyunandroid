package com.micyun.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class HeadsetPlugReceiver extends BroadcastReceiver {

	/**
	 * Intent有三个附带的值：
	 * <p/>
	 * state —— 0代表拔出，1代表插入
	 * <p/>
	 * name —— 字符串，代表headset的类型
	 * <p/>
	 * microphone —— 1代表插入的headset有麦克风，0表示没有麦克风
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.hasExtra("state")) {
			Bundle bundle = intent.getExtras();
			// String name = intent.getStringExtra("name");
			// int microphone = intent.getIntExtra("microphone", -1);
			int state = intent.getIntExtra("state", 2);
			switch (state) {
			case 0:// 拔出
				System.out.println("耳机拔出：" + bundle.toString());
				break;
			case 1:// 插入
				System.out.println("耳机插入：" + bundle.toString());
				break;
			default:
				break;
			}
		}
	}

	public static BroadcastReceiver register(Context context) {
		BroadcastReceiver receiver = new HeadsetPlugReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_HEADSET_PLUG);
		context.registerReceiver(receiver, filter);
		return receiver;
	}

	public static void unregister(Context context, BroadcastReceiver receiver) {
		context.unregisterReceiver(receiver);
	}
}
