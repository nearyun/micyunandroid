package com.micyun.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;

import com.ncore.util.LogUtil;

public class BluetoothReceiver extends BroadcastReceiver {
	private static final String TAG = "BluetoothReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		int state = intent.getIntExtra(BluetoothAdapter.EXTRA_CONNECTION_STATE, -1);
		Bundle bundle = intent.getExtras();
		LogUtil.e(TAG, "蓝牙：" + bundle.toString());
		switch (state) {
		case BluetoothAdapter.STATE_DISCONNECTED:
			LogUtil.e(TAG, "蓝牙：未连接");
			disableBluetoothRouting(context);
			break;
		case BluetoothAdapter.STATE_CONNECTING:
			LogUtil.e(TAG, "蓝牙：正在连接");
			break;
		case BluetoothAdapter.STATE_CONNECTED:
			LogUtil.e(TAG, "蓝牙：已连接");
			enableBluetoothRouting(context);
			break;
		case BluetoothAdapter.STATE_DISCONNECTING:
			LogUtil.e(TAG, "蓝牙：正在断开连接");
			break;
		case BluetoothAdapter.STATE_TURNING_OFF:
			LogUtil.e(TAG, "蓝牙：STATE_TURNING_OFF");
			break;
		case BluetoothAdapter.STATE_TURNING_ON:
			LogUtil.e(TAG, "蓝牙：STATE_TURNING_ON");
			break;
		case BluetoothAdapter.STATE_OFF:
			LogUtil.e(TAG, "蓝牙：STATE_OFF");
			break;
		case BluetoothAdapter.STATE_ON:
			LogUtil.e(TAG, "蓝牙：STATE_ON");
			break;
		case BluetoothAdapter.ERROR:
			LogUtil.e(TAG, "蓝牙：ERROR");
			break;
		default:
			LogUtil.e(TAG, "蓝牙：未知连接" + state);
			break;
		}
	}

	// 使用蓝牙通话
	private void enableBluetoothRouting(Context context) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		audioManager.startBluetoothSco();
		audioManager.setBluetoothScoOn(true);
	}

	// 关闭蓝牙通话
	private void disableBluetoothRouting(Context context) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		audioManager.stopBluetoothSco();
		audioManager.setBluetoothScoOn(true);
	}

	public static BroadcastReceiver register(Context context) {
		BroadcastReceiver receiver = new BluetoothReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
		context.registerReceiver(receiver, filter);
		return receiver;
	}

	public static void unregister(Context context, BroadcastReceiver receiver) {
		context.unregisterReceiver(receiver);
	}
}
