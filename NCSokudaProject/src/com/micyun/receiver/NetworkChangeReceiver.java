package com.micyun.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;

import com.ncore.util.LogUtil;


public class NetworkChangeReceiver extends BroadcastReceiver {
	private static final String TAG = "NetworkChangeReceiver";

	public interface OnNetworkConnectedListener {
		public void onConnected(boolean flag, String type);
	}

	private OnNetworkConnectedListener mNetworkConnectedListener = null;

	public void setOnNetworkChangedListener(OnNetworkConnectedListener listener) {
		mNetworkConnectedListener = listener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable()) {
				LogUtil.d(TAG, "网络状态 on ");
				if (mNetworkConnectedListener != null) {
					mNetworkConnectedListener.onConnected(true, networkInfo.getTypeName());
				}
			} else {
				LogUtil.d(TAG, "网络状态 off ");
				if (mNetworkConnectedListener != null) {
					mNetworkConnectedListener.onConnected(false, "");
				}
			}
		}
	}

	public static NetworkChangeReceiver register(Context context) {
		NetworkChangeReceiver receiver = new NetworkChangeReceiver();
		IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		context.registerReceiver(receiver, filter);
		return receiver;
	}

	public static void unregister(Context context, BroadcastReceiver receiver) {
		if (context != null && receiver != null)
			context.unregisterReceiver(receiver);
	}

	@SuppressWarnings("unused")
	private void onNetworkChanged(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		System.out.println(wifiNetworkInfo.toString());
		System.out.println(mobileNetworkInfo.toString());

		State wifiState = wifiNetworkInfo != null ? wifiNetworkInfo.getState() : null;
		State mobileState = mobileNetworkInfo != null ? mobileNetworkInfo.getState() : null;

		if (wifiState != null && mobileState != null && State.CONNECTED != wifiState && State.CONNECTED != mobileState) {
			// 手机没有任何的网络
			LogUtil.d(TAG, "手机没有任何的网络");
		} else if (wifiState != null && mobileState != null && State.CONNECTED != wifiState
				&& State.CONNECTED == mobileState) {
			// 手机网络连接成功
			LogUtil.d(TAG, "手机网络连接成功");
		} else if (wifiState != null && State.CONNECTED == wifiState) {
			// 无线网络连接成功
			LogUtil.d(TAG, "无线网络连接成功");
		} else if (wifiState != null && mobileState != null
				&& (State.CONNECTING == wifiState || State.CONNECTING == mobileState)) {
			// 网络正在连接
			LogUtil.d(TAG, "网络正在连接");
		}
	}
}
