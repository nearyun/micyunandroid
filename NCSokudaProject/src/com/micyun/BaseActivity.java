package com.micyun;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.micyun.receiver.NetworkChangeReceiver;
import com.micyun.receiver.NetworkChangeReceiver.OnNetworkConnectedListener;
import com.ncore.util.LogUtil;
import com.tornado.util.NetworkUtil;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseActivity extends Activity {
	protected String TAG = BaseActivity.class.getSimpleName();
	protected Activity mActivity;
	protected NCApplication mApp;

	private View networkLossView;
	private NetworkChangeReceiver mNetworkChangeReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = getClass().getSimpleName();

		mActivity = this;
		mApp = (NCApplication) getApplication();

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_theme_color));
			actionBar.setDisplayShowHomeEnabled(false);
		}
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		initailNetworkSignFlag();
	}

	private void initailNetworkSignFlag() {
		networkLossView = findViewById(R.id.networkLossView);
		if (networkLossView == null)
			return;
		networkLossView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
				startActivity(intent);
			}
		});

		mNetworkChangeReceiver = NetworkChangeReceiver.register(mActivity);
		mNetworkChangeReceiver.setOnNetworkChangedListener(new OnNetworkConnectedListener() {

			@Override
			public void onConnected(boolean flag, String type) {
				if (networkLossView == null)
					return;
				networkLossView.setVisibility(flag ? View.GONE : View.VISIBLE);
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mNetworkChangeReceiver != null)
			NetworkChangeReceiver.unregister(mActivity, mNetworkChangeReceiver);
	}

	/**
	 * 显示toast信息
	 * 
	 * @param context
	 * @param msg
	 */
	public void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		LogUtil.i(TAG, "showToast:" + msg);
	}

	/**
	 * 判断当前网络是否可用
	 * 
	 * @return
	 */
	public boolean isNetworkAvailable() {
		if (NetworkUtil.IsNetWorkEnable(mActivity)) {
			return true;
		} else {
			showToast(getString(R.string.network_not_available));
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
