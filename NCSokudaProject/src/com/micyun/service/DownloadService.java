package com.micyun.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.Header;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.SparseArray;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.ncore.util.LogUtil;

public class DownloadService extends IntentService {

	private static final String TAG = DownloadService.class.getSimpleName();
	private static final String KEY_URL = "KEY_URL";
	private static final String KEY_FILE_NAME = "KEY_FILE_NAME";

	private AsyncHttpClient asyncHttpClient;

	private static int notifyID = 0;
	private NotificationManager manager;
	private SparseArray<NotificationCompat.Builder> notifications;// 通知队列

	public static final void startDownload(Context context, String url, String fileName) {
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(KEY_URL, url);
		intent.putExtra(KEY_FILE_NAME, fileName);
		context.startService(intent);
	}

	public DownloadService() {
		super(TAG);
		asyncHttpClient = new SyncHttpClient();
		asyncHttpClient.setUserAgent("android");
		asyncHttpClient.addHeader("Connection", "close");

		notifications = new SparseArray<NotificationCompat.Builder>();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (manager == null)
			manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if (intent == null) {
			LogUtil.w(TAG, "warn: intent is null");
			return;
		}
		String url = intent.getStringExtra(KEY_URL);
		String fileName = intent.getStringExtra(KEY_FILE_NAME);
		if (TextUtils.isEmpty(url)) {
			LogUtil.w(TAG, "warn: url is null");
			return;
		}

		if (TextUtils.isEmpty(fileName)) {
			LogUtil.w(TAG, "warn: file name is null");
			return;
		}

		LogUtil.i(TAG, "download url:" + url);
		LogUtil.i(TAG, "set file name:" + fileName);

		File downloadDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		if (downloadDirectory == null) {
			LogUtil.w(TAG, "warn:can not find download directory");
			return;
		} else if (!downloadDirectory.exists()) {
			downloadDirectory.mkdirs();
		}

		File file = getDownloadFullPath(downloadDirectory, fileName);
		executeDownload(url, file);
		LogUtil.d(TAG, "执行完下载函数");
	}

	private String getExtendsFileName(String fileName, int seq) {
		if (seq == 0)
			return fileName;
		try {
			int index = fileName.lastIndexOf(".");
			if (index != -1) {
				String _name = fileName.substring(0, index);
				String _extension = fileName.substring(index + 1);
				return _name + "_" + seq + "." + _extension;
			}
		} catch (IndexOutOfBoundsException e) {
			LogUtil.printStackTrace(e);
		}
		return "tmp_" + fileName;
	}

	private File getDownloadFullPath(File downloadDirectory, String fileName) {
		return getDownloadFullPath(downloadDirectory, fileName, 0);
	}

	private File getDownloadFullPath(File downloadDirectory, String fileName, int seq) {
		File file = new File(downloadDirectory, getExtendsFileName(fileName, seq));
		if (file.exists()) {
			return getDownloadFullPath(downloadDirectory, fileName, seq + 1);
		}
		return file;
	}

	private final String[] allowContentTypes = { ".*" };

	private void printHeaders(Header[] headers) {
		if (headers != null) {
			for (int i = 0; i < headers.length; i++) {
				LogUtil.w(TAG, headers[i].getName() + ":" + headers[i].getValue());
			}
		}
	}

	private void executeDownload(String url, final File file) {
		asyncHttpClient.get(url, new BinaryHttpResponseHandler(allowContentTypes) {
			int notifyId = notifyID++;

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
				LogUtil.e(TAG, "onFailure:" + statusCode);
				failureNotification(notifyId);
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
				byte2File(binaryData, file);
				LogUtil.e(TAG, "下载完成 " + file.toString());
				finishNotification(notifyId);
			}

			@Override
			public void onProgress(int bytesWritten, int totalSize) {
				LogUtil.e(TAG, bytesWritten + ":" + totalSize);
				updateNotification(notifyId, (int) (bytesWritten * 100.0 / totalSize));
			}

			@Override
			public void onStart() {
				addNotification(notifyId, file.getName());
			}

		});
	}

	private void addNotification(int id, String title) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setContentTitle(title);
		builder.setContentText("正在下载..." + "0%");
		builder.setSmallIcon(android.R.drawable.ic_dialog_info);
		/* 设置点击消息时，显示的界面 */
		// Intent nextIntent = new Intent(this, MainActivity.class);
		// TaskStackBuilder task = TaskStackBuilder.create(this);
		// task.addNextIntent(nextIntent);
		// PendingIntent pengdingIntent = task.getPendingIntent(0,
		// PendingIntent.FLAG_UPDATE_CURRENT);
		// builder.setContentIntent(pengdingIntent);
		builder.setProgress(100, 0, false);
		builder.setAutoCancel(true);
		builder.setTicker("下载中....");
		notifications.put(id, builder);
		manager.notify(id, builder.build());
	}

	// 更新下载进度
	private void updateNotification(int id, int progress) {
		NotificationCompat.Builder notification = notifications.get(id);
		notification.setContentText("正在下载..." + progress + "%");
		notification.setProgress(100, progress, false);
		manager.notify(id, notification.build());
	}

	private void finishNotification(int id) {
		NotificationCompat.Builder notification = notifications.get(id);
		notification.setTicker("下载完成!");
		notification.setContentText("下载完成!");
		manager.notify(id, notification.build());
		notifications.remove(id);
	}

	private void failureNotification(int id) {
		NotificationCompat.Builder notification = notifications.get(id);
		notification.setContentTitle("下载失败");
		manager.notify(id, notification.build());
	}

	public void byte2File(byte[] buf, File file) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(buf);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
