package com.micyun.service;

import java.io.File;
import java.util.ArrayList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.RequestHandle;
import com.micyun.dao.upload.UploadDaoHelper;
import com.micyun.model.upload.UploadFileInfo;
import com.ncore.callback.OnUploadCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;

public class UploadService extends Service {
	private static final String TAG = "UploadService";

	public static final String KEY_UPLOADER = "KEY_UPLOADER";
	public static final String KEY_UPLOAD_FILE_ARRAY = "KEY_UPLOAD_FILE_ARRAY";

	/** 已经上传完成标识 */
	public static final String ACTION_COMPLETE_UPLOAD = "com.micyun.service.UploadService.ACTION_COMPLETE_UPLOAD";
	/** 刷新上传列表 */
	public static final String ACTION_REFRESH_UPLOAD_LIST = "com.micyun.service.UploadService.ACTION_REFRESH_UPLOAD_LIST";

	public interface IControlUploadService {
		public void cancel(int id, String uploader);

		public void stop(int id, String uploader);

		public void restart(int id, String uploader);

		public ArrayList<UploadFileInfo> getAllUploadFile(String uploader);
	}

	public static final void startService(Context context, String[] data, String uploader) {
		Intent intent = new Intent(context, UploadService.class);
		intent.putExtra(KEY_UPLOAD_FILE_ARRAY, data);
		intent.putExtra(KEY_UPLOADER, uploader);
		context.startService(intent);
	}

	public class UploadBinder extends Binder implements IControlUploadService {

		public void cancel(int id, String uploader) {
			database.update(id, UploadFileInfo.TYPE_CANCEL, uploader);
			executeUpload(uploader);
		}

		public void restart(int id, String uploader) {
			database.update(id, UploadFileInfo.TYPE_WAITTING, uploader);
			executeUpload(uploader);
		}

		public void stop(int id, String uploader) {
			if (mRequestHandler != null)
				mRequestHandler.cancel(true);
		}

		@Override
		public ArrayList<UploadFileInfo> getAllUploadFile(String uploader) {
			return database.selectAll(uploader);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(TAG, "onBind");
		return mUploadBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(TAG, "onUnbind");
		return super.onUnbind(intent);
	}

	private UploadDaoHelper database;
	private UploadBinder mUploadBinder;
	private RequestHandle mRequestHandler;

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate");
		database = new UploadDaoHelper(this);
		mUploadBinder = new UploadBinder();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		if (intent != null) {
			String uploader = intent.getStringExtra(KEY_UPLOADER);
			String[] uploadFileArray = intent.getStringArrayExtra(KEY_UPLOAD_FILE_ARRAY);
			if (uploadFileArray != null && !TextUtils.isEmpty(uploader)) {
				for (int i = 0; i < uploadFileArray.length; i++) {
					database.insert(new UploadFileInfo(uploadFileArray[i]), uploader);
				}
				executeUpload(uploader);
			} else {
				Log.e(TAG, "error: upload parameter is not complete, can't upload");
				if (!isUploading)
					stopSelf();
			}
		}

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		database.close();
	}

	private boolean isUploading = false;

	/**
	 * 如果数据库没有数据，停止服务</p>
	 * 
	 * 如果没有正在上传的事例，添加上传</p>
	 * 
	 * 如果存在正在上传的事例，更新列表</p>
	 * 
	 */
	private synchronized void executeUpload(final String uploader) {
		Intent intent = new Intent();

		ArrayList<UploadFileInfo> waitingList = database.selectAllInWaitting(uploader);

		if (waitingList.size() == 0) {
			LogUtil.d(TAG, "上传完成");
			intent.setAction(ACTION_COMPLETE_UPLOAD);
			sendBroadcast(intent);
			stopSelf();
			return;
		}

		LogUtil.d(TAG, "请求刷新列表");
		intent.setAction(ACTION_REFRESH_UPLOAD_LIST);
		sendBroadcast(intent);

		if (isUploading) {
			return;
		}

		final UploadFileInfo file = waitingList.get(0);
		isUploading = true;

		File tmpFile = new File(file.getPath());
		if (!tmpFile.exists()) {
			database.update(file.getId(), UploadFileInfo.TYPE_NOT_FOUND, uploader);
			executeUpload(uploader);
			return;
		}
		mRequestHandler = Client.getInstance().uploadFile(tmpFile, new OnUploadCallback() {
			@Override
			public void onStart() {
				database.update(file.getId(), UploadFileInfo.TYPE_UPLOADING, uploader);
				executeUpload(uploader);
			}

			@Override
			public void onCancel() {
				database.update(file.getId(), UploadFileInfo.TYPE_CANCEL, uploader);
				isUploading = false;
				executeUpload(uploader);
			}

			@Override
			public void onFailure(int errorCode, String message) {
				database.update(file.getId(), UploadFileInfo.TYPE_FAILURE, uploader);
				isUploading = false;
				executeUpload(uploader);
			}

			@Override
			public void onSuccess(String response) {
				LogUtil.i(TAG, "上传完成 " + response);
				database.update(file.getId(), UploadFileInfo.TYPE_SUCCESS, uploader);
				isUploading = false;
				executeUpload(uploader);
			}

			private long oldTime = 0L;

			@Override
			public void onTransferred(long transfer, long total) {
				long now = System.currentTimeMillis();
				if (now - oldTime >= 1000) {
					oldTime = now;
					database.update(file.getId(), transfer, uploader);
					executeUpload(uploader);
				}

			}

		});

	}

}
