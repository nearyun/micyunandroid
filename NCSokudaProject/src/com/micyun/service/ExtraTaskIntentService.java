package com.micyun.service;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;

import com.micyun.common.Contansts;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;

public class ExtraTaskIntentService extends IntentService implements OnServiceConnectionListener {
	private static final String TAG = ExtraTaskIntentService.class.getSimpleName();

	private static final String ACTION_CLEAR_PASS_SEVEN_DAYS = "com.micyun.service.ACTION_CLEAR_PASS_SEVEN_DAYS";
	private static final String ACTION_REFRESH_LOGIN_INFO = "com.micyun.service.ACTION_REFRESH_LOGIN_INFO";

	private static final String ACTION_XIAOMI_CALLBACK = "com.micyun.service..ACTION_XIAOMI_CALLBACK";
	private static final String KEY_XIAOMI_CONTENT = "KEY_XIAOMI_CONTENT";

	public static final void startClearService(Context context) {
		Intent intent = new Intent(context, ExtraTaskIntentService.class);
		intent.setAction(ACTION_CLEAR_PASS_SEVEN_DAYS);
		context.startService(intent);
	}

	public static final void startXiaomiCallback(Context context, String content) {
		Intent intent = new Intent(context, ExtraTaskIntentService.class);
		intent.setAction(ACTION_XIAOMI_CALLBACK);
		intent.putExtra(KEY_XIAOMI_CONTENT, content);
		context.startService(intent);
	}

	public static final void startRefreshLoginInfoService(Context context) {
		Intent intent = new Intent(context, ExtraTaskIntentService.class);
		intent.setAction(ACTION_REFRESH_LOGIN_INFO);
		context.startService(intent);
	}

	private SipPhoneService mSipPhoneService = null;

	public ExtraTaskIntentService() {
		super(TAG);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mSipPhoneService = new SipPhoneService(this);
		mSipPhoneService.setOnServiceConnectionListener(this);
		mSipPhoneService.doBindService();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mSipPhoneService.doUnbindService();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String action = intent.getAction();
		if (TextUtils.equals(action, ACTION_XIAOMI_CALLBACK)) {
			feedbackXiaomi(intent);
		} else if (TextUtils.equals(action, ACTION_CLEAR_PASS_SEVEN_DAYS)) {
			clear7DaysFile();
		} else if (TextUtils.equals(action, ACTION_REFRESH_LOGIN_INFO)) {
			refreshLoginInfo();
		}
	}

	/**
	 * 反馈给小米
	 * 
	 * @param intent
	 */
	private void feedbackXiaomi(Intent intent) {
		String content = intent.getStringExtra(KEY_XIAOMI_CONTENT);
		if (TextUtils.isEmpty(content) || !(content.startsWith("{") && content.endsWith("}"))) {
			return;
		}
		try {
			LogUtil.d(TAG, content);
			JSONObject json = new JSONObject(content);
			int type = json.optInt("type", -1);
			JSONObject contentJson = json.optJSONObject("content");
			if (contentJson != null) {
				String confId = contentJson.optString("confid");
				String phone = contentJson.optString("phone");
				if (type == -1 || TextUtils.isEmpty(confId) || TextUtils.isEmpty(phone)) {
					return;
				}

				LogUtil.d(TAG, "xiaomi feedbakc --> confid:" + confId + ", phone:" + phone);
				int i = 0;
				boolean result = false;
				while (i++ < 6) {
					if (mSipPhoneService.isRegistered()) {
						result = true;
						break;
					}
					SystemClock.sleep(500);
				}
				Client.getInstance().sendXiaomiReceiveResult(confId, result, phone, null);
			}
		} catch (JSONException e) {
			LogUtil.printStackTrace(e);
		}
	}

	/**
	 * 清除7天前的冗余log数据
	 */
	private void clear7DaysFile() {
		File logsDir = new File(getExternalCacheDir(), Contansts.DIRECTORY_LOG);
		clearFile(logsDir);

		logsDir = new File(getExternalCacheDir(), Contansts.DIRECTORY_VOIP_LOG);
		clearFile(logsDir);
	}

	private void clearFile(File logsDir) {
		if (logsDir != null && logsDir.exists() && logsDir.isDirectory()) {
			File[] files = logsDir.listFiles();
			if (files == null || files.length == 0)
				return;
			for (int i = 0; i < files.length; i++) {
				if (files[i] == null || !files[i].isFile())
					return;
				long lastModified = files[i].lastModified();
				long days = (System.currentTimeMillis() - lastModified) / 604800000;
				if (days > 7) {
					LogUtil.d(TAG, "out of time, delete:" + files[i].toString());
					files[i].delete();
				}
			}
		}
	}

	private void refreshLoginInfo() {
		Client.getInstance().autoLogin(null);
	}

	@Override
	public void onConnected() {

	}

	@Override
	public void onDisconnected() {

	}

}
