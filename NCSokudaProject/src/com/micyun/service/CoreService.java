package com.micyun.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.ncore.config.NCConstants;
import com.ncore.model.User;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;

public class CoreService extends Service {
	private static final String TAG = CoreService.class.getSimpleName();

	@Override
	public void onCreate() {
		super.onCreate();
		Client.create(this);
		if (Client.getInstance().hasUserData()) {
			User mUser = Client.getInstance().getUser();
			SipPhoneService mSipPhoneService = new SipPhoneService(this);
			SipAccount sipAccount = new SipAccount(mUser.getNickname(), mUser.getSipUser(), NCConstants.getSipDomain(),
					NCConstants.getOutproxyServer(), mUser.getSipUser(), mUser.getSipPassword(), mUser.getInstanceId(),
					NCConstants.getIceServer());
			mSipPhoneService.doStartService(sipAccount);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtil.d(TAG, "onStartCommand " + this.toString());
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new Demo();
	}

	private class Demo extends Binder {

	}
}
