package com.micyun.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 上传文件的数据库
 * 
 * @author xiaohua
 * 
 */
public class UploadFileDatabaseHelper extends SQLiteOpenHelper {
	/** 数据库版本号 */
	private static final int DATABASE_VERSION = 2;
	/** 数据库名 */
	private static final String DATABASE_NAME = "upload.db";

	public static final String TABLE_UPLOAD_INFO = "UploadInfoTable"; // 数据表名
	public static final String COLUMN_NAME_ID = "_id"; // 唯一标识id
	public static final String COLUMN_NAME_NAME = "name"; // 文件名
	public static final String COLUMN_NAME_PATH = "path"; // 文件全路径
	public static final String COLUMN_NAME_PROGRESS = "progress"; // 上传进度
	public static final String COLUMN_NAME_SIZE = "size"; // 大小
	public static final String COLUMN_NAME_EXTENSION_NAME = "extension"; // 扩展名
	public static final String COLUMN_NAME_STATE = "state"; // 当前状态值
	public static final String COLUMN_NAME_UPLOADER = "uploader"; // 上传者

	public UploadFileDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("CREATE TABLE " + TABLE_UPLOAD_INFO + " (");
		sBuffer.append(COLUMN_NAME_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,");
		sBuffer.append(COLUMN_NAME_NAME + " TEXT,");
		sBuffer.append(COLUMN_NAME_PATH + " TEXT,");
		sBuffer.append(COLUMN_NAME_PROGRESS + " INTEGER,");
		sBuffer.append(COLUMN_NAME_SIZE + " INTEGER,");
		sBuffer.append(COLUMN_NAME_EXTENSION_NAME + " TEXT,");
		sBuffer.append(COLUMN_NAME_UPLOADER + " TEXT,");
		sBuffer.append(COLUMN_NAME_STATE + " INTEGER)");
		db.execSQL(sBuffer.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_UPLOAD_INFO);
		onCreate(db);
	}

}
