package com.micyun.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FrequentContactDatabaseHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "frequentContact.db";

	public FrequentContactDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static final String TABLE_FREQUENT_CONTACT = "frequent";
	public static final String COLUMN_NAME_ID = "_id"; // 唯一标识id
	public static final String COLUMN_NAME_NAME = "name";
	public static final String COLUMN_NAME_PHONE = "phone";
	public static final String COLUMN_NAME_TIME = "time";// 插入时间
	public static final String COLUMN_NAME_USER = "user";

	@Override
	public void onCreate(SQLiteDatabase db) {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("CREATE TABLE " + TABLE_FREQUENT_CONTACT + " (");
		sBuffer.append(COLUMN_NAME_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,");
		sBuffer.append(COLUMN_NAME_NAME + " TEXT,");
		sBuffer.append(COLUMN_NAME_PHONE + " TEXT,");
		sBuffer.append(COLUMN_NAME_TIME + " INTEGER,");
		sBuffer.append(COLUMN_NAME_USER + " TEXT)");
		db.execSQL(sBuffer.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FREQUENT_CONTACT);
		onCreate(db);
	}

}
