package com.micyun.dao.upload;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.micyun.dao.UploadFileDatabaseHelper;
import com.micyun.model.upload.UploadFileInfo;

/**
 * 上传文档操作数据库工具
 * 
 * @author xiaohua
 * 
 */
public class UploadDaoHelper implements IUploadDaoHelper {
	private static final String TAG = "UploadDaoHelper";
	private UploadFileDatabaseHelper dbHelper = null;

	public UploadDaoHelper(Context context) {
		dbHelper = new UploadFileDatabaseHelper(context);
	}

	@Override
	public void insert(UploadFileInfo info, String uploader) {
		if (info == null)
			return;
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_NAME, info.getName());
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_PATH, info.getPath());
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_SIZE, info.getSize());
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_STATE, info.getState());
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_EXTENSION_NAME, info.getExtension());
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER, uploader);
			db.insert(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, null, cv);
			db.close();
		}
	}

	@Override
	public void delete(int id, String uploader) {
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			String whereClause = UploadFileDatabaseHelper.COLUMN_NAME_ID + "=? and "
					+ UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?";// 删除的条件
			String[] whereArgs = { id + "", uploader };// 删除的条件参数
			db.delete(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, whereClause, whereArgs);// 执行删除
			db.close();
		}
	}

	@Override
	public void deleteAll(String uploader) {
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			String whereClause = UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?";// 删除的条件
			String[] whereArgs = { uploader };// 删除的条件参数
			db.delete(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, whereClause, whereArgs);
			db.close();
		}
	}

	@Override
	public void update(int id, int state, String uploader) {
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_STATE, state);
			String whereClause = UploadFileDatabaseHelper.COLUMN_NAME_ID + "=? and "
					+ UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?";
			String[] whereArgs = { id + "", uploader };
			db.update(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, cv, whereClause, whereArgs);
			db.close();
		}
	}

	@Override
	public void update(int id, long progress, String uploader) {
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(UploadFileDatabaseHelper.COLUMN_NAME_PROGRESS, progress);
			String whereClause = UploadFileDatabaseHelper.COLUMN_NAME_ID + "=? and "
					+ UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?";
			String[] whereArgs = { id + "", uploader };
			db.update(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, cv, whereClause, whereArgs);
			db.close();
		}
	}

	@Override
	public ArrayList<UploadFileInfo> select(String path, String uploader) {
		ArrayList<UploadFileInfo> list = new ArrayList<UploadFileInfo>();
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return list;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			Cursor c = db.query(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, null,
					UploadFileDatabaseHelper.COLUMN_NAME_PATH + "=? and "
							+ UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?", new String[] { path, uploader },
					null, null, UploadFileDatabaseHelper.COLUMN_NAME_STATE + " ASC");
			try {
				if (c.moveToFirst()) {
					for (int i = 0; i < c.getCount(); i++) {
						UploadFileInfo info = new UploadFileInfo();
						info.setId(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_ID)));
						info.setName(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_NAME)));
						info.setPath(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PATH)));
						info.setSize(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_SIZE)));
						info.setState(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_STATE)));
						info.setProgress(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PROGRESS)));
						info.setExtension(c.getString(c
								.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_EXTENSION_NAME)));
						list.add(info);
						c.moveToNext();
					}
				}
			} finally {
				if (c != null)
					c.close();
				db.close();
			}
		}
		return list;
	}

	@Override
	public ArrayList<UploadFileInfo> selectAllInWaitting(String uploader) {
		ArrayList<UploadFileInfo> list = new ArrayList<UploadFileInfo>();
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return list;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			Cursor c = db
					.query(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, null, "("
							+ UploadFileDatabaseHelper.COLUMN_NAME_STATE + "=? OR "
							+ UploadFileDatabaseHelper.COLUMN_NAME_STATE + "=?) and "
							+ UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?", new String[] {
							UploadFileInfo.TYPE_WAITTING + "", UploadFileInfo.TYPE_UPLOADING + "", uploader }, null,
							null, null);
			try {
				if (c.moveToFirst()) {
					for (int i = 0; i < c.getCount(); i++) {
						UploadFileInfo info = new UploadFileInfo();
						info.setId(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_ID)));
						info.setName(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_NAME)));
						info.setPath(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PATH)));
						info.setSize(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_SIZE)));
						info.setState(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_STATE)));
						info.setProgress(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PROGRESS)));
						info.setExtension(c.getString(c
								.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_EXTENSION_NAME)));
						list.add(info);
						c.moveToNext();
					}
				}
			} finally {
				if (c != null)
					c.close();
				db.close();
			}
		}
		return list;
	}

	@Override
	public ArrayList<UploadFileInfo> selectAll(String uploader) {
		ArrayList<UploadFileInfo> list = new ArrayList<UploadFileInfo>();
		if (TextUtils.isEmpty(uploader)) {
			Log.e(TAG, "error: uploader can not be null, it will be do nothing!");
			return list;
		}
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			Cursor c = db.query(UploadFileDatabaseHelper.TABLE_UPLOAD_INFO, null,
					UploadFileDatabaseHelper.COLUMN_NAME_UPLOADER + "=?", new String[] { uploader }, null, null,
					UploadFileDatabaseHelper.COLUMN_NAME_STATE + " ASC");
			try {
				if (c.moveToFirst()) {
					for (int i = 0; i < c.getCount(); i++) {
						UploadFileInfo info = new UploadFileInfo();
						info.setId(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_ID)));
						info.setName(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_NAME)));
						info.setPath(c.getString(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PATH)));
						info.setSize(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_SIZE)));
						info.setState(c.getInt(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_STATE)));
						info.setProgress(c.getLong(c.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_PROGRESS)));
						info.setExtension(c.getString(c
								.getColumnIndex(UploadFileDatabaseHelper.COLUMN_NAME_EXTENSION_NAME)));
						list.add(info);
						c.moveToNext();
					}
				}
			} finally {
				if (c != null)
					c.close();
				db.close();
			}
		}
		return list;
	}

	@Override
	public void close() {
		if (dbHelper != null)
			dbHelper.close();
	}
}
