package com.micyun.dao.upload;

import java.util.ArrayList;

import com.micyun.model.upload.UploadFileInfo;

/**
 * 上传文档操作数据库接口
 * 
 * @author xiaohua
 * 
 */
public interface IUploadDaoHelper {

	public void close();

	public void insert(UploadFileInfo info, String uploader);

	public void delete(int id, String uploader);

	public void deleteAll(String uploader);

	/**
	 * 改变状态
	 * 
	 * @param id
	 * @param state
	 * @param uploader
	 */
	public void update(int id, int state, String uploader);

	/**
	 * 改变上传进度
	 * 
	 * @param id
	 * @param progress
	 * @param uploader
	 */
	public void update(int id, long progress, String uploader);

	public ArrayList<UploadFileInfo> select(String path, String uploader);

	public ArrayList<UploadFileInfo> selectAllInWaitting(String uploader);

	public ArrayList<UploadFileInfo> selectAll(String uploader);
}
