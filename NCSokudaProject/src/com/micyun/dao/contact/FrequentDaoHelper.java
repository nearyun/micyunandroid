package com.micyun.dao.contact;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.micyun.dao.FrequentContactDatabaseHelper;
import com.nearyun.contact.model.Contact;
import com.nearyun.contact.model.ContactEx;

public class FrequentDaoHelper {
	private static final String TAG = FrequentDaoHelper.class.getSimpleName();
	private FrequentContactDatabaseHelper dbHelper = null;

	public FrequentDaoHelper(Context context) {
		dbHelper = new FrequentContactDatabaseHelper(context);
	}

	public void close() {
		if (dbHelper != null)
			dbHelper.close();
	}

	public void insert(String name, String phone, String user) {
		if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(user))
			return;
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();

			Cursor c = db.query(FrequentContactDatabaseHelper.TABLE_FREQUENT_CONTACT, null,
					FrequentContactDatabaseHelper.COLUMN_NAME_USER + "=? AND "
							+ FrequentContactDatabaseHelper.COLUMN_NAME_PHONE + "=? ", new String[] { user, phone },
					null, null, null);
			try {
				if (c.getCount() > 0) {
					update(name, phone, user);
					return;
				}
			} finally {
				if (c != null)
					c.close();
				db.close();
			}

			db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_NAME, name);
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_PHONE, phone);
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_TIME, System.currentTimeMillis());
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_USER, user);
			db.insert(FrequentContactDatabaseHelper.TABLE_FREQUENT_CONTACT, null, cv);
			db.close();
		}
	}

	private void update(String name, String phone, String user) {
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_NAME, name);
			cv.put(FrequentContactDatabaseHelper.COLUMN_NAME_TIME, System.currentTimeMillis());
			db.update(FrequentContactDatabaseHelper.TABLE_FREQUENT_CONTACT, cv,
					FrequentContactDatabaseHelper.COLUMN_NAME_USER + "=? AND "
							+ FrequentContactDatabaseHelper.COLUMN_NAME_PHONE + "=?", new String[] { user, phone });
			db.close();
		}
	}

	public ArrayList<ContactEx> queryAll(String user) {
		ArrayList<ContactEx> mData = new ArrayList<ContactEx>();
		if (TextUtils.isEmpty(user))
			return mData;
		synchronized (dbHelper) {
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			Cursor c = db.query(FrequentContactDatabaseHelper.TABLE_FREQUENT_CONTACT, null,
					FrequentContactDatabaseHelper.COLUMN_NAME_USER + "=? ", new String[] { user }, null, null,
					FrequentContactDatabaseHelper.COLUMN_NAME_TIME + " DESC");
			try {
				if (c.moveToFirst()) {
					for (int i = 0; i < c.getCount(); i++) {
						String name = c.getString(c.getColumnIndex(FrequentContactDatabaseHelper.COLUMN_NAME_NAME));
						String phone = c.getString(c.getColumnIndex(FrequentContactDatabaseHelper.COLUMN_NAME_PHONE));
						mData.add(new ContactEx(new Contact(name, phone)));
						c.moveToNext();
					}
				}
			} finally {
				if (c != null)
					c.close();
				db.close();
			}
		}
		return mData;
	}

}
