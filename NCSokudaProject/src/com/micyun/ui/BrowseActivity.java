package com.micyun.ui;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;

public class BrowseActivity extends BaseActivity {

	private SipPhoneService mSipPhoneService;
	private String conferenceId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}
		String data = intent.getDataString();
		if (TextUtils.isEmpty(data) || !data.startsWith("micyun://")) {
			finish();
			return;
		}

		data = data.replace("micyun://", "");
		String result = new String(Base64.decode(data, Base64.NO_WRAP));
		LogUtil.e(TAG, result);

		try {
			JSONObject json = new JSONObject(result);
			conferenceId = json.optString("confid");
		} catch (JSONException e) {
			e.printStackTrace();
			showToast("数据解析错误！");
			SplashActivity.newInstance(mActivity);
			finish();
			return;
		}

		mSipPhoneService = new SipPhoneService(this);
		mSipPhoneService.setOnServiceConnectionListener(new OnServiceConnectionListener() {

			@Override
			public void onDisconnected() {
				SplashActivity.newInstance(mActivity);
				finish();
			}

			@Override
			public void onConnected() {
				if (!TextUtils.isEmpty(conferenceId) && mSipPhoneService.isRegistered()) {
					if (isNetworkAvailable())
						ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(conferenceId, Client
								.getInstance().getUser().getUserId(), Client.getInstance().getUser().getNickname()));
					finish();
				} else {
					SplashActivity.newInstance(mActivity);
					finish();
				}
			}
		});
		mSipPhoneService.doBindService();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mSipPhoneService.doUnbindService();
	}
}
