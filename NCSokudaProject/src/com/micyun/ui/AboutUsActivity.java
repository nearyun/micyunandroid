package com.micyun.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.TestConferenceActivity;
import com.tornado.util.SystemUtil;

/**
 * 关于界面
 * 
 * @author xiaohua
 * 
 */
public class AboutUsActivity extends BaseActivity {

	public static final void newInstance(Context cxt) {
		Intent intent = new Intent(cxt, AboutUsActivity.class);
		cxt.startActivity(intent);
	}

	private int countOnClick = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		TextView versionTV = (TextView) findViewById(R.id.versionTextView);
		versionTV.setText("v " + SystemUtil.getAppVersionName(mActivity));
		versionTV.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (countOnClick++ < 10) {
					return;
				}
				countOnClick = 0;
				// TestActivity.newInstance(mActivity);
				TestConferenceActivity.newInstance(mActivity);
			}
		});

	}
}
