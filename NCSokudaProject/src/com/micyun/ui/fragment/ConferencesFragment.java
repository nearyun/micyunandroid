package com.micyun.ui.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

import com.free.listview.expandable.PinnedHeaderExpandableListView;
import com.free.listview.expandable.PinnedHeaderExpandableListView.OnHeaderUpdateListener;
import com.micyun.BaseFragment;
import com.micyun.R;
import com.micyun.adapter.TimelineExpandableListAdapter;
import com.micyun.adapter.TimelineExpandableListAdapter.OnOperationListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.model.MeetingGroup;
import com.micyun.model.MeetingInfo;
import com.micyun.ui.conference.ConferenceDetailActivity;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.micyun.ui.conference.MainActivity;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.User;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.tornado.util.ACache;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

public class ConferencesFragment extends BaseFragment implements OnOperationListener, OnRefreshListener,
		OnGroupExpandListener, OnGroupCollapseListener, OnChildClickListener, OnHeaderUpdateListener,
		OnItemLongClickListener {

	public static final Fragment newInstance(User user) {
		Fragment fragment = new ConferencesFragment();
		return fragment;
	}

	private final String STATISTICS = "conference_statistics";

	private User mUser;

	private boolean isRefreshing = false;
	private SwipeRefreshLayout swipeRefreshContainer;
	private PinnedHeaderExpandableListView contentExpandableListView;
	private TimelineExpandableListAdapter mTimelineExpandableListAdapter;

	private final SparseIntArray expandArray = new SparseIntArray();

	private final Comparator<MeetingGroup> comparator = new Comparator<MeetingGroup>() {

		@Override
		public int compare(MeetingGroup lhs, MeetingGroup rhs) {
			if (lhs.getSeq() < rhs.getSeq())
				return 1;
			else if (lhs.getSeq() == rhs.getSeq())
				return 0;
			else
				return -1;
		}

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!Client.getInstance().hasUserData())
			throw new NullPointerException("error: user argment must be not null");
		mUser = Client.getInstance().getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_conferences_layout, container, false);
			contentExpandableListView = (PinnedHeaderExpandableListView) mRootView
					.findViewById(R.id.contentExpandableListView);
			mTimelineExpandableListAdapter = new TimelineExpandableListAdapter(mActivity);
			mTimelineExpandableListAdapter.setOnOperationListener(this);
			View emptyView = mRootView.findViewById(R.id.emptyView);
			View newConferenceView = mRootView.findViewById(R.id.newConferenceButton);
			newConferenceView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					onCreate();
				}
			});
			contentExpandableListView.setEmptyView(emptyView);
			contentExpandableListView.setAdapter(mTimelineExpandableListAdapter);
			contentExpandableListView.setOnHeaderUpdateListener(this);
			contentExpandableListView.setOnChildClickListener(this);
			contentExpandableListView.setOnGroupExpandListener(this);
			contentExpandableListView.setOnGroupCollapseListener(this);
			contentExpandableListView.setOnItemLongClickListener(this);

			swipeRefreshContainer = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipeRefreshContainer);
			swipeRefreshContainer.setColorSchemeResources(android.R.color.holo_red_light,
					android.R.color.holo_red_light, android.R.color.holo_red_light, android.R.color.holo_red_light);
			swipeRefreshContainer.setOnRefreshListener(this);
		}

		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (isRefreshing)
			return;
		isRefreshing = true;
		swipeRefreshContainer.setRefreshing(isRefreshing);
		new ReadStatisticsAsyncTask().execute();
	}

	@Override
	public void onReject(final int groupPosition, final int childPosition, String conferenceId, String inviteId) {
		if (!isNetworkAvailable())
			return;
		Client.getInstance().rejectInvite(conferenceId, mUser.getUserId(), inviteId, new OnCallback() {

			@Override
			public void onSuccess() {
				mTimelineExpandableListAdapter.delete(groupPosition, childPosition);
				mTimelineExpandableListAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
			}

		});
	}

	@Override
	public void onAgree(final String conferenceId, String inviterId) {
		if (!isNetworkAvailable())
			return;
		Client.getInstance().agreeInvite(conferenceId, mUser.getUserId(), inviterId, new OnCallback() {

			@Override
			public void onSuccess() {
				if (!isNetworkAvailable())
					return;
				String userId = mUser.getUserId();
				String nickName = mUser.getNickname();
				if (!TextUtils.isEmpty(conferenceId) && !TextUtils.isEmpty(userId))
					ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(conferenceId, userId,
							nickName));
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
			}
		});
	}

	@Override
	public void onCreate() {
		if (mActivity instanceof MainActivity) {
			((MainActivity) mActivity).openConveneConference();
		}
	}

	@Override
	public void onEnter(String conferenceId) {
		String userId = mUser.getUserId();
		String nickName = mUser.getNickname();
		if (!TextUtils.isEmpty(conferenceId) && !TextUtils.isEmpty(userId))
			ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(conferenceId, userId, nickName));
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
		MeetingInfo meetingInfo = (MeetingInfo) mTimelineExpandableListAdapter.getChild(groupPosition, childPosition);
		String confId = meetingInfo.getConfId();
		int type = meetingInfo.getType();
		ConferenceDetailActivity.newInstance(mActivity, confId, type, meetingInfo.getInviterId());
		return false;
	}

	@Override
	public void onGroupCollapse(int groupPosition) {
		expandArray.delete(groupPosition);
	}

	@Override
	public void onGroupExpand(final int groupPosition) {
		expandArray.put(groupPosition, groupPosition);
		final MeetingGroup mg = (MeetingGroup) mTimelineExpandableListAdapter.getGroup(groupPosition);

		String data = ACache.get(mActivity, mUser.getUserId()).getAsString(mg.getConferenceSelectRange());
		if (!TextUtils.isEmpty(data))
			parseConferenceDayData(mg, data);
		Client.getInstance().obtainConferenceHistory(mg.getConferenceSelectRange(), new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
			}

			@Override
			public void onSuccess(String content) {
				parseConferenceDayData(mg, content);
				new DayDataSaveThread(mg.getConferenceSelectRange(), content).start();
			}
		});
	}

	@Override
	public void onRefresh() {
		if (isRefreshing)
			return;
		isRefreshing = true;
		initExpandableListViewData();
	}

	private boolean parseConferenceDayData(MeetingGroup mg, String content) {
		mg.clear();
		try {
			JSONArray array = new JSONArray(content);
			for (int i = 0; i < array.length(); i++) {
				mg.add(new MeetingInfo(array.optJSONObject(i)));
			}
			mg.sort();
			mTimelineExpandableListAdapter.notifyDataSetChanged();
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean parseConferenceStatistics(String content) {
		ArrayList<MeetingGroup> dataList = new ArrayList<MeetingGroup>();
		try {
			JSONArray array = new JSONArray(content);
			for (int i = 0; i < array.length(); i++) {
				JSONObject json = array.optJSONObject(i);
				MeetingGroup mg = new MeetingGroup(json);
				if (mg.getCount() > 0)
					dataList.add(mg);
			}
			if (dataList.size() > 0) {
				Collections.sort(dataList, comparator);

				MeetingGroup mg = dataList.get(0);
				if (!TextUtils.equals(mg.getTitle(), "今天")) {
					dataList.add(0, new MeetingGroup(TimeUtil.getCurrentTime("yyyy-MM-dd")));
				}
			} else {
				dataList.add(new MeetingGroup(TimeUtil.getCurrentTime("yyyy-MM-dd")));
			}
			mTimelineExpandableListAdapter.setGroupData(dataList);
			mTimelineExpandableListAdapter.notifyDataSetChanged();
			for (int i = 0; i < expandArray.size(); i++) {
				contentExpandableListView.expandGroup(expandArray.keyAt(i));
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	private class ReadStatisticsAsyncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			return ACache.get(mActivity, mUser.getUserId()).getAsString(STATISTICS);
		}

		@Override
		protected void onPostExecute(String result) {
			if (!TextUtils.isEmpty(result))
				parseConferenceStatistics(result);
			initExpandableListViewData();
		}

	}

	private class DayDataSaveThread extends Thread {
		private String key;
		private String content;

		public DayDataSaveThread(String key, String content) {
			this.key = key;
			this.content = content;
		}

		@Override
		public void run() {
			if (!TextUtils.isEmpty(key))
				ACache.get(mActivity, mUser.getUserId()).put(key, content);
		}
	}

	private class StatisticsSaveThread extends Thread {
		private String content;

		public StatisticsSaveThread(String content) {
			this.content = content;
		}

		@Override
		public void run() {
			ACache.get(mActivity, mUser.getUserId()).put(STATISTICS, content);
		}

	}

	/**
	 * 获取会议的统计数据,并展开今天的数据
	 */
	private void initExpandableListViewData() {
		if (expandArray.size() > 0) {
			for (int i = 0; i < expandArray.size(); i++) {
				try {
					contentExpandableListView.expandGroup(expandArray.keyAt(i));
				} catch (IndexOutOfBoundsException e) {
				}
			}
			isRefreshing = false;
			swipeRefreshContainer.setRefreshing(isRefreshing);
			return;
		} else {
			expandArray.put(0, 0);
		}

		Client.getInstance().obtainConferenceStatistics(new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
				if (mTimelineExpandableListAdapter.getGroupCount() == 0) {
					ArrayList<MeetingGroup> dataList = new ArrayList<MeetingGroup>();
					dataList.add(new MeetingGroup(TimeUtil.getCurrentTime("yyyy-MM-dd")));
					mTimelineExpandableListAdapter.setGroupData(dataList);
					mTimelineExpandableListAdapter.notifyDataSetChanged();
				}
				for (int i = 0; i < expandArray.size(); i++) {
					contentExpandableListView.expandGroup(expandArray.keyAt(i));
				}
				isRefreshing = false;
				swipeRefreshContainer.setRefreshing(isRefreshing);
			}

			@Override
			public void onSuccess(String content) {
				if (parseConferenceStatistics(content))
					new StatisticsSaveThread(content).start();
				isRefreshing = false;
				swipeRefreshContainer.setRefreshing(isRefreshing);
			}
		});
	}

	@Override
	public View getPinnedHeader() {
		View headerView = (ViewGroup) LayoutInflater.from(mActivity).inflate(R.layout.item_timeline_group_layout, null);
		headerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		return headerView;
	}

	@Override
	public void updatePinnedHeader(View headerView, int firstVisibleGroupPos) {
		MeetingGroup mg = (MeetingGroup) mTimelineExpandableListAdapter.getGroup(firstVisibleGroupPos);
		boolean isExpanded = contentExpandableListView.isGroupExpanded(firstVisibleGroupPos);
		TextView groupDateTextView = ViewHolderUtil.get(headerView, R.id.groupDateTextView);
		groupDateTextView.setText(mg.getTitle());
		groupDateTextView.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? R.drawable.ic_arrow_expand
				: R.drawable.ic_arrow_collect, 0, 0, 0);

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		// final Object confId = view.getTag(R.id.conference_id);
		// if (confId == null)
		// return false;
		// final Object groupPos = view.getTag(R.id.group_position);
		// final Object childPos = view.getTag(R.id.child_position);
		// if (confId instanceof String && groupPos instanceof Integer &&
		// childPos instanceof Integer) {
		//
		// new AlertDialog.Builder(mActivity).setItems(new String[] { "删除" },
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// Client.getInstance().deleteConference((String) confId, new
		// OnCallback() {
		//
		// @Override
		// public void onSuccess() {
		// mTimelineExpandableListAdapter.delete((Integer) groupPos, (Integer)
		// childPos);
		// mTimelineExpandableListAdapter.notifyDataSetChanged();
		// }
		//
		// @Override
		// public void onFailure(int errorCode, String reason) {
		// LogUtil.e(TAG, errorCode + reason);
		// }
		// });
		// }
		// }).create().show();
		// }
		return false;
	}

}
