package com.micyun.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.micyun.BaseFragment;
import com.micyun.R;

public class GuideFragment extends BaseFragment {
	private View mView;
	private int imageResource = 0;
	private boolean isLastView = false;

	private static final String RES_ID_KEY = "resId";
	private static final String LAST_KEY = "lastKey";

	public interface OnFinishListener {
		public void onFinish();
	}

	public static final GuideFragment newInstance(int resId, boolean isLast) {
		GuideFragment guideFragment = new GuideFragment();
		Bundle args = new Bundle();
		args.putInt(RES_ID_KEY, resId);
		args.putBoolean(LAST_KEY, isLast);
		guideFragment.setArguments(args);
		return guideFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
		Bundle args = getArguments();
		this.imageResource = args.getInt(RES_ID_KEY);
		this.isLastView = args.getBoolean(LAST_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_guide, container, false);
		ImageView imageView = (ImageView) mView.findViewById(R.id.guide_imageview);
		imageView.setImageResource(imageResource);
		View tryNowButton = mView.findViewById(R.id.try_now_button);
		if (isLastView) {
			tryNowButton.setVisibility(View.VISIBLE);
			mView.findViewById(R.id.try_now_button).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mActivity instanceof OnFinishListener) {
						((OnFinishListener) mActivity).onFinish();
					}
				}
			});
		} else {
			tryNowButton.setVisibility(View.GONE);
		}
		return mView;
	}

}
