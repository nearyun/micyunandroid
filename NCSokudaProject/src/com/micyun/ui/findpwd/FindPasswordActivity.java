package com.micyun.ui.findpwd;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 找回密码
 * 
 * @author xiaohua
 * 
 */
public class FindPasswordActivity extends BaseActivity implements TextWatcher {

	private static final String KEY_PHONE = "key_phone";
	private static final String KEY_USER_ID = "key_userid";

	private EditText newPwdEditText = null;
	private EditText newPwdEditTextAgain = null;
	private Button confirmButton = null;
	private TextView phoneTextView = null;

	private String password = "";
	private String userId = "";
	private String phone = "";

	public static final void newInstance(Context context, String phone, String userId) {
		Intent intent = new Intent(context, FindPasswordActivity.class);
		intent.putExtra(KEY_USER_ID, userId);
		intent.putExtra(KEY_PHONE, phone);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_retrieve_password);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		userId = intent.getStringExtra(KEY_USER_ID);
		phone = intent.getStringExtra(KEY_PHONE);

		newPwdEditText = (EditText) findViewById(R.id.new_pwd_edittext);
		newPwdEditText.addTextChangedListener(this);
		newPwdEditTextAgain = (EditText) findViewById(R.id.new_pwd_edittext_again);
		newPwdEditTextAgain.addTextChangedListener(this);

		confirmButton = (Button) findViewById(R.id.confirm_button);
		confirmButton.setEnabled(false);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				doConfirm();
			}
		});
		phoneTextView = (TextView) findViewById(R.id.phone_textview);
		phoneTextView.setText(phone);
	}

	private void doConfirm() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().retrievePassword(userId, password, new OnCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				loadingDialog.dismiss();
				showToast(message);
			}

			@Override
			public void onSuccess() {
				loadingDialog.dismiss();
				showToast("设置新密码成功");
				finish();
			}
		});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		boolean flag = newPwdEditText.getText().length() >= 6 && newPwdEditTextAgain.getText().length() >= 6;
		confirmButton.setEnabled(flag);
	}

	private boolean validate() {
		password = newPwdEditText.getText().toString();
		if (TextUtils.isEmpty(password)) {
			showToast("密码不可为空");
			return false;
		}
		String tmpPwd = newPwdEditTextAgain.getText().toString();
		if (TextUtils.isEmpty(tmpPwd)) {
			showToast("二次密码不可为空");
			return false;
		}
		if (!TextUtils.equals(password, tmpPwd)) {
			showToast("两次密码不一致");
			return false;
		}
		return true;
	}
}
