package com.micyun.ui.findpwd;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.VerificationType;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog.OnLeaveClickListener;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.micyun.util.SMSContentObserver;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

/**
 * 找回密码的匹配验证码的界面
 * 
 * @author xiaohua
 * 
 */
public class FindPasswordForMatchIdentifyCodeActivity extends IdentifyCodeBaseActivity {

	private static final String KEY_PHONE = "key_phone";
	private static final String KEY_IDENTIFY = "key_identify";
	private static final String KEY_EMAIL = "key_email";

	private TextView informationTextView = null;
	private EditText codeEditText = null;
	private Button resendButton = null;
	private Button nextButton = null;

	private String mPhone = "";
	private String mIdentify = "";// 验证码唯一标识

	private String mVerifyCode = "";// 验证码

	private int mReSendTime = 60;

	private SMSContentObserver mSMSContentObserver;
	private static final int MSG_CODE_TIMER = 0x100;
	private static final int MSG_CODE_SMS = 0x101;

	public static final void newInstance(Context context, String phone, String identify) {
		Intent intent = new Intent(context, FindPasswordForMatchIdentifyCodeActivity.class);
		intent.putExtra(KEY_PHONE, phone);
		intent.putExtra(KEY_IDENTIFY, identify);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_password_for_match_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		initView();

		Intent intent = getIntent();
		mPhone = intent.getStringExtra(KEY_PHONE);
		mIdentify = intent.getStringExtra(KEY_IDENTIFY);

		handler.sendEmptyMessage(MSG_CODE_TIMER);
		informationTextView.setText("验证码已通过短信发送到 " + mPhone);

		mSMSContentObserver = new SMSContentObserver(handler, mActivity, MSG_CODE_SMS);
		mSMSContentObserver.registerContentObserver();
	}

	@Override
	protected void initView() {
		informationTextView = (TextView) findViewById(R.id.verify_information_textview);

		codeEditText = (EditText) findViewById(R.id.verify_code_edittext);
		codeEditText.addTextChangedListener(this);

		resendButton = (Button) findViewById(R.id.verify_resend_button);
		resendButton.setText("重发(60)");
		resendButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeReapply();
			}
		});
		nextButton = (Button) findViewById(R.id.verify_next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	private void executeReapply() {
		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyVerificationCode(mPhone, VerificationType.TYPE_FIND_PASSWORD, true,
				new OnSampleCallback() {

					@Override
					public void onFailure(int errorCode, String message) {
						showToast(message);
						dialog.dismiss();
					}

					@Override
					public void onSuccess(String content) {
						try {
							JSONObject json = new JSONObject(content);
							mIdentify = json.optString("identifier");
							handler.sendEmptyMessage(MSG_CODE_TIMER);
							dialog.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							dialog.dismiss();
						}
					}
				});
	}

	@Override
	protected boolean validate() {
		mVerifyCode = codeEditText.getText().toString().trim();
		if (TextUtils.isEmpty(mVerifyCode)) {
			showToast("验证码不可为空");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().sendVerificationCode(mVerifyCode, mIdentify, new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				loadingDialog.dismiss();
				showToast(message);
			}

			@Override
			public void onSuccess(String content) {
				JSONObject json;
				try {
					json = new JSONObject(content);
					String userId = json.getString("userId");
					loadingDialog.dismiss();
					FindPasswordActivity.newInstance(mActivity, mPhone, userId);
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
					loadingDialog.dismiss();
				}

			}
		});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		nextButton.setEnabled(s.length() >= 4);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == MSG_CODE_SMS) {
				if (msg.obj != null) {
					codeEditText.setText((String) msg.obj);
					executeClick();
				}
			} else if (msg.what == MSG_CODE_TIMER) {
				if (mReSendTime > 1) {
					mReSendTime--;
					resendButton.setEnabled(false);
					resendButton.setText("重发(" + mReSendTime + ")");
					handler.sendEmptyMessageDelayed(MSG_CODE_TIMER, 1000);
				} else {
					mReSendTime = 60;
					resendButton.setEnabled(true);
					resendButton.setText("重发");
				}
			}
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSMSContentObserver.unregisterContentObserver();
		handler.removeMessages(MSG_CODE_TIMER);
		handler.removeMessages(MSG_CODE_SMS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		new IdentifyCodeAlertDialog(this).setLeaveButtonClickListener(new OnLeaveClickListener() {

			@Override
			public void onLeave() {
				finish();
			}
		}).show();
	}
}
