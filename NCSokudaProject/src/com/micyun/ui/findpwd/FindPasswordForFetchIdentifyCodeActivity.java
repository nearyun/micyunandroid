package com.micyun.ui.findpwd;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.VerificationType;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

/**
 * 找回密码的请求验证码界面
 * 
 * @author xiaohua
 * 
 */
public class FindPasswordForFetchIdentifyCodeActivity extends IdentifyCodeBaseActivity {

	private TextView informationTextView;
	private EditText phoneEditText;
	private TextView phoneNoticeTextView;
	private Button nextButton;

	private String mPhone = "";

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, FindPasswordForFetchIdentifyCodeActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_password_for_fetch_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		initView();
	}

	@Override
	protected void initView() {
		informationTextView = (TextView) findViewById(R.id.information_textview);
		phoneEditText = (EditText) findViewById(R.id.phone_edittext);
		phoneEditText.addTextChangedListener(this);
		phoneNoticeTextView = (TextView) findViewById(R.id.tips_show_textview);
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyVerificationCode(mPhone, VerificationType.TYPE_FIND_PASSWORD,  false,new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				showToast(message);
				dialog.dismiss();
			}

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					String identifier = json.optString("identifier");

					FindPasswordForMatchIdentifyCodeActivity.newInstance(mActivity, mPhone, identifier);
					dialog.dismiss();
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
					dialog.dismiss();
				}
			}
		});
	}

	@Override
	protected boolean validate() {
		mPhone = phoneEditText.getText().toString().trim();
		if (TextUtils.isEmpty(mPhone)) {
			showToast("号码不可为空");
			return false;
		}
		if (!isMobile(mPhone)) {
			showToast("号码格式不正确");
			return false;
		}
		return true;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (s.toString().length() > 0) {
			phoneNoticeTextView.setVisibility(View.VISIBLE);
			char[] chars = s.toString().toCharArray();
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < chars.length; i++) {
				if (i % 4 == 2) {
					buffer.append(chars[i] + "  ");
				} else {
					buffer.append(chars[i]);
				}
			}
			phoneNoticeTextView.setText(buffer.toString());
			nextButton.setEnabled(true);
		} else {
			phoneNoticeTextView.setVisibility(View.GONE);
			nextButton.setEnabled(false);
		}
	}

}
