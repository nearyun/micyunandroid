package com.micyun.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 注册界面
 * 
 * @author xiaohua
 * 
 */
public class RegisterActivity extends BaseActivity implements TextWatcher {

	private static final String KEY_PHONE = "key_userid";

	private EditText nickNameEditText;
	private EditText passwordEditText;
	private Button registerButton;

	private String nickname = "";
	private String password = "";
	private String userId = "";

	public static final void newInstance(Context context, String userId) {
		Intent intent = new Intent(context, RegisterActivity.class);
		intent.putExtra(KEY_PHONE, userId);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		userId = intent.getStringExtra(KEY_PHONE);

		nickNameEditText = (EditText) findViewById(R.id.nickname_edittext);
		nickNameEditText.addTextChangedListener(this);
		passwordEditText = (EditText) findViewById(R.id.password_edittext);
		passwordEditText.addTextChangedListener(this);
		registerButton = (Button) findViewById(R.id.register_button);
		registerButton.setEnabled(false);
		registerButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	private void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();

		Client.getInstance().register(userId, nickname, password, new OnCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				loadingDialog.dismiss();
				showToast(message);
			}

			@Override
			public void onSuccess() {
				loadingDialog.dismiss();
				showToast("注册成功");
				finish();
			}
		});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		boolean flag = nickNameEditText.getText().length() > 0 && passwordEditText.getText().length() >= 6;
		registerButton.setEnabled(flag);
	}

	private boolean validate() {
		nickname = nickNameEditText.getText().toString().trim();
		if (TextUtils.isEmpty(nickname)) {
			showToast("昵称不可为空");
			return false;
		}
		password = passwordEditText.getText().toString();
		if (TextUtils.isEmpty(password)) {
			showToast("注册密码不可为空");
			return false;
		}

		return true;
	}

}
