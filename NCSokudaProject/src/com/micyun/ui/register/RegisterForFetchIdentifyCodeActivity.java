package com.micyun.ui.register;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.VerificationType;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.UserProtocolsActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.micyun.util.IntentSpan;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

/**
 * 注册界面--获取验证码
 * 
 * @author xiaohua
 * 
 */
public class RegisterForFetchIdentifyCodeActivity extends IdentifyCodeBaseActivity {
	private TextView informationTextView;
	private EditText phoneEditText;
	private TextView phoneNoticeTextView;
	private Button nextButton;

	private String mPhone = "";

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, RegisterForFetchIdentifyCodeActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_for_fetch_identify_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		initView();

		informationTextView.setText("请输入您的手机号");
	}

	@Override
	protected void initView() {
		informationTextView = (TextView) findViewById(R.id.information_textview);

		phoneEditText = (EditText) findViewById(R.id.phone_edittext);
		phoneEditText.addTextChangedListener(this);

		phoneNoticeTextView = (TextView) findViewById(R.id.tips_show_textview);

		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});

		TextView protocolTextView = (TextView) findViewById(R.id.protocol_entry_textview);
		addHyperlinks(protocolTextView, 11, 20, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				UserProtocolsActivity.newInstance(mActivity);
			}
		});
	}

	private void addHyperlinks(TextView textView, int start, int end, OnClickListener listener) {

		String text = textView.getText().toString().trim();
		SpannableString sp = new SpannableString(text);
		sp.setSpan(new IntentSpan(listener), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		sp.setSpan(new ForegroundColorSpan(Color.parseColor("#155cf7")), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		textView.setText(sp);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	protected boolean validate() {
		mPhone = phoneEditText.getText().toString().trim();
		if (TextUtils.isEmpty(mPhone)) {
			showToast("号码不可为空");
			return false;
		}

		if (!isMobile(mPhone)) {
			showToast("号码格式不正确");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyVerificationCode(mPhone, VerificationType.TYPE_REGISTER, false,
				new OnSampleCallback() {

					@Override
					public void onFailure(int errorCode, String message) {
						showToast(message);
						dialog.dismiss();
					}

					@Override
					public void onSuccess(String content) {
						try {
							JSONObject json = new JSONObject(content);
							String identifier = json.optString("identifier");

							RegisterForMatchIdentifyCodeActivity.newInstance(mActivity, mPhone, identifier);
							dialog.dismiss();
							finish();
						} catch (JSONException e) {
							e.printStackTrace();
							dialog.dismiss();
						}
					}
				});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (s.toString().length() > 0) {
			phoneNoticeTextView.setVisibility(View.VISIBLE);
			char[] chars = s.toString().toCharArray();
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < chars.length; i++) {
				if (i % 4 == 2) {
					buffer.append(chars[i] + "  ");
				} else {
					buffer.append(chars[i]);
				}
			}
			phoneNoticeTextView.setText(buffer.toString());
			nextButton.setEnabled(true);
		} else {
			phoneNoticeTextView.setVisibility(View.GONE);
			nextButton.setEnabled(false);
		}
	}

}
