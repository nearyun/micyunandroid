package com.micyun.ui.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.free.R;
import com.free.overscroll.OverscrollViewPager;

public class OverscrollHackyViewPager extends OverscrollViewPager {

	public OverscrollHackyViewPager(Context context) {
		super(context);
	}

	public OverscrollHackyViewPager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public OverscrollHackyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected ViewPager createOverscrollView() {
		HackyViewPager viewpager = new HackyViewPager(getContext());
		viewpager.setId(R.id.default_viewpager);
		return viewpager;
	}

}
