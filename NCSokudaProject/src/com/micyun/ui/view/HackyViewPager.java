package com.micyun.ui.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class HackyViewPager extends ViewPager {

	/** 是否可以滑动 */
	private boolean isLocked;

	public HackyViewPager(Context context) {
		super(context);
		this.isLocked = false;
	}

	public HackyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.isLocked = false;
	}

	// 触摸没有反应就可以了
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!isLocked) {
			return super.onTouchEvent(event);
		}
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (!isLocked) {
			try {
				return super.onInterceptTouchEvent(event);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean isLocked() {
		return isLocked;
	}
}
