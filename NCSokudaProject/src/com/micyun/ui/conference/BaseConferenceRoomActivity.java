package com.micyun.ui.conference;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.micyun.BaseActivity;
import com.micyun.droidplay.ScreenShot;
import com.micyun.model.ConferenceArgument;
import com.ncore.event.IEventBus;
import com.ncore.model.conference.OnDisplayingScreenChangedListener;
import com.ncore.util.LogUtil;
import com.nearyun.apl.service.AndroidAirPlay;
import com.nearyun.sip.model.SipPhoneIntentFilter;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;
import com.nearyun.sip.receiver.AbsSipBroadcastReceiver;

/**
 * 会议室的基类
 * 
 * @author xiaohua
 * 
 */
public abstract class BaseConferenceRoomActivity extends BaseActivity implements IEventBus, View.OnClickListener,
		OnServiceConnectionListener, OnDisplayingScreenChangedListener {

	protected static final String KEY_CONFERENCE_ARGUMENT = "KEY_CONFERENCE_ARGUMENT";

	protected static Bundle getConferenceBundle(ConferenceArgument argument) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(KEY_CONFERENCE_ARGUMENT, argument);
		return bundle;
	}

	protected ConferenceArgument mConferenceArgument;
	protected SipPhoneService mSipPhoneService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mSipPhoneService = new SipPhoneService(this);
		mSipPhoneService.setOnServiceConnectionListener(this);
		mSipPhoneService.doBindService();

		registerReceiver(sipBroadcastReceiver, SipPhoneIntentFilter.getFilter(mActivity));
	}

	protected boolean hasIntentData() {
		Intent intent = getIntent();
		if (intent == null) {
			LogUtil.e(TAG, "Error:conference start failure!");
			finish();
			return false;
		}

		Serializable object = intent.getSerializableExtra(KEY_CONFERENCE_ARGUMENT);
		if (object == null || !(object instanceof ConferenceArgument)) {
			LogUtil.e(TAG, "Error:argument is null");
			finish();
			return false;
		}
		mConferenceArgument = (ConferenceArgument) object;
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopFlipPageTimer();
		
		unregisterReceiver(sipBroadcastReceiver);
	}

	private Timer flipPageTimer = null;

	protected void executeFlipTimer(View view) {
		stopFlipPageTimer();
		flipPageTimer = new Timer();
		flipPageTimer.schedule(new FlipPageTimeTask(view), 300);
	}

	protected void stopFlipPageTimer() {
		if (flipPageTimer != null) {
			flipPageTimer.cancel();
			flipPageTimer = null;
		}
	}

	private class FlipPageTimeTask extends TimerTask {
		private View view;

		public FlipPageTimeTask(View v) {
			view = v;
		}

		@Override
		public void run() {
			if (!AndroidAirPlay.getInstance().isWorking())
				return;
			Bitmap bitmap = ScreenShot.takeScreenCapture(view);
			byte[] source = ScreenShot.bitmap2Bytes(bitmap);
			if (source != null && source.length != 0) {
				AndroidAirPlay.getInstance().clearQueue();
				AndroidAirPlay.getInstance().putImage(source);
			}
		}
	}

	/**
	 * 初始化界面
	 */
	protected abstract void initializeView();

	/**
	 * 初始化数据
	 */
	protected abstract void initializeData();

	/**
	 * 刷新演示屏
	 * 
	 * @param docId
	 */
	protected abstract void refreshDisplayingScreen(String docId);

	private BroadcastReceiver sipBroadcastReceiver = new AbsSipBroadcastReceiver() {

		@Override
		protected void onSipMediaError(Context context, int index) {
			BaseConferenceRoomActivity.this.onSipMediaError(index);
		}

		@Override
		protected void onSipRedirected(Context context, int index, String target) {
			BaseConferenceRoomActivity.this.onSipRedirected(index, target);
		}

		@Override
		protected void onSipProceeding(Context context, int index, int proceedingStatus) {
			BaseConferenceRoomActivity.this.onSipProceeding(index, proceedingStatus);
		}

		@Override
		protected void onSipIdle(Context context, int index) {
			BaseConferenceRoomActivity.this.onSipIdle(index);
		}

		@Override
		protected void onSipFailure(Context context, int index, int failureStatus, String reason) {
			BaseConferenceRoomActivity.this.onSipFailure(index, failureStatus, reason);
		}

		@Override
		protected void onSipDisconnected(Context context, int index) {
			BaseConferenceRoomActivity.this.onSipDisconnected(index);
		}

		@Override
		protected void onSipConnected(Context context, int index) {
			BaseConferenceRoomActivity.this.onSipConnected(index);
		}

		@Override
		protected void onSipAnswer(Context context, int index) {
			BaseConferenceRoomActivity.this.onSipAnswer(index);
		}

		@Override
		protected void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName) {
			BaseConferenceRoomActivity.this.onSipInvite(index, callerUri, callerId, callerName);
		}

		@Override
		protected void onSipUnregistered(Context context) {
			BaseConferenceRoomActivity.this.onSipUnregistered();
		}

		@Override
		protected void onSipRegistered(Context context) {
			BaseConferenceRoomActivity.this.onSipRegistered();
		}

	};

	protected void onSipRegistered() {
		// TODO Auto-generated method stub

	}

	protected void onSipMediaError(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipRedirected(int index, String target) {
		// TODO Auto-generated method stub

	}

	protected void onSipProceeding(int index, int proceedingStatus) {
		// TODO Auto-generated method stub

	}

	protected void onSipIdle(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipFailure(int index, int failureStatus, String reason) {
		// TODO Auto-generated method stub

	}

	protected void onSipDisconnected(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipConnected(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipAnswer(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipInvite(int index, String callerUri, String callerId, String callerName) {
		// TODO Auto-generated method stub

	}

	protected void onSipUnregistered() {
		// TODO Auto-generated method stub

	}
}
