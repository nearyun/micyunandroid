package com.micyun.ui.conference;

import java.util.ArrayList;

import javax.jmdns.ServiceInfo;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.free.overscroll.OverscrollContainer.OnHorizontalOverscrollListener;
import com.micyun.R;
import com.micyun.adapter.FilePageInfoViewerAdapter;
import com.micyun.adapter.TabPagerAdapter;
import com.micyun.listener.OnTabPageChangeListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.AllContactsActivity;
import com.micyun.ui.MyUploadActivity;
import com.micyun.ui.NotificationUtil;
import com.micyun.ui.view.OverscrollHackyViewPager;
import com.micyun.ui.widget.OvershootMenu;
import com.micyun.ui.widget.dialog.ConferenceAlertDialog;
import com.micyun.ui.widget.dialog.ConferenceInitializeDataDialog;
import com.micyun.ui.widget.dialog.CustomAlertDialog;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.event.BaseEvent;
import com.ncore.event.ConferenceStatusChangedEvent;
import com.ncore.event.ParticipantStatusChangedEvent;
import com.ncore.event.SharingFileStatusChangedEvent;
import com.ncore.model.InvitePhoneInfo;
import com.ncore.model.Participant;
import com.ncore.model.SipDevice;
import com.ncore.model.client.impl.Client;
import com.ncore.model.conference.IConferenceJsonDataManager;
import com.ncore.model.conference.OnConferenceInitializeCallback;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.util.LogUtil;
import com.nearyun.apl.callback.DialogCallback;
import com.nearyun.apl.service.AndroidAirPlay;
import com.nearyun.contact.model.ContactEx;
import com.nearyun.sip.SipClient;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tornado.util.PhoneUtil;
import com.tornado.util.SystemUtil;
import com.tornado.util.TimeUtil;

import de.greenrobot.event.EventBus;

/**
 * 会议主界面
 * 
 * @author xiaohua
 * 
 */
public class ConferenceMainRoomActivity extends BaseConferenceRoomActivity {
	private static final int REQUEST_CODE_FOR_INVITE = 0X100;
	private static final int REQUEST_CODE_FOR_UPLOAD = 0X101;

	private static boolean running = false;

	public static final boolean isRunning() {
		return running;
	}

	public static final void newInstance(final Context context, final ConferenceArgument argmuent) {
		if (running) {
			Toast.makeText(context, "当前正在会议中", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent intent = new Intent(context, ConferenceMainRoomActivity.class);
		intent.putExtras(getConferenceBundle(argmuent));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private static IConferenceJsonDataManager mIConferenceJsonDataManager;

	public static final IConferenceJsonDataManager getIConferenceJsonDataManager() {
		return mIConferenceJsonDataManager;
	}

	private Handler conferenceRoomHandler = new Handler();
	private Button conferenceControlButton;
	private TextView durationTextView;
	private ImageButton zoomOutButton;
	private TextView fileNameTextView;
	private TextView filePageNumTextView;
	private TextView confSubjectTextView;
	private CheckBox voiceButton;
	private CheckBox speakerButton;
	private View defaultImageView;
	private OvershootMenu mOvershootMenu;
	private View airplayDeviceListButton;

	private OverscrollHackyViewPager mOverscrollHackyViewPager;
	private FilePageInfoViewerAdapter mFilePageInfoViewerAdapter;
	private ViewPager mOverscrollViewPager;

	private ViewPager mTabViewPager;
	private TabPagerAdapter mPagerAdapter;
	private TabWidget mTabWidget;
	private final String[] tabNameArray = { "成员", "资料" };
	private final Button[] mBtnTabs = new Button[tabNameArray.length];
	private ConferenceInitializeDataDialog mConferenceInitializeDataDialog;

	private boolean isInitilized = false;
	private boolean hasLoadParticipantData = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		running = true;
		super.onCreate(savedInstanceState);

		if (!hasIntentData())
			return;

		mIConferenceJsonDataManager = Client.getInstance().createDataManager(mConferenceArgument.getConferenceId());

		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(networkChangedReceiver, filter);

		setContentView(R.layout.activity_conference_main_room);

		AndroidAirPlay.getInstance();
		AndroidAirPlay.getInstance().init(mActivity);

		initializeData();
		initializeView();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		NotificationUtil.showNotification(mActivity, getClass(), "正在会议中", "正在会议中", "点击返回会议");
		conferenceRoomHandler.post(durationRunnable);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (isInitilized) {
			refreshUI();
			voiceButton.setChecked(mSipPhoneService.isMute());
			speakerButton.setChecked(mSipPhoneService.isUseSpeaker());
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
		conferenceRoomHandler.removeCallbacks(durationRunnable);
	}

	/**
	 * 更新会议易变的数据
	 */
	private void updateConferenceArgument() {
		mConferenceArgument.setOwner(mIConferenceJsonDataManager.getConference().getOwner());
		mConferenceArgument.setController(mIConferenceJsonDataManager.getConference().getController());
		mConferenceArgument.setDisplaying(mIConferenceJsonDataManager.getConference().getDisplayingInfo());
		if (mConferenceArgument.hasControlPermission()) {
			conferenceControlButton.setEnabled(true);
			conferenceControlButton.setBackgroundResource(R.drawable.bg_theme_color);
		} else {
			conferenceControlButton.setEnabled(false);
			conferenceControlButton.setBackgroundColor(Color.parseColor("#AAAAAA"));
		}

	}

	private void refreshUI() {
		updateConferenceArgument();
		mPagerAdapter.refresh();
		refreshDisplayingScreen(mConferenceArgument.getDisplaying());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		running = false;
		AndroidAirPlay.getInstance().release();
		conferenceRoomHandler.removeCallbacks(reconnectSipRunnable);
		NotificationUtil.dismissNotification(mActivity);
		unregisterReceiver(networkChangedReceiver);

		mIConferenceJsonDataManager.reset();
		if (SipClient.STATE_SIPCLIENT_BUSY == mSipPhoneService.getState())
			mSipPhoneService.hangup();
		else
			LogUtil.w(TAG, "warn:state is not busy " + mSipPhoneService.getState());
		mSipPhoneService.doUnbindService();

		ImageLoader.getInstance().stop();
		System.gc();
	}

	@Override
	protected void initializeData() {
		if (mConferenceInitializeDataDialog == null)
			mConferenceInitializeDataDialog = new ConferenceInitializeDataDialog(mActivity);

		mIConferenceJsonDataManager.initialize(new OnConferenceInitializeCallback() {

			@Override
			public void onStart() {
				if (!mConferenceInitializeDataDialog.isShowing())
					mConferenceInitializeDataDialog.show();
			}

			@Override
			public void onWSConnectFailure() {
				if (mConferenceInitializeDataDialog.isShowing())
					mConferenceInitializeDataDialog.dismiss();
				showDataErrorAlertDialog("获取数据失败");
			}

			@Override
			public void onProgress(int progress, int total, String message) {
				mConferenceInitializeDataDialog.setContext(message);
				mConferenceInitializeDataDialog.setSubmessage("(" + progress + "/" + total + ")");
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				if (mConferenceInitializeDataDialog.isShowing())
					mConferenceInitializeDataDialog.dismiss();
				String message = null;
				if (errorCode == 402 || errorCode == 404) {
					message = "会议不存在";
				} else if (errorCode == 407) {
					message = "会议已结束";
				}
				if (!TextUtils.isEmpty(message)) {
					showConferenceAlertDialog(message);
				} else {
					showDataErrorAlertDialog("获取数据失败");
				}

			}

			@Override
			public void onCompleted() {
				if (mConferenceInitializeDataDialog.isShowing())
					mConferenceInitializeDataDialog.dismiss();

				isInitilized = true;
				hasLoadParticipantData = true;
				confSubjectTextView.setText(mIConferenceJsonDataManager.getConference().getSubject());
				conferenceRoomHandler.postDelayed(sendNetworkSignalRunnable, 500);
				refreshUI();
				executeCallin();
			}
		});
	}

	private Runnable durationRunnable = new Runnable() {
		@Override
		public void run() {
			long opentime = mIConferenceJsonDataManager.getConference().getOpentime();
			if (opentime != 0) {
				long diff = System.currentTimeMillis() - opentime * 1000;
				durationTextView.setText(TimeUtil.second2HMS((int) (diff / 1000), true));
			}
			conferenceRoomHandler.postDelayed(this, 1000);
		}
	};

	private void showDataErrorAlertDialog(String message) {
		new CustomAlertDialog(mActivity).setMessage(message).setNegaticeText("离开").setPositiveText("重试")
				.setOnNegativeClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						executeExit();
					}
				}).setOnPositiveClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						initializeData();
					}
				}).show();
	}

	private void showConferenceAlertDialog(String message) {
		new ConferenceAlertDialog(mActivity).setMessage(message).setOnCloseListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				executeExit();
			}
		}).show();
	}

	/**
	 * 执行呼叫自己
	 */
	private void executeCallin() {
		final int phoneState = mSipPhoneService.getState();
		LogUtil.i(TAG, "sip status: " + phoneState);
		if (phoneState == -1) {
			showToast("语音设备未准备好");
			executeExit();
			return;
		}

		if (SipClient.STATE_SIPCLIENT_NEW != phoneState && SipClient.STATE_SIPCLIENT_IDLE != phoneState
				&& SipClient.STATE_SIPCLIENT_HANGUP != phoneState) {

		} else {
			// 发送邀呼请求
			if (PhoneUtil.isTelPhoneBusy(mActivity)) {
				LogUtil.w(TAG, "当前有一通电话");
				return;
			}
			mIConferenceJsonDataManager.requestCallin(new OnCallback() {

				@Override
				public void onSuccess() {

				}

				@Override
				public void onFailure(int errorCode, String reason) {
					if (errorCode == 407)
						showConferenceAlertDialog("会议已结束");
					else
						showDataErrorAlertDialog("语音无法接入");
				}
			});
		}
	}

	/**
	 * 退出前，清空数据
	 */
	private void executeExit() {
		mIConferenceJsonDataManager.removeMyself();
		finish();
	}

	@Override
	protected void initializeView() {
		initTabWidget();

		findViewById(R.id.top_view).setOnClickListener(this);
		defaultImageView = findViewById(R.id.defaultImageView);

		durationTextView = (TextView) findViewById(R.id.durationTextView);
		confSubjectTextView = (TextView) findViewById(R.id.confSubjectTextView);

		fileNameTextView = (TextView) findViewById(R.id.fileNameTextView);
		filePageNumTextView = (TextView) findViewById(R.id.fileNumTextView);

		conferenceControlButton = (Button) findViewById(R.id.conferenceControlButton);
		conferenceControlButton.setOnClickListener(this);

		zoomOutButton = (ImageButton) findViewById(R.id.zoomOutButton);
		zoomOutButton.setOnClickListener(this);

		airplayDeviceListButton = findViewById(R.id.airplayDeviceListButton);
		airplayDeviceListButton.setOnClickListener(this);
		checkWifiConnection();

		voiceButton = (CheckBox) findViewById(R.id.voiceButton);
		voiceButton.setChecked(mSipPhoneService.isMute());
		voiceButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mSipPhoneService.mute(isChecked);
			}

		});

		speakerButton = (CheckBox) findViewById(R.id.speakerButton);
		speakerButton.setChecked(mSipPhoneService.isUseSpeaker());
		speakerButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mSipPhoneService.useSpeaker(isChecked);
			}
		});

		mOvershootMenu = (OvershootMenu) findViewById(R.id.overshootMenuView);
		mOvershootMenu.setNewFileOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				MyUploadActivity.newInstance(mActivity, REQUEST_CODE_FOR_UPLOAD);
			}
		});
		mOvershootMenu.setNewMemberButton(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AllContactsActivity.newInstance(mActivity, REQUEST_CODE_FOR_INVITE);
			}
		});

		findViewById(R.id.closeConferenceButton).setOnClickListener(this);

		mOverscrollHackyViewPager = (OverscrollHackyViewPager) findViewById(R.id.topViewPager);
		mOverscrollHackyViewPager.setOnHorizontalOverscrollListener(new OnHorizontalOverscrollListener() {

			@Override
			public void onOverscrollLeft() {
				if (mTabViewPager.getChildCount() > 2)
					showToast("当前已是首页");
			}

			@Override
			public void onOverscrollRight() {
				if (mTabViewPager.getChildCount() > 2)
					showToast("当前已是最后一页");
			}
		});
		mOverscrollViewPager = mOverscrollHackyViewPager.getOverscrollView();
		mFilePageInfoViewerAdapter = new FilePageInfoViewerAdapter(getFragmentManager(), false);
		mOverscrollViewPager.setAdapter(mFilePageInfoViewerAdapter);
		mOverscrollViewPager.setPageMargin(4);
		mOverscrollViewPager.addOnPageChangeListener(new OnTabPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				synchronized (mOverscrollHackyViewPager) {
					if (mCurrentSharingFile != null && !isSettingPagerData
							&& mConferenceArgument.hasControlPermission()) {
						mIConferenceJsonDataManager.flipOverDocument(mCurrentSharingFile.getSequenceNumber(),
								position + 1, null);
					}
				}
				executeFlipTimer(mOverscrollViewPager);
			}

		});

		updateTabColor(0);
	}

	private void initTabWidget() {
		mTabWidget = (TabWidget) findViewById(R.id.topTabWidget);
		mTabWidget.setStripEnabled(false);
		for (int i = 0; i < tabNameArray.length; i++) {
			mBtnTabs[i] = getTabItemView(tabNameArray[i]);
			mTabWidget.addView(mBtnTabs[i]);
			mBtnTabs[i].setOnClickListener(mTabClickListener);
		}

		mTabViewPager = (ViewPager) findViewById(R.id.mainViewPager);
		mPagerAdapter = new TabPagerAdapter(getFragmentManager(), mConferenceArgument);
		mTabViewPager.setAdapter(mPagerAdapter);
		mTabViewPager.addOnPageChangeListener(new OnTabPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				mTabWidget.setCurrentTab(position);
				for (int i = 0; i < mBtnTabs.length; i++) {
					if (i == position) {
						mBtnTabs[i].setTextColor(getResources().getColor(R.color.tab_red_color));
						mBtnTabs[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_tab_line);
					} else {
						mBtnTabs[i].setTextColor(getResources().getColor(R.color.tab_gray_color));
						mBtnTabs[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
					}
				}
			}

		});
	}

	private void updateTabColor(int position) {
		mTabWidget.setCurrentTab(position);
		mTabViewPager.setCurrentItem(position);
		for (int i = 0; i < mBtnTabs.length; i++) {
			mBtnTabs[i].setTextColor(getResources().getColor(R.color.tab_gray_color));
			mBtnTabs[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
		mBtnTabs[position].setTextColor(getResources().getColor(R.color.tab_red_color));
		mBtnTabs[position].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_tab_line);
	}

	private Button getTabItemView(String tabName) {
		Button tabButton = new Button(mActivity);
		tabButton.setText(tabName);
		tabButton.setPadding(0, 0, 0, 1);
		tabButton.setBackgroundResource(android.R.color.transparent);
		return tabButton;
	}

	private OnClickListener mTabClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			for (int i = 0; i < mBtnTabs.length; i++) {
				mBtnTabs[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			}
			if (v == mBtnTabs[0]) {
				mTabViewPager.setCurrentItem(0);
				mBtnTabs[0].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_tab_line);
			} else if (v == mBtnTabs[1]) {
				mTabViewPager.setCurrentItem(1);
				mBtnTabs[1].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_tab_line);
			}
		}
	};

	@Override
	public void onEvent(BaseEvent evt) {
		LogUtil.printf("Room: " + evt.toString());
		if (evt instanceof ParticipantStatusChangedEvent) {
			Participant participant = mIConferenceJsonDataManager
					.getParticipant(mConferenceArgument.getCurrentUserId());
			if (hasLoadParticipantData && (participant == null || !participant.isOnline())) {
				conferenceRoomHandler.removeCallbacks(reconnectSipRunnable);
				if (mActivity.isFinishing())
					return;
				showToast("您被请出会议");
				executeExit();
			}
		} else if (evt instanceof ConferenceStatusChangedEvent) {
			updateConferenceArgument();
			refreshDisplayingScreen(mConferenceArgument.getDisplaying());
		} else if (evt instanceof SharingFileStatusChangedEvent) {
			refreshDisplayingScreen(mConferenceArgument.getDisplaying());
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.closeConferenceButton:
			showExitDialog();
			break;
		case R.id.zoomOutButton:
			executeZoomOut();
			break;
		case R.id.airplayDeviceListButton:
			openAirplayDialog();
			break;
		case R.id.conferenceControlButton:
			executePopupMoreMenu(v);
			break;
		default:
			break;
		}
	}

	private final int MENU_MUTE_ALL = Menu.FIRST + 1;
	private final int MENU_UMUTE_ALL = Menu.FIRST + 2;

	private void executePopupMoreMenu(View view) {
		PopupMenu pop = new PopupMenu(mActivity, view);
		if (mConferenceArgument.hasControlPermission()) {
			pop.getMenu().add(Menu.NONE, MENU_MUTE_ALL, Menu.NONE, "全部静音");
			pop.getMenu().add(Menu.NONE, MENU_UMUTE_ALL, Menu.NONE, "全部取消静音");
		}
		pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				if (MENU_MUTE_ALL == item.getItemId()) {
					Client.getInstance().setAllMute(mConferenceArgument.getConferenceId(), null);
				} else if (MENU_UMUTE_ALL == item.getItemId()) {
					Client.getInstance().setAllUnmute(mConferenceArgument.getConferenceId(), null);
				}
				return false;
			}

		});
		pop.show();
	}

	private void openAirplayDialog() {
		AndroidAirPlay.getInstance().showServiceList(mActivity, new DialogCallback() {

			@Override
			public void onSwitchChange(boolean flag) {

			}

			@Override
			public void onServiceSelected(ServiceInfo serviceInfo) {
				executeFlipTimer(mOverscrollViewPager);
			}
		});
	}

	@Override
	public void onBackPressed() {
		showExitDialog();
	}

	private void showExitDialog() {
		new CustomAlertDialog(mActivity).setMessage("离开会议?").setOnPositiveClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeExit();
			}
		}).show();
	}

	private void executeZoomOut() {
		ConferenceSharingViewerActivity.newInstance(mActivity, mConferenceArgument);
	}

	@Override
	protected void refreshDisplayingScreen(final String docId) {
		mIConferenceJsonDataManager.refreshDisplayingScreen(docId, this);
	}

	@Override
	protected void onSipInvite(int index, String callerUri, String callerId, String callerName) {
		LogUtil.d(TAG, "onSipInvite: " + index);
		String voip = Client.getInstance().getUser().getVoip();
		if (TextUtils.equals(callerId, voip)) {
			mSipPhoneService.answer();
		} else {
			queryInviteInfo(index, callerId, callerName);
		}
	}

	@Override
	protected void onSipConnected(int index) {
		LogUtil.d(TAG, "onSipConnected: " + index);
		speakerButton.setChecked(speakerButton.isChecked());
		voiceButton.setChecked(voiceButton.isChecked());
	}

	@Override
	protected void onSipDisconnected(int index) {
		LogUtil.d(TAG, "onSipDisconnected: " + index);
		conferenceRoomHandler.postDelayed(reconnectSipRunnable, 2000);// 此步骤必须
	}

	private BroadcastReceiver networkChangedReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
				LogUtil.w(TAG, "connectivity 网络改变");
				conferenceRoomHandler.removeCallbacks(sendNetworkSignalRunnable);
				conferenceRoomHandler.postDelayed(sendNetworkSignalRunnable, 1500);
				checkWifiConnection();
			}
		}

	};

	private Runnable sendNetworkSignalRunnable = new Runnable() {

		@Override
		public void run() {
			mIConferenceJsonDataManager.updateNetworkType(mActivity);
		}
	};

	private void queryInviteInfo(final int index, String callerId, final String callerName) {
		Client.getInstance().obtainInvitationInfo(callerId, new OnSampleCallback() {

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					final String inviterId = json.optString("inviter_id");
					final String confId = json.optString("confid");
					String subject = json.optString("subject");
					if (TextUtils.isEmpty(inviterId) || TextUtils.isEmpty(confId)) {
						mSipPhoneService.callReject(index, SipClient.CODE_REJECT_603);
						return;
					}
					if (TextUtils.equals(confId, mConferenceArgument.getConferenceId())) {
						mSipPhoneService.answer();
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
						builder.setMessage(subject);
						builder.setTitle(callerName + " 邀请您参加会议");
						builder.setPositiveButton("切换会议", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								mSipPhoneService.callHangup(mSipPhoneService.currentcall());
								mSipPhoneService.callAnswer(index);
								Client.getInstance().agreeInvite(confId, Client.getInstance().getUser().getUserId(),
										inviterId, null);
								executeExit();
								conferenceRoomHandler.postDelayed(new Runnable() {

									@Override
									public void run() {
										newInstance(mApp,
												new ConferenceArgument(confId, mConferenceArgument.getCurrentUserId(),
														mConferenceArgument.getUserNickName()));
									}
								}, 1500);
							}
						});
						builder.setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								mSipPhoneService.callReject(index, SipClient.CODE_REJECT_603);
								Client.getInstance().rejectInvite(confId, Client.getInstance().getUser().getUserId(),
										inviterId, null);
								dialog.dismiss();
							}
						});
						builder.create().show();
						SystemUtil.vibrate(mActivity);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					mSipPhoneService.reject(SipClient.CODE_REJECT_603);
				}
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				mSipPhoneService.reject(SipClient.CODE_REJECT_603);
			}
		});
	}

	private Runnable reconnectSipRunnable = new Runnable() {
		@Override
		public void run() {
			if (!isNetworkAvailable())
				return;
			Participant participant = mIConferenceJsonDataManager
					.getParticipant(mConferenceArgument.getCurrentUserId());
			if (participant == null || !participant.isAndroidOnline()) {
				return;
			}
			SipDevice[] mSipDevices = participant.getSipDevices();
			if (mSipDevices == null)
				return;
			boolean handup = false;
			for (int i = 0; i < mSipDevices.length; i++) {
				handup |= mSipDevices[i].isHandup();
			}
			if (handup)
				return;
			mIConferenceJsonDataManager.requestCallin(new OnCallback() {

				@Override
				public void onSuccess() {

				}

				@Override
				public void onFailure(int errorCode, String reason) {
					if (errorCode == 407)
						showConferenceAlertDialog("会议已结束");
					else
						conferenceRoomHandler.postDelayed(reconnectSipRunnable, 2000);
				}
			});
		}
	};

	@Override
	public void onConnected() {
		LogUtil.i(TAG, "sip service onConnected");
		mSipPhoneService.register();
		mSipPhoneService.mute(false);
		runKeepalive();
		if (voiceButton != null)
			voiceButton.setChecked(mSipPhoneService.isMute());
		if (speakerButton != null)
			speakerButton.setChecked(mSipPhoneService.isUseSpeaker());
	}

	@Override
	public void onDisconnected() {
		LogUtil.w(TAG, "sip service onDisconnected");
	}

	private void runKeepalive() {
		new Thread() {
			public void run() {
				while (running) {
					mSipPhoneService.keepalive();
					SystemClock.sleep(5000);
				}
			}
		}.start();
	}

	protected void checkWifiConnection() {
		if (airplayDeviceListButton == null)
			return;
		ConnectivityManager cm = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		State wifiState = wifiNetworkInfo != null ? wifiNetworkInfo.getState() : null;
		if (wifiState != null && State.CONNECTED == wifiState) {
			airplayDeviceListButton.setVisibility(View.VISIBLE);
			AndroidAirPlay.getInstance().startJmDnsSearch();
		} else {
			airplayDeviceListButton.setVisibility(View.INVISIBLE);
			AndroidAirPlay.getInstance().stopJmDNSSearch();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_FOR_INVITE) {
			updateTabColor(0);
			if (resultCode != RESULT_OK || data == null)
				return;
			ArrayList<ContactEx> dataFromContact = (ArrayList<ContactEx>) data
					.getSerializableExtra(AllContactsActivity.KEY_DATA);
			executeInvite(dataFromContact);
		} else if (requestCode == REQUEST_CODE_FOR_UPLOAD) {
			updateTabColor(1);
		}

	}

	private void executeInvite(ArrayList<ContactEx> data) {

		if (data == null)
			return;

		int len = data.size();
		ArrayList<InvitePhoneInfo> phones = new ArrayList<InvitePhoneInfo>();
		for (int i = 0; i < len; i++) {
			String phone = data.get(i).getPhone();
			if (!TextUtils.isEmpty(phone)) {
				phones.add(new InvitePhoneInfo(data.get(i).getName(), data.get(i).getPhone()));
			}
		}
		if (phones.size() == 0)
			return;

		Client.getInstance().sendInvitation(mConferenceArgument.getConferenceId(),
				Client.getInstance().getUser().getSipUser(), phones, false, new OnCallback() {

					@Override
					public void onSuccess() {
					}

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, reason + errorCode);
						showToast("邀请信息发送失败");
					}

				});
	}

	@Override
	public void onEmptyViewer() {
		fileNameTextView.setText("");
		airplayDeviceListButton.setVisibility(View.INVISIBLE);
		filePageNumTextView.setVisibility(View.GONE);
		defaultImageView.setVisibility(View.VISIBLE);
		zoomOutButton.setVisibility(View.GONE);
		mOverscrollHackyViewPager.setVisibility(View.GONE);
		mOverscrollHackyViewPager.setOverscroll(false);
	}

	private boolean isSettingPagerData = false;
	private SharingFile mCurrentSharingFile;

	@Override
	public void onDisplayViewer(SharingFile sharingFile, ArrayList<SharingFilePageInfo> list) {
		defaultImageView.setVisibility(View.GONE);
		zoomOutButton.setVisibility(View.VISIBLE);
		airplayDeviceListButton.setVisibility(View.VISIBLE);
		fileNameTextView.setText(sharingFile.getFileName());
		filePageNumTextView.setVisibility(View.VISIBLE);
		filePageNumTextView.setText(sharingFile.getCurrentPage() + "/" + sharingFile.getFilePages());
		mOverscrollHackyViewPager.setVisibility(View.VISIBLE);
		mOverscrollHackyViewPager.setOverscroll(true);

		synchronized (mOverscrollHackyViewPager) {
			isSettingPagerData = true;
			mCurrentSharingFile = sharingFile;
			mFilePageInfoViewerAdapter.setData(sharingFile.getDocId(), list);
			final int gotoPosition = sharingFile.getCurrentPage();
			mOverscrollViewPager.setCurrentItem(gotoPosition - 1);
			isSettingPagerData = false;
			return;
		}
	}

	@Override
	public void onErrorViewer() {
		fileNameTextView.setText("");
		airplayDeviceListButton.setVisibility(View.INVISIBLE);
		filePageNumTextView.setVisibility(View.GONE);
		defaultImageView.setVisibility(View.VISIBLE);
		zoomOutButton.setVisibility(View.GONE);
		mOverscrollHackyViewPager.setVisibility(View.GONE);
		mOverscrollHackyViewPager.setOverscroll(false);
	}

}
