package com.micyun.ui.conference;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.listener.OnTextChangedListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnSampleCallback;
import com.ncore.http.request.HttpResponseCode;
import com.ncore.model.User;
import com.ncore.model.client.impl.Client;

/**
 * 召开会议
 * 
 * @author xiaohua
 * 
 */
public class ConveneConferenceActivity extends BaseActivity {

	public static final void newInstance(Context context, User user) {
		Intent intent = new Intent(context, ConveneConferenceActivity.class);
		context.startActivity(intent);
	}

	private User mUser;
	private EditText subjectEditText;
	private Button startButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_convene_conference);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		if (!Client.getInstance().hasUserData()) {
			finish();
			return;
		}

		mUser = Client.getInstance().getUser();

		startButton = (Button) findViewById(R.id.startButton);
		subjectEditText = (EditText) findViewById(R.id.subjectEditText);
		subjectEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				startButton.setEnabled(s.length() > 0);
			}
		});
		subjectEditText.setText(mUser.getNickname() + "的会议");
		subjectEditText.setSelection(subjectEditText.getText().length() - 1);

		startButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				doComplete();
			}
		});
	}

	private void doComplete() {
		if (!isNetworkAvailable())
			return;
		String subject = subjectEditText.getText().toString();
		if (TextUtils.isEmpty(subject)) {
			showToast("主题不可为空");
			return;
		}
		final LoadingDialog dialog = new LoadingDialog(this);
		dialog.show();
		Client.getInstance().conveneConference(subject, new OnSampleCallback() {

			@Override
			public void onSuccess(String confId) {
				if (isNetworkAvailable())
					ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(confId, mUser.getUserId(),
							mUser.getNickname()));
				dialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(HttpResponseCode.parseCode(errorCode, reason));
				dialog.dismiss();
			}

		});
	}
}
