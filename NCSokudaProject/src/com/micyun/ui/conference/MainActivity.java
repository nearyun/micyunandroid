package com.micyun.ui.conference;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;

import com.micyun.BaseActivity;
import com.micyun.NCApplication;
import com.micyun.R;
import com.micyun.adapter.ModulesAdapter;
import com.micyun.model.ModuleItem;
import com.micyun.push.BrandPushStrategy;
import com.micyun.service.ExtraTaskIntentService;
import com.micyun.ui.AboutUsActivity;
import com.micyun.ui.FeedbackActivity;
import com.micyun.ui.LoginActivity;
import com.micyun.ui.PersonInfoActivity;
import com.micyun.ui.fragment.ConferencesFragment;
import com.micyun.ui.widget.ModuleListHeader;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.config.NCConstants;
import com.ncore.model.User;
import com.ncore.model.client.impl.Client;
import com.nearyun.contact.ui.util.ContactsHelper;
import com.nearyun.sip.io.SipSharePerference;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;
import com.tornado.util.ACache;
import com.uml.UpdateAgent;

/**
 * 主入口界面
 * 
 * @author xiaohua
 * 
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		context.startActivity(intent);
	}

	public static final void newInstanceInNewTask(Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private DrawerLayout mDrawerLayout;
	private View mLeftDrawer;

	private ModuleListHeader moduleListHeader;

	private ListView mModuleListView;

	private SipPhoneService mSipPhoneService;

	private User mUser = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mUser = Client.getInstance().getUser();
		BrandPushStrategy.updateToken(mActivity, mUser.getUserId());

		mApp.keepDataSafe(mUser.getUserId());

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mLeftDrawer = findViewById(R.id.left_drawer_layout);

		findViewById(R.id.leftButton).setOnClickListener(this);
		findViewById(R.id.rightButton).setOnClickListener(this);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		initLeftDrawer();

		refreshFragment();

		mSipPhoneService = new SipPhoneService(this);
		SipAccount sipAccount = new SipAccount(mUser.getNickname(), mUser.getSipUser(), NCConstants.getSipDomain(),
				NCConstants.getOutproxyServer(), mUser.getSipUser(), mUser.getSipPassword(), mUser.getInstanceId(),
				NCConstants.getIceServer());
		mSipPhoneService.doStartService(sipAccount);
		mSipPhoneService.doBindService();

		UpdateAgent.update(mActivity);// 检查版本是否需要更新
		IntentFilter filter = new IntentFilter(ACTION_LOGOUT);
		registerReceiver(mainBroadcastReceiver, filter);

		ContactsHelper.insertDefaultData(mActivity, getString(R.string.contact_name),
				getString(R.string.contact_phone), getString(R.string.contact_website));
		ContactsHelper.insertDefaultData(mActivity, getString(R.string.contact_service_name),
				getString(R.string.contact_service_phone), getString(R.string.contact_website));

		ExtraTaskIntentService.startClearService(mActivity);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshUserData();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSipPhoneService.doUnbindService();
		unregisterReceiver(mainBroadcastReceiver);
	}

	private void refreshUserData() {
		moduleListHeader.setUserName(mUser.getNickname());
		ImageLoaderUtil.refreshAvatar(mUser, moduleListHeader.getAvatarImageView());
		mSipPhoneService.register();
	}

	private void initLeftDrawer() {
		moduleListHeader = new ModuleListHeader(mActivity);
		moduleListHeader.setOnAvatarClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PersonInfoActivity.newInstance(mActivity);
			}
		});

		final ArrayList<ModuleItem> moduleDataList = new ArrayList<ModuleItem>();
		moduleDataList.add(new ModuleItem(R.drawable.ic_feedback, "意见反馈") {

			@Override
			public void executeOnClick() {
				FeedbackActivity.newInstance(mActivity);
			}
		});
		moduleDataList.add(new ModuleItem(R.drawable.ic_about_us, "关于我们") {

			@Override
			public void executeOnClick() {
				AboutUsActivity.newInstance(mActivity);
			}
		});
		mModuleListView = (ListView) findViewById(R.id.drawer_listview);
		mModuleListView.addHeaderView(moduleListHeader);
		mModuleListView.setAdapter(new ModulesAdapter(mActivity, moduleDataList));
		mModuleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0)
					return;
				ModuleItem item = moduleDataList.get(position - 1);
				if (item != null)
					item.executeOnClick();
				mDrawerLayout.closeDrawer(mLeftDrawer);
			}
		});
	}

	private void refreshFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		Fragment fragment = ConferencesFragment.newInstance(mUser);
		fragmentManager.beginTransaction().replace(R.id.mainContentLayout, fragment).commit();
	}

	private void logout() {
		BrandPushStrategy.clearToken(mActivity, mUser.getUserId());
		SipSharePerference.cleanSipAccount(mActivity);
		mSipPhoneService.doStopService();
		Client.getInstance().logout(null);
		mApp.clearImageCache();
		ACache.get(mActivity, mUser.getUserId()).clear();
		NCApplication.setLoginFlag(false);
		LoginActivity.newInstance(mActivity);
		finish();
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(mLeftDrawer)) {
			mDrawerLayout.closeDrawer(mLeftDrawer);
			return;
		}

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.leftButton:
			if (!mDrawerLayout.isDrawerOpen(mLeftDrawer)) {
				mDrawerLayout.openDrawer(mLeftDrawer);
			}
			break;
		case R.id.rightButton:
			openConveneConference();
			break;
		default:
			break;
		}
	}

	public void openConveneConference() {
		ConveneConferenceActivity.newInstance(mActivity, mUser);
	}

	public static final String ACTION_LOGOUT = "com.micyun.ui.conference.logout";
	private BroadcastReceiver mainBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (!TextUtils.equals(ACTION_LOGOUT, action))
				return;
			logout();
		}

	};

}
