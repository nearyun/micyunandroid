package com.micyun.ui.conference;

import java.util.ArrayList;

import javax.jmdns.ServiceInfo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.free.circleimageview.CircleImageView;
import com.free.overscroll.OverscrollContainer.OnHorizontalOverscrollListener;
import com.micyun.R;
import com.micyun.adapter.FilePageInfoViewerAdapter;
import com.micyun.listener.OnTabPageChangeListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.sensor.GravityOrientationSensor;
import com.micyun.sensor.GravityOrientationSensor.OnOrientationChangedListener;
import com.micyun.ui.conference.fragment.viewer.FilePageInfoFragment.OnToolbarClickListener;
import com.micyun.ui.view.OverscrollHackyViewPager;
import com.micyun.ui.widget.popupwindow.SharingFileThumbnailPopupWindow;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.event.BaseEvent;
import com.ncore.event.ConferenceStatusChangedEvent;
import com.ncore.event.ParticipantStatusChangedEvent;
import com.ncore.event.SharingFileStatusChangedEvent;
import com.ncore.model.Participant;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.util.LogUtil;
import com.nearyun.apl.callback.DialogCallback;
import com.nearyun.apl.service.AndroidAirPlay;
import com.nearyun.sip.model.SipPhoneIntentFilter;

import de.greenrobot.event.EventBus;

/**
 * 共享文档全屏模式
 * 
 * @author xiaohua
 * 
 */
public class ConferenceSharingViewerActivity extends BaseConferenceRoomActivity implements OnToolbarClickListener {

	private boolean isSettingPagerData = false;// 是否在设置文档信息
	private OverscrollHackyViewPager mOverscrollViewPager;
	private ViewPager mViewPager;
	private FilePageInfoViewerAdapter pagerAdapter;

	private SharingFile mCurrentSharingFile;

	private CircleImageView mCircleImageView; // 主持人头像
	private CheckBox voiceButton;
	private CheckBox speakerButton;
	private View toolsBarLayout;
	private TextView fileNumTextView;
	private View airplayDeviceListButton;
	private GravityOrientationSensor mGravityOrientation;

	private Handler conferenceRoomHandler = new Handler();

	public static final void newInstance(Activity context, ConferenceArgument argmuent) {
		Intent intent = new Intent(context, ConferenceSharingViewerActivity.class);
		intent.putExtras(getConferenceBundle(argmuent));
		context.startActivityForResult(intent, 100);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!hasIntentData())
			return;

		setContentView(R.layout.activity_conference_sharing_viewer);

		IntentFilter filter = SipPhoneIntentFilter.getFilter(mActivity);
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(sipBroadcastReceiver, filter);

		initializeView();
		initializeData();
	}

	@Override
	protected void initializeView() {
		fileNumTextView = (TextView) findViewById(R.id.fileNumTextView);

		toolsBarLayout = findViewById(R.id.toolsBarLayout);
		mCircleImageView = (CircleImageView) findViewById(R.id.controllerImageView);

		findViewById(R.id.backButton).setOnClickListener(this);
		findViewById(R.id.thumbnailButton).setOnClickListener(this);
		airplayDeviceListButton = findViewById(R.id.airplayDeviceListButton);
		airplayDeviceListButton.setOnClickListener(this);

		voiceButton = (CheckBox) findViewById(R.id.voiceButton);
		voiceButton.setChecked(mSipPhoneService.isMute());
		voiceButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mSipPhoneService.mute(isChecked);
			}

		});

		speakerButton = (CheckBox) findViewById(R.id.speakerButton);
		speakerButton.setChecked(mSipPhoneService.isUseSpeaker());
		speakerButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mSipPhoneService.useSpeaker(isChecked);
			}
		});

		mOverscrollViewPager = (OverscrollHackyViewPager) findViewById(R.id.document_viewpager);
		mOverscrollViewPager.setOnHorizontalOverscrollListener(new OnHorizontalOverscrollListener() {

			@Override
			public void onOverscrollLeft() {
				if (mViewPager.getChildCount() > 2)
					showToast("当前已是首页");
			}

			@Override
			public void onOverscrollRight() {
				if (mViewPager.getChildCount() > 2)
					showToast("当前已是尾页");
			}
		});
		mViewPager = mOverscrollViewPager.getOverscrollView();
		pagerAdapter = new FilePageInfoViewerAdapter(getFragmentManager(), true);
		mViewPager.setAdapter(pagerAdapter);
		mViewPager.setPageMargin(8);
		mViewPager.addOnPageChangeListener(mOnTabPageChangeListener);

		conferenceRoomHandler.postDelayed(topBarRunnable, DELAY_HIDE_MILLIS);
		mGravityOrientation = new GravityOrientationSensor(this);
		mGravityOrientation.setOnOrientationChanged(new OnOrientationChangedListener() {

			@Override
			public void onPortrait() {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}

			@Override
			public void onLandscape() {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		});
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		EventBus.getDefault().register(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
		mGravityOrientation.register();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mGravityOrientation.unregister();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		AndroidAirPlay.getInstance().dismissServiceListDialog();
		EventBus.getDefault().unregister(this);
		mSipPhoneService.doUnbindService();
		unregisterReceiver(sipBroadcastReceiver);
	}

	@Override
	protected void initializeData() {
		refreshDisplayingScreen(mConferenceArgument.getDisplaying());
		refreshControllerAvatar();
	}

	private void refreshControllerAvatar() {
		Participant participant = null;
		if (mConferenceArgument.hasController())
			participant = ConferenceMainRoomActivity.getIConferenceJsonDataManager().getParticipant(
					mConferenceArgument.getController());
		else if (TextUtils.equals(mConferenceArgument.getOwner(), mConferenceArgument.getCurrentUserId()))
			participant = ConferenceMainRoomActivity.getIConferenceJsonDataManager().getParticipant(
					mConferenceArgument.getCurrentUserId());
		if (participant == null) {
			mCircleImageView.setImageResource(R.drawable.default_avator);
		} else {
			ImageLoaderUtil.refreshAvatar(participant, mCircleImageView);
		}
	}

	@Override
	protected void refreshDisplayingScreen(final String docId) {
		ConferenceMainRoomActivity.getIConferenceJsonDataManager().refreshDisplayingScreen(docId, this);
	}

	private OnTabPageChangeListener mOnTabPageChangeListener = new OnTabPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			synchronized (mOverscrollViewPager) {
				if (mCurrentSharingFile != null && !isSettingPagerData && mConferenceArgument.hasControlPermission()) {
					ConferenceMainRoomActivity.getIConferenceJsonDataManager().flipOverDocument(
							mCurrentSharingFile.getSequenceNumber(), position + 1, null);
				}
				executeFlipTimer(mOverscrollViewPager);
			}
		}

	};

	@Override
	public void onEvent(BaseEvent evt) {
		LogUtil.printf("SharingViewer: " + evt.toString());
		if (evt instanceof ParticipantStatusChangedEvent) {

		} else if (evt instanceof ConferenceStatusChangedEvent) {
			mConferenceArgument.setController(ConferenceMainRoomActivity.getIConferenceJsonDataManager()
					.getConference().getController());
			mConferenceArgument.setDisplaying(ConferenceMainRoomActivity.getIConferenceJsonDataManager()
					.getConference().getDisplayingInfo());
			refreshControllerAvatar();
			refreshDisplayingScreen(mConferenceArgument.getDisplaying());
		} else if (evt instanceof SharingFileStatusChangedEvent) {
			refreshDisplayingScreen(mConferenceArgument.getDisplaying());
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backButton:
			onBackPressed();
			break;
		case R.id.thumbnailButton:
			executeAllPageThumbail(v);
			break;
		case R.id.airplayDeviceListButton:
			openAirplayDialog();
			break;
		default:
			break;
		}
	}

	private void openAirplayDialog() {
		AndroidAirPlay.getInstance().showServiceList(mActivity, new DialogCallback() {

			@Override
			public void onSwitchChange(boolean flag) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onServiceSelected(ServiceInfo serviceInfo) {
				executeFlipTimer(mOverscrollViewPager);
			}
		});
	}

	private SharingFileThumbnailPopupWindow mThumbnailPopWin = null;

	private void executeAllPageThumbail(View view) {
		if (mThumbnailPopWin == null)
			mThumbnailPopWin = new SharingFileThumbnailPopupWindow(mActivity, mConferenceArgument, mCurrentSharingFile);
		mThumbnailPopWin.show(view);
		mThumbnailPopWin.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				mThumbnailPopWin = null;
			}
		});
	}

	@Override
	public void onToolbarClick() {
		changeToolBarStatus();
	}

	private final int DELAY_HIDE_MILLIS = 5000;
	private boolean isShowToolBar = true;
	private Runnable topBarRunnable = new Runnable() {

		@Override
		public void run() {
			if (isShowToolBar) {
				toolsBarLayout.setVisibility(View.INVISIBLE);
				isShowToolBar = false;
			}
		}

	};

	private void changeToolBarStatus() {
		conferenceRoomHandler.removeCallbacks(topBarRunnable);
		if (isShowToolBar) {
			conferenceRoomHandler.post(topBarRunnable);
		} else {
			toolsBarLayout.setVisibility(View.VISIBLE);
			isShowToolBar = true;
			conferenceRoomHandler.postDelayed(topBarRunnable, DELAY_HIDE_MILLIS);
		}
	}

	@Override
	public void onConnected() {
		if (voiceButton != null)
			voiceButton.setChecked(mSipPhoneService.isMute());
		if (speakerButton != null)
			speakerButton.setChecked(mSipPhoneService.isUseSpeaker());
	}

	@Override
	public void onDisconnected() {

	}

	@Override
	public void onDisplayViewer(SharingFile sharingFile, ArrayList<SharingFilePageInfo> list) {
		synchronized (mOverscrollViewPager) {
			isSettingPagerData = true;
			mCurrentSharingFile = sharingFile;
			pagerAdapter.setData(sharingFile.getDocId(), list);
			final int gotoPosition = sharingFile.getCurrentPage();
			mViewPager.setCurrentItem(gotoPosition - 1);
			isSettingPagerData = false;
			fileNumTextView.setText(gotoPosition + "/" + sharingFile.getFilePages());
			fileNumTextView.setVisibility(View.VISIBLE);
			return;
		}
	}

	private BroadcastReceiver sipBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			switch (action) {
			case SipPhoneIntentFilter.ACTION_SIP_EVENT_REGISTERED:
				LogUtil.d(TAG, "BroadcastReceiver --> 注册成功");
				break;
			case SipPhoneIntentFilter.ACTION_SIP_EVENT_UNREGISTERED:
				LogUtil.d(TAG, "BroadcastReceiver --> 注销成功");
				break;
			case SipPhoneIntentFilter.ACTION_SIP_EVENT_ON_INVITE:
				LogUtil.d(TAG, "BroadcastReceiver --> 收到邀请");
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_ANSWER:
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_CONNECTED:
				LogUtil.d(TAG, "BroadcastReceiver --> 语音接通");
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_DISCONNECTED:
				LogUtil.d(TAG, "BroadcastReceiver --> 语音断开");
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_FAILURE:
				LogUtil.d(TAG, "BroadcastReceiver --> 语音连接失败");
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_IDLE:
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_PROCEEDING:
				break;
			case SipPhoneIntentFilter.ACTION_EVENT_ON_REDIRECTED:
				break;
			case ConnectivityManager.CONNECTIVITY_ACTION:
				checkWifiConnection();
				break;
			default:
				break;
			}
		}

	};

	protected void checkWifiConnection() {
		if (airplayDeviceListButton == null)
			return;
		ConnectivityManager cm = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		State wifiState = wifiNetworkInfo != null ? wifiNetworkInfo.getState() : null;
		if (wifiState != null && State.CONNECTED == wifiState) {
			airplayDeviceListButton.setVisibility(View.VISIBLE);
		} else {
			airplayDeviceListButton.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onEmptyViewer() {
		pagerAdapter.reset();
		fileNumTextView.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onErrorViewer() {
		pagerAdapter.reset();
		fileNumTextView.setVisibility(View.INVISIBLE);

	}
}
