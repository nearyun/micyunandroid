package com.micyun.ui.conference;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.BaseListAdapter;
import com.micyun.ui.CommonPersonDetailActivity;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.Participant;
import com.tornado.util.ViewHolderUtil;

/**
 * 会议详细信息的所有参会成员列表
 * 
 * @author xiaohua
 * 
 */
public class ParticipantsListActivity extends BaseActivity {

	private static final ArrayList<Participant> participants = new ArrayList<Participant>();

	public static final void newInstance(Context context, ArrayList<Participant> data) {
		participants.clear();
		participants.addAll(data);
		Intent intent = new Intent(context, ParticipantsListActivity.class);
		context.startActivity(intent);
	}

	private ListView participantsListView;
	private SampleParticipantAdapter mSampleParticipantAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_participants_list);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		participantsListView = (ListView) findViewById(R.id.participantsListView);
		participantsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Participant p = participants.get(position);
				CommonPersonDetailActivity.newInstance(mActivity, p.getUserId());
			}
		});
		mSampleParticipantAdapter = new SampleParticipantAdapter(mActivity);
		participantsListView.setAdapter(mSampleParticipantAdapter);
		mSampleParticipantAdapter.addAll(participants);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		participants.clear();
	}

	private class SampleParticipantAdapter extends BaseListAdapter<Participant> {

		public SampleParticipantAdapter(Context cxt) {
			super(cxt);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null)
				convertView = mInflater.inflate(R.layout.item_sample_participant_layout, parent, false);
			Participant participant = mDataList.get(position);
			ImageView avatarImageView = ViewHolderUtil.get(convertView, R.id.itemAvatarImageView);
			ImageLoaderUtil.refreshAvatar(participant, avatarImageView);

			TextView nameTextView = ViewHolderUtil.get(convertView, R.id.itemNameTextView);
			nameTextView.setText(participant.getNickName());

			TextView departmentTextView = ViewHolderUtil.get(convertView, R.id.itemDepartmentTextView);
			departmentTextView.setText(participant.getDepartment());

			TextView mobileTextView = ViewHolderUtil.get(convertView, R.id.itemMobileTextView);
			mobileTextView.setText(participant.getMobile());

			return convertView;
		}

	}
}
