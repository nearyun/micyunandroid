package com.micyun.ui.conference;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.InviteMemberRecycleViewAdapter;
import com.micyun.adapter.InviteMemberRecycleViewAdapter.OnAddListener;
import com.micyun.adapter.InviteSharingRecycleViewAdapter;
import com.micyun.listener.OnTextChangedListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.model.InvitedParticipant;
import com.micyun.model.SharingFileWanted;
import com.micyun.ui.AllContactsActivity;
import com.micyun.ui.NetBoxActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnSampleCallback;
import com.ncore.http.request.HttpResponseCode;
import com.ncore.model.client.impl.Client;
import com.ncore.model.conference.impl.Conference;
import com.ncore.util.LogUtil;
import com.nearyun.contact.model.ContactEx;
import com.tornado.util.ACache;

/**
 * 重新召开
 * 
 * @author xiaohua
 * 
 */
public class ReconveneActivity extends BaseActivity {
	private static final String KEY_USER_ID = "KEY_USER_ID";
	private static final String KEY_CONF_ID = "KEY_CONF_ID";
	private static final int REQUEST_CODE_FOR_INVITE = 0x100;
	private static final int REQUEST_CODE_FOR_FILE = 0X200;

	public static final void newInstance(Context context, String userId, String confId) {
		Intent intent = new Intent(context, ReconveneActivity.class);
		intent.putExtra(KEY_CONF_ID, confId);
		intent.putExtra(KEY_USER_ID, userId);
		context.startActivity(intent);
	}

	private String userId, confId, ownerName;
	private Conference mConference = new Conference();

	private View addSharingView;
	private TextView ownerTextView;
	private EditText subjectEditText;
	private RecyclerView membersRecyclerView;
	private InviteMemberRecycleViewAdapter mInviteMemberRecycleViewAdapter;
	private RecyclerView sharingFileRecyclerView;
	private InviteSharingRecycleViewAdapter mInviteSharingRecycleViewAdapter;
	private Button conveneRightNowBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reconvene);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		ownerTextView = (TextView) findViewById(R.id.ownerTextView);

		addSharingView = findViewById(R.id.addSharingView);
		addSharingView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				NetBoxActivity.newInstance(mActivity, mInviteSharingRecycleViewAdapter.toStringArray(),
						REQUEST_CODE_FOR_FILE);
			}
		});

		subjectEditText = (EditText) findViewById(R.id.subjectEditText);
		subjectEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				conveneRightNowBtn.setEnabled(s.length() > 0);
			}
		});

		conveneRightNowBtn = (Button) findViewById(R.id.conveneRightNowBtn);
		conveneRightNowBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeConvene();
			}

		});

		membersRecyclerView = (RecyclerView) findViewById(R.id.membersRecyclerView);
		membersRecyclerView.setHasFixedSize(true);
		membersRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 4));
		mInviteMemberRecycleViewAdapter = new InviteMemberRecycleViewAdapter(mActivity, membersRecyclerView);
		membersRecyclerView.setAdapter(mInviteMemberRecycleViewAdapter);
		mInviteMemberRecycleViewAdapter.setOnAddListener(new OnAddListener() {

			@Override
			public void onAddClick() {
				AllContactsActivity.newInstance(mActivity, REQUEST_CODE_FOR_INVITE);
			}
		});

		sharingFileRecyclerView = (RecyclerView) findViewById(R.id.sharingFileRecyclerView);
		sharingFileRecyclerView.setHasFixedSize(true);
		LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
		sharingFileRecyclerView.setLayoutManager(layoutManager);
		mInviteSharingRecycleViewAdapter = new InviteSharingRecycleViewAdapter(mActivity, sharingFileRecyclerView);
		sharingFileRecyclerView.setAdapter(mInviteSharingRecycleViewAdapter);

		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(KEY_CONF_ID) && intent.hasExtra(KEY_USER_ID)) {
			userId = intent.getStringExtra(KEY_USER_ID);
			confId = intent.getStringExtra(KEY_CONF_ID);
			String confData = ACache.get(mActivity, userId).getAsString(confId);
			if (!TextUtils.isEmpty(confData)) {
				parseConferenceDetail(confData);
			}
		}

	}

	protected void executeConvene() {
		JSONObject argument = new JSONObject();
		String subject = subjectEditText.getText().toString();
		try {
			argument.put("confno", mConference.getName());
			argument.put("confpwd", mConference.getPassword());
			argument.put("subject", subject);
			JSONObject members = new JSONObject();
			members.put("sipno", Client.getInstance().getUser().getSipUser());
			members.put("pstn", false);
			members.put("list", mInviteMemberRecycleViewAdapter.toJSONArray());
			argument.put("members", members);
			JSONObject sharings = new JSONObject();
			sharings.put("fileOwner", Client.getInstance().getUser().getUserId());
			sharings.put("ownerName", Client.getInstance().getUser().getNickname());
			sharings.put("list", mInviteSharingRecycleViewAdapter.toJSONArray());
			argument.put("sharings", sharings);
		} catch (JSONException e) {
			LogUtil.printStackTrace(e);
		}
		final Dialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().reconveneConference(argument.toString(), new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(HttpResponseCode.parseCode(errorCode, reason));
				loadingDialog.dismiss();
			}

			@Override
			public void onSuccess(String content) {
				loadingDialog.dismiss();
				try {
					JSONObject json = new JSONObject(content);
					String conferenceId = json.optString("confid");
					if (!TextUtils.isEmpty(conferenceId)) {
						ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(conferenceId, userId,
								Client.getInstance().getUser().getNickname()));
						finish();
					}
				} catch (JSONException e) {
					LogUtil.printStackTrace(e);
				}
			}
		});
	}

	private void parseConferenceDetail(String content) {
		try {
			JSONObject json = new JSONObject(content);
			JSONObject confdoc = json.optJSONObject("confdoc");
			mConference.refreshConferenceData(confdoc);
			String subject = mConference.getSubject();
			subjectEditText.setText(subject);
			subjectEditText.setSelection(subject.length());

			ArrayList<InvitedParticipant> mParticipants = new ArrayList<InvitedParticipant>();
			JSONArray members = json.optJSONArray("members");
			for (int i = 0; i < members.length(); i++) {
				InvitedParticipant p = InvitedParticipant.create(i, members.getJSONObject(i));
				if (!TextUtils.equals(p.getUserId(), userId))
					mParticipants.add(p);
			}
			mInviteMemberRecycleViewAdapter.refreshData(mParticipants);

			ArrayList<SharingFileWanted> mSharings = new ArrayList<SharingFileWanted>();
			JSONArray sharings = json.optJSONArray("sharings");
			for (int i = 0; i < sharings.length(); i++) {
				SharingFileWanted f = SharingFileWanted.createFromCopy(sharings.getJSONObject(i));
				// 排除默认文档
				if (!f.isDefaultFile()) {
					mSharings.add(f);
				}
			}
			mInviteSharingRecycleViewAdapter.refreshData(mSharings);

			JSONObject ownerInfo = json.optJSONObject("ownerInfo");
			ownerName = ownerInfo.getString("nickname");
			ownerTextView.setText(ownerName);
		} catch (JSONException e) {
			LogUtil.printStackTrace(e);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_FOR_INVITE) {
			if (resultCode != RESULT_OK || data == null)
				return;
			ArrayList<ContactEx> dataFromContact = (ArrayList<ContactEx>) data
					.getSerializableExtra(AllContactsActivity.KEY_DATA);
			for (int i = 0; i < dataFromContact.size(); i++) {
				ContactEx cex = dataFromContact.get(i);
				boolean exist = false;
				for (int j = 0; j < mInviteMemberRecycleViewAdapter.getItemCount() - 2; j++) {
					InvitedParticipant ip = mInviteMemberRecycleViewAdapter.getItem(j);
					if (TextUtils.equals(cex.getPhone(), ip.getPhone())) {
						exist = true;
						break;
					}
				}
				if (!exist) {
					InvitedParticipant iParticipant = InvitedParticipant.create(cex.getName(), cex.getPhone());
					mInviteMemberRecycleViewAdapter.addItem(iParticipant);
				}
			}
			mInviteMemberRecycleViewAdapter.notifyDataSetChangedToResize();
		} else if (requestCode == REQUEST_CODE_FOR_FILE) {
			if (resultCode != RESULT_OK || data == null)
				return;
			String content = data.getStringExtra(NetBoxActivity.KEY_RESULT_DATA);
			try {
				JSONArray array = new JSONArray(content);
				for (int i = 0; i < array.length(); i++) {
					SharingFileWanted f = SharingFileWanted.createFromNetBox(array.getJSONObject(i));
					mInviteSharingRecycleViewAdapter.addItem(f);
				}
				mInviteSharingRecycleViewAdapter.notifyDataSetChangedToResize();
			} catch (JSONException e) {
				LogUtil.printStackTrace(e);
			}
		}
	}

}
