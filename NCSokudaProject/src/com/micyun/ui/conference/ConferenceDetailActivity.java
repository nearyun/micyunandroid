package com.micyun.ui.conference;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.SampleMemberRecycleViewAdapter;
import com.micyun.adapter.SampleMemberRecycleViewAdapter.OnClickMoreListener;
import com.micyun.adapter.SampleSharingFileRecycleViewAdapter;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.Participant;
import com.ncore.model.client.impl.Client;
import com.ncore.model.conference.impl.Conference;
import com.ncore.model.sharing.SharingFile;
import com.ncore.util.LogUtil;
import com.tornado.util.ACache;
import com.tornado.util.TimeUtil;

/**
 * 查看会议详情
 * @author xiaohua
 *
 */
public class ConferenceDetailActivity extends BaseActivity implements View.OnClickListener, OnRefreshListener {

	private static final String KEY_TYPE = "key_type";
	private static final String KEY_CONF_ID = "key_conf_id";
	private static final String KEY_INVITER_ID = "key_inviter_id";

	/**
	 * 
	 * @param context
	 * @param confId
	 * @param type
	 *            0:未参加，待确定；1:已参加
	 * @param inviterId
	 *            可以为空
	 */
	public static final void newInstance(Context context, String confId, int type, String inviterId) {
		Intent intent = new Intent(context, ConferenceDetailActivity.class);
		intent.putExtra(KEY_TYPE, type);
		intent.putExtra(KEY_CONF_ID, confId);
		intent.putExtra(KEY_INVITER_ID, inviterId);
		context.startActivity(intent);
	}

	private int type = -1;// 0:未参加，待确定；1:已参加
	private String confId = null;
	private String ownerName = null;
	private Conference mConference = new Conference();
	private ArrayList<Participant> mParticipants = new ArrayList<Participant>();
	private ArrayList<SharingFile> mSharings = new ArrayList<SharingFile>();

	private TextView subjectTextView;
	private TextView ownerTextView;
	private TextView conferenceDateTextView;
	private TextView conferenceTimeTextView;
	private TextView sharingFileCountTextView;
	private TextView membersCountTextView;
	private RecyclerView membersRecyclerView;
	private SampleMemberRecycleViewAdapter mSampleMemberRecycleViewAdapter;
	private RecyclerView sharingFileRecyclerView;
	private SampleSharingFileRecycleViewAdapter mSampleSharingFileRecycleViewAdapter;

	private View waitLayout;
	private Button rejectInviteBtn;
	private Button agreeInviteBtn;

	private View doItNowLayout;
	private Button enterConfNowBtn;

	private View recreateLayout;
	private Button recreateConfBtn;

	private boolean isRefreshing = false;
	private SwipeRefreshLayout swipeRefreshContainer;

	private String userId;
	private String nickName;
	private String inviterId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conference_detail);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		if (intent == null) {
			return;
		}

		type = intent.getIntExtra(KEY_TYPE, -1);
		confId = intent.getStringExtra(KEY_CONF_ID);

		if (type == -1 || TextUtils.isEmpty(confId)) {
			showToast("参数错误");
			return;
		}

		userId = Client.getInstance().getUser().getUserId();
		nickName = Client.getInstance().getUser().getNickname();
		inviterId = intent.getStringExtra(KEY_INVITER_ID);

		initializeUI();

		String confData = ACache.get(mActivity, userId).getAsString(confId);
		if (!TextUtils.isEmpty(confData))
			parseConferenceDetail(confData);
		pullRefreshData();
	}

	private void pullRefreshData() {
		isRefreshing = true;
		Client.getInstance().obtainConferenceDetail(confId, new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				isRefreshing = false;
				swipeRefreshContainer.setRefreshing(isRefreshing);
			}

			@Override
			public void onSuccess(String content) {
				parseConferenceDetail(content);
				new ConferenceDetailSaveThread(confId, content).start();
				isRefreshing = false;
				swipeRefreshContainer.setRefreshing(isRefreshing);
			}
		});
	}

	private class ConferenceDetailSaveThread extends Thread {
		private String key, content;

		public ConferenceDetailSaveThread(String key, String content) {
			this.key = key;
			this.content = content;
		}

		@Override
		public void run() {
			ACache.get(mActivity, userId).put(key, content);
		}
	}

	private void initializeUI() {
		subjectTextView = (TextView) findViewById(R.id.subjectTextView);
		ownerTextView = (TextView) findViewById(R.id.ownerTextView);
		conferenceDateTextView = (TextView) findViewById(R.id.conferenceDateTextView);
		conferenceTimeTextView = (TextView) findViewById(R.id.conferenceTimeTextView);

		sharingFileCountTextView = (TextView) findViewById(R.id.sharingFileCountTextView);
		membersCountTextView = (TextView) findViewById(R.id.membersCountTextView);

		membersRecyclerView = (RecyclerView) findViewById(R.id.membersRecyclerView);
		membersRecyclerView.setHasFixedSize(true);
		membersRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 4));
		mSampleMemberRecycleViewAdapter = new SampleMemberRecycleViewAdapter(mActivity, membersRecyclerView);
		membersRecyclerView.setAdapter(mSampleMemberRecycleViewAdapter);
		mSampleMemberRecycleViewAdapter.setOnClickMoreListener(new OnClickMoreListener() {

			@Override
			public void onClickMore() {
				ParticipantsListActivity.newInstance(mActivity, mParticipants);
			}
		});

		sharingFileRecyclerView = (RecyclerView) findViewById(R.id.sharingFileRecyclerView);
		sharingFileRecyclerView.setHasFixedSize(true);
		LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
		sharingFileRecyclerView.setLayoutManager(layoutManager);
		mSampleSharingFileRecycleViewAdapter = new SampleSharingFileRecycleViewAdapter(mActivity,
				sharingFileRecyclerView);
		sharingFileRecyclerView.setAdapter(mSampleSharingFileRecycleViewAdapter);

		waitLayout = findViewById(R.id.waitLayout);
		waitLayout.setVisibility(View.GONE);
		rejectInviteBtn = (Button) findViewById(R.id.rejectInviteBtn);
		rejectInviteBtn.setOnClickListener(this);
		agreeInviteBtn = (Button) findViewById(R.id.agreeInviteBtn);
		agreeInviteBtn.setOnClickListener(this);

		doItNowLayout = findViewById(R.id.doItNowLayout);
		doItNowLayout.setVisibility(View.GONE);
		enterConfNowBtn = (Button) findViewById(R.id.enterConfNowBtn);
		enterConfNowBtn.setOnClickListener(this);

		recreateLayout = findViewById(R.id.recreateLayout);
		recreateLayout.setVisibility(View.GONE);
		recreateConfBtn = (Button) findViewById(R.id.recreateConfBtn);
		recreateConfBtn.setOnClickListener(this);

		swipeRefreshContainer = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshContainer);
		swipeRefreshContainer.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_red_light,
				android.R.color.holo_red_light, android.R.color.holo_red_light);
		swipeRefreshContainer.setOnRefreshListener(this);
	}

	private void refreshUI() {
		subjectTextView.setText(mConference.getSubject());
		ownerTextView.setText(ownerName);

		conferenceDateTextView.setText(TimeUtil.longToString(mConference.getOpentime() * 1000,
				TimeUtil.FORMAT_DATA_SHORT));
		long starttime = mConference.getOpentime();
		long endtime = mConference.getEndtime();
		String timeString = TimeUtil.longToString(starttime * 1000, TimeUtil.FORMAT_TIME) + "--";
		timeString += (endtime == 0) ? "" : TimeUtil.longToString(endtime * 1000, TimeUtil.FORMAT_TIME);
		conferenceTimeTextView.setText(timeString);

		membersCountTextView.setText("(" + mParticipants.size() + ")");
		mSampleMemberRecycleViewAdapter.refreshData(mParticipants);
		mSampleMemberRecycleViewAdapter.notifyDataSetChangedToResize();

		sharingFileCountTextView.setText("(" + mSharings.size() + ")");
		mSampleSharingFileRecycleViewAdapter.refreshData(mSharings);
		mSampleSharingFileRecycleViewAdapter.notifyDataSetChangedToResize();

		int state = mConference.getState();
		if (Conference.CONF_STATE_DESTROYED == state || Conference.CONF_STATE_STOPPED == state) {
			doItNowLayout.setVisibility(View.GONE);
			waitLayout.setVisibility(View.GONE);
			if (TextUtils.equals(mConference.getOwner(), userId))
				recreateLayout.setVisibility(View.VISIBLE);
			else
				recreateLayout.setVisibility(View.GONE);
		} else {
			if (type == 0) {
				doItNowLayout.setVisibility(View.GONE);
				waitLayout.setVisibility(View.VISIBLE);
				recreateLayout.setVisibility(View.GONE);
			} else if (type == 1) {
				doItNowLayout.setVisibility(View.VISIBLE);
				waitLayout.setVisibility(View.GONE);
				recreateLayout.setVisibility(View.GONE);
			} else {
				doItNowLayout.setVisibility(View.GONE);
				waitLayout.setVisibility(View.GONE);
				recreateLayout.setVisibility(View.GONE);
			}
		}
	}

	private void parseConferenceDetail(String content) {
		try {
			JSONObject json = new JSONObject(content);
			JSONObject confdoc = json.optJSONObject("confdoc");
			mConference.refreshConferenceData(confdoc);
			JSONArray members = json.optJSONArray("members");
			mParticipants.clear();
			for (int i = 0; i < members.length(); i++) {
				mParticipants.add(Participant.create(i, members.getJSONObject(i)));
			}
			JSONArray sharings = json.optJSONArray("sharings");
			mSharings.clear();
			for (int i = 0; i < sharings.length(); i++) {
				mSharings.add(SharingFile.create(i, sharings.getJSONObject(i)));
			}
			JSONObject ownerInfo = json.optJSONObject("ownerInfo");
			ownerName = ownerInfo.getString("nickname");

			refreshUI();
		} catch (JSONException e) {
			e.printStackTrace();
			showToast("无法解析数据");
			return;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rejectInviteBtn:
			doRejectInvite();
			break;
		case R.id.agreeInviteBtn:
			doAgreeInvite();
			break;
		case R.id.enterConfNowBtn:
			doEnterConferenceNow();
			break;
		case R.id.recreateConfBtn:
			doRecreateConf();
			break;
		default:
			break;
		}
	}

	private void doRecreateConf() {
		ReconveneActivity.newInstance(mActivity, userId, confId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		int state = mConference.getState();
		if (Conference.CONF_STATE_DESTROYED == state || Conference.CONF_STATE_STOPPED == state)
			getMenuInflater().inflate(R.menu.menu_conference_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_delete:
			doDelete();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void doDelete() {
		if (!isNetworkAvailable())
			return;
		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().deleteConference(confId, new OnCallback() {

			@Override
			public void onSuccess() {
				loadingDialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, errorCode + reason);
				loadingDialog.dismiss();
			}
		});
	}

	private void doEnterConferenceNow() {
		if (!isNetworkAvailable())
			return;
		if (!TextUtils.isEmpty(confId) && !TextUtils.isEmpty(userId))
			ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(confId, userId, nickName));
		finish();
	}

	private void doAgreeInvite() {
		if (!isNetworkAvailable())
			return;
		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().agreeInvite(confId, userId, inviterId, new OnCallback() {

			@Override
			public void onSuccess() {
				if (!isNetworkAvailable())
					return;
				loadingDialog.dismiss();
				ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(confId, userId, nickName));
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
				loadingDialog.dismiss();
			}
		});
	}

	private void doRejectInvite() {
		if (!isNetworkAvailable())
			return;
		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().rejectInvite(confId, userId, inviterId, new OnCallback() {

			@Override
			public void onSuccess() {
				loadingDialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, reason + errorCode);
				loadingDialog.dismiss();
			}

		});
	}

	@Override
	public void onRefresh() {
		if (isRefreshing)
			return;
		pullRefreshData();
	}
}
