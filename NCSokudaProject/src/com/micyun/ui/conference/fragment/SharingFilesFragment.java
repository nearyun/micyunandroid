package com.micyun.ui.conference.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.micyun.R;
import com.micyun.adapter.SharingFileRecycleViewAdapter;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.ncore.callback.OnCallback;
import com.ncore.event.BaseEvent;
import com.ncore.event.ConferenceStatusChangedEvent;
import com.ncore.event.ParticipantStatusChangedEvent;
import com.ncore.event.SharingFileStatusChangedEvent;
import com.ncore.util.LogUtil;

/**
 * 共享文档列表界面
 * 
 * @author xiaohua
 * 
 */
public class SharingFilesFragment extends BaseConferenceFragment {

	public static Fragment newInstance(ConferenceArgument argument) {
		SharingFilesFragment message = new SharingFilesFragment();
		message.setArguments(getConferenceBundle(argument));
		return message;
	}

	private RecyclerView sharingfileRecyclerView;
	private SharingFileRecycleViewAdapter mSharingFileAdapter;
	private SwipeRefreshLayout swipeRefreshLayout;

	private boolean isRefreshing = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_sharingfiles_layout, container, false);
			sharingfileRecyclerView = (RecyclerView) mRootView.findViewById(R.id.sharingfileRecyclerView);
			sharingfileRecyclerView.setHasFixedSize(true);
			LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
			sharingfileRecyclerView.setLayoutManager(layoutManager);
			mSharingFileAdapter = new SharingFileRecycleViewAdapter(mActivity, mConferenceArgument);
			sharingfileRecyclerView.setAdapter(mSharingFileAdapter);

			swipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipeRefreshContainer);
			swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_red_light,
					android.R.color.holo_red_light, android.R.color.holo_red_light);
			swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

				@Override
				public void onRefresh() {
					if (isRefreshing)
						return;
					isRefreshing = true;
					ConferenceMainRoomActivity.getIConferenceJsonDataManager().listSharings(new OnCallback() {

						@Override
						public void onSuccess() {
							isRefreshing = false;
							refreshListView();
							swipeRefreshLayout.setRefreshing(false);
						}

						@Override
						public void onFailure(int errorCode, String reason) {
							isRefreshing = false;
							swipeRefreshLayout.setRefreshing(false);
							LogUtil.e(TAG, reason + " " + errorCode);
							showToast("刷新失败");
						}
					});
				}
			});
		}

		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	@Override
	protected void refreshListView() {
		mSharingFileAdapter.addAllWithClear(ConferenceMainRoomActivity.getIConferenceJsonDataManager()
				.getSharingFiles());
		mSharingFileAdapter.notifyDataSetChanged();
	}

	@Override
	public void onEvent(BaseEvent evt) {
		LogUtil.printf("sharing: " + evt.toString());
		if (evt instanceof ParticipantStatusChangedEvent) {

		} else if (evt instanceof ConferenceStatusChangedEvent) {
			mSharingFileAdapter.notifyDataSetChanged();
		} else if (evt instanceof SharingFileStatusChangedEvent) {
			refreshListView();
		}
	}

}
