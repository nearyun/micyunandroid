package com.micyun.ui.conference.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.micyun.R;
import com.micyun.adapter.MemberRecycleViewAdapter;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.ncore.callback.OnCallback;
import com.ncore.event.BaseEvent;
import com.ncore.event.ConferenceStatusChangedEvent;
import com.ncore.event.ParticipantStatusChangedEvent;
import com.ncore.event.SharingFileStatusChangedEvent;
import com.ncore.model.Participant;
import com.ncore.util.LogUtil;

/**
 * 人员列表
 * 
 * @author xiaohua
 * 
 */
public class MembersFragment extends BaseConferenceFragment {

	public static Fragment newInstance(ConferenceArgument argument) {
		MembersFragment message = new MembersFragment();
		message.setArguments(getConferenceBundle(argument));
		return message;
	}

	private RecyclerView membersRecyclerView;
	private MemberRecycleViewAdapter mMembersAdapter;
	private SwipeRefreshLayout swipeRefreshLayout;
	private boolean isRefreshing = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_members_layout, container, false);

			membersRecyclerView = (RecyclerView) mRootView.findViewById(R.id.membersRecyclerView);
			membersRecyclerView.setHasFixedSize(true);
			LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
			membersRecyclerView.setLayoutManager(layoutManager);
			mMembersAdapter = new MemberRecycleViewAdapter(mActivity, mConferenceArgument);
			membersRecyclerView.setAdapter(mMembersAdapter);

			swipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipeRefreshContainer);
			swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_red_light,
					android.R.color.holo_red_light, android.R.color.holo_red_light);
			swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

				@Override
				public void onRefresh() {
					if (isRefreshing)
						return;
					isRefreshing = true;
					ConferenceMainRoomActivity.getIConferenceJsonDataManager().listMembers(new OnCallback() {

						@Override
						public void onSuccess() {
							isRefreshing = false;
							refreshListView();
							swipeRefreshLayout.setRefreshing(false);
						}

						@Override
						public void onFailure(int errorCode, String reason) {
							isRefreshing = false;
							swipeRefreshLayout.setRefreshing(false);
							LogUtil.e(TAG, reason + " " + errorCode);
							showToast("刷新失败");
						}
					});
				}
			});

		}

		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	private final int FLAG_OWNER = 3;
	private final int FLAG_CONTROLLER = 2;
	private final int FLAG_ME = 1;
	private final int FLAG_ONLINE = 0;
	private final int FLAG_OFFLINE = -1;

	@Override
	protected void refreshListView() {
		ArrayList<Participant> data = ConferenceMainRoomActivity.getIConferenceJsonDataManager().getParticipants();
		for (int i = 0; i < data.size(); i++) {
			Participant p = data.get(i);
			if (TextUtils.equals(p.getUserId(), mConferenceArgument.getOwner())) {
				p.setOrder(FLAG_OWNER);
			} else if (TextUtils.equals(p.getUserId(), mConferenceArgument.getController())) {
				p.setOrder(FLAG_CONTROLLER);
			} else if (TextUtils.equals(p.getUserId(), mConferenceArgument.getCurrentUserId())) {
				p.setOrder(FLAG_ME);
			} else if (p.isOnline()) {
				p.setOrder(FLAG_ONLINE);
			} else {
				p.setOrder(FLAG_OFFLINE);
			}
		}
		Collections.sort(data, mComparator);
		mMembersAdapter.addAllWithClear(data);
		mMembersAdapter.notifyDataSetChanged();

	}

	private Comparator<Participant> mComparator = new Comparator<Participant>() {

		@Override
		public int compare(Participant lhs, Participant rhs) {
			if (lhs.getOrder() > rhs.getOrder())
				return -1;
			else if (lhs.getOrder() == rhs.getOrder())
				return 0;
			else
				return 1;
		}

	};

	@Override
	public void onEvent(BaseEvent evt) {
		LogUtil.printf("Member: " + evt.toString());
		if (evt instanceof ParticipantStatusChangedEvent) {
			refreshListView();
		} else if (evt instanceof ConferenceStatusChangedEvent) {
			refreshListView();
		} else if (evt instanceof SharingFileStatusChangedEvent) {

		}
	}

}
