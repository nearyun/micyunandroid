package com.micyun.ui.conference.fragment.viewer;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.free.photoview.library.PhotoView;
import com.free.photoview.library.PhotoViewAttacher.OnMatrixChangedListener;
import com.free.photoview.library.PhotoViewAttacher.OnViewTapListener;
import com.micyun.BaseFragment;
import com.micyun.R;
import com.micyun.droidplay.ScreenShot;
import com.micyun.ui.view.CircleProgressBar;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.util.LogUtil;
import com.nearyun.apl.service.AndroidAirPlay;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

/**
 * 每一页的信息
 * 
 * @author xiaohua
 * 
 */
public class FilePageInfoFragment extends BaseFragment implements OnClickListener {
	private static final String KEY_SHARING_FILE_PAGE_INFO = "KEY_SHARING_FILE_PAGE_INFO";
	private static final String KEY_ZOOMABLE = "KEY_ZOOMABLE";

	private ImageViewAware mImageViewAware = null;
	private PhotoView mImageView;
	private CircleProgressBar mCircleProgressBar;
	private Button retryButton;
	@SuppressWarnings("unused")
	private TextView mPercentTextView;

	private static final DisplayImageOptions options = new DisplayImageOptions.Builder().resetViewBeforeLoading(true)
			.cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).displayer(new FadeInBitmapDisplayer(50))
			.build();

	public static final Fragment newInstance(SharingFilePageInfo pageInfo, boolean zoomable) {
		Fragment fragment = new FilePageInfoFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(KEY_SHARING_FILE_PAGE_INFO, pageInfo);
		bundle.putBoolean(KEY_ZOOMABLE, zoomable);
		fragment.setArguments(bundle);
		return fragment;
	}

	private SharingFilePageInfo mPageInfo;
	private boolean zoomable = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getArguments();
		mPageInfo = (SharingFilePageInfo) bundle.getSerializable(KEY_SHARING_FILE_PAGE_INFO);
		zoomable = bundle.getBoolean(KEY_ZOOMABLE);
	}

	public interface OnToolbarClickListener {
		public void onToolbarClick();
	}

	private void toggleTools() {
		if (mActivity instanceof OnToolbarClickListener) {
			((OnToolbarClickListener) mActivity).onToolbarClick();
		}
	}

	private void putImage2Airplay() {
		stopPutImageTimer();
		putTimer = new Timer();
		putTimer.schedule(new PutImageTask(), 100);
	}

	private void stopPutImageTimer() {
		if (putTimer != null) {
			putTimer.cancel();
			putTimer = null;
		}
	}

	private Timer putTimer = null;

	private class PutImageTask extends TimerTask {

		@Override
		public void run() {
			if (isVisibleToUser && AndroidAirPlay.getInstance().isWorking()) {
				final Bitmap bitmap = ScreenShot.takeScreenCapture(mImageView);
				byte[] source = ScreenShot.bitmap2Bytes(bitmap);
				if (source != null && source.length != 0)
					AndroidAirPlay.getInstance().putImage(source);
				if (bitmap != null && !bitmap.isRecycled())
					bitmap.recycle();
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_viewer_layout, container, false);
			mImageView = (PhotoView) mRootView.findViewById(R.id.zoom_photo_view);
			mImageView.setZoomable(zoomable);
			if (zoomable)
				mImageView.setOnMatrixChangeListener(new OnMatrixChangedListener() {

					@Override
					public void onMatrixChanged(RectF rect) {
						putImage2Airplay();
					}
				});
			// mImageView.setScaleType(ScaleType.CENTER_CROP);
			mCircleProgressBar = (CircleProgressBar) mRootView.findViewById(R.id.progress_circle_bar);
			mPercentTextView = (TextView) mRootView.findViewById(R.id.percent_textview);
			retryButton = (Button) mRootView.findViewById(R.id.retry_button);
			retryButton.setOnClickListener(this);

			mRootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					toggleTools();
				}
			});

			mImageView.setOnViewTapListener(new OnViewTapListener() {

				@Override
				public void onViewTap(View view, float x, float y) {
					toggleTools();
				}
			});

			// mImageView.setOnScaleChangeListener(new OnScaleChangeListener() {
			//
			// @Override
			// public void onScaleChanged(float scale) {
			// mHandler.removeCallbacks(percentDismissRunnable);
			// mPercentTextView.setVisibility(View.VISIBLE);
			// mPercentTextView.setText("缩放:" + (int) (scale * 100) + "%");
			// mHandler.postDelayed(percentDismissRunnable, 1500);
			// }
			// });

			mImageViewAware = new ImageViewAware(mImageView) {

				@Override
				public int getHeight() {
					return 1200;
				}

				@Override
				public int getWidth() {
					return 1200;
				}

			};
		}
		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	// private final Handler mHandler = new Handler();
	// private final Runnable percentDismissRunnable = new Runnable() {
	// public void run() {
	// mPercentTextView.setVisibility(View.GONE);
	// }
	// };

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		executeImageLoader(mPageInfo);
	}

	private boolean isVisibleToUser = false;

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			// 相当于Fragment的 onResume
		} else {
			// 相当于Fragment的 onPause
		}
		this.isVisibleToUser = isVisibleToUser;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopPutImageTimer();
		ImageLoader.getInstance().cancelDisplayTask(mImageViewAware);
	}

	private void executeImageLoader(SharingFilePageInfo spf) {

		ImageLoader.getInstance().displayImage(spf.getImageUrl(), mImageViewAware, options, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				mCircleProgressBar.setVisibility(View.VISIBLE);
				retryButton.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				mCircleProgressBar.setVisibility(View.GONE);
				retryButton.setVisibility(View.VISIBLE);
				retryButton.setText(R.string.txt_load_failure_retry_it);
				switch (failReason.getType()) { // 获取图片失败类型
				case IO_ERROR: // 文件I/O错误
					LogUtil.e(TAG, "文件I/O错误:" + mPageInfo.getPageNum());
					break;
				case DECODING_ERROR: // 解码错误
					LogUtil.e(TAG, "解码错误:" + mPageInfo.getPageNum());
					break;
				case NETWORK_DENIED: // 网络延迟
					LogUtil.e(TAG, "网络延迟:" + mPageInfo.getPageNum());
					break;
				case OUT_OF_MEMORY: // 内存不足
					LogUtil.e(TAG, "内存不足:" + mPageInfo.getPageNum());
					break;
				case UNKNOWN: // 原因不明
					LogUtil.e(TAG, "原因不明:" + mPageInfo.getPageNum());
					break;
				}
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				mCircleProgressBar.setVisibility(View.GONE);
				retryButton.setVisibility(View.GONE);
				putImage2Airplay();
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				mCircleProgressBar.setVisibility(View.GONE);
				retryButton.setVisibility(View.VISIBLE);
				retryButton.setText(R.string.txt_load_failure_retry_it);
			}
		}, new ImageLoadingProgressListener() {

			@Override
			public void onProgressUpdate(String imageUri, View view, int progress, int max) {
				mCircleProgressBar.setProgress(progress, max);
			}

		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.retry_button:
			if (isNetworkAvailable()) {
				executeImageLoader(mPageInfo);
				retryButton.setVisibility(View.GONE);
			}
			break;

		default:
			break;
		}
	}

}
