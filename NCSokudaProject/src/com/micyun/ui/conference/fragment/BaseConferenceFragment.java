package com.micyun.ui.conference.fragment;

import android.os.Bundle;

import com.micyun.BaseFragment;
import com.micyun.model.ConferenceArgument;
import com.ncore.event.IEventBus;

import de.greenrobot.event.EventBus;

/**
 * 抽象会议表
 * 
 * @author xiaohua
 * 
 */
public abstract class BaseConferenceFragment extends BaseFragment implements IEventBus {
	protected static final String KEY_CONFERENCE_ARGUMENT = "KEY_CONFERENCE_ARGUMENT";

	protected ConferenceArgument mConferenceArgument;

	protected static Bundle getConferenceBundle(ConferenceArgument conferenceId) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(KEY_CONFERENCE_ARGUMENT, conferenceId);
		return bundle;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		mConferenceArgument = (ConferenceArgument) bundle.getSerializable(KEY_CONFERENCE_ARGUMENT);
		if (mConferenceArgument == null) {
			showToast("参数不可为空");
			mActivity.finish();
			return;
		}

	}

	/**
	 * 刷新列表
	 */
	protected abstract void refreshListView();

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		refreshListView();
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

}
