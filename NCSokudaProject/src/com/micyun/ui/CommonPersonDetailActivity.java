package com.micyun.ui;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.Person;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.tornado.util.ACache;

/**
 * 查看用户信息
 * @author xiaohua
 *
 */
public class CommonPersonDetailActivity extends BaseActivity {
	private static final String KEY_USER_ID = "KEY_USER_ID";

	public static final void newInstance(Context context, String userId) {
		Intent intent = new Intent(context, CommonPersonDetailActivity.class);
		intent.putExtra(KEY_USER_ID, userId);
		context.startActivity(intent);
	}

	private ImageView avatarImageView;
	private TextView nameTextView;
	private TextView companyTextView;
	private TextView departmentTextView;
	private TextView mobileTextView;
	private TextView emailTextView;

	private Person person;
	private String userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_common_person_detail);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		initialUI();

		userId = intent.getStringExtra(KEY_USER_ID);
		String content = ACache.get(mActivity, Client.getInstance().getUser().getUserId()).getAsString(userId);
		if (!TextUtils.isEmpty(content)) {
			parsePersonInformation(content);
		}

		Client.getInstance().queryPersonInformation(userId, new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.w(TAG, errorCode + " " + reason);
			}

			@Override
			public void onSuccess(String content) {
				parsePersonInformation(content);
			}

		});

	}

	private void initialUI() {
		avatarImageView = (ImageView) findViewById(R.id.avatarImageView);
		nameTextView = (TextView) findViewById(R.id.nameTextView);
		companyTextView = (TextView) findViewById(R.id.companyTextView);
		departmentTextView = (TextView) findViewById(R.id.departmentTextView);
		mobileTextView = (TextView) findViewById(R.id.mobileTextView);
		emailTextView = (TextView) findViewById(R.id.emailTextView);
	}

	private void refreshUI(Person p) {
		ImageLoaderUtil.refreshAvatar(p, avatarImageView);
		nameTextView.setText(p.getNickName());
		String company = p.getCompany();
		companyTextView.setText(TextUtils.isEmpty(company) ? "未填写" : company);
		String department = p.getDepartment();
		departmentTextView.setText(TextUtils.isEmpty(department) ? "未填写" : department);
		String mobile = p.getMobile();
		mobileTextView.setText(TextUtils.isEmpty(mobile) ? "未填写" : mobile);
		String mail = p.getUserMail();
		emailTextView.setText(TextUtils.isEmpty(mail) ? "未填写" : mail);
	}

	private void parsePersonInformation(String content) {
		try {
			JSONObject json = new JSONObject(content);
			person = Person.create(json);
			refreshUI(person);
			new PersonInforSaveThread(userId, content).start();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private class PersonInforSaveThread extends Thread {
		private String content;
		private String key;

		public PersonInforSaveThread(String key, String content) {
			this.key = key;
			this.content = content;
		}

		@Override
		public void run() {
			if (TextUtils.isEmpty(userId) || TextUtils.isEmpty(content))
				return;
			ACache.get(mActivity, Client.getInstance().getUser().getUserId()).put(key, content);
		}
	}

}
