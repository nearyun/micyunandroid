package com.micyun.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.conference.MainActivity;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 重置密码
 * 
 * @author xiaohua
 * 
 */
public class ModifyPasswordActivity extends BaseActivity implements TextWatcher {
	private static final String KEY_USER_ID = "key_user_id";

	private EditText oldPasswordEditText;
	private EditText newPasswordEditText;
	private EditText newPasswordEditTextAgain;
	private Button confirmButton;

	private String userId = null;

	public static final void newInstance(Activity context, String userId, int requestCode) {
		Intent intent = new Intent(context, ModifyPasswordActivity.class);
		intent.putExtra(KEY_USER_ID, userId);
		context.startActivityForResult(intent, requestCode);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_password);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		userId = getIntent().getStringExtra(KEY_USER_ID);

		oldPasswordEditText = (EditText) findViewById(R.id.old_pwd_edittext);
		oldPasswordEditText.addTextChangedListener(this);

		newPasswordEditText = (EditText) findViewById(R.id.new_pwd_edittext);
		newPasswordEditText.addTextChangedListener(this);

		newPasswordEditTextAgain = (EditText) findViewById(R.id.new_pwd_edittext_again);
		newPasswordEditTextAgain.addTextChangedListener(this);

		confirmButton = (Button) findViewById(R.id.confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				doConfirm();
			}
		});
	}

	private void doConfirm() {

		if (!validate() || !isNetworkAvailable()) {
			return;
		}

		Client.getInstance().modifyPassword(userId, oldPassword, newPassword, new OnCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				showToast(message);
			}

			@Override
			public void onSuccess() {
				showToast("修改成功");
				setResult(RESULT_OK);
				finish();
			}
		});
	}

	private String oldPassword = "";
	private String newPassword = "";

	private boolean validate() {
		oldPassword = oldPasswordEditText.getText().toString();
		if (TextUtils.isEmpty(oldPassword)) {
			showToast("旧密码不可为空");
			return false;
		}
		newPassword = newPasswordEditText.getText().toString();
		if (TextUtils.isEmpty(newPassword)) {
			showToast("新密码不可为空");
			return false;
		}
		String tmpPwd = newPasswordEditTextAgain.getText().toString();
		if (TextUtils.isEmpty(tmpPwd)) {
			showToast("二次密码不可为空");
			return false;
		}
		if (!TextUtils.equals(newPassword, tmpPwd)) {
			showToast("两次新密码不一致");
			return false;
		}
		return true;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		boolean flag = oldPasswordEditText.getText().length() > 0 && newPasswordEditText.getText().length() >= 6
				&& newPasswordEditTextAgain.getText().length() >= 6;
		confirmButton.setEnabled(flag);
	}
}
