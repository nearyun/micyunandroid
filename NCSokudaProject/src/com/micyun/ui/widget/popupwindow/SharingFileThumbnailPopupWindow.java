package com.micyun.ui.widget.popupwindow;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.micyun.R;
import com.micyun.adapter.ThumbnailRecycleViewAdapter;
import com.micyun.adapter.ThumbnailRecycleViewAdapter.OnPageSelectedListener;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.ncore.callback.OnCallback;
import com.ncore.model.conference.IConferenceJsonDataManager;
import com.ncore.model.conference.OnDisplayingScreenChangedListener;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.util.LogUtil;
import com.tornado.util.DensityUtil;
import com.tornado.util.NetworkUtil;

public class SharingFileThumbnailPopupWindow extends PopupWindow implements OnPageSelectedListener {
	private static final String TAG = SharingFileThumbnailPopupWindow.class.getSimpleName();

	private Context mContext;
	private RecyclerView thumbnailRecyclerView;
	private ThumbnailRecycleViewAdapter thumbnailRecycleViewAdapter;
	private ConferenceArgument mConferenceArgument;
	private SharingFile currentSharingFile;

	private IConferenceJsonDataManager getConferenceManager() {
		return ConferenceMainRoomActivity.getIConferenceJsonDataManager();
	}

	public SharingFileThumbnailPopupWindow(Context context, ConferenceArgument argument, SharingFile sharingfile) {
		super(context);
		mContext = context;
		mConferenceArgument = argument;
		currentSharingFile = sharingfile;

		View mRootView = LayoutInflater.from(context).inflate(R.layout.popup_sharingfile_thumbnail_layout, null);
		thumbnailRecyclerView = (RecyclerView) mRootView.findViewById(R.id.thumbnailRecyclerView);
		thumbnailRecyclerView.setHasFixedSize(true);
		LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
		thumbnailRecyclerView.setLayoutManager(layoutManager);
		thumbnailRecycleViewAdapter = new ThumbnailRecycleViewAdapter(context);
		thumbnailRecyclerView.setAdapter(thumbnailRecycleViewAdapter);
		thumbnailRecycleViewAdapter.setOnPageSelectedListener(this);

		this.setContentView(mRootView);
		this.setWidth(LayoutParams.MATCH_PARENT);
		this.setHeight(DensityUtil.dp2px(162, mContext));
		this.setFocusable(true);
		this.setAnimationStyle(R.style.AnimBottom);
		ColorDrawable dw = new ColorDrawable(0xb0000000);
		this.setBackgroundDrawable(dw);
		this.setOutsideTouchable(true); // 设置点击窗口外边窗口消失

		initializeData(currentSharingFile.getDocId());
	}

	public void show(View view) {
		showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	private void initializeData(final String docId) {
		getConferenceManager().refreshDisplayingScreen(docId, new OnDisplayingScreenChangedListener() {

			@Override
			public void onEmptyViewer() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onDisplayViewer(SharingFile sharingFile, ArrayList<SharingFilePageInfo> list) {
				executeRefreshViewer(docId, list);
			}

			@Override
			public void onErrorViewer() {
				// TODO Auto-generated method stub

			}

		});

	}

	private void executeRefreshViewer(String docId, ArrayList<SharingFilePageInfo> list) {
		int curpage = currentSharingFile.getCurrentPage();
		thumbnailRecycleViewAdapter.addAllWithClear(docId, list, curpage);
		thumbnailRecyclerView.scrollToPosition(curpage - 1);
	}

	@Override
	public void onItemClick(SharingFilePageInfo page) {
		
		if (!NetworkUtil.IsNetWorkEnable(mContext)) {
			Toast.makeText(mContext, R.string.network_not_available, Toast.LENGTH_SHORT).show();
			return ;
		}

		if (!mConferenceArgument.hasControlPermission()) {
			return;
		}

		getConferenceManager().flipOverDocument(currentSharingFile.getSequenceNumber(), page.getPageNum(),
				new OnCallback() {

					@Override
					public void onSuccess() {
						dismiss();
					}

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, reason + " " + errorCode);
						Toast.makeText(mContext, "操作失败", Toast.LENGTH_SHORT).show();
					}
				});
	}
}
