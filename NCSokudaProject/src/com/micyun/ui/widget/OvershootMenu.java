package com.micyun.ui.widget;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;

import com.micyun.R;
import com.tornado.util.DensityUtil;

public class OvershootMenu extends FrameLayout implements View.OnClickListener {

	private View rootLayout;
	private Button addNewFileButton;
	private Button addNewMemberButton;
	private Button mainMenuButton;

	private int[] itemIdViews = { R.id.addNewFileView, R.id.addNewMemeberView };
	private View[] itemViews = new View[itemIdViews.length];

	private View.OnClickListener newFileListener = null;
	private View.OnClickListener newMemberListener = null;

	public void setNewFileOnClickListener(View.OnClickListener listener) {
		newFileListener = listener;
	}

	public void setNewMemberButton(View.OnClickListener listener) {
		newMemberListener = listener;
	}

	public OvershootMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.widget_overshoot_menu_layout, this);

		rootLayout = findViewById(R.id.root_layout);
		rootLayout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isExpanded)
					closeAnimation();
			}
		});

		for (int i = 0; i < itemViews.length; i++) {
			itemViews[i] = findViewById(itemIdViews[i]);
		}

		addNewFileButton = (Button) findViewById(R.id.addNewFileButton);
		addNewFileButton.setOnClickListener(this);
		addNewMemberButton = (Button) findViewById(R.id.addNewMemberButton);
		addNewMemberButton.setOnClickListener(this);
		mainMenuButton = (Button) findViewById(R.id.mainMenuButton);
		mainMenuButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isExpanded)
					startAnimation();
				else
					closeAnimation();
			}
		});
	}

	private boolean isExpanded = false;

	protected void closeAnimation() {
		if (!isExpanded)
			return;
		ObjectAnimator rotationY = ObjectAnimator.ofFloat(mainMenuButton, View.ROTATION, 315, 0);
		rotationY.setDuration(200);
		rotationY.start();

		for (int i = 0; i < itemViews.length; i++) {
			AnimatorSet set = new AnimatorSet();
			ObjectAnimator ob1 = ObjectAnimator.ofFloat(itemViews[i], View.TRANSLATION_Y,
					DensityUtil.dp2px(-64 * (i + 1), getContext()), 0);
			ob1.setDuration(200);
			ob1.setStartDelay(50 * i);
			ob1.start();

			ObjectAnimator ob2 = ObjectAnimator.ofFloat(itemViews[i], View.ALPHA, 1f, 0f);
			ob2.setDuration(200);
			ob1.setStartDelay(50 * i);
			set.play(ob1);
			set.play(ob2);
			set.start();
		}
		rootLayout.setVisibility(View.INVISIBLE);
		ObjectAnimator rlAnim = ObjectAnimator.ofFloat(rootLayout, View.ALPHA, 1f, 0f);
		rlAnim.setDuration(200);
		rlAnim.start();
		isExpanded = false;
	}

	private void startAnimation() {
		if (isExpanded)
			return;

		ObjectAnimator rotationY = ObjectAnimator.ofFloat(mainMenuButton, View.ROTATION, 0, 315);
		rotationY.setDuration(200);
		rotationY.start();

		for (int i = 0; i < itemViews.length; i++) {
			ObjectAnimator ob1 = ObjectAnimator.ofFloat(itemViews[i], View.TRANSLATION_Y, 0,
					DensityUtil.dp2px(-64 * (i + 1), getContext()));
			ob1.setInterpolator(new OvershootInterpolator());
			ob1.setDuration(200);
			ob1.setStartDelay(50 * i);
			ob1.start();

			ObjectAnimator ob2 = ObjectAnimator.ofFloat(itemViews[i], View.ALPHA, 0f, 1f);
			ob2.setDuration(200);
			ob2.setStartDelay(50 * i);
			ob2.start();
		}

		rootLayout.setVisibility(View.VISIBLE);
		ObjectAnimator rlAnim = ObjectAnimator.ofFloat(rootLayout, View.ALPHA, 0f, 1f);
		rlAnim.setDuration(200);
		rlAnim.start();

		isExpanded = true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addNewMemberButton:
			closeAnimation();
			if (newMemberListener != null)
				newMemberListener.onClick(v);
			break;
		case R.id.addNewFileButton:
			closeAnimation();
			if (newFileListener != null)
				newFileListener.onClick(v);
			break;
		default:
			break;
		}

	}
}
