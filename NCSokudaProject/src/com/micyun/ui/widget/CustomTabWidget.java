package com.micyun.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TabWidget;

public class CustomTabWidget extends TabWidget {

	public CustomTabWidget(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomTabWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomTabWidget(Context context) {
		super(context);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// do nothing
	}
}
