package com.micyun.ui.widget.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;

import com.micyun.R;

public class PasswordDilaogUtil {
	public interface OnPositiveButtonClickListener {
		public void onClick(String content);
	}

	public static void showDialog(Context context, final OnPositiveButtonClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final EditText mEditText = new EditText(context);
		mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		builder.setView(mEditText);
		builder.setIcon(R.drawable.ic_launcher);
		builder.setTitle("请提供登录密码");
		builder.setCancelable(false);
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (listener != null) {
					String content = mEditText.getText().toString();
					listener.onClick(content);
				}
			}
		});
		builder.setNegativeButton("取消", null);

		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		mEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.length() > 0);
			}
		});
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
	}
}
