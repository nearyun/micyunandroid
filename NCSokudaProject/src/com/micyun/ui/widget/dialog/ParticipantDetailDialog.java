package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.util.ImageLoaderUtil;

public class ParticipantDetailDialog extends Dialog {
	private ImageView avatarImageView;
	private TextView nameTextView;
	private TextView mobileTextView;
	private TextView companyTextView;
	private TextView departmentTextView;

	private String fullAvatarPath;
	private String nickName;
	private String mobile;
	private String company;
	private String department;

	public ParticipantDetailDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public ParticipantDetailDialog(Context context, int theme) {
		super(context, theme);
	}

	public ParticipantDetailDialog setName(String name) {
		this.nickName = name;
		return this;
	}

	public ParticipantDetailDialog setAvatarPath(String path) {
		this.fullAvatarPath = path;
		return this;
	}

	public ParticipantDetailDialog setMobile(String mobile) {
		this.mobile = mobile;
		return this;
	}

	public ParticipantDetailDialog setCompany(String company) {
		this.company = company;
		return this;
	}

	public ParticipantDetailDialog setDepartment(String department) {
		this.department = department;
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_participant_dialog);

		avatarImageView = (ImageView) findViewById(R.id.avatarImageView);
		ImageLoaderUtil.refreshAvatar(fullAvatarPath, avatarImageView);

		nameTextView = (TextView) findViewById(R.id.nameTextView);
		nameTextView.setText(nickName);

		mobileTextView = (TextView) findViewById(R.id.mobileTextView);
		mobileTextView.setText(mobile);

		companyTextView = (TextView) findViewById(R.id.companyTextView);
		companyTextView.setText(TextUtils.isEmpty(company) ? "--" : company);

		departmentTextView = (TextView) findViewById(R.id.departmentTextView);
		departmentTextView.setText(TextUtils.isEmpty(department) ? "--" : department);

		View closeButton = findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

	}
}
