package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.micyun.R;

public class ConferenceInitializeDataDialog extends Dialog {
	public ConferenceInitializeDataDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public ConferenceInitializeDataDialog(Context context, int theme) {
		super(context, theme);
	}

	private TextView messageTextView;
	private TextView subMessageTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_initialize_data_layout);
		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);

		messageTextView = (TextView) findViewById(R.id.messageTextView);
		messageTextView.setText(content);
		subMessageTextView = (TextView) findViewById(R.id.subMessageTextView);
		subMessageTextView.setText(submessage);
	}

	private String content = "";

	public void setContext(String cnt) {
		content = cnt;
		if (messageTextView != null)
			messageTextView.setText(content);
	}

	private String submessage = "";

	public void setSubmessage(String message) {
		submessage = message;
		if (subMessageTextView != null)
			subMessageTextView.setText(submessage);
	}

	@Override
	public void dismiss() {
		if (this.isShowing())
			super.dismiss();
	}
}
