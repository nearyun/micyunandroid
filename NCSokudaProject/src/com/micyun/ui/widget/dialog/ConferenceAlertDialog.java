package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.micyun.R;

public class ConferenceAlertDialog extends Dialog {

	private TextView messageTextView;
	private Button closeButton;

	private String message;
	private View.OnClickListener mOnClickListener;

	public ConferenceAlertDialog setOnCloseListener(View.OnClickListener listener) {
		mOnClickListener = listener;
		return this;
	}

	public ConferenceAlertDialog setMessage(String msg) {
		message = msg;
		return this;
	}

	public ConferenceAlertDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public ConferenceAlertDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_conference_alert_dialog);

		messageTextView = (TextView) findViewById(R.id.messageTextView);
		messageTextView.setText(message);

		closeButton = (Button) findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (mOnClickListener != null)
					mOnClickListener.onClick(v);
			}
		});
	}

}
