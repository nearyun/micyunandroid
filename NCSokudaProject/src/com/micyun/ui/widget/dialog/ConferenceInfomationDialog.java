package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.micyun.R;
import com.tornado.util.TimeUtil;

/**
 * 查看会议信息
 * 
 * @author xiaohua
 * 
 */
public class ConferenceInfomationDialog extends Dialog {

	private String subject;
	private long opentime;

	private TextView subjectTextView, opentimeTextView, durationTextView;

	public ConferenceInfomationDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public ConferenceInfomationDialog(Context context, int theme) {
		super(context, theme);
	}

	public ConferenceInfomationDialog setSubject(String _subject) {
		subject = _subject;
		return this;
	}

	public ConferenceInfomationDialog setOpentime(long _opentime) {
		opentime = _opentime;
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_conference_info_dialog);

		subjectTextView = (TextView) findViewById(R.id.subjectTextView);
		subjectTextView.setText(subject);
		opentimeTextView = (TextView) findViewById(R.id.opentimeTextView);
		opentimeTextView.setText(TimeUtil.longToString(opentime * 1000, TimeUtil.FORMAT_DATE_TIME));
		durationTextView = (TextView) findViewById(R.id.durationTextView);

		View closeButton = findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

	}

	private Handler mHandler = new Handler();
	private Runnable duration = new Runnable() {
		@Override
		public void run() {
			long diff = System.currentTimeMillis() - opentime * 1000;
			durationTextView.setText(TimeUtil.second2HMS((int) (diff / 1000), true));
			mHandler.postDelayed(this, 1000);
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		mHandler.post(duration);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mHandler.removeCallbacks(duration);
	}

}
