package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.micyun.R;

public class IdentifyCodeAlertDialog extends Dialog implements View.OnClickListener {

	public IdentifyCodeAlertDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public IdentifyCodeAlertDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_identify_code_dialog);

		findViewById(R.id.wait_button).setOnClickListener(this);
		findViewById(R.id.leave_button).setOnClickListener(this);

		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);

	}

	public interface OnLeaveClickListener {
		public void onLeave();
	}

	private OnLeaveClickListener mLeaveListener = null;

	public IdentifyCodeAlertDialog setLeaveButtonClickListener(OnLeaveClickListener listener) {
		mLeaveListener = listener;
		return this;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.wait_button:
			dismiss();
			break;
		case R.id.leave_button:
			if (mLeaveListener != null)
				mLeaveListener.onLeave();
			dismiss();
			break;
		default:
			break;
		}

	}
}
