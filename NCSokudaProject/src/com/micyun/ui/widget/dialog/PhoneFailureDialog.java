//package com.micyun.ui.widget.dialog;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.TextView;
//
//import com.micyun.R;
//import com.micyun.ui.widget.dialog.CustomAlertDialog.OnPositiveClickListener;
//
//public class PhoneFailureDialog extends Dialog implements View.OnClickListener {
//
//	private String message;
//
//	public PhoneFailureDialog(Context context) {
//		this(context, R.style.CustomDialog);
//	}
//
//	public PhoneFailureDialog(Context context, int theme) {
//		super(context, theme);
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.widget_kick_dialog);
//		this.setCancelable(false);
//		this.setCanceledOnTouchOutside(false);
//
//		findViewById(R.id.confirm_button).setOnClickListener(this);
//		TextView textview = (TextView) findViewById(R.id.message_textview);
//		textview.setText(message);
//	}
//
//	public PhoneFailureDialog setMessage(String msg) {
//		message = msg;
//		return this;
//	}
//
//	private OnPositiveClickListener positiveListener = null;
//
//	public PhoneFailureDialog setPositiveButtonOnClickListener(OnPositiveClickListener listener) {
//		positiveListener = listener;
//		return this;
//	}
//
//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.confirm_button:
//			dismiss();
//			if (positiveListener != null)
//				positiveListener.onPositiveClick();
//			break;
//		default:
//			break;
//		}
//	}
//
//	@Override
//	public void dismiss() {
//		if (this.isShowing())
//			super.dismiss();
//	}
// }
