package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.micyun.R;

public class CustomAlertDialog extends Dialog implements View.OnClickListener {

	private TextView messageTextView;
	private Button positiveButton, negativeButton;

	private String message;
	private String positiveName = "确认";
	private String negaticeName = "取消";
	private View.OnClickListener mOnPositiveClickListener;
	private View.OnClickListener mOnNegativeClickListener;

	public CustomAlertDialog setMessage(String msg) {
		message = msg;
		return this;
	}

	public CustomAlertDialog setNegaticeText(String name) {
		negaticeName = name;
		return this;
	}

	public CustomAlertDialog setPositiveText(String name) {
		positiveName = name;
		return this;
	}

	public CustomAlertDialog setOnNegativeClickListener(View.OnClickListener listener) {
		mOnNegativeClickListener = listener;
		return this;
	}

	public CustomAlertDialog setOnPositiveClickListener(View.OnClickListener listener) {
		mOnPositiveClickListener = listener;
		return this;
	}

	public CustomAlertDialog(Context context) {
		this(context, R.style.CustomDialog);
	}

	public CustomAlertDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_custom_alert_dialog);

		messageTextView = (TextView) findViewById(R.id.messageTextView);
		messageTextView.setText(message);

		positiveButton = (Button) findViewById(R.id.confirm_button);
		positiveButton.setText(positiveName);
		positiveButton.setOnClickListener(this);

		negativeButton = (Button) findViewById(R.id.cancel_button);
		negativeButton.setText(negaticeName);
		negativeButton.setOnClickListener(this);

		setCancelable(false);
		setCanceledOnTouchOutside(false);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel_button:
			dismiss();
			if (mOnNegativeClickListener != null)
				mOnNegativeClickListener.onClick(v);
			break;
		case R.id.confirm_button:
			dismiss();
			if (mOnPositiveClickListener != null)
				mOnPositiveClickListener.onClick(v);
			break;
		default:
			break;
		}
	}

	@Override
	public void dismiss() {
		if (this.isShowing())
			super.dismiss();
	}

}
