//package com.micyun.ui.widget.dialog;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.micyun.R;
//
///**
// * 显示会议的二维码
// */
//public class ConferenceDetailDialog extends Dialog {
//
//	private TextView conferenceNameView;
//	private TextView conferencePwdView;
//	private ImageView qcodeImageView;
//	private Bitmap qrCodeBitmap;
//
//	public ConferenceDetailDialog(Context context) {
//		this(context, R.style.CustomDialog);
//	}
//
//	public ConferenceDetailDialog(Context context, int theme) {
//		super(context, theme);
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.widget_conference_info_dialog_layout);
//
//		conferenceNameView = (TextView) findViewById(R.id.conference_name_textview);
//		conferencePwdView = (TextView) findViewById(R.id.conference_password_textview);
//		qcodeImageView = (ImageView) findViewById(R.id.conference_qcode_imageview);
//
//		conferenceNameView.setText(confName);
//		conferencePwdView.setText(confPwd);
//		// showQcode(confName, password);
//		showQcode(conferenceId);
//	}
//
//	private String confName = "";
//	private String confPwd = "";
//	private String conferenceId = "";
//
//	public ConferenceDetailDialog setConferenceInfo(String conferenceId, String name, String pwd) {
//		confName = name;
//		confPwd = pwd;
//		this.conferenceId = conferenceId;
//		return this;
//	}
//
//	private void showQcode(String conferencdId) {
//		// try {
//		// String result = QCodeUtil.create(conferencdId);
//		// qrCodeBitmap = EncodingHandler.createQRCode(result, 300);
//		// qcodeImageView.setImageBitmap(qrCodeBitmap);
//		// } catch (WriterException e) {
//		// e.printStackTrace();
//		// }
//	}
//
//	/**
//	 * @deprecated
//	 * @param name
//	 * @param password
//	 */
//	private void showQcode(String name, String password) {
//		// try {
//		// String result = QCodeUtil.create(name, password);
//		// qrCodeBitmap = EncodingHandler.createQRCode(result, 300);
//		// qcodeImageView.setImageBitmap(qrCodeBitmap);
//		// } catch (WriterException e) {
//		// e.printStackTrace();
//		// }
//	}
//
//	@Override
//	public void dismiss() {
//		super.dismiss();
//		if (qrCodeBitmap != null && !qrCodeBitmap.isRecycled()) {
//			qrCodeBitmap.recycle();
//		}
//	}
//
//}
