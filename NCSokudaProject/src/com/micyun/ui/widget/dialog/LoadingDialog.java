package com.micyun.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.micyun.R;
import com.ncore.util.LogUtil;

public class LoadingDialog extends Dialog {
	private static final String TAG = "LoadingDialog";

	public LoadingDialog(Context context) {
		super(context, R.style.LoadingDialog);
	}

	public LoadingDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_loading_dialog_layout);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
	}

	@Override
	public void dismiss() {
		try {
			if (isShowing())
				super.dismiss();
		} catch (IllegalArgumentException e) {
			LogUtil.w(TAG, e.getMessage());
		}
	}
}
