package com.micyun.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.micyun.R;

/**
 * 模块列表的头，可显示用户头像，用户姓名
 * 
 * @author xiaohua
 * 
 */
public class ModuleListHeader extends LinearLayout {
	private ImageView avatarView;
	private TextView userNameView;

	public ModuleListHeader(Context context) {
		this(context, null);
	}

	public ModuleListHeader(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.widget_module_list_header, this);
		avatarView = (ImageView) findViewById(R.id.avatar_imageview);
		userNameView = (TextView) findViewById(R.id.username_textview);
	}

	public void setUserName(String name) {
		userNameView.setText(name);
	}

	public ImageView getAvatarImageView() {
		return avatarView;
	}

	public void setOnAvatarClickListener(View.OnClickListener l) {
		avatarView.setOnClickListener(l);
	}
}
