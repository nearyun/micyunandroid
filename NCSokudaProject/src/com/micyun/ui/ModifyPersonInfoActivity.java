package com.micyun.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.listener.OnTextChangedListener;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;

public class ModifyPersonInfoActivity extends BaseActivity {
	private static final String KEY_CONTENT = "key_content";
	private static final String KEY_FLAG = "key_flag";

	private static final int LABEL_MODIFY_COMPANY = 0;
	private static final int LABEL_MODIFY_DEPARTMENT = 1;
	private static final int LABEL_MODIFY_NICKNAME = 2;

	public static final void newInstanceForCompany(Context context, String content) {
		newInstance(context, LABEL_MODIFY_COMPANY, content);
	}

	public static final void newInstanceForDepartment(Context context, String content) {
		newInstance(context, LABEL_MODIFY_DEPARTMENT, content);
	}

	public static final void newInstanceForNickname(Context context, String content) {
		newInstance(context, LABEL_MODIFY_NICKNAME, content);
	}

	private static final void newInstance(Context context, int label, String content) {
		Intent intent = new Intent(context, ModifyPersonInfoActivity.class);
		intent.putExtra(KEY_FLAG, label);
		intent.putExtra(KEY_CONTENT, content);
		context.startActivity(intent);
	}

	private EditText informationEditText;
	private Button comfireButton;

	private int label = 0;
	private String content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_person_info);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		label = intent.getIntExtra(KEY_FLAG, -1);
		if (label < 0) {
			finish();
			return;
		}
		content = intent.getStringExtra(KEY_CONTENT);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		informationEditText = (EditText) findViewById(R.id.information_edittext);
		informationEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				comfireButton.setEnabled(s.length() > 0);
			}
		});
		comfireButton = (Button) findViewById(R.id.confirm_button);
		comfireButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String info = informationEditText.getText().toString();
				executeOnClick(info);
			}
		});

		initUI();
	}

	private void initUI() {
		informationEditText.setText(content);
		informationEditText.setSelection(content.length());
		switch (label) {
		case LABEL_MODIFY_COMPANY:
			setTitle("修改公司名");
			informationEditText.setHint("请输入公司名");
			break;
		case LABEL_MODIFY_DEPARTMENT:
			setTitle("修改部门");
			informationEditText.setHint("请输入部门");
			break;
		case LABEL_MODIFY_NICKNAME:
			setTitle("修改昵称");
			informationEditText.setHint("请输入昵称");
			break;
		default:
			finish();
			break;
		}
	}

	private void executeOnClick(String info) {
		if (TextUtils.isEmpty(info) && info.trim().length() > 0) {
			showToast("内容不可为空");
			return;
		}
		switch (label) {
		case LABEL_MODIFY_COMPANY:
			modifyCompany(info);
			break;
		case LABEL_MODIFY_DEPARTMENT:
			modifyDepartment(info);
			break;
		case LABEL_MODIFY_NICKNAME:
			modifyNickName(info);
			break;
		}
	}

	private OnCallback mOnCallback = new OnCallback() {

		@Override
		public void onSuccess() {
			showToast("修改成功");
			finish();
		}

		@Override
		public void onFailure(int errorCode, String reason) {
			LogUtil.e(TAG, errorCode + reason);
			showToast("修改失败");
		}

	};

	private void modifyNickName(String nickName) {
		Client.getInstance().modifyNickName(nickName, mOnCallback);
	}

	private void modifyDepartment(String department) {
		Client.getInstance().modifyDepartment(department, mOnCallback);
	}

	private void modifyCompany(String company) {
		Client.getInstance().modifyCompany(company, mOnCallback);
	}

}
