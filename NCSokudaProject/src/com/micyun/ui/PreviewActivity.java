package com.micyun.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.PreviewFilePageInfoViewerAdapter;
import com.micyun.listener.OnTabPageChangeListener;
import com.micyun.ui.view.OverscrollHackyViewPager;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.util.LogUtil;

/**
 * 文档预览界面
 * 
 * @author xiaohua
 * 
 */
public class PreviewActivity extends BaseActivity {
	private static final String KEY_DOC_ID = "KEY_DOC_ID";
	private static final String KEY_FILE_NAME = "KEY_FILE_NAME";
	private static final String KEY_SESSION_ID = "KEY_SESSION_ID";
	private static final String KEY_PREVIEW_ONLY = "KEY_PREVIEW_ONLY";

	/**
	 * 
	 * @param context
	 * @param sessionId
	 *            文档副本的sessionId
	 */
	public static final void newInstance(Context context, String docId, String sessionId, String fileName) {
		newInstance(context, docId, sessionId, fileName, false);
	}

	public static final void newInstanceForPreviewOnly(Context context, String docId, String sessionId, String fileName) {
		newInstance(context, docId, sessionId, fileName, true);
	}

	private static final void newInstance(Context context, String docId, String sessionId, String fileName,
			boolean preview) {
		Intent intent = new Intent(context, PreviewActivity.class);
		intent.putExtra(KEY_DOC_ID, docId);
		intent.putExtra(KEY_FILE_NAME, fileName);
		intent.putExtra(KEY_SESSION_ID, sessionId);
		intent.putExtra(KEY_PREVIEW_ONLY, preview);
		context.startActivity(intent);
	}

	private TextView fileNumPageTextView;
	private OverscrollHackyViewPager mOverscrollViewPager;
	private ViewPager mViewPager;
	private PreviewFilePageInfoViewerAdapter pagerAdapter;

	private String docId;
	private String sessionId;
	private String fileName;
	private boolean previewOnly = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preview);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		initialUI();

		fileName = intent.getStringExtra(KEY_FILE_NAME);
		setTitle(fileName);

		sessionId = intent.getStringExtra(KEY_SESSION_ID);
		docId = intent.getStringExtra(KEY_DOC_ID);

		previewOnly = intent.getBooleanExtra(KEY_PREVIEW_ONLY, true);

		if (!TextUtils.isEmpty(sessionId))
			Client.getInstance().obtainSharingDocumentAllPageData(sessionId, mOnSampleCallback);
		else
			Client.getInstance().obtainSharingDocumentAllData(docId, mOnSampleCallback);

	}

	private OnSampleCallback mOnSampleCallback = new OnSampleCallback() {
		@Override
		public void onFailure(int errorCode, String reason) {
			LogUtil.w(TAG, errorCode + " " + reason);
		}

		@Override
		public void onSuccess(String content) {
			ArrayList<SharingFilePageInfo> list = parsePageInfo(content);
			pagerAdapter.setData(docId, list);
			fileNumPageTextView.setText(1 + "/" + pagerAdapter.getCount());
		}
	};

	private void initialUI() {
		fileNumPageTextView = (TextView) findViewById(R.id.fileNumPageTextView);

		mOverscrollViewPager = (OverscrollHackyViewPager) findViewById(R.id.document_viewpager);
		mViewPager = mOverscrollViewPager.getOverscrollView();
		pagerAdapter = new PreviewFilePageInfoViewerAdapter(getFragmentManager());

		mViewPager.setAdapter(pagerAdapter);
		mViewPager.setPageMargin(8);
		mViewPager.addOnPageChangeListener(new OnTabPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				fileNumPageTextView.setText((position + 1) + "/" + pagerAdapter.getCount());
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!previewOnly && !SharingFile.isDefaultFile(docId))
			getMenuInflater().inflate(R.menu.menu_preview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_download:
			executeDownload();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private ArrayList<SharingFilePageInfo> parsePageInfo(String content) {
		ArrayList<SharingFilePageInfo> list = new ArrayList<SharingFilePageInfo>();
		try {
			JSONArray jsonArray = new JSONArray(content);
			int len = jsonArray.length();
			for (int i = 0; i < len; i++) {
				list.add(new SharingFilePageInfo(jsonArray.getJSONObject(i)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}

	private void executeDownload() {
		Client.getInstance().getDownloadUrl(sessionId, new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(reason);
			}

			@Override
			public void onSuccess(String content) {
				showToast("开始下载...");
				try {
					JSONObject json = new JSONObject(content);
					String url = json.optString("download_url");
					download(url, fileName);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void download(String url, String fileName) {
		DownloadManager manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
		DownloadManager.Request down = new DownloadManager.Request(Uri.parse(url));
		down.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
		down.setDestinationInExternalFilesDir(mActivity, null, fileName);
		down.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		manager.enqueue(down);
	}
}
