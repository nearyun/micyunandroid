package com.micyun.ui.bindingemail;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog.OnLeaveClickListener;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

public class BindingEmailForMatchIdentifyCodeActivity extends IdentifyCodeBaseActivity {
	private static final String KEY_EMAIL = "KEY_EMAIL";

	public static final void newInstance(Context context, String email) {
		Intent intent = new Intent(context, BindingEmailForMatchIdentifyCodeActivity.class);
		intent.putExtra(KEY_EMAIL, email);
		context.startActivity(intent);
	}

	private EditText verifyCodeEdittext;
	private Button confirmButton;

	private String verifyCode = "";
	private String bindingEmail = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_binding_email_for_match_identify_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		bindingEmail = getIntent().getStringExtra(KEY_EMAIL);

		initView();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		confirmButton.setEnabled(verifyCodeEdittext.getText().length() >= 4);
	}

	@Override
	protected void initView() {
		TextView informationTextview = (TextView) findViewById(R.id.information_textview);
		informationTextview.setText("验证码已通过邮件发送到" + bindingEmail + "，请前往该邮箱获取");

		verifyCodeEdittext = (EditText) findViewById(R.id.verify_code_edittext);
		verifyCodeEdittext.addTextChangedListener(this);

		confirmButton = (Button) findViewById(R.id.confirm_button);
		confirmButton.setEnabled(false);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	@Override
	protected boolean validate() {
		verifyCode = verifyCodeEdittext.getText().toString();
		if (TextUtils.isEmpty(verifyCode)) {
			showToast("验证码不可为空");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();

		Client.getInstance().bindingEmail(bindingEmail, verifyCode, new OnCallback() {

			@Override
			public void onSuccess() {
				showToast("绑定成功");
				loadingDialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(reason);
				loadingDialog.dismiss();
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		new IdentifyCodeAlertDialog(this).setLeaveButtonClickListener(new OnLeaveClickListener() {

			@Override
			public void onLeave() {
				finish();
			}
		}).show();
	}
}
