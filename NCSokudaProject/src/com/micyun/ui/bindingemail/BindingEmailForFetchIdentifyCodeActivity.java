package com.micyun.ui.bindingemail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;
import com.tornado.util.ValidateUtils;

/**
 * 获取绑定邮箱的验证码
 * 
 * @author xiaohua
 * 
 */
public class BindingEmailForFetchIdentifyCodeActivity extends IdentifyCodeBaseActivity {
	private static final String KEY_EMAIL = "KEY_EMAIL";

	public static final void newInstance(Context context) {
		newInstance(context, null);
	}

	public static final void newInstance(Context context, String email) {
		Intent intent = new Intent(context, BindingEmailForFetchIdentifyCodeActivity.class);
		intent.putExtra(KEY_EMAIL, email);
		context.startActivity(intent);
	}

	private Button nextButton;
	private EditText bindingEmailEditText = null;
	private String bindingEmail = "";
	private String currentEmail = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_binding_email_for_fetch_identify_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		currentEmail = getIntent().getStringExtra(KEY_EMAIL);

		initView();
	}

	@Override
	protected void initView() {
		bindingEmailEditText = (EditText) findViewById(R.id.binding_email_edittext);
		bindingEmailEditText.addTextChangedListener(this);

		TextView infomationTextview = (TextView) findViewById(R.id.infomation_textview);
		if (TextUtils.isEmpty(currentEmail)) {
			infomationTextview.setText("绑定邮箱地址后，下次可使用绑定的邮箱地址登录。");
		} else {
			infomationTextview.setText("更换邮箱地址后，下次可使用新的邮箱地址登录。当前邮箱地址:" + currentEmail);
		}
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setEnabled(false);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	@Override
	protected boolean validate() {
		bindingEmail = bindingEmailEditText.getText().toString();
		if (TextUtils.isEmpty(bindingEmail)) {
			showToast("邮箱地址不可为空");
			return false;
		}

		if (!ValidateUtils.checkEmail(bindingEmail)) {
			showToast("填写的邮箱地址格式不正确");
			return false;
		}

		if (TextUtils.equals(bindingEmail, currentEmail)) {
			showToast("更换的邮箱地址不能与当前的邮箱地址一致");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyEmailVerificationCode(bindingEmail, Client.getInstance().getUser().getUserId(),
				new OnCallback() {

					@Override
					public void onSuccess() {
						BindingEmailForMatchIdentifyCodeActivity.newInstance(mActivity, bindingEmail);
						dialog.dismiss();
						finish();
					}

					@Override
					public void onFailure(int errorCode, String reason) {
						showToast(reason);
						dialog.dismiss();
					}
				});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		nextButton.setEnabled(s.length() > 0 && isEmail(s.toString()));
	}
}
