package com.micyun.ui.bindingemail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.widget.dialog.PasswordDilaogUtil;
import com.micyun.ui.widget.dialog.PasswordDilaogUtil.OnPositiveButtonClickListener;
import com.ncore.model.client.impl.Client;

public class EmailBindingActivity extends BaseActivity {

	private static final String KEY_EMAIL = "key_email";

	private String email = null;

	public static final void newInstance(Context context) {
		newInstance(context, null);
	}

	public static final void newInstance(Context context, String email) {
		Intent intent = new Intent(context, EmailBindingActivity.class);
		intent.putExtra(KEY_EMAIL, email);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_email_binding);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		TextView phoneNumTextView = (TextView) findViewById(R.id.email_textview);
		Button bindButton = (Button) findViewById(R.id.change_email_btn);
		ImageView bindingImageView = (ImageView) findViewById(R.id.binding_image_view);

		bindButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog();
			}
		});

		email = getIntent().getStringExtra(KEY_EMAIL);
		if (email == null) {
			phoneNumTextView.setText("您还没绑定邮箱地址");
			bindingImageView.setImageResource(R.drawable.icon_unbinding_email);
			bindButton.setText("绑定邮箱地址");
		} else {
			phoneNumTextView.setText("您的邮箱地址\n" + email);
			bindingImageView.setImageResource(R.drawable.icon_binding_email);
			bindButton.setText("更换邮箱地址");
		}
	}

	private void showDialog() {
		PasswordDilaogUtil.showDialog(mActivity, new OnPositiveButtonClickListener() {

			@Override
			public void onClick(String content) {
				checkPassword(content);
			}
		});
	}

	private void checkPassword(String content) {
		if (TextUtils.isEmpty(content)) {
			showToast("密码不可为空");
			return;
		}

		if (!Client.getInstance().isValidatePwd(content)) {
			showToast("密码不正确");
			return;
		}

		startChangeBindingEmailActivity();
		finish();
	}

	private void startChangeBindingEmailActivity() {
		if (email == null) {
			BindingEmailForFetchIdentifyCodeActivity.newInstance(mActivity);
		} else {
			BindingEmailForFetchIdentifyCodeActivity.newInstance(mActivity, email);
		}
	}
}
