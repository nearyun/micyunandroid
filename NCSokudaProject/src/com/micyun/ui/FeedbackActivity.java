package com.micyun.ui;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.listener.OnTextChangedListener;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 帮助与反馈
 * 
 * @author xiaohua
 * 
 */
public class FeedbackActivity extends BaseActivity {

	private static final String FEED_BACK_FILE = "feedback";
	private static final String CONTENT_KEY = "content";

	private SharedPreferences preferences = null;
	private final int MAX_LENGTH = 400;

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, FeedbackActivity.class);
		context.startActivity(intent);
	}

	private EditText contentEditText;
	private TextView wordsTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		preferences = getSharedPreferences(FEED_BACK_FILE, MODE_PRIVATE);

		contentEditText = (EditText) findViewById(R.id.feedback_content_edittext);
		contentEditText.setText(preferences.getString(CONTENT_KEY, ""));
		contentEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				int len = s.length();
				refreshTextLength(len);
			}
		});
		contentEditText.setSelection(contentEditText.getText().length());
		wordsTextView = (TextView) findViewById(R.id.content_words_textview);
		refreshTextLength(contentEditText.getText().length());

	}

	private void sendFeedback(String content) {
		if (!isNetworkAvailable())
			return;

		if (TextUtils.isEmpty(content)) {
			showToast("内容不可为空");
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().feedback(Client.getInstance().getUser().getUserId(), content, 5, new OnCallback() {

			@Override
			public void onSuccess() {
				showToast("感谢您的意见与建议");
				contentEditText.setText("");
				loadingDialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(reason);
				loadingDialog.dismiss();
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		preferences.edit().putString(CONTENT_KEY, contentEditText.getText().toString()).apply();
	}

	private void refreshTextLength(int len) {
		wordsTextView.setText((MAX_LENGTH - len) + "/" + MAX_LENGTH);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_feedback, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_send:
			String content = contentEditText.getText().toString();
			sendFeedback(content);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
