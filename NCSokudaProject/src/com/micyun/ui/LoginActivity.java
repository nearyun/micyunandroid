package com.micyun.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.micyun.BaseActivity;
import com.micyun.NCApplication;
import com.micyun.R;
import com.micyun.listener.OnTextChangedListener;
import com.micyun.ui.conference.MainActivity;
import com.micyun.ui.findpwd.FindPasswordForFetchIdentifyCodeActivity;
import com.micyun.ui.register.RegisterForFetchIdentifyCodeActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 登录界面
 * 
 * @author xiaohua
 * 
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, LoginActivity.class);
		context.startActivity(intent);
	}

	private EditText userNameEditText;
	private EditText passwordEditText;
	private View loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		CheckBox beta = (CheckBox) findViewById(R.id.betaTextView);
		if (com.ncore.config.NCConstants.BETA) {
			beta.setVisibility(View.VISIBLE);
			final SharedPreferences pref = getSharedPreferences("beta.io", MODE_PRIVATE);
			beta.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					com.ncore.config.NCConstants.changePhpUrl(isChecked);
					pref.edit().putBoolean("beta", isChecked).apply();
				}
			});
			beta.setChecked(pref.getBoolean("beta", false));
		} else {
			beta.setVisibility(View.GONE);
		}

		userNameEditText = (EditText) findViewById(R.id.username_et);
		userNameEditText.addTextChangedListener(mOnTextChangedListener);

		passwordEditText = (EditText) findViewById(R.id.password_et);
		passwordEditText.addTextChangedListener(mOnTextChangedListener);

		loginButton = findViewById(R.id.login_button);
		loginButton.setOnClickListener(this);

		findViewById(R.id.fotget_password_view).setOnClickListener(this);
		findViewById(R.id.register_view).setOnClickListener(this);

		userNameEditText.setText(Client.getInstance().getUserName());
		passwordEditText.setText("");

	}

	private OnTextChangedListener mOnTextChangedListener = new OnTextChangedListener() {

		@Override
		public void afterTextChanged(Editable s) {
			boolean flag = userNameEditText.getText().length() > 0 && passwordEditText.getText().length() >= 6;
			loginButton.setEnabled(flag);
		}

	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_button:
			login();
			break;
		case R.id.register_view:
			openRegister();
			break;
		case R.id.fotget_password_view:
			openFindPassword();
			break;
		default:
			break;
		}
	}

	private void openFindPassword() {
		FindPasswordForFetchIdentifyCodeActivity.newInstance(mActivity);
	}

	private void openRegister() {
		RegisterForFetchIdentifyCodeActivity.newInstance(mActivity);
	}

	private void login() {
		final String username = userNameEditText.getText().toString();
		final String password = passwordEditText.getText().toString();

		if (TextUtils.isEmpty(username)) {
			showToast("用户名不可为空");
			return;
		}
		if (TextUtils.isEmpty(password)) {
			showToast("密码不可为空");
			return;
		}

		if (!isNetworkAvailable()) {
			return;
		}

		loginButton.setEnabled(false);

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().login(username, password, new OnCallback() {

			@Override
			public void onSuccess() {
				NCApplication.setLoginFlag(true);
				MainActivity.newInstance(mActivity);
				loadingDialog.dismiss();
				finish();
			}

			@Override
			public void onFailure(int errorCode, String message) {
				loadingDialog.dismiss();
				showToast(message);
				loginButton.setEnabled(true);
			}

		});
	}

}
