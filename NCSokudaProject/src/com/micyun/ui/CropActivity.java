package com.micyun.ui;

import java.io.File;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.view.CropImageView;
import com.micyun.util.PhotoUtil;

public class CropActivity extends BaseActivity {
	private static final String FILE_PATH_KEY = "file_path_key";
	public static final String OUT_PUT_KEY = "out_put_key";

	private final int cropWidth = 240;
	private final int cropHeight = 240;

	private CropImageView mCropImage;
	private View saveButton;
	private String mOutputFile;

	public static final void newInstance(Activity cxt, String filepath, String outputpath, int requestCode) {
		Intent intent = new Intent(cxt, CropActivity.class);
		intent.putExtra(FILE_PATH_KEY, filepath);
		intent.putExtra(OUT_PUT_KEY, outputpath);
		cxt.startActivityForResult(intent, requestCode);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			finish();
			return;
		}

		setContentView(R.layout.activity_crop);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		mCropImage = (CropImageView) findViewById(R.id.testImage);
		saveButton = findViewById(R.id.save_button);
		saveButton.setEnabled(false);
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isNetworkAvailable()) {
					return;
				}
				Bitmap bitmap = mCropImage.getCropImage();
				File file = new File(mOutputFile);
				PhotoUtil.saveBitmap(file, bitmap, 100);
				setResult(RESULT_OK);
				finish();
			}
		});

		Intent intent = getIntent();
		if (intent != null) {
			String filepath = intent.getStringExtra(FILE_PATH_KEY);
			mOutputFile = intent.getStringExtra(OUT_PUT_KEY);
			if (!TextUtils.isEmpty(filepath) && !TextUtils.isEmpty(mOutputFile)) {
				mCropImage.setBitmapResource(filepath, cropWidth, cropHeight);
				saveButton.setEnabled(true);
			}
		} else {
			finish();
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		setResult(RESULT_CANCELED);
		finish();
	}

}
