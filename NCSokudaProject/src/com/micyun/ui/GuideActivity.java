package com.micyun.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.micyun.BaseActivity;
import com.micyun.NCApplication;
import com.micyun.R;
import com.micyun.ui.conference.MainActivity;
import com.micyun.ui.fragment.GuideFragment;
import com.micyun.ui.fragment.GuideFragment.OnFinishListener;
import com.ncore.model.client.impl.Client;
import com.tornado.util.SystemUtil;

public class GuideActivity extends BaseActivity implements OnFinishListener {
	private static final String KEY_GUIDE = "guide";

	public static final void newInstance(Context cxt) {
		Intent intent = new Intent(cxt, GuideActivity.class);
		cxt.startActivity(intent);
	}

	public static final boolean shouldShowGuideActivity(Context cxt) {
		int version = SystemUtil.getAppVersionCode(cxt);
		SharedPreferences pref = cxt.getSharedPreferences(KEY_GUIDE, MODE_PRIVATE);
		return pref.getBoolean(version + "", false);
	}

	private ViewPager guideViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);

		guideViewPager = (ViewPager) findViewById(R.id.guide_viewpager);
		guideViewPager.setAdapter(new GuideViewPagerAdapter(getFragmentManager()));
	}

	@Override
	public void onBackPressed() {
		// do nothing
	}

	private class GuideViewPagerAdapter extends FragmentPagerAdapter {

		private Fragment[] mFragmentArray = { GuideFragment.newInstance(R.drawable.bg_guide_01, false),
				GuideFragment.newInstance(R.drawable.bg_guide_02, false),
				GuideFragment.newInstance(R.drawable.bg_guide_03, false),
				GuideFragment.newInstance(R.drawable.bg_guide_04, true) };

		public GuideViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentArray[position];
		}

		@Override
		public int getCount() {
			return mFragmentArray.length;
		}

	}

	@Override
	public void onFinish() {
		int version = SystemUtil.getAppVersionCode(mActivity);
		SharedPreferences pref = getSharedPreferences(KEY_GUIDE, MODE_PRIVATE);
		pref.edit().putBoolean(version + "", true).apply();
		if (Client.getInstance().hasUserData()) {
			NCApplication.setLoginFlag(true);
			MainActivity.newInstance(mActivity);
		} else
			LoginActivity.newInstance(mActivity);
		finish();
	}
}
