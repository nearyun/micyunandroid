package com.micyun.ui;

import java.io.File;
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.LocalFileMultiChoiceAdapter;
import com.micyun.model.FileManager;
import com.micyun.model.LocalFile;
import com.micyun.service.UploadService;
import com.ncore.model.client.impl.Client;

/**
 * 本地SD卡文件管理器
 * 
 * @author xiaohua
 * 
 */
public class LocalSDCardActivity extends BaseActivity {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, LocalSDCardActivity.class);
		context.startActivity(intent);
	}

	private TextView directoryPathTextView;
	private FileManager fileManager = new FileManager();
	private LocalFileMultiChoiceAdapter fileAdapter = null;
	private boolean hasItemSelected = false;
	private Button hotFileButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			finish();
			return;
		}

		setContentView(R.layout.activity_local_sdcard);

		directoryPathTextView = (TextView) findViewById(R.id.directory_path_textview);

		// 上传
		hotFileButton = (Button) findViewById(R.id.hotfile_button);
		hotFileButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isNetworkAvailable()) {
					return;
				}

				// 加入队列
				AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

					private ProgressDialog dialog = new ProgressDialog(mActivity);

					@Override
					protected void onPreExecute() {
						dialog.setMessage("正在加入上传队列……");
						dialog.show();
					}

					@Override
					protected Void doInBackground(Void... params) {
						ArrayList<LocalFile> list = fileAdapter.getSelectedItems();
						int size = list.size();
						String[] uploadArray = new String[size];
						for (int i = 0; i < size; i++) {
							LocalFile tmpFile = list.get(i);
							uploadArray[i] = tmpFile.getAbsolutePath();
						}
						UploadService.startService(mActivity, uploadArray, Client.getInstance().getUser().getUserId());
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						dialog.dismiss();
						UploadFileQueueActivity.newInstance(mActivity, Client.getInstance().getUser().getUserId());
						finish();
					}

				};

				task.execute();

			}
		});

		ListView fileListView = (ListView) findViewById(R.id.local_sd_listview);
		fileAdapter = new LocalFileMultiChoiceAdapter(mActivity);
		fileAdapter.setOnItemClickListener(new LocalFileMultiChoiceAdapter.OnItemClickListener() {

			@Override
			public void onItemClick(LocalFile file, int position) {
				if (file.isDirectory()) {
					changePath(file.getAbsolutePath());
				} else {
					refreshBottomBarUI();
				}
			}
		});
		fileListView.setAdapter(fileAdapter);

		changePath(fileManager.getRootPath());
	}

	protected void changePath(String path) {
		ArrayList<LocalFile> tmpList = fileManager.getFileList(path);
		fileAdapter.clearAndAdd(tmpList);
		fileAdapter.notifyDataSetChanged();

		ActionBar actionBar = getActionBar();
		actionBar.setTitle(fileManager.locatedInRootPath() ? "本地文件" : "上一级");
		actionBar.setDisplayHomeAsUpEnabled(true);
		directoryPathTextView.setText(fileManager.getCurrentFilePath().replace(fileManager.getRootPath(), "SD卡"));

		refreshBottomBarUI();
	}

	private void refreshBottomBarUI() {
		int count = fileAdapter.hasSelectedItemCount();
		hasItemSelected = fileAdapter.hasSelectedItemCount() > 0;

		hotFileButton.setText(hasItemSelected ? "上传(" + count + ")" : "上传");
		hotFileButton.setEnabled(hasItemSelected);
	}

	@Override
	public void onBackPressed() {
		if (fileManager.locatedInRootPath())
			finish();
		changePath(new File(fileManager.getCurrentFilePath()).getParent());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
