package com.micyun.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.NetworkFileListAdapter;
import com.micyun.model.NetworkFileInformation;
import com.micyun.service.UploadService;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.multi.imageselector.MultiImageSelectorActivity;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.model.conference.IConferenceDataManager;
import com.ncore.model.sharing.NetworkFileInfo;
import com.ncore.model.sharing.SharingFile;
import com.ncore.util.LogUtil;

/**
 * 我的上传，显示所有我已经上传到服务器的文档
 * 
 * 可操控文档是否在当前会议共享
 * 
 * @author xiaohua
 * 
 */
public class MyUploadActivity extends BaseActivity implements View.OnClickListener {
	private final int REQUESTCODE_PHOTO_ALBUM = 0x100;

	private NetworkFileListAdapter fileAdapter = null;
	private SwipeRefreshLayout swipeRefreshLayout;

	public static final void newInstance(Activity context, int requestCode) {
		Intent intent = new Intent(context, MyUploadActivity.class);
		context.startActivityForResult(intent, requestCode);
	}

	private IConferenceDataManager getCurrentConference() {
		return ConferenceMainRoomActivity.getIConferenceJsonDataManager();
	}

	// private IControlUploadService mControlUploadService;

	private ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// mControlUploadService = (IControlUploadService) service;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// mControlUploadService = null;
		}

	};

	private boolean hasIntentData() {
		Intent intent = getIntent();
		if (intent == null) {
			LogUtil.d(TAG, "参数有误");
			return false;
		}

		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			finish();
			return;
		}

		if (!hasIntentData()) {
			finish();
			return;
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_my_upload);

		findViewById(R.id.picture_button).setOnClickListener(this);
		findViewById(R.id.file_manager_button).setOnClickListener(this);

		ListView fileListView = (ListView) findViewById(R.id.net_box_listview);
		View emptyView = findViewById(R.id.emptyView);
		fileListView.setEmptyView(emptyView);
		fileAdapter = new NetworkFileListAdapter(mActivity);
		fileListView.setAdapter(fileAdapter);
		fileAdapter.setOnCallback(new NetworkFileListAdapter.OnCallback() {

			@Override
			public void onShare(final NetworkFileInformation nfi) {
				if (!isNetworkAvailable())
					return;

				if (nfi.isInSharingQueue()) {
					showToast("已添加至共享列表");
				} else {
					final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
					loadingDialog.show();
					String conferenceId = getCurrentConference().getConference().getId();
					String currentUserId = Client.getInstance().getUser().getUserId();
					String nickName = Client.getInstance().getUser().getNickname();
					Client.getInstance().addDocumentToList(conferenceId, nfi.getNetworkFile(), currentUserId, nickName,
							new OnCallback() {

								@Override
								public void onSuccess() {
									nfi.setIsInSharingQueue(true);
									fileAdapter.notifyDataSetChanged();
									loadingDialog.dismiss();
								}

								@Override
								public void onFailure(int errorCode, String reason) {
									loadingDialog.dismiss();
								}
							});
				}
			}

			@Override
			public void onDelete(final NetworkFileInformation nfi) {
				if (!isNetworkAvailable())
					return;

				NetworkFileInfo netFile = nfi.getNetworkFile();
				if (netFile == null)
					return;
				Client.getInstance().deleteDocuments(netFile.getDocId(), new OnCallback() {

					@Override
					public void onSuccess() {
						fileAdapter.remove(nfi);
						fileAdapter.notifyDataSetChanged();
					}

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, reason);
					}
				});
			}

			@Override
			public void onCancelConvert(final NetworkFileInformation nfi) {
				if (!isNetworkAvailable())
					return;

				NetworkFileInfo netFile = nfi.getNetworkFile();
				if (netFile == null)
					return;
				Client.getInstance().cancelDocumentConveting(netFile.getDocId(), new OnCallback() {

					@Override
					public void onSuccess() {
						onDelete(nfi);
						fileAdapter.notifyDataSetChanged();
					}

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, reason);
					}
				});
			}
		});

		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_red_light,
				android.R.color.holo_red_light, android.R.color.holo_red_light);
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				if (isLoading)
					return;
				refreshData();
			}
		});

		refreshData();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(UploadService.ACTION_COMPLETE_UPLOAD);
		registerReceiver(uploadReceiver, filter);
		
		Intent intent = new Intent(mActivity, UploadService.class);
		bindService(intent, conn, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(uploadReceiver);
		unbindService(conn);
	}

	private BroadcastReceiver uploadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (UploadService.ACTION_COMPLETE_UPLOAD.equals(intent.getAction())) {
				refreshData();
			}

		}

	};

	private boolean isLoading = false;

	private void stopRefreshing() {
		isLoading = false;
		swipeRefreshLayout.setRefreshing(false);
	}

	/**
	 * 从网盘获取曾经上传的文档，与当前的共享队列做对比
	 */
	private void refreshData() {
		if (!isNetworkAvailable()) {
			stopRefreshing();
			return;
		}
		isLoading = true;
		swipeRefreshLayout.setRefreshing(true);
		Client.getInstance().obtainDocuments(new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				stopRefreshing();
				showToast(reason);
			}

			@Override
			public void onSuccess(String content) {
				fileAdapter.addAllWithClear(handleData(content));
				fileAdapter.notifyDataSetChanged();
				stopRefreshing();
			}
		});

	}

	private ArrayList<NetworkFileInformation> handleData(String content) {
		ArrayList<NetworkFileInformation> fileList = new ArrayList<NetworkFileInformation>();
		if (TextUtils.isEmpty(content))
			return fileList;
		try {
			JSONArray array = new JSONArray(content);
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				fileList.add(new NetworkFileInformation(new NetworkFileInfo(obj), false));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (fileList.size() > 0) {
			ArrayList<SharingFile> sharingFilrList = getCurrentConference().getSharingFiles();
			for (int i = 0; i < fileList.size(); i++) {
				NetworkFileInformation nfi = fileList.get(i);
				for (int j = 0; j < sharingFilrList.size(); j++) {
					SharingFile sf = sharingFilrList.get(j);
					NetworkFileInfo netFile = nfi.getNetworkFile();
					if (netFile.getDocId().equals(sf.getDocId())) {
						nfi.setIsInSharingQueue(true);
						break;
					}
				}
			}
		}
		return fileList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.my_upload_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_upload_queue:
			UploadFileQueueActivity.newInstance(mActivity, Client.getInstance().getUser().getUserId());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case REQUESTCODE_PHOTO_ALBUM:
			ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
			if (mSelectPath != null && mSelectPath.size() > 0) {
				String[] uploadArray = { mSelectPath.get(0) };
				UploadService.startService(mActivity, uploadArray, Client.getInstance().getUser().getUserId());
				UploadFileQueueActivity.newInstance(mActivity, Client.getInstance().getUser().getUserId());
			} else {
				showToast("获取照片失败");
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.picture_button:
			MultiImageSelectorActivity.newInstance(mActivity, REQUESTCODE_PHOTO_ALBUM);
			break;
		case R.id.file_manager_button:
			LocalSDCardActivity.newInstance(mActivity);
			break;
		default:
			break;
		}
	}
}
