package com.micyun.ui.bindingphone;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.VerificationType;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

/**
 * 更换手机号，发送【验证码请求】
 * 
 * @author xiaohua
 * 
 */
public class BindingPhoneForFetchIdentifyCodeActivity extends IdentifyCodeBaseActivity {
	private static final String KEY_PHONE = "KEY_PHONE";

	private Button nextButton;
	private EditText bindingPhoneEditText = null;
	private String bindingPhone = "";
	private String currentPhone = "";

	public static final void newInstance(Context context, String phone) {
		Intent intent = new Intent(context, BindingPhoneForFetchIdentifyCodeActivity.class);
		intent.putExtra(KEY_PHONE, phone);
		context.startActivity(intent);
	}

	public static final void newInstance(Context context) {
		newInstance(context, null);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_identify_code_for_binding_phone);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		currentPhone = getIntent().getStringExtra(KEY_PHONE);

		initView();
	}

	@Override
	protected void initView() {
		bindingPhoneEditText = (EditText) findViewById(R.id.binding_phone_edittext);
		bindingPhoneEditText.addTextChangedListener(this);

		TextView infomationTextview = (TextView) findViewById(R.id.infomation_textview);
		if (TextUtils.isEmpty(currentPhone)) {
			infomationTextview.setText("绑定手机号后，下次可使用绑定的手机号登录。");
		} else {
			infomationTextview.setText("更换手机号后，下次可使用新的手机号登录。当前手机号:" + currentPhone);
		}
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setEnabled(false);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	@Override
	protected boolean validate() {
		bindingPhone = bindingPhoneEditText.getText().toString();
		if (TextUtils.isEmpty(bindingPhone)) {
			showToast("手机号不可为空");
			return false;
		}

		if (TextUtils.equals(bindingPhone, currentPhone)) {
			showToast("更换的手机号不能与当前的手机号一致");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyVerificationCode(bindingPhone, VerificationType.TYPE_BINDING_PHONE, false,
				new OnSampleCallback() {

					@Override
					public void onFailure(int errorCode, String message) {
						dialog.dismiss();
						showToast(message);
					}

					@Override
					public void onSuccess(String content) {
						try {
							JSONObject json = new JSONObject(content);
							String identifier = json.optString("identifier");
							BindingPhoneForMatchIdentifyCodeActivity.newInstance(mActivity, bindingPhone, identifier);
							dialog.dismiss();
							finish();
						} catch (JSONException e) {
							e.printStackTrace();
							dialog.dismiss();
						}
					}
				});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		nextButton.setEnabled(bindingPhoneEditText.getText().length() > 0 && isMobile(s.toString()));
	}

}
