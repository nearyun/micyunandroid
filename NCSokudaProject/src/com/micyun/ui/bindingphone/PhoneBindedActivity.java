package com.micyun.ui.bindingphone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.ui.widget.dialog.PasswordDilaogUtil;
import com.micyun.ui.widget.dialog.PasswordDilaogUtil.OnPositiveButtonClickListener;
import com.ncore.model.client.impl.Client;

/**
 * 更换绑定的手机号界面
 * 
 * @author xiaohua
 * 
 */
public class PhoneBindedActivity extends BaseActivity {
	private static final String KEY_PHONE = "key_phone";

	public static final void newInstance(Context context) {
		newInstance(context, null);
	}

	public static final void newInstance(Context context, String phone) {
		Intent intent = new Intent(context, PhoneBindedActivity.class);
		intent.putExtra(KEY_PHONE, phone);
		context.startActivity(intent);
	}

	private String mPhone = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_binding);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		mPhone = getIntent().getStringExtra(KEY_PHONE);
		TextView phoneNumTextView = (TextView) findViewById(R.id.phone_number_textview);
		Button bindButton = (Button) findViewById(R.id.change_phone_btn);
		ImageView bindingImageView = (ImageView) findViewById(R.id.binding_image_view);

		bindButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog();
			}
		});

		if (mPhone == null) {
			phoneNumTextView.setText("您还没绑定手机号");
			bindingImageView.setImageResource(R.drawable.icon_unbinding_phone);
			bindButton.setText("绑定手机号");
		} else {
			phoneNumTextView.setText("您的手机号 " + mPhone);
			bindingImageView.setImageResource(R.drawable.icon_binding_phone);
			bindButton.setText("更换手机号");
		}

	}

	private void showDialog() {
		PasswordDilaogUtil.showDialog(mActivity, new OnPositiveButtonClickListener() {

			@Override
			public void onClick(String content) {
				checkPassword(content);
			}
		});
	}

	private void checkPassword(String content) {
		if (TextUtils.isEmpty(content)) {
			showToast("密码不可为空");
			return;
		}

		if (!Client.getInstance().isValidatePwd(content)) {
			showToast("密码不正确");
			return;
		}

		startChangeBindingPhoneActivity();
		finish();
	}

	private void startChangeBindingPhoneActivity() {
		if (mPhone == null)
			BindingPhoneForFetchIdentifyCodeActivity.newInstance(mActivity);
		else
			BindingPhoneForFetchIdentifyCodeActivity.newInstance(mActivity, mPhone);
	}
}
