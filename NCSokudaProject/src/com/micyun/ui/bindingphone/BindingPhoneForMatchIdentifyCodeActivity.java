package com.micyun.ui.bindingphone;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.VerificationType;
import com.micyun.ui.IdentifyCodeBaseActivity;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog;
import com.micyun.ui.widget.dialog.IdentifyCodeAlertDialog.OnLeaveClickListener;
import com.micyun.ui.widget.dialog.LoadingDialog;
import com.micyun.util.SMSContentObserver;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

/**
 * 等待接收验证码，并验证正确性
 * 
 * @author xiaohua
 * 
 */
public class BindingPhoneForMatchIdentifyCodeActivity extends IdentifyCodeBaseActivity {

	private static final String KEY_PHONE = "KEY_PHONE";
	private static final String KEY_IDENTIFY = "KEY_IDENTIFY";

	public static final void newInstance(Context context, String phone, String identify) {
		Intent intent = new Intent(context, BindingPhoneForMatchIdentifyCodeActivity.class);
		intent.putExtra(KEY_PHONE, phone);
		intent.putExtra(KEY_IDENTIFY, identify);
		context.startActivity(intent);
	}

	private EditText verifyCodeEdittext;
	private Button confirmButton;
	private Button resendButton;

	private String verifyCode = "";
	private String bindingPhone = "";
	private String identify = "";

	private int mReSendTime = 60;

	private SMSContentObserver mSMSContentObserver;
	private static final int MSG_CODE_TIMER = 0x100;
	private static final int MSG_CODE_SMS = 0x101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_binding_phone_identify_code);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		bindingPhone = getIntent().getStringExtra(KEY_PHONE);
		identify = getIntent().getStringExtra(KEY_IDENTIFY);

		initView();

		handler.sendEmptyMessage(MSG_CODE_TIMER);

		mSMSContentObserver = new SMSContentObserver(handler, mActivity, MSG_CODE_SMS);
		mSMSContentObserver.registerContentObserver();
	}

	@Override
	protected void initView() {
		TextView informationTextview = (TextView) findViewById(R.id.information_textview);
		informationTextview.setText("验证码已通过短信发送到" + bindingPhone);

		verifyCodeEdittext = (EditText) findViewById(R.id.verify_code_edittext);
		verifyCodeEdittext.addTextChangedListener(this);

		resendButton = (Button) findViewById(R.id.verify_btn_resend);
		resendButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeReapply();
			}
		});

		confirmButton = (Button) findViewById(R.id.confirm_button);
		confirmButton.setEnabled(false);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeClick();
			}
		});
	}

	private void executeReapply() {
		if (!isNetworkAvailable())
			return;
		final LoadingDialog dialog = new LoadingDialog(mActivity);
		dialog.show();
		Client.getInstance().applyVerificationCode(bindingPhone, VerificationType.TYPE_BINDING_PHONE, true,
				new OnSampleCallback() {

					@Override
					public void onFailure(int errorCode, String message) {
						dialog.dismiss();
						showToast("系统繁忙，请稍后再试:" + errorCode);
					}

					@Override
					public void onSuccess(String content) {
						try {
							JSONObject json = new JSONObject(content);
							identify = json.optString("identifier");
							handler.sendEmptyMessage(MSG_CODE_TIMER);
							dialog.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							dialog.dismiss();
						}
					}
				});
	}

	@Override
	protected boolean validate() {
		verifyCode = verifyCodeEdittext.getText().toString();
		if (TextUtils.isEmpty(verifyCode)) {
			showToast("验证码不可为空");
			return false;
		}
		return true;
	}

	@Override
	protected void executeClick() {
		if (!isNetworkAvailable()) {
			return;
		}

		if (!validate()) {
			return;
		}

		final LoadingDialog loadingDialog = new LoadingDialog(mActivity);
		loadingDialog.show();
		Client.getInstance().bindingPhoneNum(bindingPhone, verifyCode, identify, new OnCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				loadingDialog.dismiss();
				showToast(message);
			}

			@Override
			public void onSuccess() {
				loadingDialog.dismiss();
				showToast("绑定成功");
				finish();
			}
		});
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		confirmButton.setEnabled(verifyCodeEdittext.getText().length() >= 4);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == MSG_CODE_SMS) {
				if (msg.obj != null) {
					verifyCodeEdittext.setText((String) msg.obj);
					executeClick();
				}
			} else if (msg.what == MSG_CODE_TIMER) {
				if (mReSendTime > 1) {
					mReSendTime--;
					resendButton.setEnabled(false);
					resendButton.setText("重发(" + mReSendTime + ")");
					handler.sendEmptyMessageDelayed(MSG_CODE_TIMER, 1000);
				} else {
					mReSendTime = 60;
					resendButton.setEnabled(true);
					resendButton.setText("重发");
				}
			}
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSMSContentObserver.unregisterContentObserver();
		handler.removeMessages(MSG_CODE_TIMER);
		handler.removeMessages(MSG_CODE_SMS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		new IdentifyCodeAlertDialog(this).setLeaveButtonClickListener(new OnLeaveClickListener() {

			@Override
			public void onLeave() {
				finish();
			}
		}).show();
	}
}
