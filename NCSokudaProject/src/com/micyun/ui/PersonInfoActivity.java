package com.micyun.ui;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.free.circleimageview.CircleImageView;
import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.PersonInfoAdapter;
import com.micyun.model.PersonInfoItem;
import com.micyun.ui.bindingemail.EmailBindingActivity;
import com.micyun.ui.bindingphone.PhoneBindedActivity;
import com.micyun.ui.conference.MainActivity;
import com.micyun.ui.widget.dialog.CustomAlertDialog;
import com.micyun.util.ImageLoaderUtil;
import com.multi.imageselector.MultiImageSelectorActivity;
import com.multi.imageselector.utils.FileUtils;
import com.ncore.callback.OnSampleCallback;
import com.ncore.callback.OnUploadCallback;
import com.ncore.model.User;
import com.ncore.model.client.impl.Client;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tornado.util.FileIOUtil;

/**
 * 我的资料界面
 * 
 * @author xiaohua
 * 
 */
public class PersonInfoActivity extends BaseActivity {

	private final int REQUESTCODE_AVATOR = 0x100;
	private final int REQUESTCODE_AVATOR_CORP = 0x300;
	private final int REQUESTCODE_SUCCESS_MODIFY_PASSWORD = 0x200;

	private String savefilePath = null;

	private TextView balanceTextView, consumeTextView;
	private TextView nickNameTextView;
	private CircleImageView mAvatarCircleImageView;

	private ListView mPersonInfoListView;
	private PersonInfoAdapter mPersonInfoAdapter;

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, PersonInfoActivity.class);
		context.startActivity(intent);
	}

	private User mUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_person_info);

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		mUser = Client.getInstance().getUser();

		getActionBar().setDisplayHomeAsUpEnabled(true);

		balanceTextView = (TextView) findViewById(R.id.balanceTextView);
		consumeTextView = (TextView) findViewById(R.id.consumeTextView);

		mAvatarCircleImageView = (CircleImageView) findViewById(R.id.avatar_item_imageview);
		mAvatarCircleImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				savefilePath = FileUtils.createTmpFile(mActivity).getAbsolutePath();
				MultiImageSelectorActivity.newInstance(mActivity, REQUESTCODE_AVATOR);
			}
		});

		nickNameTextView = (TextView) findViewById(R.id.nickname_item_textview);
		nickNameTextView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ModifyPersonInfoActivity.newInstanceForNickname(mActivity, mUser.getNickname());
			}
		});

		mPersonInfoListView = (ListView) findViewById(R.id.personInfoListView);
		mPersonInfoAdapter = new PersonInfoAdapter(mActivity);
		mPersonInfoListView.setAdapter(mPersonInfoAdapter);
		mPersonInfoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				PersonInfoItem item = (PersonInfoItem) mPersonInfoAdapter.getItem(position);
				if (item != null)
					item.executeCallback();
			}
		});

		ImageLoaderUtil.refreshAvatar(mUser, mAvatarCircleImageView);

		Client.getInstance().getBalance(new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				balanceTextView.setText("--");
				consumeTextView.setText("--");
			}

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					balanceTextView.setText(json.optDouble("balance") + "元");
					consumeTextView.setText(json.optDouble("expense") + "元");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshUI();
	}

	private void refreshUI() {
		nickNameTextView.setText(mUser.getNickname());

		ArrayList<PersonInfoItem> dataList = new ArrayList<PersonInfoItem>();
		String company = mUser.getCompany();
		dataList.add(new PersonInfoItem("公司", TextUtils.isEmpty(company) ? "未填写" : company) {

			@Override
			public void executeCallback() {
				ModifyPersonInfoActivity.newInstanceForCompany(mActivity, mUser.getCompany());
			}

		});
		String department = mUser.getDepartment();
		dataList.add(new PersonInfoItem("部门", TextUtils.isEmpty(department) ? "未填写" : department) {

			@Override
			public void executeCallback() {
				ModifyPersonInfoActivity.newInstanceForDepartment(mActivity, mUser.getDepartment());
			}

		});
		String phone = mUser.getMobile();
		dataList.add(new PersonInfoItem("手机号", TextUtils.isEmpty(phone) ? "未绑定" : phone) {

			@Override
			public void executeCallback() {
				String phone = mUser.getMobile();
				if (TextUtils.isEmpty(phone)) {
					PhoneBindedActivity.newInstance(mActivity);
				} else {
					PhoneBindedActivity.newInstance(mActivity, phone);
				}
			}

		});
		String email = mUser.getEmail();
		dataList.add(new PersonInfoItem("邮箱地址", TextUtils.isEmpty(email) ? "未绑定" : email) {

			@Override
			public void executeCallback() {
				String email = mUser.getEmail();
				if (TextUtils.isEmpty(email)) {
					EmailBindingActivity.newInstance(mActivity);
				} else {
					EmailBindingActivity.newInstance(mActivity, email);
				}
			}

		});
		dataList.add(new PersonInfoItem("修改密码") {

			@Override
			public void executeCallback() {
				ModifyPasswordActivity.newInstance(mActivity, mUser.getUserId(), REQUESTCODE_SUCCESS_MODIFY_PASSWORD);
			}

		});

		File directory = ImageLoader.getInstance().getDiskCache().getDirectory();
		long dirSize = FileIOUtil.getDirectorySize(directory);
		String sizeFormat = FileIOUtil.getFileSizeStringFormat(dirSize);
		dataList.add(new PersonInfoItem("清空缓存", sizeFormat) {

			@Override
			public void executeCallback() {
				ImageLoader.getInstance().clearDiskCache();
				refreshUI();
			}
		});

		mPersonInfoAdapter.addAllWithClear(dataList);
		mPersonInfoAdapter.notifyDataSetChanged();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case REQUESTCODE_AVATOR:
			ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
			if (mSelectPath != null && mSelectPath.size() > 0) {
				CropActivity.newInstance(mActivity, mSelectPath.get(0), savefilePath, REQUESTCODE_AVATOR_CORP);
			} else {
				showToast("获取照片失败");
			}
			break;

		case REQUESTCODE_AVATOR_CORP:// 裁剪头像返回
			Client.getInstance().uploadAvatar(new File(savefilePath), new OnUploadCallback() {

				@Override
				public void onSuccess() {
					showToast("上传头像成功");
					ImageLoaderUtil.refreshAvatar(mUser, mAvatarCircleImageView);
				}

				@Override
				public void onFailure(int errorCode, String message) {
					showToast("上传头像失败");
				}

			});
			break;
		case REQUESTCODE_SUCCESS_MODIFY_PASSWORD:
			executeLogout();
			break;
		default:
			break;

		}
	}

	private void executeLogout() {
		Intent intent = new Intent();
		intent.setAction(MainActivity.ACTION_LOGOUT);
		sendBroadcast(intent);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_person_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_logout:
			new CustomAlertDialog(mActivity).setMessage("退出登录？").setOnPositiveClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					executeLogout();
				}
			}).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

}
