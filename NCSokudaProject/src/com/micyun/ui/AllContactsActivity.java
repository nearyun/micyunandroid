package com.micyun.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.FrequentContactsAdapter;
import com.micyun.dao.contact.FrequentDaoHelper;
import com.micyun.listener.OnTextChangedListener;
import com.micyun.ui.view.ClearEditText;
import com.ncore.model.client.impl.Client;
import com.nearyun.contact.adapter.ContactSelectedAdapter;
import com.nearyun.contact.adapter.ContactSelectedAdapter.OnRecyclerViewListener;
import com.nearyun.contact.model.Contact;
import com.nearyun.contact.model.ContactEx;
import com.nearyun.contact.ui.ContactActivity;
import com.nearyun.enterprise.ui.EnterpriseContactActivity;
import com.tornado.util.ValidateUtils;

/**
 * 联系人邀请方式界面
 * 
 * @author xiaohua
 * 
 */
public class AllContactsActivity extends BaseActivity implements View.OnClickListener {

	public static final String KEY_DATA = "KEY_DATA";
	private final int REQUEST_CODE_LOCAL_CONTACT = 0X100;
	private final int REQUEST_CODE_ENTERPRISE_CONTACT = 0X101;

	public static final void newInstance(Activity activity, int requestCode) {
		Intent intent = new Intent(activity, AllContactsActivity.class);
		activity.startActivityForResult(intent, requestCode);
	}

	private FrequentDaoHelper mFrequentDaoHelper = null;
	private FrequentContactsAdapter mFrequentContactsAdapter;
	private ListView frequentContactsListview;

	/** 已选中数据的存放地 */
	private final ArrayList<ContactEx> selectedData = new ArrayList<ContactEx>();
	private RecyclerView mHorizontalListView;
	private ContactSelectedAdapter mContactSelectedAdapter;
	private Button confirmButton;

	private ClearEditText searchEditText;
	private final int MAX_SELECTED = 10;
	private InputMethodManager inputMethodManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_invitation_way);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mFrequentDaoHelper = new FrequentDaoHelper(mActivity);

		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		final View extraRootView = findViewById(R.id.extraRootView);
		final TextView extraTipView = (TextView) findViewById(R.id.extraTipView);
		final Button extraButton = (Button) findViewById(R.id.extraButton);
		extraButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String phone = searchEditText.getText().toString();
				if (selectedData.size() == MAX_SELECTED) {
					showToast("一次只能邀请10个号码");
					return;
				} else {
					searchEditText.setText("");
					selectedData.add(new ContactEx(new Contact(phone, phone), true));
					refreshUI(selectedData.size());
				}
				hideSoftInput();
			}
		});
		searchEditText = (ClearEditText) findViewById(R.id.searchEditText);
		searchEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				String content = s.toString().trim();
				boolean flag = content.length() > 0;
				boolean isNumeric = ValidateUtils.isNumeric(content);
				if (flag && isNumeric) {
					extraRootView.setVisibility(View.VISIBLE);
					extraTipView.setText(content);
					boolean isMobile = content.length() > 3;
					extraButton.setEnabled(isMobile);
				} else {
					extraRootView.setVisibility(View.GONE);
				}
			}
		});

		findViewById(R.id.phoneContactsButton).setOnClickListener(this);
		findViewById(R.id.companyContactsButton).setOnClickListener(this);

		confirmButton = (Button) findViewById(R.id.confirmButton);
		confirmButton.setOnClickListener(this);

		frequentContactsListview = (ListView) findViewById(R.id.frequentContactsListview);
		View emptyView = findViewById(R.id.emptyView);
		frequentContactsListview.setEmptyView(emptyView);
		mFrequentContactsAdapter = new FrequentContactsAdapter(mActivity);
		frequentContactsListview.setAdapter(mFrequentContactsAdapter);
		frequentContactsListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ContactEx item = (ContactEx) mFrequentContactsAdapter.getItem(position);
				if (item.isSelected()) {
					item.setSelected(false);
					selectedData.remove(item);
				} else {
					item.setSelected(true);
					selectedData.add(item);
				}
				refreshUI(selectedData.size());
			}
		});

		mHorizontalListView = (RecyclerView) findViewById(R.id.horizontalRecyclerView);
		mContactSelectedAdapter = new ContactSelectedAdapter(mActivity, selectedData);
		mHorizontalListView.setHasFixedSize(true);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
		mHorizontalListView.setLayoutManager(layoutManager);
		mContactSelectedAdapter.setOnRecyclerViewListener(new OnRecyclerViewListener() {

			@Override
			public void onItemClick(int position) {
				ContactEx item = selectedData.get(position);
				item.setSelected(false);
				selectedData.remove(item);
				refreshUI(selectedData.size());
			}
		});
		mHorizontalListView.setAdapter(mContactSelectedAdapter);

		initFrequentData();
	}

	private void hideSoftInput() {
		if (mActivity.getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (mActivity.getCurrentFocus() != null)
				inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	private void refreshUI(int size) {
		if (size == 0) {
			confirmButton.setText("确定");
			confirmButton.setEnabled(false);
		} else {
			confirmButton.setText("确定(" + size + ")");
			confirmButton.setEnabled(true);
		}
		mContactSelectedAdapter.notifyDataSetChanged();
		mFrequentContactsAdapter.notifyDataSetChanged();
	}

	private void initFrequentData() {
		ArrayList<ContactEx> mData = mFrequentDaoHelper.queryAll(Client.getInstance().getUser().getUserId());
		mFrequentContactsAdapter.addAllWithClear(mData);
		mFrequentContactsAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;
		if (requestCode == REQUEST_CODE_LOCAL_CONTACT) {
			@SuppressWarnings("unchecked")
			ArrayList<ContactEx> tmpData = (ArrayList<ContactEx>) data.getSerializableExtra(ContactActivity.KEY_DATA);
			executeResultOK(tmpData);
		} else if (requestCode == REQUEST_CODE_ENTERPRISE_CONTACT) {
			@SuppressWarnings("unchecked")
			ArrayList<ContactEx> tmpData = (ArrayList<ContactEx>) data.getSerializableExtra(ContactActivity.KEY_DATA);
			executeResultOK(tmpData);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.phoneContactsButton:
			ArrayList<String> data = new ArrayList<String>();
			for (int i = 0; i < selectedData.size(); i++) {
				data.add(selectedData.get(i).getPhone());
			}
			ContactActivity.newInstance(mActivity, data, REQUEST_CODE_LOCAL_CONTACT);
			break;
		case R.id.companyContactsButton:
			ArrayList<String> data1 = new ArrayList<String>();
			for (int i = 0; i < selectedData.size(); i++) {
				data1.add(selectedData.get(i).getPhone());
			}
			EnterpriseContactActivity.newInstance(mActivity, data1, REQUEST_CODE_ENTERPRISE_CONTACT);
			break;
		case R.id.confirmButton:
			if (isNetworkAvailable())
				executeResultOK(selectedData);
			break;
		default:
			break;
		}

	}

	private void executeResultOK(ArrayList<ContactEx> data) {
		if (data != null) {
			for (int i = 0; i < data.size(); i++) {
				mFrequentDaoHelper.insert(data.get(i).getName(), data.get(i).getPhone(), Client.getInstance().getUser()
						.getUserId());
			}
			Intent intent = new Intent();
			intent.putExtra(KEY_DATA, data);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			Intent intent = new Intent();
			setResult(RESULT_CANCELED, intent);
			finish();
		}
	}

}
