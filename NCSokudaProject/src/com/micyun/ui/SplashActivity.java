package com.micyun.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.micyun.BaseActivity;
import com.micyun.NCApplication;
import com.micyun.R;
import com.micyun.ui.conference.MainActivity;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;

/**
 * 闪屏
 * 
 * 显示logo，若存在有效网络以及有效用户名时，自动登录
 * <p/>
 * 1、登录成功，跳转到主界面
 * <p/>
 * 2、登录失败，跳转到登录界面
 * 
 * @author xiaohua
 * 
 */
public class SplashActivity extends BaseActivity {
	private final int SPLASH_TIME = 1500;

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, SplashActivity.class);
		context.startActivity(intent);
	}

	public static final void newInstanceInNewTask(Context context) {
		Intent intent = new Intent(context, SplashActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		splashThread.start();

		autoLoad();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private boolean isLoading = false;
	private boolean loadSuccessfully = false;

	private void autoLoad() {
		isLoading = true;

		Client.getInstance().autoLogin(new OnCallback() {

			@Override
			public void onFailure(int errorCode, String message) {
				loadSuccessfully = false;
				isLoading = false;
			}

			@Override
			public void onSuccess() {
				loadSuccessfully = true;
				isLoading = false;
			}
		});
	}

	private Thread splashThread = new Thread() {
		@Override
		public void run() {
			try {
				int waited = 0;
				while (waited < SPLASH_TIME || isLoading) {
					sleep(100);
					waited += 100;
				}
			} catch (InterruptedException e) {
			} finally {
				shutdown();
			}
		}
	};

	private void shutdown() {
		boolean flag = GuideActivity.shouldShowGuideActivity(mActivity);
		if (!flag)
			GuideActivity.newInstance(mActivity);
		else {
			if (loadSuccessfully || Client.getInstance().hasUserData()) {
				NCApplication.setLoginFlag(true);
				MainActivity.newInstance(mActivity);
			} else
				LoginActivity.newInstance(mActivity);
		}
		finish();
	}
}
