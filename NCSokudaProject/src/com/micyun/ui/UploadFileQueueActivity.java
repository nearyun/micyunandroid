package com.micyun.ui;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.UploadFileAdapter;
import com.micyun.adapter.UploadFileAdapter.OnCancelListener;
import com.micyun.model.upload.UploadFileInfo;
import com.micyun.service.UploadService;
import com.micyun.service.UploadService.IControlUploadService;

/**
 * 上传文件队列界面
 * 
 * @author xiaohua
 * 
 */
public class UploadFileQueueActivity extends BaseActivity {
	private static final String KEY_UPLOADRE = "KEY_UPLOADRE";

	private UploadFileAdapter mUploadAdapter;
	private IControlUploadService mControlUploadService;

	private ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mControlUploadService = (IControlUploadService) service;
			refreshLisView();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mControlUploadService = null;
		}

	};

	public static final void newInstance(Context context, String userId) {
		Intent intent = new Intent(context, UploadFileQueueActivity.class);
		intent.putExtra(KEY_UPLOADRE, userId);
		context.startActivity(intent);
	}

	private String userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_file_queue);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		userId = getIntent().getStringExtra(KEY_UPLOADRE);
		if (TextUtils.isEmpty(userId)) {
			finish();
			return;
		}

		ListView uploadListView = (ListView) findViewById(R.id.upload_queue_listview);
		View emptyView = findViewById(R.id.emptyView);
		uploadListView.setEmptyView(emptyView);

		mUploadAdapter = new UploadFileAdapter(this);
		mUploadAdapter.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(UploadFileInfo file) {
				if (mControlUploadService != null)
					mControlUploadService.cancel(file.getId(), userId);
			}

			@Override
			public void onStop(UploadFileInfo file) {
				if (mControlUploadService != null)
					mControlUploadService.stop(file.getId(), userId);
			}

			@Override
			public void onRestart(UploadFileInfo file) {
				if (mControlUploadService != null)
					mControlUploadService.restart(file.getId(), userId);
			}
		});
		uploadListView.setAdapter(mUploadAdapter);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		IntentFilter filter = new IntentFilter();
		filter.addAction(UploadService.ACTION_REFRESH_UPLOAD_LIST);
		filter.addAction(UploadService.ACTION_COMPLETE_UPLOAD);
		registerReceiver(uploadReceiver, filter);

		Intent intent = new Intent(mActivity, UploadService.class);
		bindService(intent, conn, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(uploadReceiver);
		unbindService(conn);
	}

	private BroadcastReceiver uploadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, intent.getAction());
			if (UploadService.ACTION_REFRESH_UPLOAD_LIST.equals(intent.getAction())
					|| UploadService.ACTION_COMPLETE_UPLOAD.equals(intent.getAction())) {
				refreshLisView();
			}

		}

	};

	private ArrayList<UploadFileInfo> defaultList = new ArrayList<UploadFileInfo>();

	private void refreshLisView() {
		ArrayList<UploadFileInfo> fileInfo = mControlUploadService != null ? mControlUploadService
				.getAllUploadFile(userId) : defaultList;
		mUploadAdapter.refreshData(fileInfo);
	}
}
