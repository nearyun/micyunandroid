package com.micyun.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.micyun.adapter.NetboxFileAdapter;
import com.micyun.model.NetboxFile;
import com.micyun.service.UploadService;
import com.multi.imageselector.MultiImageSelectorActivity;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.model.sharing.NetworkFileInfo;
import com.ncore.util.LogUtil;

public class NetBoxActivity extends BaseActivity implements View.OnClickListener {

	private final int REQUESTCODE_PHOTO_ALBUM = 0x100;
	private static final String KEY_ADDED_DOCIDS = "KEY_ADDED_DOCIDS";
	public static final String KEY_RESULT_DATA = "KEY_RESULT_DATA";

	private View comfireLayout;
	private Button comfireButton;
	private NetboxFileAdapter fileAdapter = null;
	private SwipeRefreshLayout swipeRefreshLayout;
	private String[] docIds = {};// 存放传进来的文档唯一标识
	private final ArrayList<String> selectdFiles = new ArrayList<String>();

	public static final void newInstance(Activity context, String[] docIds, int requestCode) {
		Intent intent = new Intent(context, NetBoxActivity.class);
		intent.putExtra(KEY_ADDED_DOCIDS, docIds);
		context.startActivityForResult(intent, requestCode);
	}

	// private IControlUploadService mControlUploadService;
	private ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// mControlUploadService = (IControlUploadService) service;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// mControlUploadService = null;
		}

	};

	private boolean hasIntentData() {
		Intent intent = getIntent();
		if (intent == null) {
			LogUtil.d(TAG, "参数有误");
			return false;
		}
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			finish();
			return;
		}

		if (!hasIntentData()) {
			finish();
			return;
		}

		docIds = getIntent().getStringArrayExtra(KEY_ADDED_DOCIDS);
		docIds = (docIds == null ? new String[] {} : docIds);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_net_box);

		comfireLayout = findViewById(R.id.comfireLayout);
		comfireButton = (Button) findViewById(R.id.comfireButton);
		comfireButton.setOnClickListener(this);

		findViewById(R.id.picture_button).setOnClickListener(this);
		findViewById(R.id.file_manager_button).setOnClickListener(this);

		ListView fileListView = (ListView) findViewById(R.id.net_box_listview);
		View emptyView = findViewById(R.id.emptyView);
		fileListView.setEmptyView(emptyView);
		fileAdapter = new NetboxFileAdapter(mActivity);
		fileListView.setAdapter(fileAdapter);
		fileListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				NetboxFile file = fileAdapter.getNetboxFile(position);
				if (file.isSelected()) {
					selectdFiles.remove(file.getNetworkFileInfo().getDocId());
					file.setSelected(false);
				} else {
					selectdFiles.add(file.getNetworkFileInfo().getDocId());
					file.setSelected(true);
				}
				showComfireLayout();
				fileAdapter.notifyDataSetChanged();
			}

		});

		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_red_light,
				android.R.color.holo_red_light, android.R.color.holo_red_light);
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				if (isLoading)
					return;
				refreshData();
			}
		});

		refreshData();
	}

	protected void showComfireLayout() {
		int size = selectdFiles.size();
		if (size > 0) {
			comfireLayout.setVisibility(View.VISIBLE);
			comfireButton.setText("完成(" + size + ")");
		} else {
			comfireLayout.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		IntentFilter filter = new IntentFilter();
		filter.addAction(UploadService.ACTION_COMPLETE_UPLOAD);
		registerReceiver(uploadReceiver, filter);

		Intent intent = new Intent(mActivity, UploadService.class);
		bindService(intent, conn, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(uploadReceiver);
		unbindService(conn);
	}

	private BroadcastReceiver uploadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (UploadService.ACTION_COMPLETE_UPLOAD.equals(intent.getAction())) {
				refreshData();
			}
		}
	};

	private boolean isLoading = false;

	private void stopRefreshing() {
		isLoading = false;
		swipeRefreshLayout.setRefreshing(false);
		showComfireLayout();
	}

	/**
	 * 从网盘获取曾经上传的文档，与当前的共享队列做对比
	 */
	private void refreshData() {
		if (!isNetworkAvailable()) {
			stopRefreshing();
			return;
		}
		isLoading = true;
		swipeRefreshLayout.setRefreshing(true);
		Client.getInstance().obtainDocuments(new OnSampleCallback() {

			@Override
			public void onFailure(int errorCode, String reason) {
				stopRefreshing();
				showToast(reason);
			}

			@Override
			public void onSuccess(String content) {
				fileAdapter.addAllWithClear(handleData(content));
				fileAdapter.notifyDataSetChanged();
				stopRefreshing();
			}
		});

	}

	private ArrayList<NetboxFile> handleData(String content) {
		ArrayList<NetboxFile> fileList = new ArrayList<NetboxFile>();
		if (TextUtils.isEmpty(content))
			return fileList;
		try {
			JSONArray array = new JSONArray(content);
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				fileList.add(new NetboxFile(new NetworkFileInfo(obj)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < fileList.size(); i++) {
			NetboxFile file = fileList.get(i);
			for (int j = 0; j < docIds.length; j++) {
				if (TextUtils.equals(file.getNetworkFileInfo().getDocId(), docIds[j])) {
					file.setHasAdd(true);
					break;
				}
			}

			for (int m = 0; m < selectdFiles.size(); m++) {
				if (TextUtils.equals(file.getNetworkFileInfo().getDocId(), selectdFiles.get(m))) {
					file.setSelected(true);
					break;
				}
			}
		}

		return fileList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.my_upload_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_upload_queue:
			UploadFileQueueActivity.newInstance(mActivity, Client.getInstance().getUser().getUserId());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case REQUESTCODE_PHOTO_ALBUM:
			ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
			if (mSelectPath != null && mSelectPath.size() > 0) {
				String[] uploadArray = { mSelectPath.get(0) };
				UploadService.startService(mActivity, uploadArray, Client.getInstance().getUser().getUserId());
				UploadFileQueueActivity.newInstance(mActivity, Client.getInstance().getUser().getUserId());
			} else {
				showToast("获取照片失败");
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.picture_button:
			MultiImageSelectorActivity.newInstance(mActivity, REQUESTCODE_PHOTO_ALBUM);
			break;
		case R.id.file_manager_button:
			LocalSDCardActivity.newInstance(mActivity);
			break;
		case R.id.comfireButton:
			Intent data = new Intent();
			data.putExtra(KEY_RESULT_DATA, fileAdapter.getSelectedFile().toString());
			setResult(RESULT_OK, data);
			finish();
			break;
		default:
			break;
		}
	}
}
