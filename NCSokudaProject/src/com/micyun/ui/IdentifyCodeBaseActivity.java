package com.micyun.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextWatcher;

import com.micyun.BaseActivity;

public abstract class IdentifyCodeBaseActivity extends BaseActivity implements TextWatcher {

	protected abstract void initView();

	protected abstract boolean validate();

	protected abstract void executeClick();

	/**
	 * 手机号验证
	 * 
	 * @param str
	 * @return 验证通过返回true
	 */
	protected boolean isMobile(String text) {
		return Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$").matcher(text).matches();
	}

	/**
	 * 验证邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
}
