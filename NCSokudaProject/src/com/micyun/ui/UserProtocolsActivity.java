package com.micyun.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.micyun.BaseActivity;
import com.micyun.R;
import com.ncore.config.NCConstants;

/**
 * 用户协议
 * 
 * @author xiaohua
 * 
 */
public class UserProtocolsActivity extends BaseActivity {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, UserProtocolsActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_protocols);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		WebView mWebView = (WebView) findViewById(R.id.protocols_webview);
		mWebView.loadUrl(NCConstants.getProtocolUrl());
	}
}
