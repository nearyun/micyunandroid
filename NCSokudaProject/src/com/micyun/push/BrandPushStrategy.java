package com.micyun.push;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;

import com.xiaomi.mipush.sdk.MessageHandleService;
import com.xiaomi.mipush.sdk.PushMessageHandler;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.receivers.NetworkStatusReceiver;
import com.xiaomi.push.service.receivers.PingReceiver;

public class BrandPushStrategy {
	private static final String TAG = BrandPushStrategy.class.getSimpleName();

	// private static final String BRAND = test ? HUAWEI_BRAND :
	// android.os.Build.BRAND.toLowerCase(Locale.getDefault());

	public static void initPush(Context context, boolean debug) {
		XMClient.init(context);
		XMClient.setDebug(context, debug);
	}

	public static void updateToken(Context context, String userId) {
		XMClient.setAlisa(context, userId);
	}

	public static void clearToken(Context context, String userId) {
		XMClient.unsetAlisa(context, userId);
	}

	/**
	 * 开启/禁用小米组件
	 * 
	 * @param context
	 * @param enable
	 */
	@SuppressWarnings("unused")
	private static void setXiaomiComponentEnable(Context context, boolean enable) {
		PackageManager pm = context.getPackageManager();
		ComponentName comptName_XMPushService = new ComponentName(context, XMPushService.class);
		ComponentSetting.setComponentEnable(pm, comptName_XMPushService, enable);

		ComponentName comptName_PushMessageHandler = new ComponentName(context, PushMessageHandler.class);
		ComponentSetting.setComponentEnable(pm, comptName_PushMessageHandler, enable);

		ComponentName comptName_MessageHandleService = new ComponentName(context, MessageHandleService.class);
		ComponentSetting.setComponentEnable(pm, comptName_MessageHandleService, enable);

		ComponentName comptName_NetworkStatusReceiver = new ComponentName(context, NetworkStatusReceiver.class);
		ComponentSetting.setComponentEnable(pm, comptName_NetworkStatusReceiver, enable);

		ComponentName comptName_PingReceiver = new ComponentName(context, PingReceiver.class);
		ComponentSetting.setComponentEnable(pm, comptName_PingReceiver, enable);

		ComponentName comptName_XiaoMiPushMessageReceiver = new ComponentName(context, XiaoMiPushMessageReceiver.class);
		ComponentSetting.setComponentEnable(pm, comptName_XiaoMiPushMessageReceiver, enable);
	}
}
