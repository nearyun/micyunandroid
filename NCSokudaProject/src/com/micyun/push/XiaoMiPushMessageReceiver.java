package com.micyun.push;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.micyun.service.ExtraTaskIntentService;
import com.ncore.util.LogUtil;
import com.nearyun.sip.io.SipSharePerference;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

public class XiaoMiPushMessageReceiver extends PushMessageReceiver {
	private String TAG = XiaoMiPushMessageReceiver.class.getSimpleName();

	@Override
	public void onReceivePassThroughMessage(Context context, MiPushMessage message) {
		String mTopic = null;
		String mAlias = null;
		String mMessage = message.getContent();
		if (!TextUtils.isEmpty(message.getTopic())) {
			mTopic = message.getTopic();
		} else if (!TextUtils.isEmpty(message.getAlias())) {
			mAlias = message.getAlias();
		}

		LogUtil.d(TAG, "--> onReceivePassThroughMessage message:" + mMessage + ", topic:" + mTopic + ", alias:"
				+ mAlias + ", " + this.toString());

		SipAccount account = SipSharePerference.readSipAccount(context);
		if (account == null) {
			LogUtil.w(TAG, "account is null");
			return;
		}

		SipPhoneService mSipPhoneService = new SipPhoneService(context);
		mSipPhoneService.doStartService(account);

		ExtraTaskIntentService.startXiaomiCallback(context, mMessage);

	}

	@Override
	public void onNotificationMessageClicked(Context context, MiPushMessage message) {
		String mMessage = message.getContent();
		LogUtil.d(TAG, "--> onNotificationMessageClicked message:" + mMessage + ", topic:" + message.getTopic()
				+ ", alias:" + message.getAlias());
	}

	@Override
	public void onNotificationMessageArrived(Context context, MiPushMessage message) {
		String mTopic = null;
		String mAlias = null;
		String mMessage = message.getContent();
		if (!TextUtils.isEmpty(message.getTopic())) {
			mTopic = message.getTopic();
		} else if (!TextUtils.isEmpty(message.getAlias())) {
			mAlias = message.getAlias();
		}
		LogUtil.d(TAG, "--> onNotificationMessageArrived message:" + mMessage + ", topic:" + mTopic + ", alias:"
				+ mAlias);
	}

	@Override
	public void onCommandResult(Context context, MiPushCommandMessage message) {
		String command = message.getCommand();
		List<String> arguments = message.getCommandArguments();
		String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
		String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
		LogUtil.d(TAG, "onCommandResult command:" + command + ", cmdArg1:" + cmdArg1 + ", cmdArg2:" + cmdArg2);
		// if (MiPushClient.COMMAND_REGISTER.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mRegId = cmdArg1;
		// }
		// } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mAlias = cmdArg1;
		// }
		// } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mAlias = cmdArg1;
		// }
		// } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mTopic = cmdArg1;
		// }
		// } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mTopic = cmdArg1;
		// }
		// } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mStartTime = cmdArg1;
		// String mEndTime = cmdArg2;
		// }
		// }
	}

	@Override
	public void onReceiveRegisterResult(Context context, MiPushCommandMessage message) {
		String command = message.getCommand();
		List<String> arguments = message.getCommandArguments();
		String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
		String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
		LogUtil.d(TAG, "onReceiveRegisterResult command:" + command + ", cmdArg1:" + cmdArg1 + ", cmdArg2:" + cmdArg2);
		// if (MiPushClient.COMMAND_REGISTER.equals(command)) {
		// if (message.getResultCode() == ErrorCode.SUCCESS) {
		// String mRegId = cmdArg1;
		// }
		// }
	}

}
