package com.micyun.push;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.xiaomi.channel.commonutils.logger.LoggerInterface;
import com.xiaomi.mipush.sdk.Logger;
import com.xiaomi.mipush.sdk.MiPushClient;

public class XMClient {
	public static final String TAG = XMClient.class.getSimpleName();
	public static final String MI_APP_ID = "2882303761517355299";
	public static final String MI_APP_KEY = "5901735578299";

	private static boolean shouldInit(Context context) {
		ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));
		List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
		String mainProcessName = context.getPackageName();
		int myPid = Process.myPid();
		for (RunningAppProcessInfo info : processInfos) {
			if (info.pid == myPid && mainProcessName.equals(info.processName)) {
				return true;
			}
		}
		return false;
	}

	public static final void init(Context context) {
		if (shouldInit(context)) {
			MiPushClient.registerPush(context, MI_APP_ID, MI_APP_KEY);
		}

	}

	public static final void setAlisa(Context context, String alias) {
		if (!TextUtils.isEmpty(alias))
			MiPushClient.setAlias(context, alias, null);
	}

	public static final void unsetAlisa(Context context, String alias) {
		if (!TextUtils.isEmpty(alias))
			MiPushClient.unsetAlias(context, alias, null);
	}

	public static final void setDebug(Context context, boolean debug) {
		if (!debug) {
			Logger.disablePushFileLog(context);
			return;
		}
		// 打开Log
		Logger.setLogger(context, new LoggerInterface() {

			@Override
			public void setTag(String tag) {
				// ignore
			}

			@Override
			public void log(String content, Throwable t) {
				Log.d(TAG, content, t);
			}

			@Override
			public void log(String content) {
				Log.d(TAG, content);
			}
		});
	}

}
