package com.micyun.push;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;

public class ComponentSetting {
	private static final ActivityInfo getActivityInfo(Context context, ComponentName name) {
		final PackageManager pm = context.getPackageManager();
		try {
			return pm.getActivityInfo(name, PackageManager.GET_META_DATA);
		} catch (Exception e) {
		}
		return null;
	}

	private static final ServiceInfo getServiceInfo(Context context, ComponentName name) {
		final PackageManager pm = context.getPackageManager();
		try {
			return pm.getServiceInfo(name, PackageManager.GET_META_DATA);
		} catch (Exception e) {
		}
		return null;
	}

	private static final ActivityInfo getReceiverInfo(Context context, ComponentName name) {
		final PackageManager pm = context.getPackageManager();
		try {
			return pm.getReceiverInfo(name, PackageManager.GET_META_DATA);
		} catch (Exception e) {
		}
		return null;
	}

	public static final void setComponentEnable(PackageManager pm, ComponentName comptName, boolean enable) {
		int flag = enable ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
				: PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
		pm.setComponentEnabledSetting(comptName, flag, PackageManager.DONT_KILL_APP);
	}

}
