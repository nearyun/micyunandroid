package com.micyun;

import java.io.File;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.micyun.common.Contansts;
import com.micyun.push.BrandPushStrategy;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;
import com.nearyun.sip.service.VoipService;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.tornado.util.FileIOUtil;
import com.tornado.util.SystemUtil;
import com.tornado.util.TimeUtil;
import com.tornado.util.XHLogUtil;

public class NCApplication extends Application {
	private static final String TAG = NCApplication.class.getSimpleName();

	private final String FILE_LAST_USER_FLAG = "lastone.file";
	/** 上次登录用户标签 */
	private final String LAST_USERNAME_LABEL = "last_user";

	private static boolean isLogin = false;

	public static boolean isLogin() {
		return isLogin;
	}

	public static void setLoginFlag(boolean flag) {
		isLogin = flag;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		Client.create(this);

		initImageLoader(getApplicationContext());

		// 异常报错功能，跟踪bug 发送至服务器 /tmp
		// LogUtil.registerExceptionReporter(this);

		XHLogUtil.setLogDebug(this, true);

		initSipLog();

		BrandPushStrategy.initPush(this, false);

		// Intent intent = new Intent(this, CoreService.class);
		// startService(intent);
		LogUtil.d(TAG, "NCApplication --> currentVersionCode: " + SystemUtil.getAppVersionCode(this));

	}

	private void initSipLog() {
		String directory = getExternalCacheDir() + File.separator + Contansts.DIRECTORY_VOIP_LOG;
		String filename = Contansts.PREFIX_LOG_FILE
				+ TimeUtil.longToString(System.currentTimeMillis(), TimeUtil.FORMAT_DATE_COMPACT);
		File file = FileIOUtil.createNewFile(directory, filename);
		if (file != null) {
			VoipService.setSipLogFile(file.toString());
		} else {
			LogUtil.d(TAG, "can not pass vlog");
		}
	}

	/**
	 * This configuration tuning is custom. You can tune every option, you may
	 * tune some of them, or you can create default configuration by
	 * ImageLoaderConfiguration.createDefault(this); method.
	 * 
	 * @param context
	 */
	private void initImageLoader(Context context) {
		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(context);
		builder.threadPriority(Thread.NORM_PRIORITY - 2);
		builder.denyCacheImageMultipleSizesInMemory();
		builder.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		builder.tasksProcessingOrder(QueueProcessingType.LIFO);
		builder.diskCacheSize(100 * 1024 * 1024); // 50 Mb
		builder.memoryCacheSize(5 * 1024 * 1024);
		// if (BuildConfig.DEBUG)
		// builder.writeDebugLogs();

		ImageLoaderConfiguration config = builder.build();

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);

	}

	/**
	 * 当前用户非上一次登陆用户时，清空上一次用户的数据
	 * 
	 * @param currendUserId
	 */
	public void keepDataSafe(String currendUserId) {
		SharedPreferences preferences = getSharedPreferences(FILE_LAST_USER_FLAG, Context.MODE_PRIVATE);
		String lastUser = preferences.getString(LAST_USERNAME_LABEL, null);
		if (TextUtils.isEmpty(lastUser) || TextUtils.equals(lastUser, currendUserId))
			return;
		clearImageCache();
		preferences.edit().putString(LAST_USERNAME_LABEL, currendUserId).apply();
	}

	public void clearImageCache() {
		ImageLoader.getInstance().clearMemoryCache();
		ImageLoader.getInstance().clearDiskCache();
	}

}