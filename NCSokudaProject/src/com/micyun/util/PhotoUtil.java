package com.micyun.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.util.DisplayMetrics;

import com.ncore.util.LogUtil;

public class PhotoUtil {

	/**
	 * 回收垃圾 recycle
	 * 
	 * @throws
	 */
	public static void recycle(Bitmap bitmap) {
		// 先判断是否已经回收
		if (bitmap != null && !bitmap.isRecycled()) {
			// 回收并且置为null
			bitmap.recycle();
			bitmap = null;
		}
		System.gc();
	}
	
	/**
	  * 将彩色图转换为纯黑白二色
	  * 
	  * @param 位图
	  * @return 返回转换好的位图
	  */
	  public static Bitmap convertToBlackWhite(Bitmap bmp) {
	    int width = bmp.getWidth(); // 获取位图的宽
	    int height = bmp.getHeight(); // 获取位图的高
	    int[] pixels = new int[width * height]; // 通过位图的大小创建像素点数组

	    bmp.getPixels(pixels, 0, width, 0, 0, width, height);
	    int alpha = 0xFF << 24;
	    for (int i = 0; i < height; i++) {
	      for (int j = 0; j < width; j++) {
	        int grey = pixels[width * i + j];

	        //分离三原色
	        int red = ((grey & 0x00FF0000) >> 16);
	        int green = ((grey & 0x0000FF00) >> 8);
	        int blue = (grey & 0x000000FF);
	        
	        //转化成灰度像素
	        grey = (int) (red * 0.3 + green * 0.59 + blue * 0.11);
	        grey = alpha | (grey << 16) | (grey << 8) | grey;
	        pixels[width * i + j] = grey;
	      }
	    }
	    //新建图片
	    Bitmap newBmp = Bitmap.createBitmap(width, height, Config.RGB_565);
	    //设置图片数据
	    newBmp.setPixels(pixels, 0, width, 0, 0, width, height);
	    return newBmp;
	  }

	/**
	 * 获取指定路径图片和指定大小的缩略图
	 * 
	 * @return Bitmap
	 * @throws
	 */
	public static Bitmap decodeFileToImageThumbnail(String imagePath, int width, int height) {
		Bitmap bitmap = decodeFileToBitmap(imagePath, width, height);
		// 利用ThumbnailUtils来创建缩略图，这里要指定要缩放哪个Bitmap对象
		bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
		return bitmap;
	}

	/**
	 * 根据路径获得图片并压缩至指定大小，返回bitmap用于显示
	 * 
	 * @param filePath
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap decodeFileToBitmap(String filePath, int reqWidth, int reqHeight) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		// inJustDecodeBounds设置为true，可以不把图片读到内存中,但依然可以计算出图片的大小
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inPurgeable = true;
		InputStream in;
		try {
			in = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		try {
			Bitmap bitmap = BitmapFactory.decodeStream(in, null, options);
			// Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
			if (bitmap == null) {
				LogUtil.printf("can not decode " + filePath);
				return null;
			}
			int degree = readPictureDegree(filePath);
			Bitmap retBitmap = rotateBitmap(bitmap, degree);
			return retBitmap;
		} catch (OutOfMemoryError oom) {
			oom.printStackTrace();
			System.gc();
			return null;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	/**
	 * 根据路径获得图片并压缩至480*800，返回bitmap用于显示
	 * 
	 * @param filePath
	 * @return
	 */
	public static Bitmap decodeFileToBitmap(String filePath) {
		return decodeFileToBitmap(filePath, 720, 1280);// 480x800
	}

	/**
	 * 将Drawable转化为Bitmap
	 * 
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable == null)
			return null;
		int width = drawable.getIntrinsicWidth();
		int height = drawable.getIntrinsicHeight();
		Bitmap bitmap = Bitmap.createBitmap(width, height,
				drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
		return bitmap;
	}

	/**
	 * <b>计算图片的缩放值</b> </p> 一般手机的分辨率为 480*800 ，所以我们压缩后图片期望的宽带定为480，高度设为800，</p>
	 * 这2个值只是期望的宽度与高度，实际上压缩后的实际宽度和高度会比期望的要大。</p>
	 * 如果图片的原始高度或者宽度大于我们期望的宽度和高度，我们需要计算出缩放比例的数值。</p> 否则就不缩放。</p>
	 * heightRatio是图片原始高度与压缩后高度的倍数，</p> widthRatio是图片原始宽度与压缩后宽度的倍数。</p>
	 * inSampleSize为heightRatio与widthRatio中最小的那个，</p> inSampleSize就是缩放值。</p>
	 * inSampleSize为1表示宽度和高度不缩放，为2表示压缩后的宽度与高度为原来的1/2</p>
	 * 
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).

			long totalPixels = width * height / inSampleSize;

			// Anything more than 2x the requested pixels we'll sample down
			// further
			final long totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels > totalReqPixelsCap) {
				inSampleSize *= 2;
				totalPixels /= 2;
			}
		}
		return inSampleSize;
	}

	/**
	 * 按照bitmap的角度参数，旋转bitmap
	 * 
	 * @param bitmap
	 * @param rotate
	 * @return
	 */
	public static Bitmap rotateBitmap(Bitmap bitmap, int rotate) {
		if (bitmap == null)
			return null;

		if (rotate == 0) {
			return bitmap;
		}

		Bitmap retBitmap = null;
		Matrix matrix = new Matrix();
		matrix.postRotate(rotate);

		try {
			retBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		} catch (OutOfMemoryError e) {

		}

		if (retBitmap == null) {
			retBitmap = bitmap;
		} else if (bitmap != retBitmap) {
			bitmap.recycle();
		}
		return retBitmap;
	}

	/**
	 * 保存图片
	 * 
	 * @param file
	 * @param bitmap
	 */
	public static boolean saveBitmap(File file, Bitmap bitmap, int quality) {
		if (file == null || bitmap == null)
			return false;
		File parent = file.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}

		if (file.exists()) {
			file.delete();
		}

		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file);
			if (bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
				out.flush();
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 * 读取图片属性：旋转的角度
	 * 
	 * @param path
	 *            图片绝对路径
	 * @return degree旋转的角度
	 */

	public static int readPictureDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);
			int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;

	}

	/**
	 * 将图片变为圆角
	 * 
	 * @param bitmap
	 *            原Bitmap图片
	 * @param pixels
	 *            图片圆角的弧度(单位:像素(px))
	 * @return 带有圆角的图片(Bitmap 类型)
	 */
	public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	/**
	 * 将图片转化为圆形头像
	 * 
	 * @Title: toRoundBitmap
	 * @throws
	 */
	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height) {
			roundPx = width / 2;

			left = 0;
			top = 0;
			right = width;
			bottom = width;

			height = width;

			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;

			float clip = (width - height) / 2;

			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;

			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right, (int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top, (int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);// 设置画笔无锯齿

		canvas.drawARGB(0, 0, 0, 0); // 填充整个Canvas

		// 以下有两种方法画圆,drawRounRect和drawCircle
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);// 画圆角矩形，第一个参数为图形显示区域，第二个参数和第三个参数分别是水平圆角半径和垂直圆角半径。
		// canvas.drawCircle(roundPx, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));// 设置两张图片相交时的模式,参考http://trylovecatch.iteye.com/blog/1189452
		canvas.drawBitmap(bitmap, src, dst, paint); // 以Mode.SRC_IN模式合并bitmap和已经draw了的Circle

		return output;
	}

	/**
	 * 改变分辨率
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Bitmap resolvePhoto(Activity activity, Bitmap bitmap) {
		Bitmap newbmp = bitmap;
		// 获取屏幕分辨率
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		// 图片分辨率与屏幕分辨率
		float scale = bitmap.getWidth() / (float) dm.widthPixels;
		if (scale > 1) {
			newbmp = zoomBitmap(bitmap, bitmap.getWidth() / scale, bitmap.getHeight() / scale);
			recycle(bitmap);
		}
		return newbmp;
	}

	private static Bitmap zoomBitmap(Bitmap bitmap, float width, float height) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Matrix matrix = new Matrix();
		float scaleWidth = ((float) width / w);
		float scaleHeight = ((float) height / h);
		matrix.postScale(scaleWidth, scaleHeight);// 利用矩阵进行缩放不会造成内存溢出
		Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
		recycle(bitmap);
		return newbmp;
	}
}
