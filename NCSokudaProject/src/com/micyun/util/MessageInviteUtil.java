package com.micyun.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;

import com.ncore.util.InviteUtil;

/**
 * 短信邀请的模板生成，与解析
 * 
 * @author xiaohua
 * 
 */
public class MessageInviteUtil {

	/**
	 * 生成短信内容
	 * 
	 * @return
	 */
	public static String generateMessage(String confNo, String confPwd, String nickName) {
		String template = "点击下面链接加入我的会议 %s【会享】";
		return String.format(template, InviteUtil.generateInviteUrl(confNo, confPwd, nickName));
	}

	/**
	 * 解析邀请链接
	 * 
	 * @param uri
	 * @return 返回一个字符串数组，下标0，1，2分别对应会议编号，会议密码，参会昵称
	 */
	public static String[] parsingInvitationLink(Uri uri) {
		String[] retStringArray = null;
		String linkString = uri.toString().replace("micyun://", "");
		if (!TextUtils.isEmpty(linkString)) {
			byte[] queryBytes = Base64.decode(linkString.getBytes(), Base64.NO_WRAP);
			try {
				retStringArray = new String[3];
				JSONObject json = new JSONObject(new String(queryBytes));
				retStringArray[0] = json.optString("confno");
				retStringArray[1] = json.optString("confpwd");
				retStringArray[2] = json.optString("nickname");
				return retStringArray;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;

	}
}
