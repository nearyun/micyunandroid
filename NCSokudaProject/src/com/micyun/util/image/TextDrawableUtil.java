package com.micyun.util.image;

import com.free.drawable.TextDrawable;
import com.free.drawable.util.ColorGenerator;

public class TextDrawableUtil {
	private static final ColorGenerator generator = ColorGenerator.MATERIAL;

	/**
	 * 
	 * @param display
	 *            显示的文字
	 * @param tag
	 *            唯一标识字符串
	 * @param width
	 *            图像宽
	 * @param height
	 *            图像高
	 * @return
	 */
	public static final TextDrawable create(String display, String tag, int width, int height) {
		int color = generator.getColor(tag);
		int fontSize = width > height ? height : width;
		return TextDrawable.builder().beginConfig().width(width).height(height).fontSize(fontSize / 2).bold()
				.toUpperCase().endConfig().buildRect(display, color);
	}

	/**
	 * 默认宽高为100px
	 * 
	 * @param display
	 * @param tag
	 * @return
	 */
	public static final TextDrawable create(String display, String tag) {
		return create(display, tag, 100, 100);
	}
}
