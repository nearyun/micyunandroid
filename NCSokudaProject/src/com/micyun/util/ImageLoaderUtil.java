package com.micyun.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.free.drawable.TextDrawable;
import com.micyun.R;
import com.micyun.util.image.TextDrawableUtil;
import com.ncore.config.NCConstants;
import com.ncore.model.Participant;
import com.ncore.model.Person;
import com.ncore.model.User;
import com.ncore.util.LogUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ImageLoaderUtil {

	public static void refreshAvatar(String url, ImageView imageView) {
		if (imageView == null)
			return;
		if (TextUtils.isEmpty(url)) {
			imageView.setImageResource(R.drawable.default_avator);
			return;
		}
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.default_avator)
				.showImageOnFail(R.drawable.default_avator).cacheInMemory(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new SimpleBitmapDisplayer()).build();
		ImageLoader.getInstance().displayImage(NCConstants.PHP_BASE_URL + url, imageView, options);
	}

	public static void refreshFileThumbnail(Drawable drawable, String url, ImageView imageView) {
		if (imageView == null)
			return;

		if (TextUtils.isEmpty(url)) {
			imageView.setImageDrawable(drawable);
			return;
		}

		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(drawable)
				.showImageOnLoading(drawable).showImageOnFail(drawable).cacheInMemory(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new SimpleBitmapDisplayer()).build();
		ImageLoader.getInstance().displayImage(url, imageView, options);
	}

	public static void refreshAvatar(Person person, ImageView imageView) {
		if (person == null || imageView == null)
			return;
		TextDrawable drawable = TextDrawableUtil.create(person.getTextAvatar(), person.toString());
		String avatar = person.getAvatar();
		if (TextUtils.isEmpty(avatar)) {
			imageView.setImageDrawable(drawable);
			return;
		}
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(drawable)
				.showImageOnFail(drawable).cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).displayer(new SimpleBitmapDisplayer())
				.build();
		ImageLoader.getInstance().displayImage(person.getFullAvatar(), imageView, options);
	}

	/**
	 * 刷新头像
	 * 
	 * @param user
	 */
	public static void refreshAvatar(User user, ImageView imageView) {
		if (user == null || imageView == null)
			return;

		TextDrawable drawable = TextDrawableUtil.create(user.getTextAvatar(), user.getUserId());
		String avatar = user.getAvatar();
		if (TextUtils.isEmpty(avatar)) {
			imageView.setImageDrawable(drawable);
			return;
		}
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(drawable)
				.showImageOnFail(drawable).cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).displayer(new SimpleBitmapDisplayer())
				.build();
		ImageLoader.getInstance().displayImage(user.getFullAvatar(), imageView, options);
	}

	public static void refreshAvatar(Context context, String name, String identiy, ImageView imageView) {
		if (context == null || TextUtils.isEmpty(name) || imageView == null)
			return;
		Drawable drawable = null;
		if (!TextUtils.isEmpty(name)) {
			name = name.substring(0, 1);
			drawable = TextDrawableUtil.create(name, TextUtils.isEmpty(identiy) ? name : identiy);
		} else {
			drawable = context.getResources().getDrawable(R.drawable.default_avator);
		}
		imageView.setImageDrawable(drawable);
	}

	private static final DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
			.cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).build();

	public static void refreshAvatar(final Participant participant, final ImageView mImageView) {
		if (participant == null || mImageView == null)
			return;

		final TextDrawable textDrawable = TextDrawableUtil.create(participant.getTextAvatar(), participant.getUserId(),
				48, 48);
		mImageView.setImageDrawable(textDrawable);

		String avatar = participant.getAvatar();
		if (TextUtils.isEmpty(avatar)) {
			return;
		}

		final String avatarUrl = participant.getFullAvatar();

		ImageLoader.getInstance().displayImage(avatarUrl, mImageView, mDisplayImageOptions, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				loadDefaultAvatar(mImageView, avatarUrl, textDrawable, imageUri);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				LogUtil.e("ImageLoader", "onLoadingFailed " + failReason + " " + imageUri);
				loadDefaultAvatar(mImageView, avatarUrl, textDrawable, imageUri);
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
				// if (!participant.isOnline()) {
				// mImageView.setImageBitmap(PhotoUtil.convertToBlackWhite(bitmap));
				// }
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				LogUtil.i("ImageLoader", "onLoadingCancelled " + imageUri);
			}
		});
	}

	private static void loadDefaultAvatar(ImageView mImageView, String srcUrl, Drawable drawable, String targetUrl) {
		if (TextUtils.equals(srcUrl, targetUrl)) {
			mImageView.setImageDrawable(drawable);
		} else {
			mImageView.setImageResource(R.drawable.default_avator);
		}
	}
}
