package com.micyun.util;

import com.ncore.util.InviteUtil;

public class QCodeUtil {

	/**
	 * 生成二维码图
	 * 
	 * @param name
	 * @param password
	 * @return
	 */
	public static final String create(String confno, String confpwd) {
		return InviteUtil.generateInviteUrl(confno, confpwd);
	}

	public static final String create(String confId) {
		return InviteUtil.generateInviteUrl(confId);
	}

	/**
	 * 解析二维码图</P>
	 * 
	 * (example http://192.168.1.119:88/j?confid=343424-34134-3241-434231)
	 * 
	 * @param content
	 * @return
	 */
	public static final String resolve(String content) {
		try {
			String body = content.substring(content.indexOf("?") + 1);
			String[] mapArray = body.split("&");

			for (int i = 0; i < mapArray.length; i++) {
				String[] map = mapArray[i].split("=");
				if (map[0].equals("confid")) {
					return map[1];
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
