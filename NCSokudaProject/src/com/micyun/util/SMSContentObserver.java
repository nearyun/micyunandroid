package com.micyun.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.micyun.R;
import com.ncore.util.LogUtil;

public class SMSContentObserver extends ContentObserver {

	private final String TAG = SMSContentObserver.class.getSimpleName();

	private final String ID = "_id";// 短信序号
	private final String ADDRESS = "address";// 对应的号码
	private final String BODY = "body";// 短信内容
	private final String TYPE = "type";// 1接收，2发送
	private final String READ = "read";// 0未读，1已读
	private final String DATE = "date";// long 型
	private final String STATUS = "status";// 短信状态-1接收，0complete,64pending,128failed

	private Handler mHandler;
	private Context mContext;
	private ContentResolver mContentResolver;
	private Uri mUri = Uri.parse("content://sms/inbox");
	private int mMsgCode;

	public SMSContentObserver(Handler handler, Context context, int msgCode) {
		super(handler);
		this.mHandler = handler;
		this.mContext = context;
		this.mMsgCode = msgCode;
		mContentResolver = mContext.getContentResolver();
	}

	public final void registerContentObserver() {
		mContentResolver.registerContentObserver(Uri.parse("content://sms"), true, this);
	}

	public final void unregisterContentObserver() {
		mContentResolver.unregisterContentObserver(this);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onChange(boolean selfChange, Uri _uri) {
		LogUtil.w(TAG, _uri != null ? _uri.toString() : "uri is null");
		Pattern cp = Pattern.compile("^content://sms/([0-9]{1,}$)");
		if (!cp.matcher(_uri.toString()).matches()) {
			LogUtil.w(TAG, "不处理通知");
			return;
		}
		String[] projection = new String[] { ID, ADDRESS, BODY, TYPE, READ, DATE, STATUS };
		Cursor cursor = mContentResolver.query(mUri, projection, READ + "=? AND " + TYPE + "=?", new String[] { "0",
				"1" }, "date desc");

		if (cursor != null) {
			String captcha = null;
			if (cursor.moveToFirst()) {
				do {
					String msgBody = cursor.getString(cursor.getColumnIndex(BODY));

					if (TextUtils.isEmpty(msgBody) || !msgBody.contains(mContext.getString(R.string.sms_filter)))
						continue;

					captcha = getCaptcha(msgBody, 6);
					break;
				} while (cursor.moveToNext());
			}

			cursor.close();
			if (!TextUtils.isEmpty(captcha)) {
				Message message = Message.obtain();
				message.what = mMsgCode;
				message.obj = captcha;
				mHandler.sendMessage(message);
			}
		}
	}

	public void onChange(boolean selfChange) {
		onChange(selfChange, null);
	}

	/**
	 * 从短信字符窜提取验证码
	 * 
	 * @param body
	 *            短信内容
	 * @param YZMLENGTH
	 *            验证码的长度 一般6位或者4位
	 * @return 接取出来的验证码
	 */
	private static final String getCaptcha(String body, int YZMLENGTH) {
		// 首先([a-zA-Z0-9]{YZMLENGTH})是得到一个连续的六位数字字母组合
		// (?<![a-zA-Z0-9])负向断言 ([0-9]{YZMLENGTH})前面不能有数字
		// (?![a-zA-Z0-9])断言 ([0-9]{YZMLENGTH})后面不能有数字出现

		// 获得数字字母组合
		// Pattern p = Pattern.compile("(?<![a-zA-Z0-9])([a-zA-Z0-9]{" +
		// YZMLENGTH + "})(?![a-zA-Z0-9])");

		// 获得纯数字
		Pattern p = Pattern.compile("(?<![0-9])([0-9]{" + YZMLENGTH + "})(?![0-9])");
		Matcher m = p.matcher(body);
		if (m.find()) {
			return m.group(0);
		}
		return null;
	}

}