package com.micyun.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class GravityOrientationSensor implements SensorEventListener {
	private static final String TAG = "GravityOrientation";
	private float avgX = 0f;
	private float avgY = 0f;

	private boolean isLandscape = false;
	private boolean isPortrait = true;

	private SensorManager sensorSensorManager;
	private Sensor gravitySensor;

	private boolean isRegistered = false;

	private OnOrientationChangedListener mOnOrientationChangedListener;

	public interface OnOrientationChangedListener {
		public void onPortrait();

		public void onLandscape();
	}

	public GravityOrientationSensor(Context context) {
		sensorSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		gravitySensor = sensorSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
	}

	public void setOnOrientationChanged(OnOrientationChangedListener orientation) {
		mOnOrientationChangedListener = orientation;
	}

	public void register() {
		if (!isRegistered) {
			isRegistered = true;
			sensorSensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	public void unregister() {
		if (isRegistered) {
			isRegistered = false;
			sensorSensorManager.unregisterListener(this, gravitySensor);
		}
	}

	private float getAvg(float value, float avgEx) {
		return value * 0.25f + avgEx * 0.75f;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (Sensor.TYPE_GRAVITY != event.sensor.getType() || mOnOrientationChangedListener == null) {
			return;
		}

		float[] values = event.values;
		float ax = values[0];
		float ay = values[1];

		avgX = getAvg(ax, avgX);
		avgY = getAvg(ay, avgY);

		if (avgY > 5 && !isPortrait) {
			isPortrait = true;
			isLandscape = false;
			mOnOrientationChangedListener.onPortrait();
			Log.i(TAG, "竖屏 y:" + avgY);
		} else if (avgX > 5 && !isLandscape) {
			isPortrait = false;
			isLandscape = true;
			mOnOrientationChangedListener.onLandscape();
			Log.i(TAG, "横屏 y:" + avgX);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}
}
