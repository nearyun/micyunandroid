package com.micyun.adapter;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;

import com.micyun.ui.conference.fragment.viewer.PreviewFilePageInfoFragment;
import com.ncore.model.sharing.SharingFilePageInfo;

/**
 * 预览文档页
 * 
 * @author xiaohua
 * 
 */
public class PreviewFilePageInfoViewerAdapter extends FragmentStatePagerAdapter {
	private String lastDocId = "lastDocId";
	private boolean isSameDocument = false;
	private String docid = null;
	private ArrayList<SharingFilePageInfo> mDataList = null;

	public void setData(String _docid, ArrayList<SharingFilePageInfo> list) {
		docid = _docid;
		mDataList = list;
		isSameDocument = TextUtils.equals(lastDocId, docid);
		lastDocId = docid;
		notifyDataSetChanged();
	}

	public void reset() {
		mDataList = null;
		isSameDocument = false;
		lastDocId = "lastDocId" + System.currentTimeMillis();
		notifyDataSetChanged();
	}

	public PreviewFilePageInfoViewerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getItemPosition(Object object) {
		return isSameDocument ? PagerAdapter.POSITION_UNCHANGED : PagerAdapter.POSITION_NONE;
	}

	@Override
	public Fragment getItem(int position) {
		return PreviewFilePageInfoFragment.newInstance(mDataList.get(position));
	}

	@Override
	public int getCount() {
		return mDataList == null ? 0 : mDataList.size();
	}

}
