package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.model.LocalFile;
import com.tornado.util.FileIOUtil;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

/**
 * 本地文件多选适配器
 * 
 * @author xiaohua
 * 
 */
public class LocalFileMultiChoiceAdapter extends BaseAdapter {

	public interface OnItemClickListener {
		public void onItemClick(LocalFile file, int position);
	}

	private LayoutInflater mInflater;
	private OnItemClickListener mItemClickListener = null;
	private final ArrayList<LocalFile> mDataList = new ArrayList<LocalFile>();

	public LocalFileMultiChoiceAdapter(Context cxt) {
		mInflater = LayoutInflater.from(cxt);
	}

	public void setOnItemClickListener(OnItemClickListener listener) {
		mItemClickListener = listener;
	}

	public void addAll(ArrayList<LocalFile> list) {
		mDataList.addAll(list);
	}

	public void clear() {
		mDataList.clear();
	}

	public void clearAndAdd(ArrayList<LocalFile> list) {
		mDataList.clear();
		addAll(list);
	}

	private void setAllSelect(boolean flag) {
		int size = mDataList.size();
		LocalFile lfi;
		for (int i = 0; i < size; i++) {
			lfi = mDataList.get(i);
			if (!lfi.isDirectory())
				lfi.setSelected(flag);
		}
		notifyDataSetChanged();
	}

	public ArrayList<LocalFile> getSelectedItems() {
		LocalFile lfi;
		int size = mDataList.size();
		ArrayList<LocalFile> list = new ArrayList<LocalFile>();
		for (int i = 0; i < size; i++) {
			lfi = mDataList.get(i);
			if (!lfi.isDirectory() && lfi.isSelected())
				list.add(lfi);
		}
		return list;
	}

	public int hasSelectedItemCount() {
		int count = 0;
		int size = mDataList.size();
		LocalFile lfi;
		for (int i = 0; i < size; i++) {
			lfi = mDataList.get(i);
			if (!lfi.isDirectory() && lfi.isSelected())
				count++;
		}
		return count;
	}

	public void selectAll() {
		setAllSelect(true);
	}

	public void unselectAll() {
		setAllSelect(false);
	}

	@Override
	public int getCount() {
		return mDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_file_multichoice_layout, parent, false);
		}
		final LocalFile fInfo = mDataList.get(position);
		TextView itemFileTextView = ViewHolderUtil.get(convertView, R.id.item_file_textview);
		ImageView itemFileImage = ViewHolderUtil.get(convertView, R.id.item_icon_imageview);
		View itemAccordion = ViewHolderUtil.get(convertView, R.id.item_directory_accordion);
		final CheckBox selectdView = ViewHolderUtil.get(convertView, R.id.item_file_selected);
		TextView itemLastModifyTextView = ViewHolderUtil.get(convertView, R.id.item_last_modified_textview);
		TextView itemSizeTextView = ViewHolderUtil.get(convertView, R.id.item_size_textview);

		itemFileTextView.setText(fInfo.getName());
		itemLastModifyTextView.setText(TimeUtil.getTime(fInfo.lastModified()));

		if (fInfo.isDirectory()) {
			itemFileImage.setImageResource(R.drawable.ic_share_folder);
			itemAccordion.setVisibility(View.VISIBLE);
			selectdView.setVisibility(View.GONE);
			itemSizeTextView.setText("");
		} else {
			itemFileImage.setImageResource(FileManager.getFormatIconResource(fInfo.getExtension()));
			itemAccordion.setVisibility(View.GONE);
			selectdView.setVisibility(View.VISIBLE);

			itemSizeTextView.setText(FileIOUtil.getFileSizeStringFormat(fInfo));

			selectdView.setChecked(fInfo.isSelected());
			selectdView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					fInfo.setSelected(!fInfo.isSelected());

					if (mItemClickListener != null) {
						mItemClickListener.onItemClick(fInfo, position);
					}
				}
			});
		}

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!fInfo.isDirectory()) {
					boolean flag = fInfo.isSelected();
					fInfo.setSelected(!flag);
					selectdView.setChecked(!flag);
				}

				if (mItemClickListener != null) {
					mItemClickListener.onItemClick(fInfo, position);
				}
			}
		});

		return convertView;
	}
}
