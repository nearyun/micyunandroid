package com.micyun.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.model.NetboxFile;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.client.impl.Client;
import com.ncore.model.sharing.NetworkFileInfo;
import com.ncore.model.sharing.SharingFile;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

public class NetboxFileAdapter extends BaseListAdapter<NetboxFile> {

	public NetboxFileAdapter(Context cxt) {
		super(cxt);
	}

	public JSONArray getSelectedFile() {
		JSONArray data = new JSONArray();
		String userId = Client.getInstance().getUser().getUserId();
		String nickName = Client.getInstance().getUser().getNickname();
		for (int i = 0; i < mDataList.size(); i++) {
			final NetboxFile fInfo = mDataList.get(i);
			final NetworkFileInfo netFile = fInfo.getNetworkFileInfo();
			if (!fInfo.hasAdd() && fInfo.isSelected()) {
				// 此处只关心文档的docid，filename，thumbail
				JSONObject json = SharingFile.toJson("", netFile, userId, nickName, "", null);
				data.put(json);
			}
		}
		return data;
	}

	public NetboxFile getNetboxFile(int index) {
		return mDataList.get(index);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_netbox_file_layout, parent, false);
		}
		final NetboxFile fInfo = mDataList.get(position);
		final NetworkFileInfo netFile = fInfo.getNetworkFileInfo();
		TextView itemFileNameTextView = ViewHolderUtil.get(convertView, R.id.item_file_name_textview);
		itemFileNameTextView.setText(netFile.getName());

		ImageView itemFileIcon = ViewHolderUtil.get(convertView, R.id.item_file_icon);
		Drawable drawable = mContext.getResources().getDrawable(R.drawable.default_logo_grey);
		ImageLoaderUtil.refreshFileThumbnail(drawable, netFile.getThumbUrl(), itemFileIcon);

		TextView itemFileUploadTimeTextView = ViewHolderUtil.get(convertView, R.id.item_file_upload_time);
		itemFileUploadTimeTextView.setText(TimeUtil.getTime(netFile.getUploadTime() * 1000));

		View itemSelectedView = ViewHolderUtil.get(convertView, R.id.item_selected_view);
		View itemExistTextView = ViewHolderUtil.get(convertView, R.id.item_exist_textview);

		TextView itemFileStatusOrPagesTextView = ViewHolderUtil
				.get(convertView, R.id.item_file_status_or_page_textview);

		int state = netFile.getState();
		String text = "";
		boolean allowSelected = false;
		switch (state) {
		case NetworkFileInfo.WAIT_CONVERT:// 等待转换
			text = "等候转换";
			break;
		case NetworkFileInfo.CONVERT_SUCCESS:// 转换成功
			boolean isPicture = FileManager.isPicture(netFile.getExtension());
			text = (isPicture ? netFile.getWidth() + "x" + netFile.getHeight() : netFile.getPages() + " 页");
			allowSelected = true;
			break;
		case NetworkFileInfo.CONVERTING:// 正在转换
			text = "正在转换";
			break;
		case NetworkFileInfo.CONVERT_FAILURE:// 转换失败
			text = "转换失败";
			break;
		case NetworkFileInfo.CONVERT_TIMEOUT:// 转换超时
			text = "转换超时";
			break;
		case NetworkFileInfo.PREPROCESSING:// 预处理，office转为png过程中
			text = "预处理中";
			break;
		default:
			break;
		}
		itemFileStatusOrPagesTextView.setText(text);
		if (fInfo.hasAdd()) {
			itemExistTextView.setVisibility(View.VISIBLE);
			itemSelectedView.setVisibility(View.GONE);
		} else {
			itemExistTextView.setVisibility(View.GONE);
			itemSelectedView.setVisibility(allowSelected ? View.VISIBLE : View.GONE);
			if (fInfo.isSelected()) {
				itemSelectedView.setBackgroundResource(R.drawable.item_check_on);
			} else {
				itemSelectedView.setBackgroundResource(R.drawable.item_check_off);
			}
		}
		return convertView;
	}

	@Override
	public boolean isEnabled(int position) {
		NetboxFile fInfo = mDataList.get(position);
		NetworkFileInfo netFile = fInfo.getNetworkFileInfo();
		if (!fInfo.hasAdd() && netFile.getState() == NetworkFileInfo.CONVERT_SUCCESS)
			return true;
		return false;
	}

}
