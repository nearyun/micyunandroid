package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.MeetingGroup;
import com.micyun.model.MeetingInfo;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.conference.impl.Conference;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

public class TimelineExpandableListAdapter extends BaseExpandableListAdapter {

	private ArrayList<MeetingGroup> mGroupDataList = new ArrayList<MeetingGroup>();

	private Context mContext;
	private LayoutInflater mInflater;

	public interface OnOperationListener {
		public void onAgree(String conferenceId, String inviterId);

		public void onReject(int groupPosition, int childPosition, String conferenceId, String inviterId);

		public void onEnter(String conferenceId);

		public void onCreate();
	}

	private OnOperationListener mOnOperationListener;

	public void setOnOperationListener(OnOperationListener listener) {
		mOnOperationListener = listener;
	}

	public TimelineExpandableListAdapter(Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	public void setGroupData(ArrayList<MeetingGroup> dataList) {
		mGroupDataList = dataList;
	}

	@Override
	public int getGroupCount() {
		return mGroupDataList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mGroupDataList.get(groupPosition).size();
	}

	public void delete(int groupPosition, int childPosition) {
		mGroupDataList.get(groupPosition).delete(childPosition);
	}

	public void notifyDataSetChanged() {
		if (mGroupDataList.size() > 0) {
			MeetingGroup mg = mGroupDataList.get(0);
			int todayCount = mg.size();
			if (todayCount == 0)
				mg.add(new MeetingInfo());
		}

		super.notifyDataSetChanged();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroupDataList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mGroupDataList.get(groupPosition).getItem(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		final MeetingInfo childMeeting = (MeetingInfo) getChild(groupPosition, childPosition);
		if (childMeeting.isEmpty())
			return false;
		else
			// return childMeeting.getType() != MeetingInfo.TYPE_WAIT_COMFIRM;
			return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_timeline_group_layout, parent, false);
		MeetingGroup mg = mGroupDataList.get(groupPosition);
		TextView groupDateTextView = ViewHolderUtil.get(convertView, R.id.groupDateTextView);
		groupDateTextView.setText(mg.getTitle());
		groupDateTextView.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? R.drawable.ic_arrow_expand
				: R.drawable.ic_arrow_collect, 0, 0, 0);

		return convertView;
	}

	@Override
	public int getChildType(int groupPosition, int childPosition) {
		MeetingInfo childMeeting = (MeetingInfo) getChild(groupPosition, childPosition);
		return childMeeting.isEmpty() ? 0 : 1;
	}

	@Override
	public int getChildTypeCount() {
		return 2;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		final int type = getChildType(groupPosition, childPosition);
		if (type == 0) {
			View emptyView = mInflater.inflate(R.layout.item_timeline_chile_empty, parent, false);
			View createConf = ViewHolderUtil.get(emptyView, R.id.newConferenceButton);
			createConf.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnOperationListener != null)
						mOnOperationListener.onCreate();
				}
			});
			return emptyView;
		}

		if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_timeline_child_layout, parent, false);
		final MeetingInfo childMeeting = (MeetingInfo) getChild(groupPosition, childPosition);
		boolean waitTypeFlag = childMeeting.getType() == MeetingInfo.TYPE_WAIT_COMFIRM;

		if (!waitTypeFlag) {
			convertView.setTag(R.id.conference_id, childMeeting.getConfId());
			convertView.setTag(R.id.group_position, groupPosition);
			convertView.setTag(R.id.child_position, childPosition);
		}

		ImageView avatarImageview = ViewHolderUtil.get(convertView, R.id.avatar_imageview);
		ImageLoaderUtil.refreshAvatar(childMeeting.getAvatar(), avatarImageview);

		TextView conferenceSubejctTextView = ViewHolderUtil.get(convertView, R.id.conferenceSubejctTextView);
		conferenceSubejctTextView.setText(childMeeting.getSubject());

		TextView conferenceNameTextView = ViewHolderUtil.get(convertView, R.id.conferenceNameTextView);
		String extra = childMeeting.isMine() ? "" : " . " + childMeeting.getOwnerName();
		conferenceNameTextView.setText("No." + childMeeting.getConfNo() + extra);

		int drawable = waitTypeFlag ? R.drawable.ic_invite_tag : (childMeeting.isMine() ? 0
				: R.drawable.ic_participant_in_tag);
		ImageView conferenceJoinTagImageView = ViewHolderUtil.get(convertView, R.id.conferenceJoinTagImageView);
		conferenceJoinTagImageView.setImageResource(drawable);

		TextView conferenceOpentimeTextView = ViewHolderUtil.get(convertView, R.id.conferenceOpentimeTextView);
		conferenceOpentimeTextView.setText(TimeUtil.getHourAndMin(childMeeting.getOpentime() * 1000));

		MeetingGroup groupMeeting = (MeetingGroup) getGroup(groupPosition);
		TextView conferenceDateTextView = ViewHolderUtil.get(convertView, R.id.conferenceDateTextView);
		if (groupMeeting.isDisplayDate()) {
			String currentChiledDate = TimeUtil.longToString(childMeeting.getOpentime() * 1000, "yyyy-MM-dd");
			conferenceDateTextView.setText(currentChiledDate);
			if (childPosition == 0)
				conferenceDateTextView.setVisibility(View.VISIBLE);
			else {
				MeetingInfo lastChildMeeting = (MeetingInfo) getChild(groupPosition, childPosition - 1);
				String lastChildDate = TimeUtil.longToString(lastChildMeeting.getOpentime() * 1000, "yyyy-MM-dd");
				if (TextUtils.equals(lastChildDate, currentChiledDate))
					conferenceDateTextView.setVisibility(View.GONE);
				else
					conferenceDateTextView.setVisibility(View.VISIBLE);
			}
		} else {
			conferenceDateTextView.setVisibility(View.GONE);
		}

		TextView conferenceStateTextView = ViewHolderUtil.get(convertView, R.id.conferenceStateTextView);
		String stateString = "";
		switch (childMeeting.getState()) {
		case Conference.CONF_STATE_CREATED:
			stateString = "创建中";
			break;
		case Conference.CONF_STATE_INIT:
			stateString = "初始化";
			break;
		case Conference.CONF_STATE_STARTED:
			stateString = "进行中";
			break;
		case Conference.CONF_STATE_STOPPED:
			stateString = "已结束";
			break;
		case Conference.CONF_STATE_DESTROYED:
			stateString = "已销毁";
			break;
		default:
			break;
		}
		conferenceStateTextView.setText(stateString);

		View operationLayout = ViewHolderUtil.get(convertView, R.id.operationLayout);
		View enterConferenceLayout = ViewHolderUtil.get(convertView, R.id.enterConferenceLayout);

		int state = childMeeting.getState();
		View enterConferenceBtn = ViewHolderUtil.get(convertView, R.id.enterConferenceButton);

		View agreeView = ViewHolderUtil.get(convertView, R.id.agreeButton);
		View rejectView = ViewHolderUtil.get(convertView, R.id.rejectButton);

		enterConferenceBtn.setOnClickListener(null);
		agreeView.setOnClickListener(null);
		rejectView.setOnClickListener(null);

		if (Conference.CONF_STATE_DESTROYED == state || Conference.CONF_STATE_STOPPED == state) {
			operationLayout.setVisibility(View.GONE);
			enterConferenceLayout.setVisibility(View.GONE);
		} else if (waitTypeFlag) {
			operationLayout.setVisibility(View.VISIBLE);
			enterConferenceLayout.setVisibility(View.GONE);
			agreeView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnOperationListener != null) {
						final String conferenceId = childMeeting.getConfId();
						final String inviterId = childMeeting.getInviterId();
						mOnOperationListener.onAgree(conferenceId, inviterId);
					}
				}
			});
			rejectView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnOperationListener != null) {
						final String conferenceId = childMeeting.getConfId();
						final String inviterId = childMeeting.getInviterId();
						mOnOperationListener.onReject(groupPosition, childPosition, conferenceId, inviterId);
					}
				}
			});
		} else {
			operationLayout.setVisibility(View.GONE);
			enterConferenceLayout.setVisibility(View.VISIBLE);
			enterConferenceBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnOperationListener != null) {
						final String conferenceId = childMeeting.getConfId();
						mOnOperationListener.onEnter(conferenceId);
					}
				}
			});
		}

		return convertView;
	}
}
