package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.ui.CommonPersonDetailActivity;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.Participant;
import com.tornado.util.DensityUtil;

public class SampleMemberRecycleViewAdapter extends
		BaseRecyclerViewAdapter<SampleMemberRecycleViewAdapter.ItemViewHolder, Participant> {

	public interface OnClickMoreListener {
		public void onClickMore();
	}

	private OnClickMoreListener mOnClickMoreListener;

	public void setOnClickMoreListener(OnClickMoreListener listener) {
		mOnClickMoreListener = listener;
	}

	private RecyclerView membersRecyclerView;

	public SampleMemberRecycleViewAdapter(Context context, RecyclerView rcview) {
		super(context, new ArrayList<Participant>());
		membersRecyclerView = rcview;
	}

	public void notifyDataSetChangedToResize() {
		super.notifyDataSetChanged();
		int count = getItemCount();
		membersRecyclerView.getLayoutParams().height = DensityUtil.dp2px(76, mContext) * (count > 4 ? 2 : 1);
	}

	public void refreshData(ArrayList<Participant> data) {
		mDataList.clear();
		mDataList.addAll(data);
		notifyDataSetChanged();
	}

	protected class ItemViewHolder extends BaseRecycleViewHolder {
		public View rootView;
		public Participant mParticipant;
		public ImageView avatarView;
		public TextView nameTextView;

		public ItemViewHolder(View view) {
			super(view);
			rootView = view;
			avatarView = (ImageView) view.findViewById(R.id.avatarImageView);
			nameTextView = (TextView) view.findViewById(R.id.nameTextView);

		}

		@Override
		public void onClick(View v) {
			CommonPersonDetailActivity.newInstance(mContext, mParticipant.getUserId());
		}

	}

	@Override
	public void onBindViewHolder(ItemViewHolder viewholder, int position) {
		ItemViewHolder itemHolder = (ItemViewHolder) viewholder;

		if (position == 7 && mDataList.size() > 8) {
			itemHolder.nameTextView.setText("更多");
			itemHolder.avatarView.setImageResource(R.drawable.ic_circle_more_detail);
			itemHolder.rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnClickMoreListener != null)
						mOnClickMoreListener.onClickMore();
				}
			});
		} else {
			Participant participant = mDataList.get(position);
			itemHolder.mParticipant = participant;
			itemHolder.nameTextView.setText(participant.getNickName());
			itemHolder.rootView.setOnClickListener(itemHolder);
			ImageLoaderUtil.refreshAvatar(participant, itemHolder.avatarView);
		}
	}

	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_rcv_sample_member, parent, false);
		return new ItemViewHolder(view);

	}

	@Override
	public int getItemCount() {
		int count = super.getItemCount();
		return count > 8 ? 8 : count;
	}

}
