package com.micyun.adapter;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;

import com.micyun.ui.conference.fragment.viewer.FilePageInfoFragment;
import com.ncore.model.sharing.SharingFilePageInfo;

/**
 * 文档每页的数据适配器
 * 
 * @author xiaohua
 * 
 */
public class FilePageInfoViewerAdapter extends FragmentStatePagerAdapter {
	private String lastDocId = "lastDocId";
	private boolean isSameDocument = false;
	private String docid = null;
	private ArrayList<SharingFilePageInfo> mDataList = null;

	public void setData(String _docid, ArrayList<SharingFilePageInfo> list) {
		docid = _docid;
		mDataList = list;
		isSameDocument = TextUtils.equals(lastDocId, docid);
		lastDocId = docid;
		notifyDataSetChanged();
	}

	public void reset() {
		mDataList = null;
		isSameDocument = false;
		lastDocId = "lastDocId" + System.currentTimeMillis();
		notifyDataSetChanged();
	}

	private boolean zoomable = true;

	public FilePageInfoViewerAdapter(FragmentManager fm, boolean zoomable) {
		super(fm);
		this.zoomable = zoomable;
	}

	@Override
	public int getItemPosition(Object object) {
		return isSameDocument ? PagerAdapter.POSITION_UNCHANGED : PagerAdapter.POSITION_NONE;
	}

	@Override
	public Fragment getItem(int position) {
		return FilePageInfoFragment.newInstance(mDataList.get(position), zoomable);
	}

	@Override
	public int getCount() {
		return mDataList == null ? 0 : mDataList.size();
	}

}
