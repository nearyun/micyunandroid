package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.ModuleItem;
import com.tornado.util.ViewHolderUtil;

/**
 * 左侧导航栏 模块适配器
 * 
 * @author xiaohua
 * 
 */
public class ModulesAdapter extends AbsBaseAdapter<ModuleItem> {

	public ModulesAdapter(Context cxt, ArrayList<ModuleItem> dataList) {
		super(cxt, dataList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_module_layout, parent, false);
		}
		ModuleItem item = mDataList.get(position);
		TextView itemNameTextView = ViewHolderUtil.get(convertView, R.id.name_textview);
		itemNameTextView.setText(item.getName());
		itemNameTextView.setCompoundDrawablesWithIntrinsicBounds(item.getIcon(), 0, 0, 0);
		return convertView;
	}

}
