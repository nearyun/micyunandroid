package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

public abstract class AbsBaseAdapter<T> extends BaseAdapter {
	protected Context mContext;
	protected LayoutInflater mInflater;
	protected ArrayList<T> mDataList;

	public AbsBaseAdapter(Context cxt, ArrayList<T> dataList) {
		mInflater = LayoutInflater.from(cxt);
		mDataList = dataList;
		mContext = cxt;
	}

	@Override
	public int getCount() {
		return mDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
