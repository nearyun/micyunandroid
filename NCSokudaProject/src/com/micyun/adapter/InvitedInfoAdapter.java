package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.InviteInfo;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

/**
 * 邀请信息适配器
 * 
 * @author xiaohua
 * 
 */
public class InvitedInfoAdapter extends AbsBaseAdapter<InviteInfo> {

	public InvitedInfoAdapter(Context cxt, ArrayList<InviteInfo> dataList) {
		super(cxt, dataList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_invited_info_layout, parent, false);
		InviteInfo info = mDataList.get(position);
		TextView subjectTextView = ViewHolderUtil.get(convertView, R.id.subjectTextView);
		subjectTextView.setText(info.getSubject());

		TextView remarkTextView = ViewHolderUtil.get(convertView, R.id.remarkTextView);
		remarkTextView.setText(info.getInviteeNickName() + " 邀请您参加会议");

		TextView dateTextView = ViewHolderUtil.get(convertView, R.id.dateTextView);
		dateTextView.setText(TimeUtil.getMessageTime(info.getInviteTime() * 1000));
		return convertView;
	}

}
