package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.micyun.ui.widget.dialog.ParticipantDetailDialog;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.Participant;
import com.ncore.model.SipDevice;
import com.ncore.model.client.impl.Client;
import com.ncore.util.LogUtil;

public class MemberRecycleViewAdapter extends BaseRecyclerViewAdapter<BaseRecycleViewHolder, Participant> {

	private ConferenceArgument mConferenceArgument;

	public MemberRecycleViewAdapter(Context context, ConferenceArgument argument) {
		super(context, new ArrayList<Participant>());
		this.mConferenceArgument = argument;
	}

	public void addAllWithClear(ArrayList<Participant> dataList) {
		mDataList.clear();
		mDataList.addAll(dataList);
	}

	protected class ItemViewHolder extends BaseRecycleViewHolder {

		public View rootView;
		public ImageView itemAvatarImageView;
		public TextView itemNameTextView;
		public TextView itemDepartmentTextView;
		public TextView itemCharacterTextView;
		public ImageView[] itemDeviceStates = new ImageView[3];
		public View itemMuteView;
		public TextView itemStateTextView;
		public ImageView itemNetworkTypeImageView;
		public TextView itemSpecialInfoTextView;
		public Participant mParticipant;

		public ItemViewHolder(View holder) {
			super(holder);
			rootView = holder.findViewById(R.id.itemRootView);
			itemAvatarImageView = (ImageView) holder.findViewById(R.id.itemAvatarImageView);
			itemAvatarImageView.setOnClickListener(this);
			itemNameTextView = (TextView) holder.findViewById(R.id.itemNameTextView);
			itemDepartmentTextView = (TextView) holder.findViewById(R.id.itemDepartmentTextView);
			itemCharacterTextView = (TextView) holder.findViewById(R.id.itemCharacterTextView);

			itemDeviceStates[0] = (ImageView) holder.findViewById(R.id.itemAndroidState);
			itemDeviceStates[1] = (ImageView) holder.findViewById(R.id.itemAppleState);
			itemDeviceStates[2] = (ImageView) holder.findViewById(R.id.itemPCState);

			itemNetworkTypeImageView = (ImageView) holder.findViewById(R.id.networkTypeImageView);
			itemSpecialInfoTextView = (TextView) holder.findViewById(R.id.itemSpecialInfoTextView);

			itemMuteView = holder.findViewById(R.id.itemMuteView);
			itemStateTextView = (TextView) holder.findViewById(R.id.itemStateTextView);
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.itemAvatarImageView:
				new ParticipantDetailDialog(mContext).setAvatarPath(mParticipant.getAvatar())
						.setName(mParticipant.getNickName()).setMobile(mParticipant.getMobile())
						.setDepartment(mParticipant.getDepartment()).show();
				break;

			case R.id.itemRootView:
				String conferenceId = ConferenceMainRoomActivity.getIConferenceJsonDataManager().getConference()
						.getId();
				String sipUser = Client.getInstance().getUser().getSipUser();
				String controller = mConferenceArgument.getController();
				String owner = mConferenceArgument.getOwner();
				String currentUserId = Client.getInstance().getUser().getUserId();
				MemberOperation2.executeOperation(mContext, conferenceId, sipUser, mParticipant,
						itemNetworkTypeImageView, controller, owner, currentUserId);
				break;
			default:
				break;
			}

		}

	}

	@Override
	public void onBindViewHolder(BaseRecycleViewHolder viewholder, int position) {
		if (!(viewholder instanceof ItemViewHolder)) {
			return;
		}
		ItemViewHolder holder = (ItemViewHolder) viewholder;

		Participant participant = mDataList.get(position);
		holder.mParticipant = participant;
		ImageLoaderUtil.refreshAvatar(participant, holder.itemAvatarImageView);

		holder.itemNameTextView.setText(participant.getNickName());
		holder.itemDepartmentTextView.setText(participant.getDepartment());

		holder.itemCharacterTextView.setText(getCharacter(participant));

		for (int i = 0; i < holder.itemDeviceStates.length; i++) {
			holder.itemDeviceStates[i].setVisibility(View.GONE);
		}

		SipDevice[] mSipDevices = participant.getSipDevices();
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] == null)
				continue;

			if (mSipDevices[i].isInterrupted()) {
				holder.itemSpecialInfoTextView.setText("对方接听电话中");
				holder.itemNetworkTypeImageView.setVisibility(View.INVISIBLE);
				holder.itemSpecialInfoTextView.setVisibility(View.VISIBLE);
			} else if (mSipDevices[i].isPoorNetwork()) {
				holder.itemSpecialInfoTextView.setText("对方网络不稳定");
				holder.itemNetworkTypeImageView.setVisibility(View.INVISIBLE);
				holder.itemSpecialInfoTextView.setVisibility(View.VISIBLE);
			} else {
				int networkType = mSipDevices[i].getNetworkType();// 网络状态
				holder.itemNetworkTypeImageView.setImageResource(getNetworkTypeIcon(networkType));
				holder.itemNetworkTypeImageView.setVisibility(View.VISIBLE);
				holder.itemSpecialInfoTextView.setVisibility(View.INVISIBLE);
			}

			holder.itemDeviceStates[i].setVisibility(View.VISIBLE);
			int deviceType = mSipDevices[i].getType();
			int offlineResId = -1;
			int onlineResId = -1;
			int animResId = -1;
			if (SipDevice.DEVICE_ANDROID == deviceType || SipDevice.DEVICE_IPHONE == deviceType) {
				offlineResId = R.drawable.ic_device_telphone_offline;
				onlineResId = R.drawable.ic_device_telphone_online;
				animResId = R.drawable.bg_anim_telephony;
			} else if (SipDevice.DEVICE_PC == deviceType) {
				offlineResId = R.drawable.ic_device_pc_offline;
				onlineResId = R.drawable.ic_device_pc_online;
				animResId = R.drawable.bg_anim_pc;
			} else {
				offlineResId = R.drawable.ic_device_pstn_offline;
				onlineResId = R.drawable.ic_device_pstn_online;
				animResId = R.drawable.bg_anim_pstn;
			}
			Drawable tmpDrawable = holder.itemDeviceStates[i].getDrawable();
			if (tmpDrawable instanceof AnimationDrawable) {
				AnimationDrawable tmpAnimDrawable = (AnimationDrawable) tmpDrawable;
				if (tmpAnimDrawable.isRunning()) {
					LogUtil.e("xiaohua", "member adapter 停止动画 " + position);
					tmpAnimDrawable.stop();
				}
			}

			int state = mSipDevices[i].getCallState();
			switch (state) {
			case SipDevice.CALL_STATE_DEFAULT:// 默认
				holder.itemDeviceStates[i].setImageResource(offlineResId);
				holder.itemStateTextView.setText(mSipDevices[i].isHandup() ? "被挂断" : "");
				break;
			case SipDevice.CALL_STATE_CALLING:// 呼叫中
				holder.itemDeviceStates[i].setImageResource(animResId);
				AnimationDrawable mAnimationDrawable = (AnimationDrawable) holder.itemDeviceStates[i].getDrawable();
				LogUtil.e("xiaohua", "member adapter 启动动画 " + position);
				mAnimationDrawable.start();
				holder.itemStateTextView.setText("等待接入");
				break;
			case SipDevice.CALL_STATE_ANSWER:// 呼叫成功
				holder.itemDeviceStates[i].setImageResource(onlineResId);
				holder.itemStateTextView.setText("");
				break;
			case SipDevice.CALL_STATE_FAILURE:// 呼叫失败
				holder.itemDeviceStates[i].setImageResource(offlineResId);
				holder.itemStateTextView.setText(getCallFailureStatus(mSipDevices[i].getSipStatus()));
				break;
			case SipDevice.CALL_STATE_UNREACHABLE:// 对方未注册
				holder.itemDeviceStates[i].setImageResource(offlineResId);
				holder.itemStateTextView.setText("离线");
				break;
			case SipDevice.CALL_STATE_BUSY:// 忙
				holder.itemDeviceStates[i].setImageResource(offlineResId);
				holder.itemStateTextView.setText("线路忙");
				break;
			default:
				holder.itemDeviceStates[i].setVisibility(View.GONE);
				holder.itemStateTextView.setText("");
				return;
			}
		}

		if (!participant.isOnline()) {
			holder.itemStateTextView.setText("已离开");
			holder.itemNetworkTypeImageView.setVisibility(View.INVISIBLE);
			holder.itemSpecialInfoTextView.setVisibility(View.INVISIBLE);
		}

		if (participant.isMute()) {
			holder.itemMuteView.setVisibility(View.VISIBLE);
		} else {
			holder.itemMuteView.setVisibility(View.GONE);
		}
		boolean isowner = TextUtils.equals(mConferenceArgument.getOwner(), mConferenceArgument.getCurrentUserId());
		boolean isController = TextUtils.equals(mConferenceArgument.getController(),
				mConferenceArgument.getCurrentUserId());
		if (isowner || isController) {
			holder.rootView.setOnClickListener(holder);
		} else {
			holder.rootView.setOnClickListener(null);
		}

	}

	private int getNetworkTypeIcon(int networkType) {
		switch (networkType) {
		case SipDevice.NETWORK_TYPE_2G:
			return R.drawable.ic_network_2g;
		case SipDevice.NETWORK_TYPE_3G:
			return R.drawable.ic_network_3g;
		case SipDevice.NETWORK_TYPE_4G:
			return R.drawable.ic_network_4g;
		case SipDevice.NETWORK_TYPE_5G:
			return 0;
		case SipDevice.NETWORK_TYPE_VPN:
			return 0;
		case SipDevice.NETWORK_TYPE_WIFI:
		case SipDevice.NETWORK_TYPE_WIFI58:
			return R.drawable.ic_network_wifi;
		case SipDevice.NETWORK_TYPE_FIXED:
		case SipDevice.NETWORK_TYPE_UNKNOWN:
		default:
			return 0;
		}
	}

	private String getCallFailureStatus(int sipstatus) {
		switch (sipstatus) {
		case SipDevice.SIP_STATUS_BUSY:
			return "线路忙";
		case SipDevice.SIP_STATUS_CANCEL:
			return "取消呼叫";
		case SipDevice.SIP_STATUS_MEDIA_ERROR:
			return "媒体错误";
		case SipDevice.SIP_STATUS_NOT_FOUND:
			return "无该账号";
		case SipDevice.SIP_STATUS_REJECT:
			return "拒听";
		case SipDevice.SIP_STATUS_DEFAULT:
			return "";
		default:
			return "呼叫失败";
		}
	}

	@Override
	public BaseRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (TYPE_FOOTER_VIEW == viewType) {
			View view = mInflater.inflate(R.layout.footer_rcview_layout, parent, false);
			return new FooterViewHolder(view);
		} else {
			View view = mInflater.inflate(R.layout.item_member_rcview_layout, parent, false);
			return new ItemViewHolder(view);
		}
	}

	private final int TYPE_FOOTER_VIEW = 1;

	@Override
	public int getItemCount() {
		return super.getItemCount() + 1;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == mDataList.size())
			return TYPE_FOOTER_VIEW;
		else
			return super.getItemViewType(position);
	}

	/**
	 * 获取当前会议的角色
	 * 
	 * @param p
	 * @return
	 */
	private String getCharacter(Participant p) {
		if (p == null)
			return "";
		boolean isOwner = mConferenceArgument.hasOwner()
				&& TextUtils.equals(p.getUserId(), mConferenceArgument.getOwner());
		boolean isController = TextUtils.equals(p.getUserId(), mConferenceArgument.getController());
		boolean isMe = TextUtils.equals(p.getUserId(), mConferenceArgument.getCurrentUserId());

		if (!isOwner && !isController && !isMe)
			return "";

		String character = "(";
		if (isOwner) {
			character += "发起人";
			// if (!hasController || isController)
			// character += ",主讲人";
		} else if (isController) {
			character += "主讲人";
		} else if (isMe) {
			character += "我)";
			return character;
		}
		character += isMe ? ",我)" : ")";

		return character;
	}

}
