package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.model.upload.UploadFileInfo;
import com.micyun.util.ImageLoaderUtil;
import com.tornado.util.ViewHolderUtil;

/**
 * 上传文档适配器
 * 
 * @author xiaohua
 * 
 */
public class UploadFileAdapter extends AbsBaseAdapter<UploadFileInfo> {

	private OnCancelListener mOnCancelListener = null;

	public UploadFileAdapter(Context context) {
		super(context, new ArrayList<UploadFileInfo>());
	}

	public void refreshData(ArrayList<UploadFileInfo> list) {
		if (list == null)
			return;
		mDataList.clear();
		mDataList.addAll(list);
		notifyDataSetChanged();
	}

	public void setOnCancelListener(OnCancelListener l) {
		mOnCancelListener = l;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_upload_file_layout, parent, false);

		final UploadFileInfo file = mDataList.get(position);

		ImageView itemIcon = ViewHolderUtil.get(convertView, R.id.item_icon);
		Drawable drawable = mContext.getResources().getDrawable(FileManager.getFormatIconResource(file.getExtension()));
		String extension = file.getExtension();
		if (TextUtils.equals(extension, "png") || TextUtils.equals(extension, "PNG")
				|| TextUtils.equals(extension, "jpg") || TextUtils.equals(extension, "JPG")
				|| TextUtils.equals(extension, "jpeg") || TextUtils.equals(extension, "JPEG"))
			ImageLoaderUtil.refreshFileThumbnail(drawable, "file://" + file.getPath(), itemIcon);
		else
			itemIcon.setImageDrawable(drawable);

		TextView itemName = ViewHolderUtil.get(convertView, R.id.item_file_name);
		TextView itemProgress = ViewHolderUtil.get(convertView, R.id.item_upload_progress);
		Button itemCancelBtn = ViewHolderUtil.get(convertView, R.id.item_cancel_btn);
		itemCancelBtn.setOnClickListener(null);
		itemCancelBtn.setText("取消");

		itemName.setText(file.getName());

		itemCancelBtn.setVisibility(View.VISIBLE);
		switch (file.getState()) {
		case UploadFileInfo.TYPE_WAITTING:
			itemProgress.setText("请稍候……");
			itemCancelBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnCancelListener != null)
						mOnCancelListener.onCancel(file);
				}
			});
			break;
		case UploadFileInfo.TYPE_UPLOADING:
			itemProgress.setText((int) (file.getProgress() * 1.0 / file.getSize() * 100) + "%");
			itemCancelBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnCancelListener != null)
						mOnCancelListener.onStop(file);
				}
			});
			break;
		case UploadFileInfo.TYPE_SUCCESS:
			itemProgress.setText("上传完成");
			itemCancelBtn.setVisibility(View.INVISIBLE);
			break;
		case UploadFileInfo.TYPE_FAILURE:
			itemCancelBtn.setText("重传");
			itemProgress.setText("上传失败");
			itemCancelBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnCancelListener != null)
						mOnCancelListener.onRestart(file);
				}
			});
			break;
		case UploadFileInfo.TYPE_CANCEL:
			itemCancelBtn.setText("重传");
			itemProgress.setText("已取消");
			itemCancelBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mOnCancelListener != null)
						mOnCancelListener.onRestart(file);
				}
			});
			break;
		default:
			itemProgress.setText("计算中……");
			itemCancelBtn.setVisibility(View.INVISIBLE);
			break;
		}
		return convertView;
	}

	public interface OnCancelListener {
		public void onCancel(UploadFileInfo file);

		public void onStop(UploadFileInfo file);

		public void onRestart(UploadFileInfo file);
	}

}
