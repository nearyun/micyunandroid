package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.free.circleimageview.SelectableRoundedImageView;
import com.micyun.R;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.sharing.SharingFilePageInfo;

public class ThumbnailRecycleViewAdapter extends
		BaseRecyclerViewAdapter<ThumbnailRecycleViewAdapter.ViewHolder, SharingFilePageInfo> {
	public interface OnPageSelectedListener {
		void onItemClick(SharingFilePageInfo page);
	}

	public void setOnPageSelectedListener(OnPageSelectedListener onRecyclerViewListener) {
		this.onRecyclerViewListener = onRecyclerViewListener;
	}

	private OnPageSelectedListener onRecyclerViewListener;

	protected void executeOnItemClick(SharingFilePageInfo page) {
		if (onRecyclerViewListener != null) {
			onRecyclerViewListener.onItemClick(page);
		}
	}

	private int currentPage = -1;
	private String currentDocId = "";

	public ThumbnailRecycleViewAdapter(Context context) {
		super(context, new ArrayList<SharingFilePageInfo>());
	}

	public void addAllWithClear(String docId, ArrayList<SharingFilePageInfo> list, int curPage) {
		if (!TextUtils.equals(docId, currentDocId)) {
			mDataList.clear();
			mDataList.addAll(list);
			currentDocId = docId;
		}
		currentPage = curPage;
		notifyDataSetChanged();
	}

	class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		public View rootView;
		public SelectableRoundedImageView thumbnailImageView;
		public TextView pageTextView;
		public SharingFilePageInfo page;

		public ViewHolder(View itemView) {
			super(itemView);
			rootView = itemView.findViewById(R.id.itemRootView);
			rootView.setOnClickListener(this);
			thumbnailImageView = (SelectableRoundedImageView) itemView.findViewById(R.id.itemPageImageView);
			pageTextView = (TextView) itemView.findViewById(R.id.itemPageNumTextView);
		}

		@Override
		public void onClick(View v) {
			executeOnItemClick(page);
		}
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		SharingFilePageInfo pageItem = mDataList.get(position);
		viewHolder.page = pageItem;
		Drawable drawable = mContext.getResources().getDrawable(R.drawable.sharing_loading_error);
		ImageLoaderUtil.refreshFileThumbnail(drawable, pageItem.getImageUrl(), viewHolder.thumbnailImageView);

		viewHolder.pageTextView.setText("" + pageItem.getPageNum());

		if (pageItem.getPageNum() == currentPage) {
			viewHolder.pageTextView.setBackgroundColor(Color.RED);
			viewHolder.thumbnailImageView.setBorderColor(Color.RED);
		} else {
			viewHolder.pageTextView.setBackgroundColor(Color.GRAY);
			viewHolder.thumbnailImageView.setBorderColor(Color.GRAY);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View view = mInflater.inflate(R.layout.item_file_thumbnail_layout, parent, false);
		return new ViewHolder(view);
	}

}
