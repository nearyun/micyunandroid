package com.micyun.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.fragment.MembersFragment;
import com.micyun.ui.conference.fragment.SharingFilesFragment;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
	private Fragment[] modues = null;
	private ConferenceArgument argument;

	public TabPagerAdapter(FragmentManager fm, ConferenceArgument argument) {
		super(fm);
		this.argument = argument;
	}

	public void refresh() {
		if (modues == null)
			modues = new Fragment[] { MembersFragment.newInstance(argument), SharingFilesFragment.newInstance(argument) };
		this.notifyDataSetChanged();
	}

	@Override
	public Fragment getItem(int position) {
		return modues[position];
	}

	@Override
	public int getCount() {
		return modues == null ? 0 : modues.length;
	}
}
