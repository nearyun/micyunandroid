package com.micyun.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.free.circleimageview.SelectableRoundedImageView;
import com.micyun.R;
import com.micyun.model.InvitedParticipant;
import com.micyun.util.ImageLoaderUtil;
import com.tornado.util.DensityUtil;

/**
 * 重开会议时邀请人员的九宫格
 * 
 * @author xiaohua
 * 
 */
public class InviteMemberRecycleViewAdapter extends
		BaseRecyclerViewAdapter<InviteMemberRecycleViewAdapter.ItemViewHolder, InvitedParticipant> {

	public interface OnAddListener {
		public void onAddClick();
	}

	private OnAddListener mOnAddListener;

	public void setOnAddListener(OnAddListener listener) {
		mOnAddListener = listener;
	}

	private boolean deleteFlag = false;
	private RecyclerView membersRecyclerView;

	public InviteMemberRecycleViewAdapter(Context context, RecyclerView view) {
		super(context, new ArrayList<InvitedParticipant>());
		membersRecyclerView = view;
	}

	public JSONArray toJSONArray() {
		JSONArray array = new JSONArray();
		for (int i = 0; i < mDataList.size(); i++) {
			try {
				JSONObject json = new JSONObject();
				json.put("nickname", mDataList.get(i).getNickName());
				json.put("phone", mDataList.get(i).getPhone());
				array.put(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return array;
	}

	public void refreshData(ArrayList<InvitedParticipant> data) {
		mDataList.clear();
		mDataList.addAll(data);
		notifyDataSetChangedToResize();
	}

	public void notifyDataSetChangedToResize() {
		int count = getItemCount();
		int row = count / 4 + (count % 4 == 0 ? 0 : 1);
		if (count == 1) // 防止删除最后一个成员后，还处于删除状态
			deleteFlag = false;
		super.notifyDataSetChanged();
		membersRecyclerView.getLayoutParams().height = DensityUtil.dp2px(76, mContext) * row;
	}

	public InvitedParticipant getItem(int position) {
		return mDataList.get(position);
	}

	public void addItem(InvitedParticipant p) {
		if (p != null)
			mDataList.add(p);
	}

	protected class ItemViewHolder extends BaseRecycleViewHolder {

		public static final int TYPE_DEFAULT = 0;
		public static final int TYPE_ADD = 1;
		public static final int TYPE_DELETE = 2;

		public SelectableRoundedImageView itemAvatarImageView;
		public TextView nameTextView;
		public View itemDeleteView;
		public int type = TYPE_DEFAULT;

		public ItemViewHolder(View view) {
			super(view);
			view.setOnClickListener(this);
			itemAvatarImageView = (SelectableRoundedImageView) view.findViewById(R.id.itemAvatarImageView);
			nameTextView = (TextView) view.findViewById(R.id.nameTextView);
			itemDeleteView = view.findViewById(R.id.itemDeleteView);
		}

		@Override
		public void onClick(View v) {
			switch (type) {
			case TYPE_ADD:
				if (mOnAddListener != null)
					mOnAddListener.onAddClick();
				break;
			case TYPE_DELETE:
				deleteFlag = !deleteFlag;
				notifyDataSetChanged();
				break;
			default:
				break;
			}
		}

	}

	@Override
	public void onBindViewHolder(ItemViewHolder viewholder, final int position) {
		ItemViewHolder itemHolder = (ItemViewHolder) viewholder;
		itemHolder.itemDeleteView.setVisibility(View.INVISIBLE);

		int count = super.getItemCount();
		if (count == 0) {
			itemHolder.itemAvatarImageView.setBorderWidthDP(0);
			itemHolder.nameTextView.setText("");
			itemHolder.itemAvatarImageView.setImageResource(R.drawable.btn_add_member_item_selector);
			itemHolder.type = ItemViewHolder.TYPE_ADD;
		} else {
			if (position == getItemCount() - 2) {// 添加
				itemHolder.itemAvatarImageView.setBorderWidthDP(0);
				itemHolder.nameTextView.setText("");
				itemHolder.itemAvatarImageView.setImageResource(R.drawable.btn_add_member_item_selector);
				itemHolder.type = ItemViewHolder.TYPE_ADD;
			} else if (position == getItemCount() - 1) {// 删除
				itemHolder.itemAvatarImageView.setBorderWidthDP(0);
				itemHolder.nameTextView.setText("");
				itemHolder.itemAvatarImageView.setImageResource(deleteFlag ? R.drawable.btn_delete_done_item_selector
						: R.drawable.btn_delete_member_item_selector);
				itemHolder.type = ItemViewHolder.TYPE_DELETE;
			} else {// 人员
				InvitedParticipant participant = mDataList.get(position);
				itemHolder.itemAvatarImageView.setBorderWidthDP(1);
				itemHolder.nameTextView.setText(participant.getNickName());
				itemHolder.itemDeleteView.setVisibility(deleteFlag ? View.VISIBLE : View.INVISIBLE);
				itemHolder.itemDeleteView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						mDataList.remove(position);
						notifyDataSetChangedToResize();
					}
				});
				itemHolder.type = ItemViewHolder.TYPE_DEFAULT;
				ImageLoaderUtil.refreshAvatar(participant.getAvatar(), itemHolder.itemAvatarImageView);
			}
		}
	}

	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_rcv_invite_member, parent, false);
		return new ItemViewHolder(view);
	}

	@Override
	public int getItemCount() {
		int count = super.getItemCount();
		return count == 0 ? 1 : count + 2;
	}
}
