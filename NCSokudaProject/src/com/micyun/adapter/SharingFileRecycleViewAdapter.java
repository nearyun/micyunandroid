package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.micyun.R;
import com.micyun.model.ConferenceArgument;
import com.micyun.model.FileManager;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.callback.OnCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.model.sharing.SharingFile;
import com.ncore.util.LogUtil;

public class SharingFileRecycleViewAdapter extends BaseRecyclerViewAdapter<BaseRecycleViewHolder, SharingFile> {
	private static final String TAG = "SharingFileRecycleViewAdapter";
	private ConferenceArgument mConferenceArgument;

	public SharingFileRecycleViewAdapter(Context context, ConferenceArgument argument) {
		super(context, new ArrayList<SharingFile>());
		mConferenceArgument = argument;
	}

	public void addAllWithClear(ArrayList<SharingFile> datalist) {
		mDataList.clear();
		mDataList.addAll(datalist);
	}

	class FileItemViewHolder extends BaseRecycleViewHolder {
		public View rootView;
		public ImageView itemThumbnailImageView;
		public TextView itemSharingStateView;
		public TextView itemNameTextView;
		public TextView itemUploaderTextView;
		public TextView itemPagesTextView;
		public ImageButton itemMoreButton;
		public SharingFile mSharingFile;

		public FileItemViewHolder(View itemView) {
			super(itemView);
			rootView = itemView.findViewById(R.id.itemRootView);
			rootView.setOnClickListener(this);
			itemThumbnailImageView = (ImageView) itemView.findViewById(R.id.itemThumbnailImageView);
			itemSharingStateView = (TextView) itemView.findViewById(R.id.itemSharingStateView);
			itemNameTextView = (TextView) itemView.findViewById(R.id.itemNameTextView);
			itemUploaderTextView = (TextView) itemView.findViewById(R.id.itemUploaderTextView);
			itemPagesTextView = (TextView) itemView.findViewById(R.id.itemPagesTextView);
			itemMoreButton = (ImageButton) itemView.findViewById(R.id.itemMoreButton);
			itemMoreButton.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.itemRootView:
				executeDisplay(mSharingFile);
				break;
			case R.id.itemMoreButton:
				showPopupMenu(v, mSharingFile);
				break;
			}
		}
	}

	private void executeDisplay(SharingFile file) {
		if (!mConferenceArgument.hasControlPermission()) {
			Toast.makeText(mContext, "您当前不是主讲人", Toast.LENGTH_LONG).show();
			return;
		}
		if (TextUtils.equals(file.getDocId(), mConferenceArgument.getDisplaying()))
			return;
		Client.getInstance().setConferenceDisplaying(mConferenceArgument.getConferenceId(), file.getDocId(),
				new OnCallback() {

					@Override
					public void onSuccess() {

					}

					@Override
					public void onFailure(int errorCode, String reason) {
						LogUtil.e(TAG, reason + " " + errorCode);
						Toast.makeText(mContext, "切换文档失败", Toast.LENGTH_LONG).show();
					}
				});
	}

	private final int ITEM_MENU_REMOVE = Menu.FIRST;

	private void showPopupMenu(View view, final SharingFile file) {
		if (file == null)
			return;
		PopupMenu popup = new PopupMenu(mContext, view);
		popup.getMenu().add(Menu.NONE, ITEM_MENU_REMOVE, Menu.NONE, "移除");

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				if (item.getItemId() == ITEM_MENU_REMOVE) {
					executeRemove(file);
				}
				return true;
			}
		});
		popup.show();
	}

	private void executeRemove(final SharingFile sharingFile) {
		if (sharingFile == null)
			return;
		if (TextUtils.equals(sharingFile.getDocId(), mConferenceArgument.getDisplaying())) {
			Client.getInstance().stopConferenceDisplaying(mConferenceArgument.getConferenceId(), new OnCallback() {

				@Override
				public void onSuccess() {
					Client.getInstance().removeDocumentFromList(mConferenceArgument.getConferenceId(),
							sharingFile.getSequenceNumber(), sharingFile.getDocId(), null);
				}

				@Override
				public void onFailure(int errorCode, String reason) {

				}
			});
		} else {
			Client.getInstance().removeDocumentFromList(mConferenceArgument.getConferenceId(),
					sharingFile.getSequenceNumber(), sharingFile.getDocId(), null);
		}
	}

	@Override
	public void onBindViewHolder(BaseRecycleViewHolder viewholder, int position) {
		if (!(viewholder instanceof FileItemViewHolder)) {
			return;
		}
		FileItemViewHolder holder = (FileItemViewHolder) viewholder;

		SharingFile sharingFile = mDataList.get(position);
		holder.mSharingFile = sharingFile;

		Drawable drawable = mContext.getResources().getDrawable(R.drawable.default_logo_grey);
		ImageLoaderUtil.refreshFileThumbnail(drawable, sharingFile.getFileThumbnail(), holder.itemThumbnailImageView);

		holder.itemNameTextView.setText(sharingFile.getFileName());

		holder.itemUploaderTextView.setText(sharingFile.getOwnerName());

		int pages = sharingFile.getFilePages();
		int width = sharingFile.getWidth();
		int height = sharingFile.getHeith();
		String w_h = (width == 0 || height == 0) ? "" : width + "x" + height;
		holder.itemPagesTextView.setText(FileManager.isPicture(sharingFile.getFileType()) ? w_h : (pages + "页"));

		boolean isDisplaying = TextUtils.equals(mConferenceArgument.getDisplaying(), sharingFile.getDocId());
		holder.itemSharingStateView.setVisibility(isDisplaying ? View.VISIBLE : View.INVISIBLE);

		boolean hasControl = mConferenceArgument.hasControlPermission();
		holder.itemMoreButton.setVisibility(hasControl ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	public BaseRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (TYPE_FOOTER_VIEW == viewType) {
			View view = mInflater.inflate(R.layout.footer_rcview_layout, parent, false);
			return new FooterViewHolder(view);
		} else {
			View view = mInflater.inflate(R.layout.item_sharingfile_rcview_layout, parent, false);
			return new FileItemViewHolder(view);
		}
	}

	private final int TYPE_FOOTER_VIEW = 1;

	@Override
	public int getItemCount() {
		return super.getItemCount() + 1;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == mDataList.size())
			return TYPE_FOOTER_VIEW;
		else
			return super.getItemViewType(position);
	}
}
