package com.micyun.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.model.NetworkFileInformation;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.sharing.NetworkFileInfo;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

/**
 * 网盘文件列表适配器
 * 
 * @author xiaohua
 * 
 */
public class NetworkFileListAdapter extends BaseListAdapter<NetworkFileInformation> {
	private final int STATE_DEFAULT = 0;
	private final int STATE_CANCEL_CONVERT = 1;
	private final int STATE_SHARE = 2;

	private final int MENU_CANCEL_CONVERT = Menu.FIRST;
	private final int MENU_SHARE = Menu.FIRST + 1;
	private final int MENU_DELETE = Menu.FIRST + 2;

	public NetworkFileListAdapter(Context cxt) {
		super(cxt);
	}

	public void remove(NetworkFileInformation nwfi) {
		mDataList.remove(nwfi);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_network_file_layout, parent, false);
		}
		final NetworkFileInformation fInfo = mDataList.get(position);
		final NetworkFileInfo netFile = fInfo.getNetworkFile();
		TextView itemFileNameTextView = ViewHolderUtil.get(convertView, R.id.item_file_name_textview);
		itemFileNameTextView.setText(netFile.getName());

		ImageView itemFileIcon = ViewHolderUtil.get(convertView, R.id.item_file_icon);
		Drawable drawable = mContext.getResources().getDrawable(R.drawable.default_logo_grey);
		ImageLoaderUtil.refreshFileThumbnail(drawable, netFile.getThumbUrl(), itemFileIcon);

		TextView itemFileUploadTimeTextView = ViewHolderUtil.get(convertView, R.id.item_file_upload_time);
		itemFileUploadTimeTextView.setText(TimeUtil.getTime(netFile.getUploadTime() * 1000));

		ImageButton itemMoreButton = ViewHolderUtil.get(convertView, R.id.item_more_button);
		TextView itemFileStatusOrPagesTextView = ViewHolderUtil
				.get(convertView, R.id.item_file_status_or_page_textview);

		int state = netFile.getState();
		String text = "";
		int opState = STATE_DEFAULT;
		boolean canDelete = false;
		boolean isPicture = false;
		switch (state) {
		case 0:// 等待转换
			text = "等候转换";
			opState = STATE_DEFAULT;
			canDelete = true;
			break;
		case 1:// 转换成功
			isPicture = FileManager.isPicture(netFile.getExtension());
			text = fInfo.isInSharingQueue() ? "已分享" : (isPicture ? netFile.getWidth() + "x" + netFile.getHeight()
					: netFile.getPages() + " 页");
			opState = fInfo.isInSharingQueue() ? STATE_DEFAULT : STATE_SHARE;
			canDelete = true;
			break;
		case 2:// 正在转换
			text = "正在转换";
			opState = STATE_CANCEL_CONVERT;
			break;
		case 3:// 转换失败
			text = "转换失败";
			opState = STATE_DEFAULT;
			canDelete = true;
			break;
		case 4:// 转换超时
			text = "转换超时";
			opState = STATE_DEFAULT;
			canDelete = true;
			break;
		case 5:// 预处理，office转为png过程中
			text = "预处理中";
			opState = STATE_CANCEL_CONVERT;
			break;
		default:
			break;
		}

		itemFileStatusOrPagesTextView.setText(text);
		if (fInfo.isInSharingQueue()) {
			itemFileStatusOrPagesTextView.setBackgroundColor(Color.GREEN);
		} else {
			itemFileStatusOrPagesTextView.setBackgroundColor(Color.TRANSPARENT);
		}

		final int _state = opState;
		final boolean _delete = canDelete;
		itemMoreButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showPopupMenu(v, _state, _delete, fInfo);
			}
		});

		return convertView;
	}

	private void showPopupMenu(View v, int opState, boolean canDelete, final NetworkFileInformation fInfo) {
		final PopupMenu menu = new PopupMenu(mContext, v);
		if (STATE_CANCEL_CONVERT == opState) {
			menu.getMenu().add(Menu.NONE, MENU_CANCEL_CONVERT, Menu.NONE, "取消转换");
		} else if (STATE_SHARE == opState) {
			menu.getMenu().add(Menu.NONE, MENU_SHARE, Menu.NONE, "共享");
		}
		if (canDelete) {
			menu.getMenu().add(Menu.NONE, MENU_DELETE, Menu.NONE, "删除");
		}
		menu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				if (oacb == null)
					return false;
				int itemId = item.getItemId();
				if (MENU_CANCEL_CONVERT == itemId) {
					oacb.onCancelConvert(fInfo);
				} else if (MENU_SHARE == itemId) {
					oacb.onShare(fInfo);
				} else if (MENU_DELETE == itemId) {
					oacb.onDelete(fInfo);
				}
				return false;
			}
		});
		menu.show();
	}

	public interface OnCallback {
		public void onShare(NetworkFileInformation nfi);

		public void onDelete(NetworkFileInformation nfi);

		public void onCancelConvert(NetworkFileInformation nfi);
	}

	private OnCallback oacb = null;

	public void setOnCallback(OnCallback cb) {
		oacb = cb;
	}
}
