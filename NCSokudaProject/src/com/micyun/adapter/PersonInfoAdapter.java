package com.micyun.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.PersonInfoItem;
import com.tornado.util.ViewHolderUtil;

/**
 * 用户详细信息修改项
 * 
 * @author xiaohua
 * 
 */
public class PersonInfoAdapter extends BaseListAdapter<PersonInfoItem> {

	public PersonInfoAdapter(Context cxt) {
		super(cxt);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_person_info_layout, parent, false);
		PersonInfoItem item = mDataList.get(position);
		TextView labelTextView = ViewHolderUtil.get(convertView, R.id.labelTextview);
		labelTextView.setText(item.getLabel());
		TextView contentTextView = ViewHolderUtil.get(convertView, R.id.contentTextview);
		contentTextView.setText(item.getContent());
		return convertView;
	}
}
