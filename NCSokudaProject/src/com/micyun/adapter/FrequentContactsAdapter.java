package com.micyun.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.util.ImageLoaderUtil;
import com.nearyun.contact.model.ContactEx;
import com.tornado.util.ViewHolderUtil;

public class FrequentContactsAdapter extends BaseListAdapter<ContactEx> {

	public FrequentContactsAdapter(Context cxt) {
		super(cxt);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_frequent_contact_layout, parent, false);
		}
		ImageView avatarImageView = ViewHolderUtil.get(convertView, R.id.item_avatar_imageview);
		TextView nameTextView = ViewHolderUtil.get(convertView, R.id.item_name_textview);
		TextView phoneTextView = ViewHolderUtil.get(convertView, R.id.item_phone_textview);
		View selectedView = ViewHolderUtil.get(convertView, R.id.item_selected_view);

		final ContactEx contact = mDataList.get(position);
		final String name = contact.getName();
		final String phone = contact.getPhone();
		nameTextView.setText(name);
		phoneTextView.setText(phone);

		if (contact.isSelected()) {
			selectedView.setBackgroundResource(R.drawable.item_check_on);
		} else {
			selectedView.setBackgroundResource(R.drawable.item_check_off);
		}
		ImageLoaderUtil.refreshAvatar(mContext, name, phone, avatarImageView);
		return convertView;
	}

}
