package com.micyun.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseRecycleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

	public BaseRecycleViewHolder(View view) {
		super(view);
	}

}
