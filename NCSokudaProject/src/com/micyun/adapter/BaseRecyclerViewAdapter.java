package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

public abstract class BaseRecyclerViewAdapter<T extends RecyclerView.ViewHolder, E> extends RecyclerView.Adapter<T> {

	protected Context mContext;
	protected LayoutInflater mInflater;
	protected ArrayList<E> mDataList;

	public BaseRecyclerViewAdapter(Context context, ArrayList<E> dataList) {
		mInflater = LayoutInflater.from(context);
		mDataList = dataList;
		mContext = context;
	}

	@Override
	public int getItemCount() {
		return mDataList.size();
	}
}
