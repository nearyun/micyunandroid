package com.micyun.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.model.SharingFileWanted;
import com.micyun.ui.PreviewActivity;
import com.micyun.util.ImageLoaderUtil;
import com.tornado.util.DensityUtil;

/**
 * 重开会议时的候选分享文档列表
 * 
 * @author xiaohua
 * 
 */
public class InviteSharingRecycleViewAdapter extends
		BaseRecyclerViewAdapter<InviteSharingRecycleViewAdapter.ItemViewHolder, SharingFileWanted> {

	private RecyclerView sharingFileRecyclerView;

	public InviteSharingRecycleViewAdapter(Context context, RecyclerView view) {
		super(context, new ArrayList<SharingFileWanted>());
		sharingFileRecyclerView = view;
	}

	public void refreshData(ArrayList<SharingFileWanted> data) {
		mDataList.clear();
		mDataList.addAll(data);
		notifyDataSetChangedToResize();
	}

	public String[] toStringArray() {
		int size = mDataList.size();
		String[] sa = new String[size];
		for (int i = 0; i < mDataList.size(); i++) {
			sa[i] = mDataList.get(i).getDocId();
		}
		return sa;
	}

	public JSONArray toJSONArray() {
		JSONArray array = new JSONArray();
		for (int i = 0; i < mDataList.size(); i++) {
			try {
				JSONObject json = new JSONObject();
				json.put("id", mDataList.get(i).getId());
				json.put("from", mDataList.get(i).getFromType());
				array.put(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return array;
	}

	public void notifyDataSetChangedToResize() {
		notifyDataSetChanged();
		int count = getItemCount();
		sharingFileRecyclerView.getLayoutParams().height = DensityUtil.dp2px(64, mContext) * count;
	}

	public void addItem(SharingFileWanted file) {
		if (file != null)
			mDataList.add(file);
	}

	protected class ItemViewHolder extends BaseRecycleViewHolder {
		public SharingFileWanted mSharingFile;
		public ImageView itemThumbnailImageView;
		public TextView itemNameTextView;
		public TextView itemUploaderTextView;
		public TextView itemPagesTextView;
		public ImageButton itemMoreButton;

		public ItemViewHolder(View view) {
			super(view);
			view.setOnClickListener(this);
			itemThumbnailImageView = (ImageView) view.findViewById(R.id.itemThumbnailImageView);
			itemNameTextView = (TextView) view.findViewById(R.id.itemNameTextView);
			itemUploaderTextView = (TextView) view.findViewById(R.id.itemUploaderTextView);
			itemPagesTextView = (TextView) view.findViewById(R.id.itemPagesTextView);
			itemMoreButton = (ImageButton) view.findViewById(R.id.itemMoreButton);
			itemMoreButton.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.itemMoreButton) {
				mDataList.remove(mSharingFile);
				notifyDataSetChangedToResize();
			} else {
				PreviewActivity.newInstanceForPreviewOnly(mContext, mSharingFile.getDocId(),
						mSharingFile.getSessionId(), mSharingFile.getFileName());
			}
		}

	}

	@Override
	public void onBindViewHolder(ItemViewHolder viewholder, int position) {
		SharingFileWanted file = mDataList.get(position);
		viewholder.mSharingFile = file;
		viewholder.itemNameTextView.setText(file.getFileName());
		viewholder.itemUploaderTextView.setText(file.getOwnerName());

		int pages = file.getFilePages();
		int width = file.getWidth();
		int height = file.getHeith();
		String w_h = (width == 0 || height == 0) ? "" : width + "x" + height;
		viewholder.itemPagesTextView.setText(FileManager.isPicture(file.getFileType()) ? w_h : (pages + "页"));

		Drawable drawable = mContext.getResources().getDrawable(R.drawable.default_logo_grey);
		ImageLoaderUtil.refreshFileThumbnail(drawable, file.getFileThumbnail(), viewholder.itemThumbnailImageView);
	}

	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_rcv_invite_sharingfile, parent, false);
		return new ItemViewHolder(view);
	}
}
