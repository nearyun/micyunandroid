package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.micyun.R;
import com.micyun.model.FileManager;
import com.micyun.ui.PreviewActivity;
import com.micyun.util.ImageLoaderUtil;
import com.ncore.model.sharing.SharingFile;
import com.tornado.util.DensityUtil;

public class SampleSharingFileRecycleViewAdapter extends
		BaseRecyclerViewAdapter<SampleSharingFileRecycleViewAdapter.ItemViewHolder, SharingFile> {

	private RecyclerView sharingFileRecyclerView;
	public SampleSharingFileRecycleViewAdapter(Context context, RecyclerView rcview) {
		super(context, new ArrayList<SharingFile>());
		sharingFileRecyclerView = rcview;
	}

	public void refreshData(ArrayList<SharingFile> data) {
		mDataList.clear();
		mDataList.addAll(data);
		notifyDataSetChanged();
	}

	public void notifyDataSetChangedToResize() {
		super.notifyDataSetChanged();
		int count = getItemCount();
		sharingFileRecyclerView.getLayoutParams().height = DensityUtil.dp2px(64, mContext) * count;
	}

	protected class ItemViewHolder extends BaseRecycleViewHolder {
		public View rootView;
		public SharingFile mSharingFile;
		public ImageView itemThumbnailImageView;
		public TextView itemNameTextView;
		public TextView itemUploaderTextView;
		public TextView itemPagesTextView;

		public ItemViewHolder(View view) {
			super(view);
			view.setOnClickListener(this);
			itemThumbnailImageView = (ImageView) view.findViewById(R.id.itemThumbnailImageView);
			itemNameTextView = (TextView) view.findViewById(R.id.itemNameTextView);
			itemUploaderTextView = (TextView) view.findViewById(R.id.itemUploaderTextView);
			itemPagesTextView = (TextView) view.findViewById(R.id.itemPagesTextView);
		}

		@Override
		public void onClick(View v) {
			PreviewActivity.newInstance(mContext, mSharingFile.getDocId(), mSharingFile.getSessionId(),
					mSharingFile.getFileName());
		}

	}

	@Override
	public void onBindViewHolder(ItemViewHolder viewholder, int position) {
		SharingFile file = mDataList.get(position);
		viewholder.mSharingFile = file;
		viewholder.itemNameTextView.setText(file.getFileName());
		viewholder.itemUploaderTextView.setText(file.getOwnerName());

		int pages = file.getFilePages();
		int width = file.getWidth();
		int height = file.getHeith();
		String w_h = (width == 0 || height == 0) ? "" : width + "x" + height;
		viewholder.itemPagesTextView.setText(FileManager.isPicture(file.getFileType()) ? w_h : (pages + "页"));

		Drawable drawable = mContext.getResources().getDrawable(R.drawable.default_logo_grey);
		ImageLoaderUtil.refreshFileThumbnail(drawable, file.getFileThumbnail(), viewholder.itemThumbnailImageView);
	}

	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_rcv_sample_sharingfile, parent, false);
		return new ItemViewHolder(view);
	}
}
