package com.micyun.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.micyun.R;
import com.ncore.model.InvitePhoneInfo;
import com.ncore.model.Participant;
import com.ncore.model.SipDevice;
import com.ncore.model.client.impl.Client;
import com.tornado.util.NetworkUtil;

public class MemberOperation2 {
	// # 创 主 与
	// 创 1 1 1
	// 主 1 1 1
	// 与 1 1 1

	private static final boolean[][] MUTE_TABLE = { { true, true, true }, { true, true, true }, { false, false, false } };

	private static final boolean[][] UNMUTE_TABLE = { { true, true, true }, { true, true, true },
			{ false, false, false } };

	private static final boolean[][] CALL_AGAIN_TABLE = { { true, true, true }, { true, true, true },
			{ true, true, true } };

	private static final boolean[][] CANCEL_CALL_TABLE = { { true, true, true }, { true, true, true },
			{ true, true, true } };

	private static final boolean[][] SET_CONTROLLER_TABLE = { { false, false, true }, { false, false, true },
			{ false, false, false } };

	private static final boolean[][] CANCEL_CONTROLLER_TABLE = { { false, true, false }, { false, false, false },
			{ false, false, false } };

	private static final boolean[][] KICK_MEMBER_TABLE = { { false, true, true }, { false, false, true },
			{ false, false, false } };

	public static final void executeOperation(final Context context, final String conferenceId, final String sipUser,
			final Participant participant, final View view, String controller, String owner, String currentUserId) {
		if (participant == null || view == null)
			return;

		/** 我是不是拥有者 */
		boolean thisIsOwner = TextUtils.equals(owner, currentUserId);
		/** 我是不是主持人 */
		boolean thisIsController = TextUtils.equals(controller, currentUserId);

		int operater = 0;
		if (thisIsOwner) {
			operater = 0;
		} else if (thisIsController) {
			operater = 1;
		} else {
			operater = 2;
		}

		/** 被操作者是不是拥有者 */
		boolean thatIsOwner = TextUtils.equals(owner, participant.getUserId());
		/** 被操作者是不是主持人 */
		boolean thatIsController = TextUtils.equals(controller, participant.getUserId());
		/** 这个与会者是不是我自己 */
		boolean thisIsMe = TextUtils.equals(currentUserId, participant.getUserId());

		int target = 0;
		if (thatIsOwner) {
			target = 0;
		} else if (thatIsController) {
			target = 1;
		} else {
			target = 2;
		}

		boolean muteMemberFlag = MUTE_TABLE[operater][target];
		boolean unmuteMemberFlag = UNMUTE_TABLE[operater][target];

		boolean setControllerFlag = SET_CONTROLLER_TABLE[operater][target];
		boolean cancelControllerFlag = CANCEL_CONTROLLER_TABLE[operater][target];

		boolean callAgainMemberFlag = CALL_AGAIN_TABLE[operater][target];
		boolean cancalCallMemberFlag = CANCEL_CALL_TABLE[operater][target];

		boolean kickMemberFlag = KICK_MEMBER_TABLE[operater][target];

		boolean thatIsAnswered = participant.isAnswered();
		// 语音通了
		boolean thatIsMuted = participant.isMute();

		PopupMenu popup = new PopupMenu(context, view);

		if (thatIsAnswered) {
			if (setControllerFlag)
				popup.getMenu().add(Menu.NONE, MENU_ITEM_CHANGE_CONTROLLER, Menu.NONE, "设为主讲人");
			if (cancelControllerFlag)
				popup.getMenu().add(Menu.NONE, MENU_ITEM_REMOVE_CONTROLLER, Menu.NONE, "收回主讲权");
			if (thatIsMuted) {
				if (unmuteMemberFlag)
					popup.getMenu().add(Menu.NONE, MENU_ITEM_UNMUTE_MEMBER, Menu.NONE, "解除静音");
			} else {
				if (muteMemberFlag)
					popup.getMenu().add(Menu.NONE, MENU_ITEM_MUTE_MEMBER, Menu.NONE, "静音");
			}
			popup.getMenu().add(0, MENU_ITEM_HAND_UP, 0, "挂断");
		} else {
			showOfflineMenu(conferenceId, participant, thisIsMe, popup, sipUser, kickMemberFlag);
		}

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				executeMenuItemClickEvent(context, conferenceId, item.getItemId(), participant, sipUser);
				return true;
			}
		});
		popup.show();
	}

	private static final void showOfflineMenu(final String conferenceId, final Participant participant,
			boolean thisIsMe, PopupMenu popup, final String sipUser, boolean kickMemberFlag) {
		SipDevice[] mSipDevices = participant.getSipDevices();
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] == null)
				continue;
			int state = mSipDevices[i].getCallState();
			if (state == SipDevice.CALL_STATE_FAILURE || state == SipDevice.CALL_STATE_UNREACHABLE
					|| state == SipDevice.CALL_STATE_DEFAULT || state == SipDevice.CALL_STATE_BUSY) {
				if (thisIsMe) {
					popup.getMenu().add(0, MENU_ITEM_CALL_MYSELF, 0, "重呼");
				} else {
					popup.getMenu().add(0, MENU_ITEM_CALL_BY_VOIP, 0, "重呼");
					if (Client.getInstance().getUser().hasPSTN())
						popup.getMenu().add(0, MENU_ITEM_CALL_BY_PSTN, 0, "重呼普通电话");
				}
				if (kickMemberFlag)
					popup.getMenu().add(Menu.NONE, MENU_ITEM_KICK_MEMBER, Menu.NONE, "移除");
			} else if (state == SipDevice.CALL_STATE_CALLING) {
				popup.getMenu().add(0, MENU_ITEM_CANCEL_CALL, 0, "取消呼叫");
			}
			return;
		}
		if (thisIsMe) {
			popup.getMenu().add(0, MENU_ITEM_CALL_MYSELF, 0, "重呼");
		} else {
			popup.getMenu().add(0, MENU_ITEM_CALL_BY_VOIP, 0, "重呼");
			if (Client.getInstance().getUser().hasPSTN())
				popup.getMenu().add(0, MENU_ITEM_CALL_BY_PSTN, 0, "重呼普通电话");
		}
		if (kickMemberFlag)
			popup.getMenu().add(Menu.NONE, MENU_ITEM_KICK_MEMBER, Menu.NONE, "移除");

	}

	/** 设为主讲人 */
	private static final int MENU_ITEM_CHANGE_CONTROLLER = Menu.FIRST;
	/** 收回主讲权 */
	private static final int MENU_ITEM_REMOVE_CONTROLLER = Menu.FIRST + 1;
	/** 静音 */
	private static final int MENU_ITEM_MUTE_MEMBER = Menu.FIRST + 2;
	/** 解除静音 */
	private static final int MENU_ITEM_UNMUTE_MEMBER = Menu.FIRST + 3;
	/** 移除 */
	private static final int MENU_ITEM_KICK_MEMBER = Menu.FIRST + 4;
	/** 重呼voip */
	private static final int MENU_ITEM_CALL_BY_VOIP = Menu.FIRST + 5;
	/** 重呼pstn */
	private static final int MENU_ITEM_CALL_BY_PSTN = Menu.FIRST + 6;
	/** 取消呼叫 */
	private static final int MENU_ITEM_CANCEL_CALL = Menu.FIRST + 7;
	/** 挂断 */
	private static final int MENU_ITEM_HAND_UP = Menu.FIRST + 8;
	/** 重呼自己 */
	private static final int MENU_ITEM_CALL_MYSELF = Menu.FIRST + 9;

	private static final void executeMenuItemClickEvent(Context context, String conferenceId, final int itemId,
			Participant participant, String sipUser) {
		if (itemId == MENU_ITEM_CHANGE_CONTROLLER) {
			Client.getInstance().changeController(conferenceId, participant.getUserId(), null);
		} else if (itemId == MENU_ITEM_MUTE_MEMBER) {
			Client.getInstance().muteMember(conferenceId, participant.getSequenceNumber(), participant.getUserId(),
					null);
		} else if (itemId == MENU_ITEM_UNMUTE_MEMBER) {
			Client.getInstance().unmuteMember(conferenceId, participant.getSequenceNumber(), participant.getUserId(),
					null);
		} else if (itemId == MENU_ITEM_KICK_MEMBER) {
			kickMember(conferenceId, participant);
		} else if (itemId == MENU_ITEM_REMOVE_CONTROLLER) {
			Client.getInstance().removeController(conferenceId, null);
		} else if (itemId == MENU_ITEM_CALL_BY_VOIP) {
			ArrayList<InvitePhoneInfo> phones = new ArrayList<InvitePhoneInfo>();
			phones.add(new InvitePhoneInfo(participant.getNickName(), participant.getMobile()));
			Client.getInstance().sendInvitation(conferenceId, sipUser, phones, false, null);
		} else if (itemId == MENU_ITEM_CALL_BY_PSTN) {
			ArrayList<InvitePhoneInfo> phones = new ArrayList<InvitePhoneInfo>();
			phones.add(new InvitePhoneInfo(participant.getNickName(), participant.getMobile()));
			Client.getInstance().sendInvitation(conferenceId, sipUser, phones, true, null);
		} else if (itemId == MENU_ITEM_CANCEL_CALL) {
			if (participant.isOnline()) {
				Client.getInstance().cancelInvite(conferenceId, participant.getMobile(), null);
			}
		} else if (itemId == MENU_ITEM_HAND_UP) {
			Client.getInstance().handupMember(conferenceId, participant.getSequenceNumber(), participant.getUserId(),
					participant.getMobile(), null);
		} else if (itemId == MENU_ITEM_CALL_MYSELF) {
			Client.getInstance().requestCallin(conferenceId, Client.getInstance().getUser().getVoip(),
					Client.getInstance().getUser().getVoip(), Client.getInstance().getUser().getNickname(), null);
		}
	}

	protected boolean isNetworkAvailable(Context context) {
		if (NetworkUtil.IsNetWorkEnable(context)) {
			return true;
		} else {
			Toast.makeText(context, R.string.network_not_available, Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	/**
	 * 移除与会者
	 * 
	 * @param conferenceId
	 * @param participant
	 */
	private static final void kickMember(String conferenceId, Participant participant) {
		Client.getInstance().removeMember(conferenceId, participant.getSequenceNumber(), participant.getUserId(), null);
		// Client.getInstance().kickMember(conferenceId,
		// participant.getSequenceNumber(), participant.getUserId(), null);
	}

	/**
	 * 移除android设备
	 * 
	 * @param conferenceId
	 * @param participant
	 */
	@SuppressWarnings("unused")
	private static final void kickAndroidDevice(String conferenceId, Participant participant) {
		String deviceId = participant.getAndroidDeviceId();
		if (!TextUtils.isEmpty(deviceId))
			Client.getInstance().kickMemberDevice(conferenceId, participant.getSequenceNumber(), deviceId, null);
	}

	/**
	 * 移除ios设备
	 * 
	 * @param conferenceId
	 * @param participant
	 */
	@SuppressWarnings("unused")
	private static final void kickIosDevice(String conferenceId, Participant participant) {
		String deviceId = participant.getAppleDeviceId();
		if (!TextUtils.isEmpty(deviceId))
			Client.getInstance().kickMemberDevice(conferenceId, participant.getSequenceNumber(), deviceId, null);
	}

	/**
	 * 移除pc设备
	 * 
	 * @param conferenceId
	 * @param participant
	 */
	@SuppressWarnings("unused")
	private static final void kickPcDevice(String conferenceId, Participant participant) {
		String deviceId = participant.getPCDeviceId();
		if (!TextUtils.isEmpty(deviceId))
			Client.getInstance().kickMemberDevice(conferenceId, participant.getSequenceNumber(), deviceId, null);
	}
}
