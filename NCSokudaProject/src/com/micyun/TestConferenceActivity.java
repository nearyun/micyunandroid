package com.micyun;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.micyun.model.ConferenceArgument;
import com.micyun.ui.conference.ConferenceMainRoomActivity;
import com.ncore.callback.OnSampleCallback;
import com.ncore.model.client.impl.Client;

public class TestConferenceActivity extends BaseActivity implements OnClickListener {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, TestConferenceActivity.class);
		context.startActivity(intent);
	}

	private Button startButton, stopButton;
	private static String KEY_RUNNING = "KEY_RUNNING";
	private static String FILENAME = "TestConferenceActivity";

	public static final boolean isTesting(Context context) {
		SharedPreferences pre = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		return pre.getBoolean(KEY_RUNNING, false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_conference);

		startButton = (Button) findViewById(R.id.startButton);
		startButton.setOnClickListener(this);
		stopButton = (Button) findViewById(R.id.stopButton);
		stopButton.setOnClickListener(this);

		SharedPreferences pre = getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		boolean isRunning = pre.getBoolean(KEY_RUNNING, false);

		changeVisiable(isRunning);
	}

	private void changeVisiable(boolean isRunning) {
		if (isRunning) {
			startButton.setEnabled(false);
			stopButton.setEnabled(true);
		} else {
			startButton.setEnabled(true);
			stopButton.setEnabled(false);
		}
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if (R.id.startButton == id) {
			doStart();
		} else if (R.id.stopButton == id) {
			doStop();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		doStop();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		SharedPreferences pre = getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		boolean isRunning = pre.getBoolean(KEY_RUNNING, false);
		if (isRunning)
			mHandler.postDelayed(mRunnable, DELAY_TIME);
	}

	private void doStop() {
		changeVisiable(false);
		SharedPreferences pre = getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		pre.edit().putBoolean(KEY_RUNNING, false).commit();
		mHandler.removeCallbacks(mRunnable);
	}

	private int DELAY_TIME = 3000;
	private Handler mHandler = new Handler();
	private Runnable mRunnable = new Runnable() {
		@Override
		public void run() {
			doStart();
		}
	};

	private void doStart() {
		changeVisiable(true);
		SharedPreferences pre = getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		pre.edit().putBoolean(KEY_RUNNING, true).commit();
		Client.getInstance().conveneConference("自动测试的会议", new OnSampleCallback() {

			@Override
			public void onSuccess(String confId) {
				String userId = Client.getInstance().getUser().getUserId();
				String nickName = Client.getInstance().getUser().getNickname();
				ConferenceMainRoomActivity.newInstance(mActivity, new ConferenceArgument(confId, userId, nickName));
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				showToast(reason);
				mHandler.postDelayed(mRunnable, DELAY_TIME);
			}

		});
	}

	private long lastTime = 0L;

	@Override
	public void onBackPressed() {
		long curTime = System.currentTimeMillis();
		if (curTime - lastTime > 2000) {
			lastTime = curTime;
			Toast.makeText(this, "再按一次，结束测试并退出", Toast.LENGTH_LONG).show();
		} else {
			finish();
		}
	}
}
