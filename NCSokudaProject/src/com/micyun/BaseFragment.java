package com.micyun;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ncore.util.LogUtil;
import com.tornado.util.NetworkUtil;

public abstract class BaseFragment extends Fragment {
	protected String TAG = BaseFragment.class.getSimpleName();
	protected Activity mActivity;
	protected View mRootView;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		TAG = getClass().getSimpleName();
		mActivity = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	/**
	 * 显示toast信息
	 * 
	 * @param context
	 * @param msg
	 */
	protected void showToast(String msg) {
		Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
		LogUtil.i(TAG, "showToast:" + msg);
	}

	/**
	 * 判断当前网络是否可用
	 * 
	 * @return
	 */
	protected boolean isNetworkAvailable() {
		if (NetworkUtil.IsNetWorkEnable(mActivity)) {
			return true;
		} else {
			showToast(getString(R.string.network_not_available));
			return false;
		}
	}
}
