package com.micyun.common;

public class Contansts {

	/**
	 * 参考assets下的microlog.properties文件</p>
	 * 
	 * microlog.appender.FileAppender.File
	 */
	public static final String DIRECTORY_LOG = "logs";

	/**
	 * voip的log目录
	 */
	public static final String DIRECTORY_VOIP_LOG = "vlogs";

	/**
	 * voip的log文件前缀
	 */
	public static final String PREFIX_LOG_FILE = "vlog_";
}
