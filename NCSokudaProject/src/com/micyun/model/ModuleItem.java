package com.micyun.model;

/**
 * 模块
 * 
 * @author xiaohua
 * 
 */
public abstract class ModuleItem {
	private int icon;
	private String name;

	public ModuleItem(int icon, String name) {
		this.icon = icon;
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public String getName() {
		return name;
	}

	public abstract void executeOnClick();
}
