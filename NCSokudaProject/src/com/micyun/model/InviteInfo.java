package com.micyun.model;

import org.json.JSONObject;

public class InviteInfo {
	// {
	// "nickname": "小华",
	// "userid": "55100263",
	// "state": "0",
	// "invite_time": "2015-05-25 15:43:51",
	// "subject": "测试主题",
	// "confid": "c8f3a9d6-02b1-11e5-a416-00224d7c553b"
	// }

	private String inviteeNickName;
	private String userId;
	private int state;// 0:正在发送；1:发送成功；2：接收邀请；3:拒绝邀请；4：邀请失败，短信发送失败
	private long inviteTime;
	private String subject;
	private String confId;

	public InviteInfo(JSONObject json) {
		inviteeNickName = json.optString("nickname");
		userId = json.optString("userid");
		state = json.optInt("state");
		inviteTime = json.optLong("invite_time");
		subject = json.optString("subject");
		confId = json.optString("confid");
	}

	public String getInviteeNickName() {
		return inviteeNickName;
	}

	public String getUserId() {
		return userId;
	}

	public int getState() {
		return state;
	}

	public long getInviteTime() {
		return inviteTime;
	}

	public String getSubject() {
		return subject;
	}

	public String getConfId() {
		return confId;
	}

}
