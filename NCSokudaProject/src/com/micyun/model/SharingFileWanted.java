package com.micyun.model;

import org.json.JSONObject;

import com.ncore.model.sharing.SharingFile;

public class SharingFileWanted {
	private static final int TYPE_FROM_NETBOX = 1;
	private static final int TYPE_FROM_COPY = 2;

	private int type = -1;
	private SharingFile mSharingFile = null;

	public static final SharingFileWanted createFromCopy(JSONObject json) {
		SharingFile s = SharingFile.create(-1, json);
		return new SharingFileWanted(TYPE_FROM_COPY, s);
	}

	public static final SharingFileWanted createFromNetBox(JSONObject json) {
		SharingFile s = SharingFile.create(-1, json);
		return new SharingFileWanted(TYPE_FROM_NETBOX, s);
	}

	private SharingFileWanted(int _type, SharingFile file) {
		type = _type;
		mSharingFile = file;
	}

	public boolean isDefaultFile() {
		return SharingFile.isDefaultFile(mSharingFile.getDocId());
	}

	public int getFromType() {
		return type;
	}

	public String getId() {
		return TYPE_FROM_COPY == type ? mSharingFile.getSessionId() : mSharingFile.getDocId();
	}

	public String getDocId() {
		return mSharingFile.getDocId();
	}

	public String getSessionId() {
		return mSharingFile.getSessionId();
	}

	public String getFileName() {
		return mSharingFile.getFileName();
	}

	public String getOwnerName() {
		return mSharingFile.getOwnerName();
	}

	public int getFilePages() {
		return mSharingFile.getFilePages();
	}

	public int getWidth() {
		return mSharingFile.getWidth();
	}

	public int getHeith() {
		return mSharingFile.getHeith();
	}

	public String getFileType() {
		return mSharingFile.getFileType();
	}

	public String getFileThumbnail() {
		return mSharingFile.getFileThumbnail();
	}

}
