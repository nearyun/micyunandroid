package com.micyun.model.upload;

import java.io.File;
import java.io.Serializable;

import com.micyun.model.FileManager;

/**
 * 文档上传的模型
 * 
 * @author xiaohua
 * 
 */
public class UploadFileInfo implements Serializable {

	private static final long serialVersionUID = 1024777579585244552L;
	/** 上传中 */
	public static final int TYPE_UPLOADING = 0;
	/** 等待上传 */
	public static final int TYPE_WAITTING = 1;
	/** 上传失败 */
	public static final int TYPE_FAILURE = 2;
	/** 取消上传 */
	public static final int TYPE_CANCEL = 3;
	/** 上传成功 */
	public static final int TYPE_SUCCESS = 4;
	/** 文件找不到 */
	public static final int TYPE_NOT_FOUND = 5;

	/** 数据库赋予的唯一ID */
	private int id;
	/** 文件名 */
	private String name;
	/** 文件全路径 */
	private String path;
	/** 扩展名 */
	private String extension;
	/** 文件状态 */
	private int state;
	/** 文件上传进度 */
	private long progress;
	/** 文件大小 */
	private long size;

	public UploadFileInfo() {

	}

	public UploadFileInfo(String path) {
		File file = new File(path);
		this.name = file.getName();
		this.path = path;
		this.extension = FileManager.getFileMineType(name);
		this.size = file.length();
		this.state = file.exists() ? TYPE_WAITTING : TYPE_NOT_FOUND;
	}

	public UploadFileInfo(String name, String path, String extension, long size) {
		this.name = name;
		this.path = path;
		this.extension = extension;
		this.size = size;
		this.state = TYPE_WAITTING;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public long getProgress() {
		return progress;
	}

	public void setProgress(long progress) {
		this.progress = progress;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "UploadFileInfo [id=" + id + ", name=" + name + ", path=" + path + ", extension=" + extension
				+ ", state=" + state + ", progress=" + progress + ", size=" + size + "]";
	}

}
