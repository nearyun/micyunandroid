package com.micyun.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.os.Environment;
import android.text.TextUtils;

import com.micyun.R;

public class FileManager {
	private String mRootFilePath; // 根的路径
	private String mcurrentFilePath; // 当前显示的路径

	public FileManager() {
		mRootFilePath = Environment.getExternalStorageDirectory().getPath();
	}

	public FileManager(String rootPath) {
		mRootFilePath = rootPath;
	}

	public String getRootPath() {
		return mRootFilePath;
	}

	public String getCurrentFilePath() {
		return mcurrentFilePath;
	}

	/**
	 * 当前路径是否是根路径
	 * 
	 * @return
	 */
	public boolean locatedInRootPath() {
		return TextUtils.equals(mRootFilePath, mcurrentFilePath);
	}

	/**
	 * 上一级目录
	 * 
	 * @return 存在上一级目录时，返回上一级目录；当前已经是根目录，则返回根目录
	 */
	public ArrayList<LocalFile> backspace() {
		File file = new File(mcurrentFilePath);

		if (TextUtils.equals(mRootFilePath, mcurrentFilePath))
			return getFileList(mRootFilePath);
		else
			return getFileList(file.getParent());
	}

	public ArrayList<LocalFile> getFileList(String filePath) {
		mcurrentFilePath = filePath;
		ArrayList<LocalFile> fileList = new ArrayList<LocalFile>();

		File[] files = scanFiles(filePath);

		if (files == null)
			return fileList;

		for (int i = 0; i < files.length; i++) {
			if (files[i].isHidden()) // 不显示隐藏文件
				continue;

			String fileName = files[i].getName();
			boolean isDirectory = files[i].isDirectory();
			String extension = getFileMineType(fileName);
			if (!isDirectory && !isValid(extension))
				continue;

			LocalFile fileInfo = new LocalFile(files[i].getAbsolutePath());
			fileList.add(fileInfo);
		}
		Collections.sort(fileList, new Comparator<LocalFile>() {

			@Override
			public int compare(LocalFile lhs, LocalFile rhs) {
				return lhs.getName().compareToIgnoreCase(rhs.getName());
			}

		});

		Collections.sort(fileList, new Comparator<LocalFile>() {

			@Override
			public int compare(LocalFile lhs, LocalFile rhs) {
				boolean lFlag = lhs.isDirectory();
				boolean rFlag = rhs.isDirectory();
				if ((lFlag && rFlag) || (!lFlag && !rFlag)) {
					return 0;
				} else if (lFlag && !rFlag) {
					return -1;
				} else {
					return 1;
				}
			}

		});
		return fileList;
	}

	public static String getFileMineType(String filename) {
		int dotPos = filename.lastIndexOf('.');
		if (0 <= dotPos) {
			return filename.substring(dotPos + 1);
		} else {
			return "";
		}
	}

	/**
	 * 扫描指定路径的目录与文件
	 * 
	 * @param path
	 * @return
	 */
	private File[] scanFiles(String path) {
		File file = new File(path);
		File[] files = file.listFiles();
		return files;
	}

	/**
	 * 判断是否本应用需要的文件
	 * 
	 * @param extension
	 * @return
	 */
	private boolean isValid(String extension) {
		return TextUtils.equals(extension, "doc") || TextUtils.equals(extension, "docx")
				|| TextUtils.equals(extension, "ppt") || TextUtils.equals(extension, "pptx")
				|| TextUtils.equals(extension, "jpg") || TextUtils.equals(extension, "JPG")
				|| TextUtils.equals(extension, "jpeg") || TextUtils.equals(extension, "JPEG")
				|| TextUtils.equals(extension, "xls") || TextUtils.equals(extension, "xlsx")
				|| TextUtils.equals(extension, "png") || TextUtils.equals(extension, "PNG")
				|| TextUtils.equals(extension, "txt");
	}

	public static boolean isPicture(String type) {
		return (TextUtils.equals(type, "png") || TextUtils.equals(type, "PNG") || TextUtils.equals(type, "jpg")
				|| TextUtils.equals(type, "JPG") || TextUtils.equals(type, "jpeg") || TextUtils.equals(type, "JPEG"));
	}

	public static int getFormatIconResource(String extension) {
		switch (extension) {
		case "txt":
			return R.drawable.ic_share_txt;
		case "pdf":
			return R.drawable.ic_share_pdf;
		case "xls":
		case "xlsx":
			return R.drawable.ic_share_xls;
		case "doc":
		case "docx":
			return R.drawable.ic_share_doc;
		case "ppt":
		case "pptx":
			return R.drawable.ic_share_ppt;
		case "jpg":
		case "JPG":
		case "jpeg":
		case "JPEG":
		case "png":
		case "PNG":
			return R.drawable.ic_share_image;
		default:
			return R.drawable.ic_share_unknown;
		}
	}

}
