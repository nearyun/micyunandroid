package com.micyun.model;

import com.ncore.model.sharing.NetworkFileInfo;

public class NetboxFile {

	private NetworkFileInfo mNetworkFile;

	private boolean hasAdd = false;
	private boolean isSelected = false;

	public NetboxFile(NetworkFileInfo file) {
		mNetworkFile = file;
	}

	public NetworkFileInfo getNetworkFileInfo() {
		return mNetworkFile;
	}

	public void setSelected(boolean flag) {
		isSelected = flag;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setHasAdd(boolean flag) {
		hasAdd = flag;
	}

	public boolean hasAdd() {
		return hasAdd;
	}

}
