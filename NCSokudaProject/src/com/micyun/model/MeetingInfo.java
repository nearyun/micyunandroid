package com.micyun.model;

import org.json.JSONObject;

/**
 * 会议基本信息，包含自主召开与被邀请的类型
 * 
 * @author xiaohua
 * 
 */
public class MeetingInfo {
	private static final String LABEL_CONF_ID = "confid";
	private static final String LABEL_CONF_NO = "confno";
	private static final String LABEL_SUBJECT = "subject";
	private static final String LABEL_STATE = "state";
	private static final String LABEL_OPENTIME = "opentime";
	private static final String LABEL_NICKNAME = "nickname";
	private static final String LABEL_MINE = "mine";
	private static final String LABEL_TYPE = "type";
	private static final String LABEL_INVITER_ID = "inviterid";
	private static final String LABEL_AVATAR = "avatar";
	private static final String LABEL_TEXT_AVATAR = "text_avatar";

	public static final int TYPE_WAIT_COMFIRM = 0;

	private String confId;
	private String subject;
	private String confNo;
	private int state;
	private String ownerName;
	private long opentime;
	private boolean isMine = false;
	private int type = 0;// 0:未参加，待确定；1:已参加
	private String inviterId;
	private String avatar;
	private String textAvatar;

	private boolean isEmpty = false;

	public MeetingInfo() {
		isEmpty = true;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public MeetingInfo(JSONObject json) {
		isEmpty = false;
		confId = json.optString(LABEL_CONF_ID);
		confNo = json.optString(LABEL_CONF_NO);
		subject = json.optString(LABEL_SUBJECT);
		state = json.optInt(LABEL_STATE);
		opentime = json.optLong(LABEL_OPENTIME);
		ownerName = json.optString(LABEL_NICKNAME);
		isMine = json.optBoolean(LABEL_MINE);
		type = json.optInt(LABEL_TYPE);
		inviterId = json.optString(LABEL_INVITER_ID);
		avatar = json.optString(LABEL_AVATAR);
		textAvatar = json.optString(LABEL_TEXT_AVATAR);
	}

	public String getInviterId() {
		return inviterId;
	}

	public int getType() {
		return type;
	}

	public boolean isMine() {
		return isMine;
	}

	public String getConfId() {
		return confId;
	}

	public String getSubject() {
		return subject;
	}

	public String getConfNo() {
		return confNo;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public int getState() {
		return state;
	}

	public long getOpentime() {
		return opentime;
	}

	public String getAvatar() {
		return avatar;
	}

	public String getTextAvatar() {
		return textAvatar;
	}
}
