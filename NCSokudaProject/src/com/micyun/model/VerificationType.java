package com.micyun.model;

public interface VerificationType {
	public int TYPE_REGISTER = 0;
	public int TYPE_FIND_PASSWORD = 1;
	public int TYPE_BINDING_PHONE = 2;
}
