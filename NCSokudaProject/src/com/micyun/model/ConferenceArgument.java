package com.micyun.model;

import android.text.TextUtils;

/**
 * 进入会议必须的参数
 * 
 * @author xiaohua
 * 
 */
public class ConferenceArgument extends BaseConferenceArgument {

	private static final long serialVersionUID = 4218578063607748831L;

	/**
	 * 会议发起人
	 */
	private String owner;

	/**
	 * 主讲人
	 */
	private String controller;

	/**
	 * 当前正在共享
	 */
	private String displaying;

	public ConferenceArgument(String conferenceId, String userId, String nickName) {
		super(conferenceId, userId, nickName);
	}

	/**
	 * 获取共享信息
	 * 
	 * @return
	 */
	public String getDisplaying() {
		return displaying;
	}

	/**
	 * 设置共享信息
	 * 
	 * @param displaying
	 */
	public void setDisplaying(String displaying) {
		synchronized (this) {
			this.displaying = displaying;
		}
	}

	/**
	 * 获取主讲人
	 * 
	 * @return
	 */
	public String getController() {
		return controller;
	}

	/**
	 * 设置主讲人
	 * 
	 * @param controller
	 */
	public void setController(String controller) {
		this.controller = controller;
	}

	/**
	 * 是否存在主讲人
	 * 
	 * @return
	 */
	public boolean hasController() {
		return !TextUtils.isEmpty(controller);
	}

	/**
	 * 设置发起人id
	 * 
	 * @param owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * 获取会议发起人id
	 * 
	 * @return
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * 存在发起人
	 * 
	 * @return
	 */
	public boolean hasOwner() {
		return !TextUtils.isEmpty(owner);
	}

	/**
	 * 判断是否有控制权
	 * 
	 * @return
	 */
	public boolean hasControlPermission() {
		return TextUtils.equals(userId, controller) || TextUtils.equals(userId, owner);
	}

}
