package com.micyun.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.text.TextUtils;

import com.tornado.util.TimeUtil;

public class MeetingGroup {
	private ArrayList<MeetingInfo> mDataList = new ArrayList<MeetingInfo>();

	private String opentime;
	private int count;

	private boolean isDisplayDate = true;

	private String title = "";
	private long seq = 0;

	public long getSeq() {
		return seq;
	}

	public MeetingGroup(String today) {
		opentime = today;
		count = 0;
		title = convert2Title();
	}

	public MeetingGroup(JSONObject json) {
		opentime = json.optString("opentime");
		count = json.optInt("c");
		title = convert2Title();
	}

	private String convert2Title() {
		if (isDataFormat(opentime)) {
			isDisplayDate = false;
			seq = TimeUtil.stringToLong(opentime, "yyyy-MM-dd");
			String current = TimeUtil.getCurrentTime("yyyy-MM-dd");
			if (TextUtils.equals(current, opentime)) {
				return "今天";
			} else {
				try {
					int days = TimeUtil.getDateSpace(current, opentime);
					if (days == 1)
						return "昨天";
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return TimeUtil.getWeekOfData(opentime, "yyyy-MM-dd");
			}
		} else if (TextUtils.equals(opentime, "lastweek")) {
			seq = -1;
			return "上周";
		} else {
			seq = -2;
			return "更早";
		}
	}

	public String getConferenceSelectRange() {
		if (isDataFormat(opentime)) {
			return TimeUtil.stringToLong(opentime, "yyyy-MM-dd") / 1000 + "";
		} else {
			return opentime;
		}
	}

	public boolean isDisplayDate() {
		return isDisplayDate;
	}

	private boolean isDataFormat(String date) {
		String eL = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(date);
		return m.matches();
	}

	public String getTitle() {
		return title;
	}

	public int getCount() {
		return count;
	}

	public int size() {
		return mDataList.size();
	}

	public MeetingInfo getItem(int position) {
		return mDataList.get(position);
	}

	public void add(MeetingInfo meeting) {
		mDataList.add(meeting);
	}

	public void delete(MeetingInfo meeting) {
		mDataList.remove(meeting);
	}

	public void delete(int position) {
		mDataList.remove(position);
	}

	public void clear() {
		mDataList.clear();
	}

	public void sort() {
		Collections.sort(mDataList, new Comparator<MeetingInfo>() {

			@Override
			public int compare(MeetingInfo lhs, MeetingInfo rhs) {
				if (lhs.getOpentime() > rhs.getOpentime())
					return -1;
				else if (lhs.getOpentime() == rhs.getOpentime())
					return 0;
				else
					return 1;
			}

		});
	}
}
