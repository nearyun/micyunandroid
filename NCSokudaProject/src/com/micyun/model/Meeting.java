package com.micyun.model;

import org.json.JSONObject;

/**
 * 我的会议基本信息
 * 
 * @author xiaohua
 * 
 */
public class Meeting {
	private static final String LABEL_CONF_ID = "confid";
	private static final String LABEL_NAME = "name";
	private static final String LABEL_SUBJECT = "subject";
	private static final String LABEL_PASSWORD = "password";
	private static final String LABEL_STATE = "state";
	private String confId;
	private String subject;
	private String name;
	private String password;
	private int state;
	private String ownerName;
	private long opentime;

	public Meeting() {

	}

	public Meeting(JSONObject json) {
		confId = json.optString(LABEL_CONF_ID);
		name = json.optString(LABEL_NAME);
		subject = json.optString(LABEL_SUBJECT);
		password = json.optString(LABEL_PASSWORD);
		state = json.optInt(LABEL_STATE);
	}

	public String getConfId() {
		return confId;
	}

	public String getSubject() {
		return subject;
	}

	public String getName() {
		return name;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public String getPassword() {
		return password;
	}

	public int getState() {
		return state;
	}

	public long getOpentime() {
		return opentime;
	}
}
