package com.micyun.model;

import java.io.Serializable;

public abstract class BaseConferenceArgument implements Serializable {

	private static final long serialVersionUID = -2865421681021181029L;
	/**
	 * 会议id
	 */
	protected String conferenceId;
	/**
	 * 当前操作者
	 */
	protected String userId;
	/**
	 * 当前操作者昵称
	 */
	protected String nickName;

	public BaseConferenceArgument(String conferenceId, String userId, String nickName) {
		this.conferenceId = conferenceId;
		this.userId = userId;
		this.nickName = nickName;
	}

	public String getUserNickName() {
		return nickName;
	}

	/**
	 * 获取会议id
	 * 
	 * @return
	 */
	public String getConferenceId() {
		return conferenceId;
	}

	/**
	 * 获取当前用户id
	 * 
	 * @return
	 */
	public String getCurrentUserId() {
		return userId;
	}
}
