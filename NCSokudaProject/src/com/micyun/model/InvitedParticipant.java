package com.micyun.model;

import org.json.JSONObject;

import com.ncore.model.Participant;

public class InvitedParticipant {

	public static final InvitedParticipant create(int seq, JSONObject json) {
		Participant p = Participant.create(seq, json);
		InvitedParticipant ip = new InvitedParticipant(p);
		return ip;
	}

	public static final InvitedParticipant create(String nickName, String phone) {
		InvitedParticipant ip = new InvitedParticipant(nickName, phone);
		return ip;
	}

	private Participant mParticipant;

	private InvitedParticipant(Participant p) {
		mParticipant = p;
	}

	private String _nickName = "";
	private String _phone = "";

	private InvitedParticipant(String nickName, String phone) {
		this._nickName = nickName;
		this._phone = phone;
	}

	public String getUserId() {
		return mParticipant != null ? mParticipant.getUserId() : null;
	}

	public String getNickName() {
		return mParticipant != null ? mParticipant.getNickName() : _nickName;
	}

	public String getPhone() {
		return mParticipant != null ? mParticipant.getMobile() : _phone;
	}

	public String getAvatar() {
		return mParticipant != null ? mParticipant.getAvatar() : null;
	}
}
