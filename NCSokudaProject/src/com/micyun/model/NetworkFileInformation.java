package com.micyun.model;

import com.ncore.model.sharing.NetworkFileInfo;

public class NetworkFileInformation {
	private NetworkFileInfo mNetworkFile;
	private boolean isInSharingQueue;

	public NetworkFileInformation(NetworkFileInfo nfi) {
		this(nfi, false);
	}

	public NetworkFileInformation(NetworkFileInfo nfi, boolean isq) {
		mNetworkFile = nfi;
		isInSharingQueue = isq;
	}

	public NetworkFileInfo getNetworkFile() {
		return mNetworkFile;
	}

	public boolean isInSharingQueue() {
		return isInSharingQueue;
	}

	public void setIsInSharingQueue(boolean flag) {
		isInSharingQueue = flag;
	}

}
