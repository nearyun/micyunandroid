package com.micyun.model;

public abstract class PersonInfoItem {

	private int icon;
	private String label;
	private String content;

	public PersonInfoItem(String label, String content) {
		this.label = label;
		this.content = content;
	}

	public PersonInfoItem(String label) {
		this(label, "");
	}

	public int getIcon() {
		return icon;
	}

	public String getLabel() {
		return label;
	}

	public String getContent() {
		return content;
	}

	public abstract void executeCallback();
}
