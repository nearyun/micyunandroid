package com.micyun.model;

import java.io.File;

/**
 * 本地文件，继承于File类，增加文件类型标识，是否被选中标识
 * 
 * @author xiaohua
 * 
 */
public class LocalFile extends File {
	private static final long serialVersionUID = 6860283862355768847L;

	private boolean mSelected = false;
	/** 文件类型 */
	private String mExtension;

	public LocalFile(String path) {
		super(path);
		mExtension = FileManager.getFileMineType(this.getName());
	}

	public void setSelected(boolean flag) {
		mSelected = flag;
	}

	public boolean isSelected() {
		return mSelected;
	}

	/**
	 * 获取文件类型
	 * 
	 * @return
	 */
	public String getExtension() {
		return mExtension;
	}
}
