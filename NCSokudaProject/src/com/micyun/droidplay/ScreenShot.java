package com.micyun.droidplay;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.view.View;

public class ScreenShot {

	public static final Bitmap takeScreenCapture(View view) {
		if (view == null)
			return null;
		// view = view.getRootView();
		try {
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache();
			Bitmap bitmap = view.getDrawingCache();
			if (bitmap == null || bitmap.isRecycled())
				return null;
			Bitmap newBitmap = Bitmap.createBitmap(bitmap);
			return newBitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			view.destroyDrawingCache();
		}
	}

	public static final byte[] bitmap2Bytes(Bitmap bm) {
		if (bm == null)
			return null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();
	}

}
