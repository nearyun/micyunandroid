package com.micyun.listener;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;

public abstract class AnimatorEndListener implements AnimatorListener {

	@Override
	public void onAnimationStart(Animator animation) {

	}

	@Override
	public void onAnimationCancel(Animator animation) {

	}

	@Override
	public void onAnimationRepeat(Animator animation) {

	}

}
