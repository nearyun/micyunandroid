package com.micyun.listener;

import android.support.v4.view.ViewPager.OnPageChangeListener;

public abstract class OnTabPageChangeListener implements OnPageChangeListener {

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}
}
