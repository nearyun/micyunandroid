package com.multi.imageselector.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.os.Environment;

/**
 * 文件操作类
 */
public class FileUtils {

	public static File createTmpFile(Context context) {

		String state = Environment.getExternalStorageState();
		if (state.equals(Environment.MEDIA_MOUNTED)) { // 已挂载
			File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
			if (!pic.exists()) {
				pic.mkdirs();
			}
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
			String fileName = "multi_image_" + timeStamp + ".jpg";
			File tmpFile = new File(pic, fileName);
			return tmpFile;
		} else {
			File cacheDir = context.getCacheDir();
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
			String fileName = "multi_image_" + timeStamp + ".jpg";
			File tmpFile = new File(cacheDir, fileName);
			return tmpFile;
		}

	}

}
