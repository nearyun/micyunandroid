package com.ncore.http.request;

public class HttpResponseCode {
	/** 登录session超时 */
	public static final int LOGIN_TIME_OUT = 401;
	/** 余额不足 */
	public static final int BALANCE_NOT_ENOUGH = 406;

	public static final String parseCode(int code, String defReason) {
		switch (code) {
		case BALANCE_NOT_ENOUGH:
			return "余额不足";
		case LOGIN_TIME_OUT:
			return "登录session超时";
		default:
			return defReason;
		}
	}
}
