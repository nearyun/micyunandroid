package com.ncore.http.request;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONObject;

import com.loopj.android.http.RequestHandle;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.callback.OnUploadCallback;
import com.ncore.model.InvitePhoneInfo;
import com.ncore.model.User;
import com.ncore.model.sharing.NetworkFileInfo;

/**
 * http请求接口
 * 
 * @author xiaohua
 * 
 */
public interface IHttpRequest {

	public void testHttlp(String name, OnCallback cb);

	/**
	 * 注销，清空数据
	 */
	public void logout(OnCallback cb);

	/**
	 * 自动登录
	 * 
	 * @param cb
	 */
	public void autoLogin(OnCallback cb);

	/**
	 * 登录
	 * 
	 * @param name
	 * @param pwd
	 * @param listener
	 */
	public void login(final String name, String pwd, OnCallback cb);

	/**
	 * 保持Cookie有效
	 */
	public void keepalive(OnCallback cb);

	/**
	 * 删除会议
	 * 
	 * @param confId
	 * @param cb
	 */
	public void deleteConference(String confId, OnCallback cb);

	/**
	 * 获取企业通讯录
	 */
	public void obtainEnterpriseContacts(String userId, int version, OnSampleCallback oscb);

	/**
	 * 把用户的信息添加至成员列表
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void addMembersToList(String conferenceId, User user, OnCallback cb);

	/**
	 * 静音某用户
	 * 
	 * @param conferenceId
	 * @param memberId
	 * @param cb
	 */
	public void muteMember(String conferenceId, int memberPosition, String userId, OnCallback cb);

	/**
	 * 对某用户解除静音
	 * 
	 * @param conferenceId
	 * @param memberId
	 * @param cb
	 */
	public void unmuteMember(String conferenceId, int memberPosition, String userId, OnCallback cb);

	/**
	 * 静音某台设备
	 * 
	 * @param conferenceId
	 * @param memberPosition
	 * @param deviceId
	 * @param cb
	 */
	public void muteMemberDevice(String conferenceId, int memberPosition, String deviceId, OnCallback cb);

	/**
	 * 取消静音某台设备
	 * 
	 * @param conferenceId
	 * @param memberPosition
	 * @param deviceId
	 * @param cb
	 */
	public void unmuteMemberDevice(String conferenceId, int memberPosition, String deviceId, OnCallback cb);

	/**
	 * 添加文档到共享列表
	 * 
	 * @param conferenceId
	 * @param nfInfo
	 * @param userId
	 * @param nickName
	 * @param cb
	 */
	public void addDocumentToList(final String conferenceId, final NetworkFileInfo nfInfo, final String userId,
			final String nickName, final OnCallback cb);

	/**
	 * 添加某文档到共享列表
	 * 
	 * @param conferenceId
	 * @param file
	 * @param cb
	 */
	public void addDocumentToList(String conferenceId, JSONObject file, OnCallback cb);

	/**
	 * 从共享队列移除文件
	 * 
	 * @param conferenceId
	 * @param position
	 * @param docId
	 * @param cb
	 */
	public void removeDocumentFromList(String conferenceId, int position, String docId, OnCallback cb);

	/**
	 * 获取共享列表数据
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void obtainSharingList(String conferenceId, OnCallback cb);

	/**
	 * 翻动到第几页
	 * 
	 * @param conferenceId
	 * @param position
	 * @param page
	 * @param cb
	 */
	public void flipOverDocument(String conferenceId, int position, int page, OnCallback cb);

	/**
	 * 召开会议，带上主题参数
	 * 
	 * @param subject
	 *            主题
	 * @param cb
	 */
	public void conveneConference(String subject, OnSampleCallback cb);

	/**
	 * 获取会议统计数据
	 * 
	 * @param cb
	 */
	public void obtainConferenceStatistics(OnSampleCallback cb);

	/**
	 * 获取会议历史召开表
	 * 
	 * @param cb
	 */
	public void obtainConferenceHistory(String time, OnSampleCallback cb);

	/**
	 * 获取我参加过的会议列表
	 * 
	 * @param cb
	 */
	public void getMyAllConference(OnSampleCallback cb);

	/**
	 * 获取我曾经召开过的会议
	 * 
	 * @param cb
	 */
	public void getMyConference(OnSampleCallback cb);

	/**
	 * 获取当前用户被邀请的信息
	 * 
	 * @param userId
	 * @param cb
	 */
	public void obtainNotifyInvitedInfoList(OnSampleCallback cb);

	/**
	 * 获取同意邀请的信息
	 * 
	 * @param cb
	 */
	public void obtainAcceptInvitedInfoList(OnSampleCallback cb);

	/**
	 * 发送邀请号码信息
	 * 
	 * @param confId
	 * @param sipNo
	 * @param phones
	 * @param cb
	 */
	public void sendInvitation(String confId, String sipNo, ArrayList<InvitePhoneInfo> phones, boolean pstn,
			OnCallback cb);

	/**
	 * 取消邀请
	 * 
	 * @param confId
	 * @param phone
	 * @param cb
	 */
	public void cancelInvite(String confId, String mobile, OnCallback cb);

	/**
	 * 获取振铃时的邀请信息
	 * 
	 * @param inviterUri
	 * @param cb
	 */
	public void obtainInvitationInfo(String inviterUri, OnSampleCallback cb);

	/**
	 * 同意邀请
	 * 
	 * @param confId
	 * @param inviteeId
	 * @param cb
	 */
	public void agreeInvite(String confId, String inviteeId, String inviterId, OnCallback cb);

	/**
	 * 拒绝邀请
	 * 
	 * @param confId
	 * @param inviteeId
	 * @param cb
	 */
	public void rejectInvite(String confId, String inviteeId, String inviterId, OnCallback cb);

	/**
	 * 获取上传至网盘的文档
	 * 
	 * @param cb
	 */
	public void obtainDocuments(OnSampleCallback cb);

	/**
	 * 上传文件
	 * 
	 * @param file
	 * @param cb
	 */
	public RequestHandle uploadFile(File file, OnUploadCallback cb);

	/**
	 * 上传头像
	 * 
	 * @param file
	 * @param cb
	 */
	public void uploadAvatar(File file, OnUploadCallback cb);

	/**
	 * 修改昵称
	 * 
	 * @param nickName
	 * @param cb
	 */
	public void modifyNickName(String nickName, OnCallback cb);

	/**
	 * 修改公司名
	 * 
	 * @param company
	 * @param cb
	 */
	public void modifyCompany(String company, OnCallback cb);

	/**
	 * 修改部门
	 * 
	 * @param department
	 * @param cb
	 */
	public void modifyDepartment(String department, OnCallback cb);

	/**
	 * 获取当前文档的所有页的数据，返回内容让view处理
	 * 
	 * @param docId
	 * @param cb
	 */
	public void obtainSharingDocumentAllData(String docId, OnSampleCallback cb);

	/**
	 * 获取当前文档的副本所有页数据
	 * 
	 * @param sessionId
	 * @param cb
	 */
	public void obtainSharingDocumentAllPageData(String sessionId, OnSampleCallback cb);

	/**
	 * 开启录音
	 * 
	 * @param confId
	 * @param listener
	 */
	public void startRecord(String confId, OnCallback cb);

	/**
	 * 停止录音
	 * 
	 * @param confId
	 * @param listener
	 */
	public void stopRecord(String confId, OnCallback cb);

	/**
	 * 开启会议锁
	 * 
	 * @param confId
	 * @param listener
	 */
	public void startLock(String confId, OnCallback cb);

	/**
	 * 解除会议锁
	 * 
	 * @param confId
	 * @param listener
	 */
	public void stopLock(String confId, OnCallback cb);

	/**
	 * 结束会议
	 * 
	 * @param confId
	 * @param listener
	 */
	public void terminateConference(String confId, OnCallback cb);

	/**
	 * 全部与会者静音
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void setAllMute(String conferenceId, OnCallback cb);

	/**
	 * 全部与会者解除静音
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void setAllUnmute(String conferenceId, OnCallback cb);

	/**
	 * 挂断某个与会者
	 * 
	 * @param confId
	 * @param memberPosition
	 * @param userId
	 * @param mobile
	 * @param cb
	 */
	public void handupMember(String confId, int memberPosition, String userId, String mobile, OnCallback cb);

	/**
	 * 移除某个与会者
	 * 
	 * @param confId
	 * @param memberPosition
	 * @param userId
	 * @param cb
	 */
	public void removeMember(String confId, int memberPosition, String userId, OnCallback cb);

	/**
	 * 剔除某个与会者
	 * 
	 * @param confId
	 * @param memberPosition
	 *            成员所在数组列表下表
	 * @param userId
	 *            成员唯一标识
	 * @param cb
	 */
	public void kickMember(String confId, int memberPosition, String userId, OnCallback cb);

	/**
	 * 剔除某个与会者的某设备
	 * 
	 * @param confId
	 * @param memberPosition
	 * @param deviceId
	 * @param cb
	 */
	public void kickMemberDevice(String confId, int memberPosition, String deviceId, OnCallback cb);

	/**
	 * 删除某个与会者的所有设备
	 * 
	 * @param confId
	 * @param memberPosition
	 * @param userId
	 * @param cb
	 */
	public void kickMemberDevices(String confId, int memberPosition, String userId, OnCallback cb);

	/**
	 * 邀请某个与会者
	 * 
	 * @param confId
	 * @param inviteInfo
	 *            邮件地址数组
	 * @param cb
	 */
	public void inviteMemberViaEmail(String confId, String[] inviteInfo, OnCallback cb);

	/**
	 * 从网盘中删除文档
	 * 
	 * @param docId
	 * @param cb
	 */
	public void deleteDocuments(String docId, OnCallback cb);
	
	/**
	 * 发送手机号码，申请短信验证码
	 * 
	 * @param phone
	 * @param type
	 *            0代表注册，1代表找回密码，2代表绑定手机
	 * @param deviceId
	 *            设备id，此处为手机的IMEI
	 * @param oscb
	 */
	public void applyVerificationCode(String phone, int type, boolean resend, OnSampleCallback oscb);

	/**
	 * 发送验证码到后台检验
	 * 
	 * @param code
	 * @param identify
	 * @param cb
	 */
	public void sendVerificationCode(String code, String identify, OnSampleCallback oscb);

	/**
	 * 发送邮箱地址，申请验证码
	 * 
	 * @param email
	 * @param username
	 * @param cb
	 */
	public void applyEmailVerificationCode(String email, String username, OnCallback cb);

	/**
	 * 发送验证码
	 * 
	 * @param email
	 * @param code
	 * @param cb
	 */
	public void bindingEmail(String email, String code, OnCallback cb);

	/**
	 * 注册
	 * 
	 * @param nickname
	 * @param password
	 */
	public void register(String phone, String nickname, String password, OnCallback cb);

	/**
	 * 修改密码
	 * 
	 * @param phone
	 * @param email
	 * @param password
	 * @param cb
	 */
	public void modifyPassword(String userId, String oldPassword, String newPassword, OnCallback cb);

	/**
	 * 找回密码
	 * 
	 * @param phone
	 * @param email
	 * @param password
	 * @param cb
	 */
	public void retrievePassword(String userId, String password, OnCallback cb);

	/**
	 * 绑定手机
	 * 
	 * @param moblie
	 * @param code
	 * @param identify
	 * @param cb
	 */
	public void bindingPhoneNum(String moblie, String code, String identify, OnCallback cb);

	/**
	 * 
	 * @param conferenceId
	 * @param phone
	 *            sip:123456@micyun.com
	 * @param callerId
	 *            sip:654321@micyun.com
	 * @param callerName
	 *            xiaoming
	 * @param cb
	 */
	public void requestCallin(String conferenceId, String phone, String callerId, String callerName, OnCallback cb);

	/**
	 * 反馈意见
	 * 
	 * @param username
	 * @param content
	 * @param cb
	 */
	public void feedback(String username, String content, int star, OnCallback cb);

	/**
	 * 改变主持权限
	 * 
	 * @param conferenceId
	 * @param userId
	 * @param cb
	 */
	public void changeController(String conferenceId, String userId, OnCallback cb);

	/**
	 * 收回主持权
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void removeController(String conferenceId, OnCallback cb);

	/**
	 * 设置当前正在共享的文档
	 * 
	 * @param conferenceId
	 * @param docId
	 * @param cb
	 */
	public void setConferenceDisplaying(String conferenceId, String docId, OnCallback cb);

	/**
	 * 停止共享
	 * 
	 * @param conferenceId
	 * @param cb
	 */
	public void stopConferenceDisplaying(String conferenceId, OnCallback cb);

	/**
	 * 取消文档转换
	 * 
	 * @param docid
	 * @param cb
	 */
	public void cancelDocumentConveting(String docid, OnCallback cb);

	/**
	 * 上传华为token
	 * 
	 * @param token
	 * @param cb
	 */
	public void sendHuaweiPushToken(String token, OnCallback cb);

	/**
	 * 获取会议详细信息
	 * 
	 * @param conference
	 * @param cb
	 */
	public void obtainConferenceDetail(String conference, OnSampleCallback cb);

	/**
	 * 查询指定userId的用户信息
	 * 
	 * @param userId
	 * @param cb
	 */
	public void queryPersonInformation(String userId, OnSampleCallback cb);

	/**
	 * 反馈小米推送结果
	 * 
	 * @param confid
	 * @param phone
	 */
	public void sendXiaomiReceiveResult(String confid, boolean result, String phone, OnCallback cb);

	/**
	 * 重新召开会议
	 * 
	 * @param argument
	 * @param cb
	 */
	public void reconveneConference(String argument, OnSampleCallback cb);

	/**
	 * 获取余额信息
	 * 
	 * @param cb
	 */
	public void getBalance(OnSampleCallback cb);

	/**
	 * 获取文件下载链接
	 * 
	 * @param seesionid
	 * @param cb
	 */
	public void getDownloadUrl(String sessionid, OnSampleCallback cb);

	/**
	 * 网络变化时传输给disp
	 * 
	 * @param confId
	 * @param mPosition
	 * @param cb
	 */
	public void postNetworkChanged(String confId, int mPosition, int networkType, String userId, String uuid,
			OnCallback cb);

	/**
	 * 受到来电或拨打电话干扰
	 * 
	 * @param confId
	 * @param mPos
	 * @param userId
	 * @param uuid
	 * @param cb
	 */
	public void postInterruption(String confId, int mPos, String userId, String uuid, OnCallback cb);

	/**
	 * 取消干扰标识
	 * 
	 * @param confId
	 * @param mPos
	 * @param userId
	 * @param uuid
	 * @param cb
	 */
	public void deleteInterruption(String confId, int mPos, String userId, String uuid, OnCallback cb);
}
