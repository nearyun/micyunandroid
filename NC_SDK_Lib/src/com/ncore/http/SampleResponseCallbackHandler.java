package com.ncore.http;

import com.ncore.callback.OnCallback;

/**
 * 简化响应的匿名回调，不太多关心返回的消息内容
 * 
 * @author xiaohua
 * 
 */
public class SampleResponseCallbackHandler extends ResponseCallbackHandler {

	protected OnCallback ocb;

	public SampleResponseCallbackHandler(OnCallback cb) {
		ocb = cb;
	}

	@Override
	public void onSuccess(String content) {
		if (ocb != null)
			ocb.onSuccess();
	}

	@Override
	public void onFailure(int statusCode, String reason) {
		if (ocb != null)
			ocb.onFailure(statusCode, reason);
	}
}
