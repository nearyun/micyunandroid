package com.ncore.http;

import com.ncore.callback.OnCallback;
import com.ncore.callback.OnUploadCallback;

public class UploadFileResponseCallbackHandler extends SampleResponseCallbackHandler {

	public UploadFileResponseCallbackHandler(OnCallback cb) {
		super(cb);
	}

	@Override
	public void onCancel() {
		if (ocb != null && ocb instanceof OnUploadCallback)
			((OnUploadCallback) ocb).onCancel();
	}

	@Override
	public void onStart() {
		if (ocb != null && ocb instanceof OnUploadCallback)
			((OnUploadCallback) ocb).onStart();
	}

	@Override
	public void onProgress(int bytesWritten, int totalSize) {
		if (ocb != null && ocb instanceof OnUploadCallback)
			((OnUploadCallback) ocb).onTransferred(bytesWritten, totalSize);
	}

	@Override
	public void onSuccess(String content) {
		if (ocb != null && ocb instanceof OnUploadCallback)
			((OnUploadCallback) ocb).onSuccess(content);
		if (ocb != null)
			ocb.onSuccess();
	}

}
