package com.ncore.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.ncore.util.LogUtil;

public class NYAsyncHttp {
	private static final String TAG = "NYAsyncHttp";

	private static final String APPLICATION_JSON = "application/json";

	private final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
	private Context mContext = null;
	private final Header[] headers = new Header[] { new BasicHeader("X-Requested-With", "XMLHttpRequest"),
			new BasicHeader("Content-Type", "application/json; charset=utf-8") };
	private final RequestParams defaultParams = new RequestParams();

	public NYAsyncHttp(Context cxt) {
		mContext = cxt;
		asyncHttpClient.setUserAgent("android");
		asyncHttpClient.addHeader("Accept", APPLICATION_JSON);
		// asyncHttpClient.addHeader("Connection", "close");

		PersistentCookieStore myCookieStore = new PersistentCookieStore(mContext);
		asyncHttpClient.setCookieStore(myCookieStore);

	}

	public void cancelRequest(Context context) {
		asyncHttpClient.cancelRequests(context, true);
	}

	public RequestHandle get(String url, int version, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "get --> " + url + "  --> version:" + version);
		Header[] headers = new Header[] { new BasicHeader("X-Ver-None-Match", version + ""),
				new BasicHeader("Content-Type", "application/json; charset=utf-8") };
		return asyncHttpClient.get(mContext, url, headers, defaultParams, new SampleAsyncHttpResponseHandler(url,
				callbackHandler));
	}

	public RequestHandle get(String url, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "get --> " + url);
		return asyncHttpClient.get(mContext, url, headers, defaultParams, new SampleAsyncHttpResponseHandler(url,
				callbackHandler));
	}

	public RequestHandle delete(String url, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "delete --> " + url);
		return asyncHttpClient.delete(mContext, url, headers, defaultParams, new SampleAsyncHttpResponseHandler(url,
				callbackHandler));
	}

	public RequestHandle post(String url, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "post --> " + url);
		return asyncHttpClient.post(mContext, url, headers, defaultParams, APPLICATION_JSON,
				new SampleAsyncHttpResponseHandler(url, callbackHandler));
	}

	public RequestHandle post(String url, String content, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "post --> " + url);
		LogUtil.d(TAG, "args --> " + content);
		StringEntity entity = null;
		try {
			entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentEncoding(HTTP.UTF_8);
			entity.setContentType(APPLICATION_JSON);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// Header[] headers = new Header[] { new BasicHeader("Content-Type",
		// "application/json; charset=utf-8") };
		// return asyncHttpClient.post(mContext, url, headers, entity,
		// "application/json",
		// new SampleAsyncHttpResponseHandler(callbackHandler));
		return asyncHttpClient.post(mContext, url, headers, entity, APPLICATION_JSON,
				new SampleAsyncHttpResponseHandler(url, callbackHandler));
	}

	public RequestHandle upload(String url, File fileObject, String fileKey, ResponseCallbackHandler callbackHandler) {
		LogUtil.d(TAG, "upload --> " + url);
		RequestParams params = new RequestParams();
		try {
			params.put(fileKey, fileObject);
			return asyncHttpClient
					.post(mContext, url, params, new SampleAsyncHttpResponseHandler(url, callbackHandler));
		} catch (FileNotFoundException e) {
			if (callbackHandler != null)
				callbackHandler.onFailure(0, e.getMessage());
			return null;
		}

	}
}
