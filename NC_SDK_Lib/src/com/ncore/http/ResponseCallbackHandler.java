package com.ncore.http;

public abstract class ResponseCallbackHandler {

	public void onProgress(int bytesWritten, int totalSize) {

	}

	public void onCancel() {

	}

	public void onStart() {

	}

	public abstract void onFailure(int statusCode, String reason);

	public abstract void onSuccess(String content);
}
