package com.ncore.http;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Looper;
import android.text.TextUtils;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.ncore.util.LogUtil;

/**
 * 简化http请求返回的匿名回调
 * 
 * @author xiaohua
 * 
 */
public class SampleAsyncHttpResponseHandler extends AsyncHttpResponseHandler {
	private static final String TAG = SampleAsyncHttpResponseHandler.class.getSimpleName();

	private ResponseCallbackHandler callbackHandler = null;
	private static final ResponseCallbackHandler defaultCallbackHandler = new ResponseCallbackHandler() {

		@Override
		public void onFailure(int statusCode, String reason) {
			LogUtil.w(TAG, "not set callback for onFailure() " + statusCode + ":" + reason);
		}

		@Override
		public void onSuccess(String content) {
			LogUtil.w(TAG, "not set callback for onSuccess(): " + content);
		}
	};

	private String requestUrl = "";

	public SampleAsyncHttpResponseHandler(String url, ResponseCallbackHandler callback) {
		super(Looper.getMainLooper());
		requestUrl = url;
		callbackHandler = (callback != null) ? callback : defaultCallbackHandler;
	}

	@SuppressWarnings("unused")
	private final void debugHeaders(Header[] headers) {
		if (headers != null) {
			LogUtil.w(TAG, "Return Headers: >>>");
			for (Header h : headers) {
				String _h = String.format("%s : %s", h.getName(), h.getValue());
				LogUtil.w(TAG, _h);
			}
			LogUtil.w(TAG, "<<<");
		}
	}

	private final boolean isValidHeader(Header[] headers) {
		if (headers == null || headers.length == 0)
			return false;
		for (int i = 0; i < headers.length; i++) {
			if (TextUtils.equals(headers[i].getName(), "Content-Type")
					&& headers[i].getValue().contains("application/json")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onCancel() {
		callbackHandler.onCancel();
	}

	@Override
	public void onStart() {
		callbackHandler.onStart();
	}

	@Override
	public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable th) {
		// debugHeaders(headers);
		if (statusCode == 0) { // 有异常
			callbackHandler.onFailure(statusCode, "网络故障，请检查手机的网络情况(" + statusCode + ")");
		} else {
			String response = (errorResponse != null) ? new String(errorResponse) : "";
			LogUtil.e(TAG, getResponseLog(statusCode, response));
			callbackHandler.onFailure(statusCode, "系统线路遇忙(" + statusCode + ")");
		}

	}

	@Override
	public void onProgress(int bytesWritten, int totalSize) {
		callbackHandler.onProgress(bytesWritten, totalSize);
	}

	private String getResponseLog(int code, String result) {
		return "## response ==> " + requestUrl + " ==>" + code + " : " + result;
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, byte[] response) {
		// debugHeaders(headers);
		String result = (response != null) ? new String(response) : "";
		LogUtil.d(TAG, getResponseLog(statusCode, result));
		if (isValidHeader(headers)) {
			if (result.contains("errorcode")) {
				try {
					JSONObject json = new JSONObject(result);
					callbackHandler.onFailure(json.optInt("errorcode"), json.optString("message"));
				} catch (JSONException e) {
					callbackHandler.onFailure(-1, "返回数据内容不完整");
				}
			} else {
				callbackHandler.onSuccess(result);
			}
		} else {
			callbackHandler.onFailure(statusCode, "响应的数据格式不正确");
		}
	}
}
