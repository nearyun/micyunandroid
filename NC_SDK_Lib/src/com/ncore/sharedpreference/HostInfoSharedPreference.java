package com.ncore.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class HostInfoSharedPreference {
	/** 文件名 */
	private static final String NC_PERFS_FILE_NAME = "domains.io";
	private static final String KEY_DOMAINS = "domains";

	/**
	 * 保存主机信息
	 * 
	 * @param context
	 * @param jsonString
	 */
	public static final void saveHostsInformation(Context context, String jsonString) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		String content = Base64.encodeToString(jsonString.toString().getBytes(), Base64.NO_WRAP);
		preferences.edit().putString(KEY_DOMAINS, content).commit();
	}

	/**
	 * 读取主机信息
	 * 
	 * @param context
	 * @return
	 */
	public static final String readHostsInformation(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		String content = preferences.getString(KEY_DOMAINS, "");
		return new String(Base64.decode(content, Base64.NO_WRAP));
	}

	/**
	 * 移除主机信息
	 * 
	 * @param context
	 */
	public static final void clearHostsInformation(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		preferences.edit().remove(KEY_DOMAINS).commit();
	}
}
