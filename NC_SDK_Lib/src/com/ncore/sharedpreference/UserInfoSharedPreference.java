package com.ncore.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;

import com.ncore.model.Secrecy;

/**
 * 用户信息配置文件
 * 
 * @author xiaohua
 * 
 */
public class UserInfoSharedPreference {

	/** 文件名 */
	private static final String NC_PERFS_FILE_NAME = "ncu.io";
	private static final String KEY_USER_INFOMATION = "kui";

	/** 用户名标签 */
	private static final String KEY_USERNAME = "kun";
	/** 密码标签 */
	private static final String KEY_SECRECY = "ksc";

	/**
	 * 保存用户信息
	 * 
	 * @param context
	 * @param jsonString
	 */
	public static final void saveUserInformation(Context context, String jsonString) {
		if (context == null || TextUtils.isEmpty(jsonString))
			return;
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		String content = Base64.encodeToString(jsonString.toString().getBytes(), Base64.NO_WRAP);
		preferences.edit().putString(KEY_USER_INFOMATION, content).commit();
	}

	/**
	 * 读取用户信息
	 * 
	 * @param context
	 * @return
	 */
	public static final String readUserInformation(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		String content = preferences.getString(KEY_USER_INFOMATION, "");
		return new String(Base64.decode(content, Base64.NO_WRAP));
	}

	/**
	 * 移除用户信息
	 * 
	 * @param context
	 */
	public static final void clearUserInformation(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		preferences.edit().remove(KEY_USER_INFOMATION).commit();
	}

	/**
	 * 保存用户账号和密码
	 * 
	 * @param context
	 * @param username
	 * @param password
	 */
	public static void saveLoginInfo(Context context, String username, Secrecy mSecrecy) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		preferences.edit().putString(KEY_USERNAME, username).putString(KEY_SECRECY, mSecrecy.toSecrecyString())
				.commit();
	}

	/**
	 * 保存用户账号
	 * 
	 * @param context
	 * @param username
	 */
	public static void saveUserName(Context context, String username) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		preferences.edit().putString(KEY_USERNAME, username).commit();
	}

	/**
	 * 读取用户账号
	 * 
	 * @param context
	 * @return
	 */
	public static String readUserName(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		return preferences.getString(KEY_USERNAME, "");
	}

	/**
	 * 读取用户密码
	 * 
	 * @param context
	 * @return
	 */
	public static Secrecy readSecrecy(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		String content = preferences.getString(KEY_SECRECY, "");
		return Secrecy.create(content);
	}

	/**
	 * 清除指定key的value
	 * 
	 * @param key
	 */
	public static void clearSecrecy(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(NC_PERFS_FILE_NAME, Context.MODE_PRIVATE);
		preferences.edit().remove(KEY_SECRECY).commit();
	}

}
