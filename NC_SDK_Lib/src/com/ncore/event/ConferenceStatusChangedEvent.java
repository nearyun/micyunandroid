package com.ncore.event;

/**
 * 会议状态改变的事件
 * 
 * @author xiaohua
 * 
 */
public class ConferenceStatusChangedEvent extends BaseEvent {
	public String conferenceId = "";

	public ConferenceStatusChangedEvent(String cid) {
		conferenceId = cid;
	}
}
