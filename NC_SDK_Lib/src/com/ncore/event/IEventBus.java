package com.ncore.event;

import com.ncore.event.BaseEvent;

public interface IEventBus {
	void onEvent(BaseEvent evt);
}
