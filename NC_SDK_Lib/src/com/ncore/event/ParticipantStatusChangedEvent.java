package com.ncore.event;

/**
 * 与会者状态改变的事件
 * 
 * @author xiaohua
 * 
 */
public class ParticipantStatusChangedEvent extends BaseEvent {
	public String conferenceId = "";

	public ParticipantStatusChangedEvent(String cid) {
		conferenceId = cid;
	}
}
