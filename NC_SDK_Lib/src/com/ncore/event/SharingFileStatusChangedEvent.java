package com.ncore.event;

/**
 * 共享文档状态改变的事件
 * 
 * @author xiaohua
 * 
 */
public class SharingFileStatusChangedEvent extends BaseEvent {

	public String conferenceId = "";
	public String docId = "";

	public SharingFileStatusChangedEvent(String cid, String fid) {
		conferenceId = cid;
		docId = fid;
	}

}
