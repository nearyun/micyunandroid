package com.ncore.callback;

/**
 * 上传回调
 * 
 * @author xiaohua
 * 
 */
public abstract class OnUploadCallback implements OnCallback {
	public void onTransferred(long transfer, long total) {
	}

	public void onStart() {
	}

	public void onCancel() {
	}

	public void onSuccess(String content) {
	}

	@Override
	public void onSuccess() {

	}

}
