package com.ncore.callback;

public interface OnCallback {
	public void onSuccess();

	public void onFailure(int errorCode, String reason);
}
