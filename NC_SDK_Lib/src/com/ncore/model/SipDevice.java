package com.ncore.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.tornado.util.NetworkUtil;

/**
 * sip设备
 * 
 * @author xiaohua
 * 
 */
public class SipDevice {

	/** PC设备 */
	public static final int DEVICE_PC = 0;
	/** android */
	public static final int DEVICE_ANDROID = 1;
	/** iphone */
	public static final int DEVICE_IPHONE = 2;
	/** pstn */
	public static final int DEVICE_PSTN = 3;

	/** 未呼叫 */
	public static final int CALL_STATE_DEFAULT = 0;
	/** 呼叫中 */
	public static final int CALL_STATE_CALLING = 1;
	/** 呼叫成功 */
	public static final int CALL_STATE_ANSWER = 2;
	/** 呼叫失败 */
	public static final int CALL_STATE_FAILURE = 3;
	/** 对方未注册 */
	public static final int CALL_STATE_UNREACHABLE = 4;
	/** 忙 */
	public static final int CALL_STATE_BUSY = 5;

	/** 默认值 */
	public static final int SIP_STATUS_DEFAULT = 0;
	/** 对方拒绝 */
	public static final int SIP_STATUS_REJECT = 480;
	/** 忙 */
	public static final int SIP_STATUS_BUSY = 486;
	/** 取消呼叫 */
	public static final int SIP_STATUS_CANCEL = 487;
	/** 媒体错误 */
	public static final int SIP_STATUS_MEDIA_ERROR = 488;
	/** not found */
	public static final int SIP_STATUS_NOT_FOUND = 404;

	private static final int STATUS_MUTE = 1 << 0; // 静音
	private static final int STATUS_HANDUP = 1 << 1;// 挂断
	private static final int STATUS_NETWORK_SIGNAL_POOR = 1 << 17;// 网络不稳定
	private static final int STATUS_INTERRUPTION = 1 << 16; // 对方接听电话
	private static final int STATUS_SELF_MUTED = 1 << 20;// 自主静音

	public static final int NETWORK_TYPE_UNKNOWN = 0;
	public static final int NETWORK_TYPE_FIXED = 1;
	public static final int NETWORK_TYPE_2G = 2;
	public static final int NETWORK_TYPE_3G = 3;
	public static final int NETWORK_TYPE_4G = 4;
	public static final int NETWORK_TYPE_5G = 5;
	public static final int NETWORK_TYPE_VPN = 10;
	public static final int NETWORK_TYPE_WIFI = 24;
	public static final int NETWORK_TYPE_WIFI58 = 58;

	/** 语音id，connectionId */
	private String cid;
	/** 设备id */
	private String uuid;
	/** 设备类型 0:未知，1:android，2:iphone，3:pc */
	private int type;
	/** 当前应用版本号 */
	private String userAgent;
	/** sip手机号 */
	private String phone;
	/** sip uri */
	private String voip;
	/** 呼叫状态 */
	private int callState;
	/** sip状态 */
	private int sipStatus;
	/** 语音状态：静音 */
	private int status;
	/** 进会时间, 单位：秒 */
	private long enterTime;
	/** 在app列表中的position */
	private int memberId;
	/** 网络类型 */
	private int networkType;

	private static final String LABEL_MEMBER_ID = "memberid"; // 与position相关
	private static final String LABEL_ID = "id"; // 语音id，唯一标识
	private static final String LABEL_UUID = "uuid"; // 设备唯一标识
	private static final String LABEL_TYPE = "type"; // 设备类型
	private static final String LABEL_STATUS = "status"; // 0:unmute, 1:mute
	private static final String LABEL_USERAGENT = "useragent"; // 手机应用版本号
	private static final String LABEL_PHONE = "phone"; // sipuser 号码
	private static final String LABEL_VOIP = "voip"; // voip
	private static final String LABEL_CALL_STATE = "callstate"; // 呼叫状态
	private static final String LABEL_SIP_STATUS = "sipstatus"; // 呼叫失败状态
	private static final String LABEL_ENTER_TIME = "enter_time"; // 进入时间
	private static final String LABEL_NETWORK = "network"; // 网络状态

	public SipDevice(JSONObject json) {
		this.memberId = json.optInt(LABEL_MEMBER_ID);
		this.cid = json.optString(LABEL_ID);
		this.uuid = json.optString(LABEL_UUID);
		this.type = json.optInt(LABEL_TYPE);
		this.voip = json.optString(LABEL_VOIP);
		this.phone = json.optString(LABEL_PHONE);
		this.status = json.optInt(LABEL_STATUS);
		this.callState = json.optInt(LABEL_CALL_STATE);
		this.sipStatus = json.optInt(LABEL_SIP_STATUS);
		this.userAgent = json.optString(LABEL_USERAGENT);
		this.enterTime = json.optLong(LABEL_ENTER_TIME);
		this.networkType = json.optInt(LABEL_NETWORK);
	}

	/**
	 * 默认生成android设备的json串
	 * 
	 * @param uuid
	 * @param phone
	 * @param useragent
	 * @param voip
	 * @return
	 */
	public static final JSONObject toJson(String uuid, String phone, String useragent, String voip) {
		return toJson(uuid, SipDevice.DEVICE_ANDROID, phone, useragent, voip);
	}

	/**
	 * 生成指定设备的json串
	 * 
	 * @param uuid
	 * @param type
	 * @param phone
	 * @param useragent
	 * @param voip
	 * @return
	 */
	public static final JSONObject toJson(String uuid, int type, String phone, String useragent, String voip) {
		JSONObject json = new JSONObject();
		try {
			json.put(LABEL_MEMBER_ID, -1);
			json.put(LABEL_ID, "");
			json.put(LABEL_UUID, uuid);
			json.put(LABEL_STATUS, 0);
			json.put(LABEL_TYPE, type);
			json.put(LABEL_PHONE, phone);
			json.put(LABEL_USERAGENT, useragent);
			json.put(LABEL_CALL_STATE, CALL_STATE_DEFAULT);
			json.put(LABEL_SIP_STATUS, SIP_STATUS_DEFAULT);
			json.put(LABEL_VOIP, voip);
			json.put(LABEL_NETWORK, NETWORK_TYPE_UNKNOWN);
			json.put(LABEL_ENTER_TIME, System.currentTimeMillis() / 1000);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	public String getCid() {
		return cid;
	}

	public int getMemberId() {
		return memberId;
	}

	public String getUUID() {
		return uuid;
	}

	public boolean isAndroidDevice() {
		return DEVICE_ANDROID == type;
	}

	public int getType() {
		return type;
	}

	public String getUseragent() {
		return userAgent;
	}

	public String getPhone() {
		return phone;
	}

	public String getVoip() {
		return voip;
	}

	public int getNetworkType() {
		return networkType;
	}

	public long getEnterTime() {
		return enterTime;
	}

	public boolean isSelfMute() {
		return (status & STATUS_SELF_MUTED) == STATUS_SELF_MUTED;
	}

	public boolean isPoorNetwork() {
		return (status & STATUS_NETWORK_SIGNAL_POOR) == STATUS_NETWORK_SIGNAL_POOR;
	}

	public boolean isInterrupted() {
		return (status & STATUS_INTERRUPTION) == STATUS_INTERRUPTION;
	}

	public boolean isMute() {
		return (status & STATUS_MUTE) == STATUS_MUTE;
	}

	public boolean isHandup() {
		return (status & STATUS_HANDUP) == STATUS_HANDUP;
	}

	/**
	 * {@link #CALL_STATE_DEFAULT}</p>{@link #CALL_STATE_CALLING}</p>
	 * {@link #CALL_STATE_ANSWER}</p>{@link #CALL_STATE_FAILURE}</p>
	 * {@link #CALL_STATE_REJECT}</p>{@link #CALL_STATE_CANCEL}</p>
	 * {@link #CALL_STATE_UNREACHABLE}</p>{@link #CALL_STATE_BUSY}</p>
	 * 
	 * @return
	 */
	public int getCallState() {
		return callState;
	}

	/**
	 * {@link #SIP_STATUS_DEFAULT}</p>{@link #SIP_STATUS_REJECT}</p>
	 * {@link #SIP_STATUS_BUSY}</p>{@link #SIP_STATUS_CANCEL}</p>
	 * {@link #SIP_STATUS_MEDIA_ERROR}</p>{@link #SIP_STATUS_NOT_FOUND}</p>
	 * 
	 * @return
	 */
	public int getSipStatus() {
		return sipStatus;
	}

	public boolean isAnswered() {
		return callState == SipDevice.CALL_STATE_ANSWER;
	}

	public static final int convert2NetworkType(Context context) {
		int nwType = NetworkUtil.getCurrentNetworkType(context);
		switch (nwType) {
		case NetworkUtil.NETWORK_CLASS_2G:
			return NETWORK_TYPE_2G;
		case NetworkUtil.NETWORK_CLASS_3G:
			return NETWORK_TYPE_3G;
		case NetworkUtil.NETWORK_CLASS_4G:
			return NETWORK_TYPE_4G;
		case NetworkUtil.NETWORK_CLASS_WIFI:
			return NETWORK_TYPE_WIFI;
		case NetworkUtil.NETWORK_CLASS_UNAVAILABLE:
		case NetworkUtil.NETWORK_CLASS_UNKNOWN:
		default:
			return NETWORK_TYPE_UNKNOWN;
		}
	}
}
