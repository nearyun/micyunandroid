package com.ncore.model;

import android.app.Service;
import android.content.Context;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.ncore.callback.OnCallback;
import com.ncore.model.client.IClient;
import com.ncore.util.LogUtil;
import com.tornado.util.NetworkUtil;
import com.tornado.util.PhoneUtil;

/**
 * 电话干扰模型
 * 
 * @author xiaohua
 * 
 */
public class Interruption {
	private final String TAG = getClass().getSimpleName();

	private boolean isInterrupted = false;// 是否被干扰了

	private Context mContext;
	private IClient client;
	private String conferenceId;
	private String userId;
	private String uuid;

	private Handler mHander = new Handler();
	private TelephonyManager telManager;
	private boolean isRunning = false; // 干扰程序是否在运行

	private final int ONE_SECOND = 1000;
	private final int TEN_SECONDS = 10000;

	private OnInterruptedParticipantCallback mOnInterrupteCallback;

	private final OnInterruptedParticipantCallback defaultOnInterrupteCallback = new OnInterruptedParticipantCallback() {

		@Override
		public Participant onInterrupted(String userId) {
			return null;
		}

	};

	public interface OnInterruptedParticipantCallback {
		public Participant onInterrupted(String userId);
	}

	public Interruption(Context cxt, IClient client, String confId, String userId, String uuid,
			OnInterruptedParticipantCallback callback) {
		this.mContext = cxt;
		this.client = client;
		this.conferenceId = confId;
		this.userId = userId;
		this.uuid = uuid;
		this.mOnInterrupteCallback = (callback == null ? defaultOnInterrupteCallback : callback);

		telManager = (TelephonyManager) cxt.getSystemService(Service.TELEPHONY_SERVICE);
	}

	public void start() {
		if (!isRunning) {
			LogUtil.i(TAG, "开启 来电干扰监听程序");
			isRunning = true;
			isInterrupted = false;
			telManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
		} else {
			LogUtil.w(TAG, "已开启 干扰监听程序");
		}
	}

	public void stop() {
		if (isRunning) {
			LogUtil.i(TAG, "停止 来电干扰监听程序");
			if (isInterrupted)
				sendDeleteInterrupted();
			isRunning = false;
			isInterrupted = false;
			telManager.listen(listener, PhoneStateListener.LISTEN_NONE);
			cancelAllTimer();
		} else {
			LogUtil.w(TAG, "已停止 干扰监听程序");
		}
	}

	public boolean isInterrupted() {
		return isInterrupted;
	}

	private final PhoneStateListener listener = new PhoneStateListener() {

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			super.onCallStateChanged(state, incomingNumber);
			if (!isRunning) {
				return;
			}
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				LogUtil.i(TAG, "state：挂断");
				setInterrupted2Flase();
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				LogUtil.i(TAG, "state：接听");
				setInterrupted2True();
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				LogUtil.i(TAG, "state：响铃");
				setInterrupted2True();
				break;
			}
		}
	};

	/**
	 * 判断当前网络是否有效</p>
	 * 
	 * 无效，继续起10秒定时器</p>
	 * 
	 * 有效，发送干扰信号，然后继续起10秒定时器
	 */
	private Runnable interruptionRunnable = new Runnable() {

		@Override
		public void run() {
			synchronized (Interruption.this) {
				if (NetworkUtil.IsNetWorkEnable(mContext)) {
					LogUtil.d(TAG, "网络可用");
					if (PhoneUtil.isTelPhoneBusy(mContext)) { // 被中断
						// 已中断，就起10秒定时器
						LogUtil.d(TAG, "发送干扰信号");
						sendPostInterrupte();
						mHander.postDelayed(this, TEN_SECONDS);
					} else {
						// 不是中断，就invoke setInterrupted2Flase();
						LogUtil.d(TAG, "误判，没有中断");
						setInterrupted2Flase();
					}
				} else {
					LogUtil.w(TAG, "无网络，继续起定时器");
					mHander.postDelayed(this, TEN_SECONDS);
				}
			}
		}
	};

	/**
	 * 先判断当前是否处于被中断</p>
	 * 
	 * 是，先发送干扰信号，然后每10秒发送一次；</p>
	 * 
	 * 否，取消定时器
	 */
	private Runnable oneSecondRunnable = new Runnable() {
		@Override
		public void run() {
			synchronized (Interruption.this) {
				if (PhoneUtil.isTelPhoneBusy(mContext)) {
					// 已中断，就先发送干扰信号出去，每隔10秒发送一次
					LogUtil.i(TAG, "目前处于中断状态");
					startTenSecondTimerTask();
				} else {
					LogUtil.i(TAG, "没有被中断");
					setInterrupted2Flase();
				}
			}
		}

	};

	private void startTenSecondTimerTask() {
		mHander.post(interruptionRunnable);
	}

	private void startOneSecondTimerTask() {
		mHander.postDelayed(oneSecondRunnable, ONE_SECOND);
	}

	private void cancelAllTimer() {
		mHander.removeCallbacks(oneSecondRunnable);
		mHander.removeCallbacks(interruptionRunnable);
	}

	private void setInterrupted2True() {
		synchronized (Interruption.this) {
			isInterrupted = true;
			cancelAllTimer();
			startOneSecondTimerTask();
		}
	}

	private void setInterrupted2Flase() {
		synchronized (Interruption.this) {
			isInterrupted = false;
			cancelAllTimer();
		}
		LogUtil.w(TAG, "发送取消干扰信号");
		sendDeleteInterrupted();
	}

	private void sendPostInterrupte() {
		Participant p = mOnInterrupteCallback.onInterrupted(userId);
		if (p == null)
			return;
		client.postInterruption(conferenceId, p.getSequenceNumber(), userId, uuid, postOnCallback);
	}

	private final OnCallback postOnCallback = new OnCallback() {

		@Override
		public void onSuccess() {
			LogUtil.i(TAG, "发送受到干扰信号 成功");
		}

		@Override
		public void onFailure(int errorCode, String reason) {
			LogUtil.w(TAG, "发送受到干扰信号 失败 " + errorCode + reason);
		}

	};

	private void sendDeleteInterrupted() {
		Participant p = mOnInterrupteCallback.onInterrupted(userId);
		if (p == null)
			return;
		client.deleteInterruption(conferenceId, p.getSequenceNumber(), userId, uuid, deleteOnCallback);
	}

	private final OnCallback deleteOnCallback = new OnCallback() {

		@Override
		public void onSuccess() {
			LogUtil.i(TAG, "发送取消干扰信号 成功");

		}

		@Override
		public void onFailure(int errorCode, String reason) {
			LogUtil.w(TAG, "发送取消干扰信号 失败 " + errorCode + reason);
		}

	};

}
