package com.ncore.model;

import org.json.JSONObject;

import com.ncore.config.NCConstants;

public class Person {
	private String nickName;
	private String mobile;
	private String department;
	private String company;
	private String avatar;
	private String userMail;
	private String textAvatar;

	private static final String LABEL_NICKNAME = "nickname";
	private static final String LABEL_MOBILE = "mobile";
	private static final String LABEL_DEPARTMENT = "department";
	private static final String LABEL_COMPANY = "company";
	private static final String LABEL_AVATAR = "avatar";
	private static final String LABEL_USERMAIL = "user_mail";
	private static final String LABEL_TEXTAVATAR = "text_avatar";

	public static Person create(JSONObject json) {
		Person p = new Person();
		p.setAvatar(json.optString(LABEL_AVATAR));
		p.setNickName(json.optString(LABEL_NICKNAME));
		p.setDepartment(json.optString(LABEL_DEPARTMENT));
		p.setCompany(json.optString(LABEL_COMPANY));
		p.setMobile(json.optString(LABEL_MOBILE));
		p.setTextAvatar(json.optString(LABEL_TEXTAVATAR));
		p.setUserMail(json.optString(LABEL_USERMAIL));
		return p;
	}

	private Person() {

	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public void setTextAvatar(String textAvatar) {
		this.textAvatar = textAvatar;
	}

	public String getNickName() {
		return nickName;
	}

	public String getMobile() {
		return mobile;
	}

	public String getDepartment() {
		return department;
	}

	public String getCompany() {
		return company;
	}

	public String getAvatar() {
		return avatar;
	}

	public String getFullAvatar() {
		return NCConstants.PHP_BASE_URL + avatar;
	}

	public String getUserMail() {
		return userMail;
	}

	public String getTextAvatar() {
		return textAvatar;
	}

}
