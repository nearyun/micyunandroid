package com.ncore.model.client;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.loopj.android.http.RequestHandle;
import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.callback.OnUploadCallback;
import com.ncore.config.NCConstants;
import com.ncore.http.NYAsyncHttp;
import com.ncore.http.SampleResponseCallbackHandler;
import com.ncore.http.UploadFileResponseCallbackHandler;
import com.ncore.http.request.IHttpRequest;
import com.ncore.model.InvitePhoneInfo;
import com.ncore.model.Participant;
import com.ncore.model.Secrecy;
import com.ncore.model.User;
import com.ncore.model.conference.IConferenceJsonDataManager;
import com.ncore.model.conference.impl.ConferenceJsonDataManager;
import com.ncore.model.sharing.NetworkFileInfo;
import com.ncore.model.sharing.SharingFile;
import com.ncore.sharedpreference.UserInfoSharedPreference;
import com.ncore.util.LogUtil;
import com.tornado.util.DeviceUtil;
import com.tornado.util.EncryptionUtil;
import com.tornado.util.SystemUtil;

public abstract class IClient implements IHttpRequest {
	protected NYAsyncHttp asyncHttp = null;

	protected Context mContext = null;

	public IClient(Context cxt) {
		mContext = cxt;
		asyncHttp = new NYAsyncHttp(cxt);
	}

	public void testHttlp(String name, OnCallback cb) {
		// JSONObject json = new JSONObject();
		// try {
		// json.put("account", name);
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		// String baseUrl = "http://www.micyun.com:80/login/nonce";
		// asyncHttp.post(baseUrl, json.toString(), new
		// SampleResponseCallbackHandler(cb));
	}

	private static String getAppVersionName(Context context) {
		String versionName = "";
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionName = pi.versionName;
		} catch (Exception e) {
		}
		return versionName;
	}

	private ConferenceJsonDataManager mConferenceJsonDataManager = null;

	public IConferenceJsonDataManager createDataManager(String conferenceId) {
		if (mConferenceJsonDataManager != null) {
			mConferenceJsonDataManager.reset();
			mConferenceJsonDataManager = null;
		}
		mConferenceJsonDataManager = new ConferenceJsonDataManager(mContext, this, conferenceId, getUser().getUserId(),
				DeviceUtil.getDeviceUUID(mContext));
		return mConferenceJsonDataManager;
	}

	/**
	 * 获取登陆用户信息
	 * 
	 * @return
	 */
	public abstract User getUser();

	/**
	 * 保存用户信息
	 * 
	 * @param accountName
	 * @param mSecrecy
	 */
	protected abstract void saveLoginInfo(String accountName, Secrecy mSecrecy);

	/**
	 * 获取用户名
	 * 
	 * @return
	 */
	public abstract String getUserName();

	/**
	 * 获取加密信息
	 * 
	 * @return
	 */
	public abstract Secrecy getSecrecy();

	/**
	 * 设置登陆用户信息
	 * 
	 * @param json
	 */
	protected abstract void setUser(JSONObject json);

	/**
	 * 是否已经有用户数据
	 * 
	 * @return
	 */
	public abstract boolean hasUserData();

	/**
	 * 判断是否合法的密码
	 * 
	 * @param pwd
	 * @return
	 */
	public abstract boolean isValidatePwd(String password);

	private Timer keepaliveTimer = null;
	private final int DELAY_TIME = 20 * 60 * 1000; // 20分钟

	/**
	 * 开始keepAlive线程
	 */
	public void startKeepAliveThread() {
		stopKeepAliveThread();
		keepaliveTimer = new Timer();
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				keepalive(null);
			}
		};
		keepaliveTimer.schedule(task, 0, DELAY_TIME);
	}

	/**
	 * 停止keepAlive线程
	 */
	public void stopKeepAliveThread() {
		if (keepaliveTimer != null) {
			keepaliveTimer.cancel();
			keepaliveTimer = null;
		}
	}

	public void autoLogin(final OnCallback cb) {
		String accountName = getUserName();
		Secrecy mSecrecy = getSecrecy();
		if (TextUtils.isEmpty(accountName) || mSecrecy == null) {
			if (cb != null)
				cb.onFailure(-1, "error:auto login failure!");
			return;
		}

		login(accountName, mSecrecy, cb);
	}

	/**
	 * 根据nonce值加密登陆信息
	 * 
	 * @param name
	 * @param pwd
	 * @param nonce
	 * @param cb
	 */
	private void login(final String accountName, final Secrecy mSecrecy, final OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("userId", mSecrecy.getUid());
			json.put("login_key", mSecrecy.getPwd());
			json.put("login_nonce", mSecrecy.getNonce());
			json.put("device_type", User.DEVICE_TYPE_ANDROID);
			json.put("device_id", DeviceUtil.getDeviceUUID(mContext));
			// json.put("mobile_type", "huawei");
			json.put("mobile_type", "xiaomi");
			// json.put("mobile_type",
			// android.os.Build.BRAND.toLowerCase(Locale.getDefault()));
			json.put("app_version", getAppVersionName(mContext));
			json.put("mobile_version", android.os.Build.VERSION.RELEASE);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/login";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				JSONObject userJson = new JSONObject();
				try {
					JSONObject json = new JSONObject(content);
					userJson = json.optJSONObject("result");
					saveLoginInfo(accountName, mSecrecy);
					setUser(userJson);

					JSONObject hosts = json.optJSONObject("hosts");
					NCConstants.setHost(mContext, hosts);

					startKeepAliveThread();
					if (cb != null)
						cb.onSuccess();
				} catch (JSONException e) {
					if (cb != null)
						cb.onFailure(0, e.getMessage());
				}
			}

		});
	}

	@Override
	public void login(final String name, final String pwd, final OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("account", name);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/login/nonce";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					String nonce = json.optString("nonce");
					String userId = json.optString("userId");
					login(name, Secrecy.create(nonce, userId, pwd), cb);
				} catch (JSONException e) {
					if (cb != null)
						cb.onFailure(0, e.getMessage());
				}
			}
		});
	}

	@Override
	public void logout(OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("device_type", User.DEVICE_TYPE_ANDROID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/phone_logout";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));

	}

	@Override
	public void keepalive(OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/keepalive";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void deleteConference(String confId, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences/{0}";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void obtainEnterpriseContacts(String userId, int version, final OnSampleCallback oscb) {
		String baseUrl = NCConstants.getContactServer() + "/enterprise_contact?version={0}&userId={1}";
		String url = MessageFormat.format(baseUrl, String.valueOf(version), userId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(oscb) {
			@Override
			public void onSuccess(String content) {
				if (oscb != null)
					oscb.onSuccess(content);
			}
		});
	}

	@Override
	public void conveneConference(String subject, final OnSampleCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("subject", subject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/V1/conferences";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					String confId = json.optJSONObject("data").optString("id");
					if (cb != null)
						cb.onSuccess(confId);
				} catch (Exception e) {
					e.printStackTrace();
					if (cb != null)
						cb.onFailure(-1, "数据解析错误");
				}
			}
		});
	}

	@Override
	public void obtainConferenceStatistics(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences_count";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void obtainConferenceHistory(String time, final OnSampleCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("timestamp", time);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/my_conf_record";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void getMyAllConference(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/all";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void getMyConference(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void obtainNotifyInvitedInfoList(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/invitees/notification";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void obtainAcceptInvitedInfoList(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/invitees/accepted";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void sendInvitation(String confId, String sipNo, ArrayList<InvitePhoneInfo> phones, boolean pstn,
			OnCallback cb) {
		// {'sipno':'383398340','phones':[{'phone':'<被邀请人号码>'}]}
		String baseUrl = NCConstants.PHP_BASE_URL + "/V1/conferences/{0}/invitees";
		String url = MessageFormat.format(baseUrl, confId);
		JSONObject rootJson = new JSONObject();
		try {
			rootJson.put("sipno", sipNo);
			JSONArray phoneArray = new JSONArray();
			for (int i = 0; i < phones.size(); i++) {
				JSONObject phoneJson = new JSONObject();
				phoneJson.put("phone", phones.get(i).getPhone());
				phoneJson.put("nickname", phones.get(i).getName());
				phoneArray.put(phoneJson);
			}
			rootJson.put("phones", phoneArray);
			rootJson.put("pstn", pstn);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		asyncHttp.post(url, rootJson.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void cancelInvite(String confId, String mobile, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences/{0}/cancel_call/{1}";
		String url = MessageFormat.format(baseUrl, confId, mobile);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void obtainInvitationInfo(String inviterUri, final OnSampleCallback cb) {
		try {
			String baseUrl = NCConstants.PHP_BASE_URL + "/inviterinfo?inviter={0}";
			String arg = URLEncoder.encode(inviterUri, HTTP.UTF_8);
			String url = MessageFormat.format(baseUrl, arg);
			asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
				@Override
				public void onSuccess(String content) {
					if (cb != null)
						cb.onSuccess(content);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			if (cb != null)
				cb.onFailure(-1, "数据无法发出");
		}
	}

	@Override
	public void agreeInvite(String confId, String inviteeId, String inviterId, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences/{0}/invitation_accept/V1";
		String url = MessageFormat.format(baseUrl, confId);
		JSONObject json = new JSONObject();
		try {
			json.put("invitee_id", inviteeId);
			json.put("inviter_id", inviterId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		asyncHttp.post(url, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void rejectInvite(String confId, String inviteeId, String inviterId, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences/{0}/invitation_reject";
		String url = MessageFormat.format(baseUrl, confId);
		JSONObject json = new JSONObject();
		try {
			json.put("invitee_id", inviteeId);
			json.put("inviter_id", inviterId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		asyncHttp.post(url, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	public void obtainConferenceAllResource(String confId, SampleResponseCallbackHandler srcb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/resources";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.get(url, srcb);
	}

	public void obtainConferenceData(String confId, int version, SampleResponseCallbackHandler srcb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/V1/conferences/{0}";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.get(url, version, srcb);
	}

	public void obtainParticipantData(String confId, int version, SampleResponseCallbackHandler srcb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/V1/conferences/{0}/members";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.get(url, version, srcb);
	}

	public void obtainSharingData(String confId, int version, SampleResponseCallbackHandler srcb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/V1/conferences/{0}/sharings";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.get(url, version, srcb);
	}

	@Override
	public void obtainDocuments(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/documents";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public RequestHandle uploadFile(File file, final OnUploadCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/documents";
		return asyncHttp.upload(baseUrl, file, "file", new UploadFileResponseCallbackHandler(cb));
	}

	@Override
	public void uploadAvatar(File file, final OnUploadCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/account/avatar";
		asyncHttp.upload(baseUrl, file, "avatar", new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject json = new JSONObject(content);
					getUser().setAvatar(json.getString("avatar"));
					UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (cb != null)
					cb.onSuccess();
			}
		});
	}

	public void modifyNickName(final String nickName, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("nickname", nickName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/modify_nickname";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				getUser().setNickname(nickName);
				UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				super.onSuccess(content);
			}
		});
	}

	public void modifyCompany(final String company, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("company", company);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/modify_company";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				getUser().setCompany(company);
				UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				super.onSuccess(content);
			}

		});
	}

	public void modifyDepartment(final String department, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("department", department);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/modify_department";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				getUser().setDepartment(department);
				UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				super.onSuccess(content);
			}

		});
	}

	@Override
	public void obtainSharingDocumentAllPageData(final String sessionId, final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/confshare/{0}/pages";
		String url = MessageFormat.format(baseUrl, sessionId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void obtainSharingDocumentAllData(final String docId, final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/documents/{0}/pages";
		String url = MessageFormat.format(baseUrl, docId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void startRecord(String confId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/Record";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void stopRecord(String confId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/Record";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void startLock(String confId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/Lock";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void stopLock(String confId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/Lock";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void terminateConference(String confId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/members";
		String url = MessageFormat.format(baseUrl, confId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void setAllMute(String conferenceId, final OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/MuteAll";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void setAllUnmute(String conferenceId, final OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/MuteAll";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void handupMember(String confId, int memberPosition, String userId, String mobile, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/invitation?userId={2}&phone={3}";
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(memberPosition), userId, mobile);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void removeMember(String confId, int memberPosition, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}?userId={2}";
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(memberPosition), userId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void kickMember(String confId, int memberPosition, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/{2}";
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(memberPosition), userId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void kickMemberDevice(String confId, int memberPosition, String deviceId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/devices/{2}";
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(memberPosition), deviceId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void kickMemberDevices(String confId, int memberPosition, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/devices?userId={2}";
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(memberPosition), userId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void inviteMemberViaEmail(String confId, String[] inviteInfo, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/{0}/participants";
		String url = MessageFormat.format(baseUrl, confId);
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		for (int i = 0; i < inviteInfo.length; i++) {
			array.put(inviteInfo[i]);
		}
		try {
			json.put("emails", array);
		} catch (JSONException e) {
			LogUtil.printStackTrace(e);
		}
		asyncHttp.post(url, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void deleteDocuments(String docId, final OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/documents/{0}";
		String url = MessageFormat.format(baseUrl, docId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void applyVerificationCode(String phone, int type, boolean resend, final OnSampleCallback oscb) {
		JSONObject json = new JSONObject();
		try {
			json.put("phone", phone);
			json.put("type", type);
			json.put("resend", resend);
			json.put("deviceid", DeviceUtil.getDeviceUUID(mContext));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/signup/message_validcode";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(oscb) {

			@Override
			public void onSuccess(String content) {
				if (oscb != null)
					oscb.onSuccess(content);
			}

		});
	}

	@Override
	public void sendVerificationCode(String code, String identify, final OnSampleCallback oscb) {
		JSONObject json = new JSONObject();
		try {
			json.put("rand_code", code);
			json.put("identifier", identify);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/signup/check_identifier";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(oscb) {
			@Override
			public void onSuccess(String content) {
				if (oscb != null)
					oscb.onSuccess(content);
			}
		});
	}

	public void applyEmailVerificationCode(String email, String userId, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("user_mail", email);
			json.put("userId", userId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/account/bind_mail";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	public void bindingEmail(final String email, String code, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("user_mail", email);
			json.put("rand_code", code);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/account/update_mail";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				getUser().setEmail(email);
				UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				super.onSuccess(content);
			}
		});
	}

	@Override
	public void register(String userId, String nickname, String password, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("userId", userId);
			json.put("nickname", nickname);
			json.put("password", EncryptionUtil.MD5(userId + password));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/signup/phone_signup";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void modifyPassword(String userId, String oldPassword, final String newPassword, OnCallback cb) {
		String oldPwd = EncryptionUtil.MD5(userId + oldPassword);
		String newPwd = EncryptionUtil.MD5(userId + newPassword);

		JSONObject json = new JSONObject();
		try {
			json.put("userId", userId);
			json.put("password", oldPwd);
			json.put("newPassword", newPwd);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/account/update_password";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
			}

		});
	}

	@Override
	public void retrievePassword(String userId, String password, OnCallback cb) {
		String encryptionPwd = EncryptionUtil.MD5(userId + password);

		JSONObject json = new JSONObject();
		try {
			json.put("userId", userId);
			json.put("password", encryptionPwd);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/signup/find_password_phone";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void bindingPhoneNum(final String mobile, String code, String identify, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("rand_code", code);
			json.put("identifier", identify);
			json.put("mobile", mobile);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/account/bind_phone";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				getUser().setMobile(mobile);
				UserInfoSharedPreference.saveUserInformation(mContext, getUser().toJsonString());
				super.onSuccess(content);
			}
		});
	}

	@Override
	public void requestCallin(String conferenceId, String phone, String callerId, String callerName, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/invitation";
		String url = MessageFormat.format(baseUrl, conferenceId);
		JSONArray jAry = new JSONArray();
		JSONObject json = new JSONObject();
		try {
			json.put("phone", phone);
			json.put("callerid", callerId);
			json.put("callername", callerName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		jAry.put(json);

		asyncHttp.post(url, jAry.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void feedback(String userId, String content, int star, OnCallback cb) {
		JSONObject json = new JSONObject();
		try {
			json.put("userId", userId);
			json.put("content", content);
			json.put("star", star);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conferences/feedback";
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void addMembersToList(String conferenceId, User user, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members";
		String url = MessageFormat.format(baseUrl, conferenceId);
		String content = Participant.toJson(user, DeviceUtil.getDeviceUUID(mContext),
				SystemUtil.getAppVersionName(mContext)).toString();
		asyncHttp.post(url, content, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void changeController(String conferenceId, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/controller/{1}";
		String url = MessageFormat.format(baseUrl, conferenceId, userId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void removeController(String conferenceId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/controller";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void muteMember(String conferenceId, int memberPosition, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/{2}/Mute";
		String url = MessageFormat.format(baseUrl, conferenceId, memberPosition, userId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void unmuteMember(String conferenceId, int memberPosition, String userId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/{2}/Mute";
		String url = MessageFormat.format(baseUrl, conferenceId, memberPosition, userId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void muteMemberDevice(String conferenceId, int memberPosition, String deviceId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/devices/{2}/Mute";
		String url = MessageFormat.format(baseUrl, conferenceId, memberPosition, deviceId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void unmuteMemberDevice(String conferenceId, int memberPosition, String deviceId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/devices/{2}/Mute";
		String url = MessageFormat.format(baseUrl, conferenceId, memberPosition, deviceId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void addDocumentToList(final String conferenceId, final NetworkFileInfo nfInfo, final String userId,
			final String nickName, final OnCallback cb) {
		String baseUrl = NCConstants.getDocumentBaseUrl() + "/documents/sharing/{0}";
		String url = MessageFormat.format(baseUrl, nfInfo.getDocId());
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb) {

			@Override
			public void onSuccess(String content) {
				try {
					JSONObject retJson = new JSONObject(content);
					String baseuri = retJson.optString("baseuri");
					String sessionId = retJson.optString("doc_session_id");
					JSONObject json = SharingFile.toJson(conferenceId, nfInfo, userId, nickName, baseuri, sessionId);
					addDocumentToList(conferenceId, json, cb);
					super.onSuccess(content);
				} catch (JSONException e) {
					e.printStackTrace();
					if (cb != null)
						cb.onFailure(-100, "无法分享");
				}
			}

		});
	}

	@Override
	public void addDocumentToList(String conferenceId, JSONObject json, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/sharings";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.post(url, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	// @Deprecated
	// @Override
	// public void removeDocumentFromList(String conferenceId, int position,
	// OnCallback cb) {
	// String baseUrl = NCConstants.getDispBaseUrl() +
	// "/conferences/{0}/sharings/{1}";
	// String url = MessageFormat.format(baseUrl, conferenceId,
	// String.valueOf(position));
	// asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	// }

	@Override
	public void removeDocumentFromList(String conferenceId, int position, String docId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/V1/conferences/{0}/sharings/{1}?docId={2}";
		String url = MessageFormat.format(baseUrl, conferenceId, String.valueOf(position), docId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void setConferenceDisplaying(String conferenceId, String docId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/displaying/{1}";
		String url = MessageFormat.format(baseUrl, conferenceId, docId);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void stopConferenceDisplaying(String conferenceId, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/displaying";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void flipOverDocument(String conferenceId, int position, int page, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/sharings/{1}/locate/{2}";
		String url = MessageFormat.format(baseUrl, conferenceId, String.valueOf(position), page);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void obtainSharingList(String conferenceId, final OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/sharings";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess();
			}
		});

	}

	@Override
	public void cancelDocumentConveting(String docid, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/documents/{0}/converting";
		String url = MessageFormat.format(baseUrl, docid);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void sendHuaweiPushToken(String token, OnCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/save_hw_token";
		JSONObject json = new JSONObject();
		try {
			json.put("hw_token", token);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		asyncHttp.post(baseUrl, json.toString(), new SampleResponseCallbackHandler(cb));
	}

	@Override
	public void obtainConferenceDetail(String conferenceId, final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/uswitch/conference/detail?confid={0}";
		String url = MessageFormat.format(baseUrl, conferenceId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void queryPersonInformation(String userId, final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/member_info/{0}";
		String url = MessageFormat.format(baseUrl, userId);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void sendXiaomiReceiveResult(String confid, boolean result, String phone, OnCallback cb) {
		try {
			String baseUrl = NCConstants.getDispBaseUrl() + "/sipregisternotify?confid={0}&phone={1}&result={2}";
			confid = URLEncoder.encode(confid, HTTP.UTF_8);
			phone = URLEncoder.encode(phone, HTTP.UTF_8);
			String url = MessageFormat.format(baseUrl, confid, phone, result);
			asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			if (cb != null)
				cb.onFailure(-1, e.getMessage());
		}
	}

	@Override
	public void reconveneConference(String argument, final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/conferences/reconvene";
		asyncHttp.post(baseUrl, argument, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void getBalance(final OnSampleCallback cb) {
		String baseUrl = NCConstants.PHP_BASE_URL + "/amount/record";
		asyncHttp.get(baseUrl, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void getDownloadUrl(String sessionid, final OnSampleCallback cb) {
		String baseUrl = NCConstants.getDocumentBaseUrl() + "/conf_doc/{0}/raw";
		String url = MessageFormat.format(baseUrl, sessionid);
		asyncHttp.get(url, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				if (cb != null)
					cb.onSuccess(content);
			}
		});
	}

	@Override
	public void postNetworkChanged(String confId, int mPosition, int networkType, String userId, String uuid,
			OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/network/{2}?userid={3}&uuid={4}";
		try {
			userId = URLEncoder.encode(userId, HTTP.UTF_8);
			uuid = URLEncoder.encode(uuid, HTTP.UTF_8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(mPosition), String.valueOf(networkType),
				userId, uuid);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	public void postInterruption(String confId, int mPosition, String userId, String uuid, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/interruption?userId={2}&uuid={3}";
		try {
			userId = URLEncoder.encode(userId, HTTP.UTF_8);
			uuid = URLEncoder.encode(uuid, HTTP.UTF_8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(mPosition), userId, uuid);
		asyncHttp.post(url, new SampleResponseCallbackHandler(cb));
	}

	public void deleteInterruption(String confId, int mPosition, String userId, String uuid, OnCallback cb) {
		String baseUrl = NCConstants.getDispBaseUrl() + "/conferences/{0}/members/{1}/interruption?userId={2}&uuid={3}";
		try {
			userId = URLEncoder.encode(userId, HTTP.UTF_8);
			uuid = URLEncoder.encode(uuid, HTTP.UTF_8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = MessageFormat.format(baseUrl, confId, String.valueOf(mPosition), userId, uuid);
		asyncHttp.delete(url, new SampleResponseCallbackHandler(cb));
	}
}
