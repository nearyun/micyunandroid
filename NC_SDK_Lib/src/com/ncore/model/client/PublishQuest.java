package com.ncore.model.client;

/**
 * 发布请求信息
 * 
 * @author xiaohua
 * 
 */
public class PublishQuest {

	public interface OnPublishCallback {
		public void onCallback(int id, boolean result);
	}

	private static int count = 0;
	private int id;
	private String content;
	private OnPublishCallback mOPCB;

	public PublishQuest(OnPublishCallback _opcb) {
		id = count++;
		mOPCB = _opcb;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	public void excuteCallback(int id, boolean result) {
		if (mOPCB != null) {
			mOPCB.onCallback(id, result);
		}
	}
}
