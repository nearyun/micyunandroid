package com.ncore.model.client.impl;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.ncore.callback.OnCallback;
import com.ncore.config.NCConstants;
import com.ncore.model.Secrecy;
import com.ncore.model.User;
import com.ncore.model.client.IClient;
import com.ncore.sharedpreference.HostInfoSharedPreference;
import com.ncore.sharedpreference.UserInfoSharedPreference;

public class Client extends IClient {
	private static Client mClient = null;

	public static Client getInstance() {
		return mClient;
	}

	public static void create(Context context) {
		mClient = new Client(context);
		// 读取本地缓存的用户数据
		String userInformation = UserInfoSharedPreference.readUserInformation(context);
		if (!TextUtils.isEmpty(userInformation)) {
			try {
				mClient.setUser(new JSONObject(userInformation));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// 读取用户账号
		String username = UserInfoSharedPreference.readUserName(context);
		if (!TextUtils.isEmpty(username)) {
			// 读取用户秘密
			Secrecy secrecy = UserInfoSharedPreference.readSecrecy(context);
			mClient.setUserNameAndSecrecy(username, secrecy);
		}
		String hostsInfomation = HostInfoSharedPreference.readHostsInformation(context);
		if (!TextUtils.isEmpty(hostsInfomation)) {
			try {
				NCConstants.setHost(context, new JSONObject(hostsInfomation));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private String loginUserName = null;
	private Secrecy mSecrecy = null;

	private void setUserNameAndSecrecy(String username, Secrecy secrecy) {
		this.loginUserName = username;
		this.mSecrecy = secrecy;
	}

	@Override
	public String getUserName() {
		return loginUserName;
	}

	public Secrecy getSecrecy() {
		return mSecrecy;
	}

	@Override
	protected void saveLoginInfo(String username, Secrecy mSecrecy) {
		setUserNameAndSecrecy(username, mSecrecy);
		UserInfoSharedPreference.saveLoginInfo(mContext, username, mSecrecy);
	}

	@Override
	public boolean isValidatePwd(String password) {
		if (mSecrecy == null)
			return false;
		return mSecrecy.isValidatePwd(password);
	}

	public Client(Context context) {
		super(context);
	}

	@Override
	public void logout(OnCallback cb) {
		super.logout(cb);
		UserInfoSharedPreference.clearSecrecy(mContext);
		UserInfoSharedPreference.clearUserInformation(mContext);
		HostInfoSharedPreference.clearHostsInformation(mContext);
		create(mContext);
	}

	private final User mUser = new User();

	@Override
	public User getUser() {
		return mUser;
	}

	private boolean hasUserData = false;

	@Override
	public boolean hasUserData() {
		return hasUserData;
	}

	@Override
	protected void setUser(JSONObject userJson) {
		hasUserData = mUser.updateUserData(userJson);
		if (hasUserData)
			UserInfoSharedPreference.saveUserInformation(mContext, userJson.toString());
	}

}
