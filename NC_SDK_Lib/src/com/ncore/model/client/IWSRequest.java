//package com.ncore.model.client;
//
//import com.ncore.model.client.PublishQuest.OnPublishCallback;
//import com.ncore.websocket.push.impl.PushWebSocket.OnWebSocketConnectedListener;
//
///**
// * websocket发送数据接口
// * 
// * @author xiaohua
// * 
// */
//public interface IWSRequest {
//	/**
//	 * 订阅
//	 * 
//	 * @param callback
//	 */
//	public void subscribe(OnPublishCallback callback);
//
//	/**
//	 * 启动websocket监听
//	 */
//	public void startWSReceive(String userId, OnWebSocketConnectedListener mOnWebSocketConnected);
//
//	/**
//	 * 停止websocket监听
//	 */
//	public void stopWSReceive();
//
//	
//
//}
