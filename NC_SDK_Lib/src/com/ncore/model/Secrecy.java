package com.ncore.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;
import android.util.Base64;

import com.tornado.util.EncryptionUtil;

/**
 * 用户登录信息缓存加密
 * 
 * @author xiaohua
 * 
 */
public class Secrecy {

	private static final String LABEL_NONE = "no";
	private static final String LABEL_UID = "ui";
	private static final String LABEL_PWD = "pd";

	/**
	 * 明文秘密加密后生成Secrecy对象
	 * 
	 * @param nonce
	 * @param uid
	 * @param pwd
	 * @return
	 */
	public static final Secrecy create(String nonce, String uid, String pwd) {
		return new Secrecy(nonce, uid, encrypt(nonce, uid, pwd));
	}

	private static final String encrypt(String nonce, String uid, String pwd) {
		return EncryptionUtil.MD5(EncryptionUtil.MD5(uid + pwd) + nonce);
	}

	/**
	 * 从缓存文件生成Secrecy对象
	 * 
	 * @param content
	 * @return 参数为空或者格式不对，都会返回null
	 */
	public static final Secrecy create(String content) {
		if (TextUtils.isEmpty(content))
			return null;
		try {
			content = new String(Base64.decode(content, Base64.NO_WRAP));
			JSONObject json = new JSONObject(content);
			String nonce = json.optString(LABEL_NONE);
			String uid = json.optString(LABEL_UID);
			String pwd = json.optString(LABEL_PWD);
			return new Secrecy(nonce, uid, pwd);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Secrecy对象加密成字符串
	 * 
	 * @return
	 */
	public String toSecrecyString() {
		try {
			JSONObject json = new JSONObject();
			json.put(LABEL_NONE, nonce);
			json.put(LABEL_UID, uid);
			json.put(LABEL_PWD, pwd);
			return Base64.encodeToString(json.toString().getBytes(), Base64.NO_WRAP);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String nonce, uid, pwd;

	private Secrecy(String nonce, String uid, String pwd) {
		this.nonce = nonce;
		this.uid = uid;
		this.pwd = pwd;
	}

	public String getNonce() {
		return nonce;
	}

	public String getUid() {
		return uid;
	}

	public String getPwd() {
		return pwd;
	}

	public boolean isValidatePwd(String password) {
		String tmp = encrypt(nonce, uid, password);
		return TextUtils.equals(tmp, pwd);
	}
}
