package com.ncore.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.ncore.config.NCConstants;

/**
 * 用户信息
 * 
 * @author xiaohua
 * 
 */
public class User {
	public static final int DEVICE_TYPE_ANDROID = 1;
	public static final int DEVICE_TYPE_IOS = 2;
	public static final int DEVICE_TYPE_PC = 3;

	private String userId;
	private String nickname;
	private String avatar;
	private String textAvatar;
	private String department;
	private String sipAndroid;
	private String sipPassword;
	private String mobile;
	private String email;
	private String androidInstanceId;
	private String company;
	private int permissions;

	private static final int PERMISSION_PSTN = 1 << 0;

	private static final String LABEL_USER_ID = "userId";
	private static final String LABEL_NICK_NAME = "nickname";
	private static final String LABEL_TEXT_AVATAR = "textavatar";
	private static final String LABEL_AVATAR = "avatar";
	private static final String LABEL_DEPARTMENT = "department";
	private static final String LABEL_SIP_ANDROID = "sipAndroid";
	private static final String LABEL_SIP_IPHONE = "sipIphone";
	private static final String LABEL_SIP_PC = "sipPC";
	private static final String LABEL_SIP_PWD = "sipPwd";
	private static final String LABEL_MOBILE = "mobile";
	private static final String LABEL_EMAIL = "email";
	private static final String LABEL_ANDROID_INSTANCE_ID = "android_device_id";
	private static final String LABEL_IOS_DEVICE_ID = "ios_device_id";
	private static final String LABEL_PC_DEVICE_ID = "pc_device_id";
	private static final String LABEL_COMPANY = "company";
	private static final String LABEL_PERMISSIONS = "permissions";

	public User() {
	}

	private JSONObject userJson = null;

	public boolean updateUserData(JSONObject json) {
		if (!json.has(LABEL_USER_ID))
			return false;
		userJson = json;
		userId = json.optString(LABEL_USER_ID);
		nickname = json.optString(LABEL_NICK_NAME);
		avatar = json.optString(LABEL_AVATAR);
		textAvatar = json.optString(LABEL_TEXT_AVATAR);
		department = json.optString(LABEL_DEPARTMENT);
		sipAndroid = json.optString(LABEL_SIP_ANDROID);
		sipPassword = json.optString(LABEL_SIP_PWD);
		mobile = json.optString(LABEL_MOBILE);
		email = json.optString(LABEL_EMAIL);
		androidInstanceId = json.optString(LABEL_ANDROID_INSTANCE_ID);
		company = json.optString(LABEL_COMPANY);
		permissions = json.optInt(LABEL_PERMISSIONS);
		return true;
	}

	public boolean hasPSTN() {
		return (PERMISSION_PSTN & permissions) == PERMISSION_PSTN;
	}

	public void setCompany(String company) {
		this.company = company;
		try {
			userJson.put(LABEL_COMPANY, company);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getCompany() {
		return company;
	}

	public String getUserId() {
		return userId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
		try {
			userJson.put(LABEL_MOBILE, mobile);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		try {
			userJson.put(LABEL_EMAIL, email);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getSipUser() {
		return sipAndroid;
	}

	public String getSipPassword() {
		return sipPassword;
	}

	public void setNickname(String nickName) {
		this.nickname = nickName;
		try {
			userJson.put(LABEL_NICK_NAME, nickName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getNickname() {
		return nickname;
	}

	public String getAvatar() {
		return avatar;
	}

	public String getFullAvatar() {
		return NCConstants.PHP_BASE_URL + avatar;
	}

	public String getTextAvatar() {
		return textAvatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
		try {
			userJson.put(LABEL_AVATAR, avatar);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void setDepartment(String department) {
		this.department = department;
		try {
			userJson.put(LABEL_DEPARTMENT, department);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getDepartment() {
		return department;
	}

	public String getVoip() {
		return "sip:" + sipAndroid + "@" + NCConstants.getSipDomain();
	}

	public String getInstanceId() {
		return androidInstanceId;
	}

	public String toJsonString() {
		return userJson == null ? null : userJson.toString();
	}
}
