package com.ncore.model;

/**
 * 邀请号码信息
 * 
 * @author xiaohua
 * 
 */
public class InvitePhoneInfo {
	private String name;
	private String phone;

	public InvitePhoneInfo(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

}
