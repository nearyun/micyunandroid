package com.ncore.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ncore.config.NCConstants;

/**
 * 参会人员
 * 
 * @author xiaocui
 * 
 */
public class Participant {

	private static final String LABEL_USERID = "userId"; // 用户唯一标识
	private static final String LABEL_NICKNAME = "nickname";// 昵称
	private static final String LABEL_DEPARTMENT = "department";// 部门
	private static final String LABEL_AVATAR = "avatar";// 头像
	private static final String LABEL_TEXT_AVATAR = "textavatar";// 文字头像
	private static final String LABEL_DEVICE = "device";// 设备
	private static final String LABEL_MOBILE = "mobile";// 移动电话

	public static JSONObject toJson(User user, String uuid, String useragent) {
		JSONObject json = new JSONObject();
		try {
			json.put(LABEL_USERID, user.getUserId());
			json.put(LABEL_AVATAR, user.getAvatar());
			json.put(LABEL_TEXT_AVATAR, user.getTextAvatar());
			json.put(LABEL_NICKNAME, user.getNickname());
			json.put(LABEL_DEPARTMENT, user.getDepartment());
			json.put(LABEL_MOBILE, user.getMobile());

			JSONArray jArray = new JSONArray();
			jArray.put(SipDevice.toJson(uuid, user.getSipUser(), useragent, user.getVoip()));
			json.put(LABEL_DEVICE, jArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	private String nickName;
	private String avatar;
	private String textAvatar;
	private String department;
	private String userId;
	private String mobile;
	private int sequenceNumber;// 在列表中的排序
	private int order = 0;

	private SipDevice[] mSipDevices;

	public static final Participant create(int seq, JSONObject json) {
		Participant p = new Participant(seq);
		p.setNickName(json.optString(LABEL_NICKNAME));
		p.setAvatar(json.optString(LABEL_AVATAR));
		p.setTextAvatar(json.optString(LABEL_TEXT_AVATAR));
		p.setDepartment(json.optString(LABEL_DEPARTMENT));
		p.setUserId(json.optString(LABEL_USERID));
		p.setMobile(json.optString(LABEL_MOBILE));

		JSONArray deviceAry = json.optJSONArray(LABEL_DEVICE);
		p.parseDevice(deviceAry);
		return p;
	}

	protected void setNickName(String nickName) {
		this.nickName = nickName;
	}

	protected void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	protected void setTextAvatar(String textAvatar) {
		this.textAvatar = textAvatar;
	}

	protected void setDepartment(String department) {
		this.department = department;
	}

	protected void setUserId(String userId) {
		this.userId = userId;
	}

	protected void setMobile(String mobile) {
		this.mobile = mobile;
	}

	protected Participant(int seq) {
		sequenceNumber = seq;
	}

	private void parseDevice(JSONArray deviceAry) {
		int len = deviceAry.length();
		ArrayList<SipDevice> list = new ArrayList<SipDevice>();
		mSipDevices = new SipDevice[len];
		for (int i = 0; i < len; i++) {
			try {
				JSONObject dj = deviceAry.getJSONObject(i);
				SipDevice device = new SipDevice(dj);
				list.add(device);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		mSipDevices = list.toArray(new SipDevice[] {});
		list.clear();
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public String getDepartment() {
		return department;
	}

	public String getUserId() {
		return userId;
	}

	public String getNickName() {
		return nickName;
	}

	public String getAvatar() {
		return avatar;
	}

	public String getMobile() {
		return mobile;
	}

	public String getFullAvatar() {
		return NCConstants.PHP_BASE_URL + getAvatar();
	}

	public String getTextAvatar() {
		return textAvatar;
	}

	public boolean isOnline() {
		return mSipDevices != null && mSipDevices.length > 0;
	}

	public boolean isMute() {
		return isOnline() ? mSipDevices[0].isMute() : false;
	}

	public boolean isAndroidOnline() {
		if (!isOnline())
			return false;
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] != null && mSipDevices[i].getType() == SipDevice.DEVICE_ANDROID)
				return true;
		}
		return false;
	}

	public String getAndroidDeviceId() {
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] != null && mSipDevices[i].getType() == SipDevice.DEVICE_ANDROID)
				return mSipDevices[i].getUUID();
		}
		return null;
	}

	public String getAppleDeviceId() {
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] != null && mSipDevices[i].getType() == SipDevice.DEVICE_IPHONE)
				return mSipDevices[i].getUUID();
		}
		return null;
	}

	public String getPCDeviceId() {
		for (int i = 0; i < mSipDevices.length; i++) {
			if (mSipDevices[i] != null && mSipDevices[i].getType() == SipDevice.DEVICE_PC)
				return mSipDevices[i].getUUID();
		}
		return null;
	}

	public SipDevice[] getSipDevices() {
		return mSipDevices;
	}

	public boolean isAnswered() {
		return isOnline() ? mSipDevices[0].isAnswered() : false;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
}
