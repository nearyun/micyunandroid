package com.ncore.model.ws;

import com.ncore.model.client.PublishQuest;
import com.ncore.model.client.PublishQuest.OnPublishCallback;

public class WSPublish extends WSPublishTools {

	/**
	 * 获取订阅patch字符串
	 * 
	 * @param conferenceId
	 * @return
	 */
	public static PublishQuest getSubscribeString(String conferenceId, OnPublishCallback cb) {
		// "{\"action\":\"subscribe\",\"channel\":\"/conferences/{id}\"}"
		return getSubscribeJson(String.format("/conferences/%s", conferenceId), cb);
	}

	/**
	 * 获取取消订阅patch字符串
	 * 
	 * @param conferenceId
	 * @return
	 */
	public static PublishQuest getUnsubscribeString(String conferenceId, OnPublishCallback cb) {
		// "{\"action\":\"unsubscribe\",\"channel\":\"/conferences/{id}\"}"
		return getUnsubscribeJson(String.format("/conferences/%s", conferenceId), cb);
	}

}
