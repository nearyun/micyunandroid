package com.ncore.model.ws;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ncore.model.client.PublishQuest;
import com.ncore.model.client.PublishQuest.OnPublishCallback;
import com.ncore.util.LogUtil;

public abstract class WSPublishTools {
	private static final JSONObject getOperateJson(String op, String path, Object value) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("op", op);
			obj.put("path", path);
			obj.put("value", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}

	/**
	 * remove补丁
	 * 
	 * @param path
	 * @param value
	 * @return
	 */
	protected static final JSONObject getRemoveOpJson(String path, Object value) {
		return getOperateJson("remove", path, value);
	}

	/**
	 * replace补丁
	 * 
	 * @param path
	 * @param value
	 * @return
	 */
	protected static final JSONObject getReplaceOpJson(String path, Object value) {
		return getOperateJson("replace", path, value);
	}

	/**
	 * add补丁
	 * 
	 * @param path
	 * @param value
	 * @return
	 */
	protected static final JSONObject getAddOpJson(String path, Object value) {
		return getOperateJson("add", path, value);
	}

	private static final PublishQuest getFullFormatPublishJson(String action, String channel, JSONArray data,
			OnPublishCallback cb) {
		PublishQuest pQuest = new PublishQuest(cb);
		JSONObject json = new JSONObject();
		try {
			if (data != null)
				json.put("data", data);
			json.put("channel", channel);
			json.put("action", action);
			json.put("id", pQuest.getId() + "");
		} catch (JSONException e) {
			LogUtil.printStackTrace(e);
		}
		pQuest.setContent(json.toString());
		return pQuest;
	}

	/**
	 * publish 数据
	 * 
	 * @param channel
	 * @param data
	 * @param cb
	 * @return
	 */
	protected static final PublishQuest getPublishJson(String channel, JSONArray data, OnPublishCallback cb) {
		return getFullFormatPublishJson("publish", channel, data, cb);
	}

	protected static final PublishQuest getSubscribeJson(String channel, OnPublishCallback cb) {
		// "{\"action\":\"subscribe\",\"channel\":\"/conferences/{id}\"}"
		return getFullFormatPublishJson("subscribe", channel, null, cb);
	}

	protected static final PublishQuest getUnsubscribeJson(String channel, OnPublishCallback cb) {
		// // "{\"action\":\"unsubscribe\",\"channel\":\"/conferences/{id}\"}"
		return getFullFormatPublishJson("unsubscribe", channel, null, cb);
	}
}
