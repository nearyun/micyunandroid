package com.ncore.model.conference.jsonpatch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import com.ncore.model.sharing.SharingFile;

/**
 * 共享文档列表
 * 
 * @author xiaohua
 * 
 */
public class SharingJsonDocument extends JsonDocument {

	private final ArrayList<SharingFile> shareFileList = new ArrayList<SharingFile>();
	private final HashMap<String, SharingFile> sharingFileHashMap = new HashMap<String, SharingFile>();

	@Override
	public void reset() {
		super.reset();
		shareFileList.clear();
		sharingFileHashMap.clear();
	}

	@Override
	protected boolean refreshData() {
		try {
			shareFileList.clear();
			sharingFileHashMap.clear();
			JSONArray json = new JSONArray(documentObject.toString());
			int len = json.length();
			SharingFile tmpSF;
			for (int i = 0; i < len; i++) {
				tmpSF = SharingFile.create(i, json.optJSONObject(i));
				if (tmpSF.isDelete())
					continue;
				shareFileList.add(tmpSF);
				sharingFileHashMap.put(tmpSF.getDocId(), tmpSF);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	public ArrayList<SharingFile> getSharingFiles() {
		synchronized (this) {
			return shareFileList;
		}
	}

	public SharingFile getSharingFile(String docId) {
		synchronized (this) {
			return sharingFileHashMap.get(docId);
		}
	}
}
