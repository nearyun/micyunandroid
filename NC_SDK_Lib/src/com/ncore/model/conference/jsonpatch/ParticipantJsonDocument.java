package com.ncore.model.conference.jsonpatch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ncore.model.Participant;

/**
 * 成员会议列表
 * 
 * @author xiaohua
 * 
 */
public class ParticipantJsonDocument extends JsonDocument {

	private final ArrayList<Participant> participantList = new ArrayList<Participant>();
	private final HashMap<String, Participant> participantHashMap = new HashMap<String, Participant>();

	@Override
	public void reset() {
		super.reset();
		participantList.clear();
		participantHashMap.clear();
	}

	@Override
	protected boolean refreshData() {
		try {
			participantList.clear();
			participantHashMap.clear();
			JSONArray jsonAry = new JSONArray(documentObject.toString());
			int len = jsonAry.length();
			Participant tmpParticipant;
			for (int i = 0; i < len; i++) {
				JSONObject json = jsonAry.optJSONObject(i);
				if (json == null)
					continue;
				tmpParticipant = Participant.create(i, json);
				participantList.add(tmpParticipant);
				participantHashMap.put(tmpParticipant.getUserId(), tmpParticipant);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public ArrayList<Participant> getParticipants() {
		synchronized (this) {
			return participantList;
		}
	}

	public Participant getParticipant(String userId) {
		synchronized (this) {
			return participantHashMap.get(userId);
		}
	}

}
