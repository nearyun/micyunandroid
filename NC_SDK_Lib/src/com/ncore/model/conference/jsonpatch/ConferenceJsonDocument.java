package com.ncore.model.conference.jsonpatch;

import org.json.JSONException;
import org.json.JSONObject;

import com.ncore.model.conference.IConference;
import com.ncore.model.conference.impl.Conference;

/**
 * 会议json文档
 * 
 * @author xiaohua
 * 
 */
public class ConferenceJsonDocument extends JsonDocument {

	private Conference mConference = null;

	public ConferenceJsonDocument() {
		mConference = new Conference();
	}

	public IConference getConference() {
		return mConference;
	}

	@Override
	protected boolean refreshData() {
		try {
			JSONObject json = new JSONObject(documentObject.toString());
			mConference.refreshConferenceData(json);
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

}
