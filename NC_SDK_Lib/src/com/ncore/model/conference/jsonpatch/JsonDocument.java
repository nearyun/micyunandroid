package com.ncore.model.conference.jsonpatch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.ncore.util.LogUtil;

public abstract class JsonDocument {
	private String TAG = getClass().getSimpleName();

	protected int version = -1;
	protected JsonNode documentObject;
	protected final ObjectMapper mapper = new ObjectMapper();

	public int getVersion() {
		return version;
	}

	protected abstract boolean refreshData();

	public void reset() {
		version = -1;
		documentObject = null;
	}

	public static final int RESULT_PATCH_OK = 1;
	public static final int RESULT_PATCH_VERSION_NOT_MATCH = 2;
	public static final int RESULT_PATCH_FAILURE = 3;

	/**
	 * 打补丁
	 * 
	 * @param version
	 * @param patch
	 */
	public int patch(final int ver, String patch) {
		try {
			if (documentObject == null)
				return RESULT_PATCH_FAILURE;
			synchronized (this) {
				JsonPatch jsonPatch = mapper.readValue(patch, JsonPatch.class);
				documentObject = jsonPatch.apply(documentObject);
				if (documentObject == null) {
					throw new NullPointerException("err: documentObject is null");
				} else {
					this.version += 1;
					if (ver == this.version) {
						refreshData();
						return RESULT_PATCH_OK;
					} else {
						LogUtil.w(TAG, "warn: version not match");
						this.version = -1;
						return RESULT_PATCH_VERSION_NOT_MATCH;
					}
				}
			}
		} catch (Exception e) {
			this.version = -1;
			LogUtil.printStackTrace(e);
			return RESULT_PATCH_FAILURE;
		}
	}

	public boolean saveJsonDocument(int version, String document) {
		try {
			synchronized (this) {
				documentObject = mapper.readTree(document);
				if (documentObject != null) {
					this.version = version;
					refreshData();
					return true;
				} else {
					this.version = -1;
					return false;
				}
			}
		} catch (Exception e) {
			LogUtil.printStackTrace(e);
			this.version = -1;
			return false;
		}
	}

}
