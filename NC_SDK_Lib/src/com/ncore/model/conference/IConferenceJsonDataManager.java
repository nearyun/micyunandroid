package com.ncore.model.conference;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.ncore.callback.OnCallback;
import com.ncore.model.Participant;
import com.ncore.model.client.PublishQuest.OnPublishCallback;
import com.ncore.model.conference.jsonpatch.ConferenceJsonDocument;
import com.ncore.model.conference.jsonpatch.ParticipantJsonDocument;
import com.ncore.model.conference.jsonpatch.SharingJsonDocument;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.websocket.push.impl.PushWebSocket.OnWebSocketConnectedListener;

public abstract class IConferenceJsonDataManager implements IConferenceDataManager {

	protected ConferenceJsonDocument mConferenceJsonDocument = new ConferenceJsonDocument();
	protected ParticipantJsonDocument mParticipantJsonDocument = new ParticipantJsonDocument();
	protected SharingJsonDocument mSharingJsonDocument = new SharingJsonDocument();

	protected final HashMap<String, ArrayList<SharingFilePageInfo>> sharingFileAllPageHashMap = new HashMap<String, ArrayList<SharingFilePageInfo>>();

	/**
	 * 解析会议数据
	 * 
	 * @param json
	 * @return
	 */
	protected boolean parseConferenceJSON(JSONObject json) {
		if (json == null)
			return false;
		int ver = json.optInt("version", -1);
		JSONObject data = json.optJSONObject("data");
		if (ver < 0 || data == null)
			return false;
		return mConferenceJsonDocument.saveJsonDocument(ver, data.toString());
	}

	/**
	 * 解析共享文档列表数据
	 * 
	 * @param json
	 * @return
	 */
	protected boolean parseSharingsJSON(JSONObject json) {
		if (json == null)
			return false;
		int ver = json.optInt("version", -1);
		JSONArray data = json.optJSONArray("data");
		if (ver < 0 || data == null)
			return false;
		return mSharingJsonDocument.saveJsonDocument(ver, data.toString());
	}

	/**
	 * 解析成员列表数据
	 * 
	 * @param json
	 * @return
	 */
	protected boolean parseParticipantJSON(JSONObject json) {
		if (json == null)
			return false;
		int ver = json.optInt("version", -1);
		JSONArray data = json.optJSONArray("data");
		if (ver < 0 || data == null)
			return false;
		return mParticipantJsonDocument.saveJsonDocument(ver, data.toString());
	}

	@Override
	public IConference getConference() {
		return mConferenceJsonDocument.getConference();
	}

	@Override
	public ArrayList<SharingFile> getSharingFiles() {
		return mSharingJsonDocument.getSharingFiles();
	}

	@Override
	public SharingFile getSharingFile(String docId) {
		return mSharingJsonDocument.getSharingFile(docId);
	}

	@Override
	public ArrayList<Participant> getParticipants() {
		return mParticipantJsonDocument.getParticipants();
	}

	@Override
	public Participant getParticipant(String userId) {
		return mParticipantJsonDocument.getParticipant(userId);
	}

	/**
	 * 获取指定文档的所有页数据
	 */
	@Override
	public ArrayList<SharingFilePageInfo> getSharingFileAllPagesInfo(String docId) {
		synchronized (sharingFileAllPageHashMap) {
			if (TextUtils.isEmpty(docId))
				return null;
			return sharingFileAllPageHashMap.get(docId);
		}
	}

	/**
	 * 删除指定文档所有页数据
	 */
	@Override
	public void deleteSharingFileAllPagesInfo(String docId) {
		synchronized (sharingFileAllPageHashMap) {
			if (TextUtils.isEmpty(docId))
				return;
			sharingFileAllPageHashMap.remove(docId);
		}
	}

	/**
	 * 保存指定文档的所有页数据
	 * 
	 * @param docId
	 * @param content
	 */
	public void saveSharingFileAllPagesInfo2HashMap(String docId, String content) {
		synchronized (sharingFileAllPageHashMap) {
			try {
				JSONArray jsonArray = new JSONArray(content);
				ArrayList<SharingFilePageInfo> list = new ArrayList<SharingFilePageInfo>();
				int len = jsonArray.length();
				if (len == 0)
					return;
				for (int i = 0; i < len; i++) {
					list.add(new SharingFilePageInfo(jsonArray.getJSONObject(i)));
				}
				sharingFileAllPageHashMap.put(docId, list);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 订阅
	 * 
	 * @param callback
	 */
	protected abstract void subscribe(OnPublishCallback callback);

	/**
	 * 启动websocket监听
	 */
	protected abstract void startWSReceive(OnWebSocketConnectedListener mOnWebSocketConnected);

	/**
	 * 停止websocket监听
	 */
	protected abstract void stopWSReceive();

	/**
	 * 更新所有json文档数据
	 */
	public abstract void refreshAllDocuments(OnCallback cb);

	/**
	 * 获取会议json数据
	 */
	public abstract void getConferenceData(OnCallback cb);

	/**
	 * 获取人员json列表
	 */
	public abstract void listMembers(OnCallback cb);

	/**
	 * 获取共享文档json列表
	 */
	public abstract void listSharings(OnCallback cb);

	/**
	 * 初始化
	 * 
	 * @param oicb
	 */
	public abstract void initialize(OnConferenceInitializeCallback oicb);

	/**
	 * 执行呼叫
	 * 
	 * @param cb
	 */
	public abstract void requestCallin(OnCallback cb);

	/**
	 * 移除自己的信息
	 */
	public abstract void removeMyself();

	/**
	 * 更新网络类型
	 * 
	 * @param networkType
	 */
	public abstract void updateNetworkType(Context context);

	/**
	 * 刷新屏幕
	 * 
	 * @param docId
	 * @param listener
	 */
	public abstract void refreshDisplayingScreen(String docId, OnDisplayingScreenChangedListener listener);

	/**
	 * 翻页
	 * 
	 * @param docId
	 * @param page
	 * @param cb
	 */
	public abstract void flipOverDocument(int sequenceNumber, int page, OnCallback cb);
}
