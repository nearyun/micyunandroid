package com.ncore.model.conference;

import java.util.ArrayList;

import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;

public interface OnDisplayingScreenChangedListener {
	public void onEmptyViewer();

	public void onDisplayViewer(SharingFile sharingFile, ArrayList<SharingFilePageInfo> list);

	public void onErrorViewer();
}
