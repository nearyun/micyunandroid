//package com.ncore.model.conference.impl;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.text.TextUtils;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.fge.jsonpatch.JsonPatch;
//import com.ncore.event.ConferenceStatusChangedEvent;
//import com.ncore.event.ParticipantStatusChangedEvent;
//import com.ncore.event.SharingFileStatusChangedEvent;
//import com.ncore.model.Participant;
//import com.ncore.model.client.PublishQuest;
//import com.ncore.model.conference.IConference;
//import com.ncore.model.conference.IConferenceDataManager;
//import com.ncore.model.sharing.SharingFile;
//import com.ncore.model.sharing.SharingFilePageInfo;
//import com.ncore.websocket.push.OnApplyPatchListener;
//import com.ncore.websocket.push.impl.PushWebSocket;
//import com.ncore.websocket.push.impl.PushWebSocket.OnWebSocketConnectedListener;
//
//import de.greenrobot.event.EventBus;
//
//public class ConferenceDataManager extends Conference implements IConferenceDataManager {
//	private final ObjectMapper mapper = new ObjectMapper();
//
//	private JsonNode mConferenceJson = null; // 会议室
//	private JsonNode mParticipantsJson = null; // 与会者
//	private JsonNode mSharingFilesJson = null; // 共享文档
//
//	private final HashMap<String, Participant> participantHashMap = new HashMap<String, Participant>();
//	private final HashMap<String, SharingFile> sharingFileHashMap = new HashMap<String, SharingFile>();
//
//	private final ArrayList<SharingFile> shareFileList = new ArrayList<SharingFile>();
//	private final ArrayList<Participant> participantList = new ArrayList<Participant>();
//
//	private final HashMap<String, ArrayList<SharingFilePageInfo>> sharingFileAllPageHashMap = new HashMap<String, ArrayList<SharingFilePageInfo>>();
//
//	private PushWebSocket pushWSRunnable;
//
//	private final Object LOCK_CONFERENCE = new Object();
//	private final Object LOCK_PARTICIPANTS = new Object();
//	private final Object LOCK_SHARING_FILES = new Object();
//	private final Object LOCK_PAGEINFO = new Object();
//
//	private OnApplyPatchListener mOnApplyPatchListener = new OnApplyPatchListener() {
//		@Override
//		public void onApplySharingFiles(String conferenceId, int etag, String patch) {
//			if (TextUtils.equals(conferenceId, ConferenceDataManager.this.getId())) {
//				applyPatch2SharingFilesJson(patch);
//				EventBus.getDefault().postSticky(new SharingFileStatusChangedEvent(conferenceId, null));
//			}
//		}
//
//		@Override
//		public void onApplyParticipants(String conferenceId, int etag, String patch) {
//			if (TextUtils.equals(conferenceId, ConferenceDataManager.this.getId())) {
//				applyPatch2ParticipantsJson(patch);
//				EventBus.getDefault().postSticky(new ParticipantStatusChangedEvent(conferenceId));
//			}
//		}
//
//		@Override
//		public void onApplyConference(String conferenceId, int etag, String patch) {
//			if (TextUtils.equals(conferenceId, ConferenceDataManager.this.getId())) {
//				applyPatch2ConferenceJson(patch);
//				EventBus.getDefault().postSticky(new ConferenceStatusChangedEvent(conferenceId));
//			}
//		}
//
//	};
//
//	public void initWebSocket(String userId, String uuid, OnWebSocketConnectedListener mOnWebSocketConnected) {
//		if (pushWSRunnable != null) {
//			pushWSRunnable.stop();
//			pushWSRunnable = null;
//		}
//		pushWSRunnable = new PushWebSocket(getId(), userId, uuid, mOnWebSocketConnected);
//
//		pushWSRunnable.start();
//		pushWSRunnable.setOnApplyPatchListener(mOnApplyPatchListener);
//	}
//
//	public void stop() {
//		if (pushWSRunnable != null)
//			pushWSRunnable.stop();
//	}
//
//	public void sendMessage(PublishQuest quest) {
//		if (pushWSRunnable != null)
//			pushWSRunnable.sendMessage(quest);
//	}
//
//	// @Override
//	// public boolean isConferenceRunning() {
//	// synchronized (LOCK_CONFERENCE) {
//	// return mConferenceJson != null && mConferenceJson.size() > 3;
//	// }
//	// }
//
//	@Override
//	public void reset() {
//		synchronized (LOCK_CONFERENCE) {
//			mConferenceJson = null;
//		}
//		synchronized (LOCK_PARTICIPANTS) {
//			mParticipantsJson = null;
//			participantHashMap.clear();
//			participantList.clear();
//		}
//		synchronized (LOCK_SHARING_FILES) {
//			mSharingFilesJson = null;
//			sharingFileHashMap.clear();
//			shareFileList.clear();
//		}
//	}
//
//	private JsonNode readAsJsonNode(String original) {
//		try {
//			return mapper.readTree(original);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	/**
//	 * 保存为会议json文档
//	 * 
//	 * @param original
//	 */
//	public void saveAsConferenceJson(String original) {
//		synchronized (LOCK_CONFERENCE) {
//			mConferenceJson = readAsJsonNode(original);
//			refreshConference();
//		}
//	}
//
//	/**
//	 * 给会议json文档打补丁
//	 * 
//	 * @param patch
//	 */
//	public void applyPatch2ConferenceJson(String patch) {
//		try {
//			JsonPatch jsonPatch = mapper.readValue(patch, JsonPatch.class);
//			synchronized (LOCK_CONFERENCE) {
//				mConferenceJson = jsonPatch.apply(mConferenceJson);
//				refreshConference();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 更新会议数据
//	 */
//	private void refreshConference() {
//		if (mConferenceJson == null)
//			return;
//		try {
//			JSONObject json = new JSONObject(mConferenceJson.toString());
//			this.refreshConferenceData(json);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 保存为正在共享文件json文档
//	 * 
//	 * @param original
//	 */
//	public void saveAsSharingFilesJson(String original) {
//		synchronized (LOCK_SHARING_FILES) {
//			mSharingFilesJson = readAsJsonNode(original);
//			refreshSharingFile();
//		}
//	}
//
//	/**
//	 * 给共享文件json文档打补丁
//	 * 
//	 * @param patch
//	 */
//	public void applyPatch2SharingFilesJson(String patch) {
//		try {
//			JsonPatch jsonPatch = mapper.readValue(patch, JsonPatch.class);
//			synchronized (LOCK_SHARING_FILES) {
//				mSharingFilesJson = jsonPatch.apply(mSharingFilesJson);
//				refreshSharingFile();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 解析共享文件队列的json内容
//	 */
//	private void refreshSharingFile() {
//		try {
//			shareFileList.clear();
//			sharingFileHashMap.clear();
//			if (mSharingFilesJson == null)
//				return;
//			JSONArray json = new JSONArray(mSharingFilesJson.toString());
//			int len = json.length();
//			SharingFile tmpSF;
//			for (int i = 0; i < len; i++) {
//				tmpSF = new SharingFile(i, json.optJSONObject(i));
//				if (tmpSF.isDelete())
//					continue;
//				shareFileList.add(tmpSF);
//				sharingFileHashMap.put(tmpSF.getDocId(), tmpSF);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 获取正在共享文档对象列表
//	 * 
//	 * @return
//	 */
//	@Override
//	public ArrayList<SharingFile> getSharingFiles() {
//		synchronized (LOCK_SHARING_FILES) {
//			return shareFileList;
//		}
//	}
//
//	@Override
//	public SharingFile getSharingFile(String docId) {
//		synchronized (LOCK_SHARING_FILES) {
//			return sharingFileHashMap.get(docId);
//		}
//	}
//
//	/**
//	 * 保存为与会者json文档
//	 * 
//	 * @param original
//	 */
//	public void saveAsParticipantsJson(String original) {
//		synchronized (LOCK_PARTICIPANTS) {
//			mParticipantsJson = readAsJsonNode(original);
//			refreshParticipant();
//		}
//	}
//
//	/**
//	 * 给与会者json文档打补丁
//	 * 
//	 * @param patch
//	 */
//	public void applyPatch2ParticipantsJson(String patch) {
//		try {
//			JsonPatch jsonPatch = mapper.readValue(patch, JsonPatch.class);
//			synchronized (LOCK_PARTICIPANTS) {
//				mParticipantsJson = jsonPatch.apply(mParticipantsJson);
//				refreshParticipant();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 解析与会人员列表的json内容
//	 */
//	private void refreshParticipant() {
//		try {
//			synchronized (LOCK_PARTICIPANTS) {
//				participantList.clear();
//				participantHashMap.clear();
//				if (mParticipantsJson == null)
//					return;
//				JSONArray jsonAry = new JSONArray(mParticipantsJson.toString());
//				int len = jsonAry.length();
//				Participant tmpParticipant;
//				for (int i = 0; i < len; i++) {
//					JSONObject json = jsonAry.optJSONObject(i);
//					if (json == null)
//						continue;
//					tmpParticipant = new Participant(i, json);
//					if (tmpParticipant.isOnline()) {
//						participantList.add(tmpParticipant);
//						participantHashMap.put(tmpParticipant.getUserId(), tmpParticipant);
//					}
//				}
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 获取与会者对象列表
//	 * 
//	 * @return
//	 */
//	@Override
//	public ArrayList<Participant> getParticipants() {
//		synchronized (LOCK_PARTICIPANTS) {
//			return participantList;
//		}
//	}
//
//	@Override
//	public Participant getParticipant(String userId) {
//		synchronized (LOCK_PARTICIPANTS) {
//			return participantHashMap.get(userId);
//		}
//	}
//
//	/**
//	 * 获取指定文档的所有页数据
//	 */
//	@Override
//	public ArrayList<SharingFilePageInfo> getSharingFileAllPagesInfo(String docId) {
//		synchronized (LOCK_PAGEINFO) {
//			if (TextUtils.isEmpty(docId))
//				return null;
//			return sharingFileAllPageHashMap.get(docId);
//		}
//	}
//
//	/**
//	 * 删除指定问的所有页数据
//	 */
//	@Override
//	public void deleteSharingFileAllPagesInfo(String docId) {
//		synchronized (LOCK_PAGEINFO) {
//			if (TextUtils.isEmpty(docId))
//				return;
//			sharingFileAllPageHashMap.remove(docId);
//		}
//	}
//
//	/**
//	 * 保存指定文档的所有页数据
//	 * 
//	 * @param docId
//	 * @param content
//	 */
//	public void saveSharingFileAllPagesInfo2HashMap(String docId, String content) {
//		synchronized (LOCK_PAGEINFO) {
//			try {
//				JSONArray jsonArray = new JSONArray(content);
//				ArrayList<SharingFilePageInfo> list = new ArrayList<SharingFilePageInfo>();
//				int len = jsonArray.length();
//				if (len == 0)
//					return;
//				for (int i = 0; i < len; i++) {
//					list.add(new SharingFilePageInfo(jsonArray.getJSONObject(i)));
//				}
//				sharingFileAllPageHashMap.put(docId, list);
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//	@Override
//	public IConference getConference() {
//		return this;
//	}
//}
