package com.ncore.model.conference.impl;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.ncore.callback.OnCallback;
import com.ncore.callback.OnSampleCallback;
import com.ncore.event.ConferenceStatusChangedEvent;
import com.ncore.event.ParticipantStatusChangedEvent;
import com.ncore.event.SharingFileStatusChangedEvent;
import com.ncore.http.SampleResponseCallbackHandler;
import com.ncore.model.Interruption;
import com.ncore.model.Interruption.OnInterruptedParticipantCallback;
import com.ncore.model.Participant;
import com.ncore.model.SipDevice;
import com.ncore.model.client.IClient;
import com.ncore.model.client.PublishQuest.OnPublishCallback;
import com.ncore.model.client.impl.Client;
import com.ncore.model.conference.IConferenceJsonDataManager;
import com.ncore.model.conference.OnConferenceInitializeCallback;
import com.ncore.model.conference.OnDisplayingScreenChangedListener;
import com.ncore.model.conference.jsonpatch.JsonDocument;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;
import com.ncore.model.ws.WSPublish;
import com.ncore.util.LogUtil;
import com.ncore.websocket.push.OnApplyPatchListener;
import com.ncore.websocket.push.impl.PushWebSocket;
import com.ncore.websocket.push.impl.PushWebSocket.OnWebSocketConnectedListener;

import de.greenrobot.event.EventBus;

public class ConferenceJsonDataManager extends IConferenceJsonDataManager implements OnApplyPatchListener {

	private static final String TAG = ConferenceJsonDataManager.class.getSimpleName();

	private String conferenceId;
	private String currentUserId;
	private String uuid;
	private IClient mIClient;
	private PushWebSocket pushWSRunnable;

	private Interruption mInterruption;

	public ConferenceJsonDataManager(Context cxt, IClient client, String confId, String userId, String uuid) {
		this.mIClient = client;
		if (TextUtils.isEmpty(confId))
			throw new RuntimeException("error: conference can not be null");
		this.conferenceId = confId;
		if (TextUtils.isEmpty(userId))
			throw new RuntimeException("error: userId can not be null");
		this.currentUserId = userId;
		this.uuid = uuid;
		mInterruption = new Interruption(cxt, client, conferenceId, currentUserId, uuid, new OnInterruptedParticipantCallback() {

			@Override
			public Participant onInterrupted(String userId) {
				return getParticipant(userId);
			}

		});
		mInterruption.start();
	}

	@Override
	public void onApplySharingFiles(String confId, int etag, String patch) {
		if (!TextUtils.equals(confId, conferenceId))
			return;
		final int ret = mSharingJsonDocument.patch(etag, patch);
		if (ret == JsonDocument.RESULT_PATCH_OK) {
			EventBus.getDefault().postSticky(new SharingFileStatusChangedEvent(conferenceId, null));
		} else {
			listSharings(new OnCallback() {

				@Override
				public void onSuccess() {
					EventBus.getDefault().postSticky(new SharingFileStatusChangedEvent(conferenceId, null));
				}

				@Override
				public void onFailure(int errorCode, String reason) {

				}
			});
		}
	}

	@Override
	public void onApplyParticipants(String confId, int etag, String patch) {
		if (!TextUtils.equals(confId, conferenceId))
			return;
		final int ret = mParticipantJsonDocument.patch(etag, patch);
		if (ret == JsonDocument.RESULT_PATCH_OK) {
			EventBus.getDefault().postSticky(new ParticipantStatusChangedEvent(conferenceId));
		} else {
			listMembers(new OnCallback() {

				@Override
				public void onSuccess() {
					EventBus.getDefault().postSticky(new ParticipantStatusChangedEvent(conferenceId));
				}

				@Override
				public void onFailure(int errorCode, String reason) {

				}
			});
		}
	}

	@Override
	public void onApplyConference(String confId, int etag, String patch) {
		if (!TextUtils.equals(confId, conferenceId))
			return;
		final int ret = mConferenceJsonDocument.patch(etag, patch);
		if (ret == JsonDocument.RESULT_PATCH_OK) {
			EventBus.getDefault().postSticky(new ConferenceStatusChangedEvent(conferenceId));
		} else {
			getConferenceData(new OnCallback() {

				@Override
				public void onSuccess() {
					EventBus.getDefault().postSticky(new ConferenceStatusChangedEvent(conferenceId));
				}

				@Override
				public void onFailure(int errorCode, String reason) {

				}
			});
		}
	}

	@Override
	protected void startWSReceive(OnWebSocketConnectedListener mOnWebSocketConnected) {
		stopWSReceive();
		pushWSRunnable = new PushWebSocket(conferenceId, currentUserId, uuid, mOnWebSocketConnected);

		pushWSRunnable.start();
		pushWSRunnable.setOnApplyPatchListener(this);
	}

	@Override
	protected void stopWSReceive() {
		if (pushWSRunnable != null) {
			pushWSRunnable.stop();
			pushWSRunnable = null;
		}
	}

	@Override
	protected void subscribe(OnPublishCallback callback) {
		if (pushWSRunnable != null)
			pushWSRunnable.sendMessage(WSPublish.getSubscribeString(conferenceId, callback));
	}

	@Override
	public void reset() {
		mInterruption.stop();
		cancelHandlerTimer();
		stopWSReceive();
		mConferenceJsonDocument.reset();
		mParticipantJsonDocument.reset();
		mSharingJsonDocument.reset();
	}

	@Override
	public void refreshAllDocuments(final OnCallback cb) {
		mIClient.obtainConferenceAllResource(conferenceId, new SampleResponseCallbackHandler(cb) {
			@Override
			public void onSuccess(String content) {
				try {
					JSONObject dataJson = new JSONObject(content);
					JSONObject conferenceJson = dataJson.optJSONObject("confdoc");
					boolean conferenceFlag = parseConferenceJSON(conferenceJson);
					if (!conferenceFlag) {
						if (cb != null)
							cb.onFailure(-100, "数据不完整");
						return;
					}
					int state = getConference().getState();
					if (Conference.CONF_STATE_DESTROYED == state || Conference.CONF_STATE_STOPPED == state) {
						if (cb != null)
							cb.onFailure(407, "会议已停止");
						return;
					}

					JSONObject sharingsJson = dataJson.optJSONObject("sharings");
					boolean sharingFlag = parseSharingsJSON(sharingsJson);
					if (!sharingFlag) {
						if (cb != null)
							cb.onFailure(-101, "数据不完整");
						return;
					}
					JSONObject participantJson = dataJson.optJSONObject("members");
					boolean participantFlag = parseParticipantJSON(participantJson);
					if (!participantFlag) {
						if (cb != null)
							cb.onFailure(-102, "数据不完整");
						return;
					}
					if (cb != null)
						cb.onSuccess();
				} catch (JSONException e) {
					e.printStackTrace();
					if (cb != null)
						cb.onFailure(-103, "数据不完整");
				}
			}
		});
	}

	@Override
	public void getConferenceData(final OnCallback cb) {
		mIClient.obtainConferenceData(conferenceId, mConferenceJsonDocument.getVersion(),
				new SampleResponseCallbackHandler(cb) {
					@Override
					public void onSuccess(String content) {
						// { "version": 8, "code": 304, "data": {} }
						try {
							JSONObject conferenceJson = new JSONObject(content);
							int code = conferenceJson.optInt("code");
							int ver = conferenceJson.optInt("version");
							if (code == 304) {
								if (mConferenceJsonDocument.getVersion() != ver)
									getConferenceData(cb);
								else if (cb != null)
									cb.onSuccess();
							} else {// code = 200
								parseConferenceJSON(conferenceJson);
								if (cb != null)
									cb.onSuccess();
							}
						} catch (JSONException e) {
							e.printStackTrace();
							if (cb != null)
								cb.onFailure(-103, "数据不完整");
						}

					}
				});
	}

	@Override
	public void listMembers(final OnCallback cb) {
		mIClient.obtainParticipantData(conferenceId, mParticipantJsonDocument.getVersion(),
				new SampleResponseCallbackHandler(cb) {
					@Override
					public void onSuccess(String content) {
						try {
							JSONObject participantJson = new JSONObject(content);
							int code = participantJson.optInt("code");
							int ver = participantJson.optInt("version");
							if (code == 304) {
								if (mParticipantJsonDocument.getVersion() != ver)
									listMembers(cb);
								else if (cb != null)
									cb.onSuccess();
							} else {// code = 200
								parseParticipantJSON(participantJson);
								if (cb != null)
									cb.onSuccess();
							}
						} catch (JSONException e) {
							e.printStackTrace();
							if (cb != null)
								cb.onFailure(-103, "数据不完整");
						}
					}
				});
	}

	@Override
	public void listSharings(final OnCallback cb) {
		mIClient.obtainSharingData(conferenceId, mSharingJsonDocument.getVersion(), new SampleResponseCallbackHandler(
				cb) {
			@Override
			public void onSuccess(String content) {
				try {
					JSONObject sharingJson = new JSONObject(content);
					int code = sharingJson.optInt("code");
					int ver = sharingJson.optInt("version");
					if (code == 304) {
						if (mSharingJsonDocument.getVersion() != ver)
							listMembers(cb);
						else if (cb != null)
							cb.onSuccess();
					} else {// code = 200
						parseSharingsJSON(sharingJson);
						if (cb != null)
							cb.onSuccess();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					if (cb != null)
						cb.onFailure(-103, "数据不完整");
				}
			}
		});
	}

	@Override
	public void updateNetworkType(Context context) {
		int networkType = SipDevice.convert2NetworkType(context);
		Participant p = getParticipant(currentUserId);
		if (p == null)
			return;
		SipDevice[] mSipDevice = p.getSipDevices();
		if (mSipDevice == null)
			return;
		for (int i = 0; i < mSipDevice.length; i++) {
			if (mSipDevice[i] == null)
				continue;
			if (mSipDevice[i].isAndroidDevice())
				mIClient.postNetworkChanged(conferenceId, p.getSequenceNumber(), networkType, currentUserId, uuid, null);
		}
	}

	@Override
	public void removeMyself() {
		Participant p = getParticipant(currentUserId);
		if (p == null)
			return;
		String deviceId = p.getAndroidDeviceId();
		if (!TextUtils.isEmpty(deviceId))
			mIClient.kickMemberDevice(conferenceId, p.getSequenceNumber(), deviceId, null);
	}

	@Override
	public void requestCallin(final OnCallback cb) {
		mIClient.requestCallin(conferenceId, mIClient.getUser().getVoip(), mIClient.getUser().getVoip(), mIClient
				.getUser().getNickname(), new OnCallback() {

			@Override
			public void onSuccess() {
				if (cb != null)
					cb.onSuccess();
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, errorCode + " 无法接入语音 " + reason);
				retryExecuteCallin(cb);
			}
		});

	}

	private CallinRunnable executeCallinRetryTimesRunnable = null;

	private void retryExecuteCallin(OnCallback cb) {
		if (executeCallinRetryTimesRunnable == null)
			executeCallinRetryTimesRunnable = new CallinRunnable(cb);
		roomHandler.postDelayed(executeCallinRetryTimesRunnable, RETRY_WAIT_TIME);
	}

	class CallinRunnable implements Runnable {
		private OnCallback cb = null;

		public CallinRunnable(OnCallback cb) {
			this.cb = cb;
		}

		/** 执行请求呼入，尝试次数 */
		private int executeCallinRetryTimes = 0;

		@Override
		public void run() {
			if (executeCallinRetryTimes++ <= MAX_RETRY_TIMERS) {
				requestCallin(cb);
			} else {
				if (cb != null)
					cb.onFailure(700, "无法接入语音");
			}
		}
	}

	private Handler roomHandler = new Handler();
	private boolean firstConnectedWebsocket = true;

	private final int TOTAL_PROGRESS = 3;

	private void cancelHandlerTimer() {
		roomHandler.removeCallbacks(waitForWebSocketConnectRunnable);
		roomHandler.removeCallbacks(addSelfToListRetryTimesRunnable);
		roomHandler.removeCallbacks(executeCallinRetryTimesRunnable);
		roomHandler.removeCallbacks(subscribeRunnable);
	}

	@Override
	public void initialize(final OnConferenceInitializeCallback oicb) {
		if (oicb != null) {
			oicb.onStart();
			oicb.onProgress(1, TOTAL_PROGRESS, "正在获取数据...");
		}
		refreshAllDocuments(new OnCallback() {

			@Override
			public void onSuccess() {
				executeStartWSReceive(oicb);
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				if (oicb != null)
					oicb.onFailure(errorCode, reason);
			}
		});
	}

	private class WebsocketConnectRunnable implements Runnable {
		private OnConferenceInitializeCallback oicb;

		public WebsocketConnectRunnable(OnConferenceInitializeCallback oicb) {
			this.oicb = oicb;
		}

		@Override
		public void run() {
			isWSRunning = false;
			if (oicb != null)
				oicb.onWSConnectFailure();
		}
	}

	private WebsocketConnectRunnable waitForWebSocketConnectRunnable = null;
	private boolean isWSRunning = false;

	private void executeStartWSReceive(final OnConferenceInitializeCallback oicb) {
		if (oicb != null) {
			oicb.onProgress(2, TOTAL_PROGRESS, "正在连接后台...");
		}
		if (waitForWebSocketConnectRunnable == null)
			waitForWebSocketConnectRunnable = new WebsocketConnectRunnable(oicb);

		isWSRunning = true;
		firstConnectedWebsocket = true;
		startWSReceive(new OnWebSocketConnectedListener() {

			@Override
			public void onConnected() {
				roomHandler.removeCallbacks(waitForWebSocketConnectRunnable);
				roomHandler.removeCallbacks(subscribeRunnable);

				if (!isWSRunning) {
					return;
				}

				// subscribe(null);
				roomHandler.post(subscribeRunnable);
				if (!firstConnectedWebsocket) {
					return;
				}
				firstConnectedWebsocket = false;

				addSelfInformationToMemberList(oicb, 500);
			}

		});
		roomHandler.postDelayed(waitForWebSocketConnectRunnable, 5000);
	}

	private Runnable subscribeRunnable = new Runnable() {

		@Override
		public void run() {
			subscribe(null);
			roomHandler.postDelayed(this, 60000);
		}

	};

	/**
	 * 添加个人的信息到会议成员列表里，若成员列表已经存在该成员的设备信息，不需要post自己的信息
	 */
	private void loopAddSelfInformationToMemberList(final OnConferenceInitializeCallback oicb) {
		if (oicb != null) {
			oicb.onProgress(3, TOTAL_PROGRESS, "正在检验数据...");
		}

		Participant self = getParticipant(currentUserId);
		if (self == null) {// 根本不存在自己的信息，post自己上去
			addSelfInformationToMemberList(oicb);
		} else {
			// 判断是否存在自己的设备
			if (TextUtils.equals(self.getAndroidDeviceId(), uuid)) {
				if (oicb != null)
					oicb.onCompleted();
			} else {
				addSelfInformationToMemberList(oicb);
			}

		}

	}

	/** 最大尝试次数 */
	private static final int MAX_RETRY_TIMERS = 3;
	/** 等待数据返回时间 */
	private static final int RETRY_WAIT_TIME = 1000;

	private class SelfRetryRunnable implements Runnable {
		/** 添加个人信息到成员列表尝试次数 */
		private int addSelfInformationToListRetryTimes = 0;
		private OnConferenceInitializeCallback oicb;

		public SelfRetryRunnable(OnConferenceInitializeCallback oicb) {
			this.oicb = oicb;
		}

		@Override
		public void run() {
			if (addSelfInformationToListRetryTimes++ < MAX_RETRY_TIMERS) {
				loopAddSelfInformationToMemberList(oicb);
			} else {
				if (oicb != null)
					oicb.onFailure(601, "网络故障(03)");
			}
		}

	}

	private SelfRetryRunnable addSelfToListRetryTimesRunnable = null;

	private void addSelfInformationToMemberList(OnConferenceInitializeCallback oicb, long delay) {
		if (addSelfToListRetryTimesRunnable == null)
			addSelfToListRetryTimesRunnable = new SelfRetryRunnable(oicb);
		roomHandler.postDelayed(addSelfToListRetryTimesRunnable, delay);
	}

	private void addSelfInformationToMemberList(final OnConferenceInitializeCallback oicb) {
		mIClient.addMembersToList(conferenceId, mIClient.getUser(), new OnCallback() {

			@Override
			public void onSuccess() {
				addSelfInformationToMemberList(oicb, RETRY_WAIT_TIME);
			}

			@Override
			public void onFailure(int errorCode, String reason) {
				LogUtil.e(TAG, errorCode + " 添加成员失败 " + reason);
				addSelfInformationToMemberList(oicb, RETRY_WAIT_TIME);
			}
		});
	}

	@Override
	public void refreshDisplayingScreen(final String docId, final OnDisplayingScreenChangedListener listener) {
		if (listener == null)
			return;
		if (TextUtils.isEmpty(docId)) {
			listener.onEmptyViewer();
			return;
		}

		// 先判断内存中是否存在该文档所有页
		ArrayList<SharingFilePageInfo> list = getSharingFileAllPagesInfo(docId);

		final SharingFile sharingFileRoot = getSharingFile(docId);
		if (list == null && sharingFileRoot != null) {
			OnSampleCallback mOnSampleCallback = new OnSampleCallback() {

				@Override
				public void onSuccess(String content) {
					// 成功，从内存HashMap中获取所有页数据，开始刷新界面
					saveSharingFileAllPagesInfo2HashMap(docId, content);
					ArrayList<SharingFilePageInfo> list = getSharingFileAllPagesInfo(docId);
					SharingFile sharingFile = getSharingFile(docId);
					if (sharingFile != null && list != null)
						listener.onDisplayViewer(sharingFile, list);
					else
						listener.onErrorViewer();
				}

				@Override
				public void onFailure(int errorCode, String reason) {
					// 获取失败，有可能网络问题，有可能服务器出现故障，显示让用户主动操作的界面
					LogUtil.e(TAG, errorCode + "  " + reason);
					listener.onErrorViewer();
				}
			};
			// 不存在，从网络获取一遍
			String seesionId = sharingFileRoot.getSessionId();
			if (TextUtils.isEmpty(seesionId)) {
				mIClient.obtainSharingDocumentAllData(docId, mOnSampleCallback);
			} else {
				mIClient.obtainSharingDocumentAllPageData(seesionId, mOnSampleCallback);
			}
		} else {
			// 存在，刷新界面
			SharingFile sharingFile = getSharingFile(docId);
			if (sharingFile != null)
				listener.onDisplayViewer(sharingFile, list);
			else
				listener.onErrorViewer();
		}
	}

	@Override
	public void flipOverDocument(int sequenceNumber, int page, OnCallback cb) {
		if (sequenceNumber < 0)
			return;
		stopFlipPageTimer();
		flipOverTimer = new Timer();
		flipOverTimer.schedule(new FlipOverTask(sequenceNumber, page, cb), 500);
	}

	private void stopFlipPageTimer() {
		if (flipOverTimer != null) {
			flipOverTimer.cancel();
			flipOverTimer = null;
		}
	}

	private Timer flipOverTimer = null;

	private class FlipOverTask extends TimerTask {

		private int sequenceNumber;
		private int page;
		private OnCallback cb;

		public FlipOverTask(int sequenceNumber, int page, OnCallback cb) {
			this.sequenceNumber = sequenceNumber;
			this.page = page;
			this.cb = cb;
		}

		@Override
		public void run() {
			Client.getInstance().flipOverDocument(conferenceId, sequenceNumber, page, cb);
		}

	}

}
