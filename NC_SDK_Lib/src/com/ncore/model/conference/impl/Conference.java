package com.ncore.model.conference.impl;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ncore.model.conference.IConference;
import com.ncore.util.LogUtil;

/**
 * 会议室
 * 
 * @author xiaocui
 * 
 */
public class Conference implements IConference {
	private static final String TAG = "Conference";

	private static final String LABEL_END_TIME = "end_time";
	private static final String LABEL_OPEN_TIME = "opentime";
	private static final String LABEL_SUBJECT = "subject";
	private static final String LABEL_MODERATOR_PASSWORD = "moderator_password";
	private static final String LABEL_CAPACITY = "capacity";
	private static final String LABEL_PASSWORD = "password";
	private static final String LABEL_STATE = "state";
	private static final String LABEL_NAME = "name";
	private static final String LABEL_ID = "id";
	private static final String LABEL_STATUS = "status";
	private static final String LABEL_DURATION = "duration";
	private static final String LABEL_TYPE = "type";
	private static final String LABEL_CONTROLLER = "controller";
	private static final String LABEL_OWNER = "owner";
	private static final String LABEL_DISPLAYING = "displaying";

	private static final int LOCKED = 1 << 1;
	private static final int RECORDING = 1 << 2;

	public static final int CONF_STATE_INIT = 0;
	public static final int CONF_STATE_CREATED = 1;
	public static final int CONF_STATE_STARTED = 2;
	public static final int CONF_STATE_STOPPED = 3;
	public static final int CONF_STATE_DESTROYED = 4;

	/** 结束时间，秒 */
	private long endtime;
	/** 创建时间，秒 */
	private long opentime;
	/** 主题 */
	private String subject;
	/** 主持人密码 */
	private String moderatorPassword;
	/** 容量 */
	private int capacity;
	/** 密码 */
	private String password;
	/** 会议开关状态 */
	private int state;
	/** 会议名 */
	private String name;
	/** 会议ID */
	private String id;
	/** 状态：静音之类 */
	private int status;
	/** 持续时间 */
	private long duration;
	/** 会议类型：aduio、video */
	private String type;
	/** 主讲人 */
	private String controller;
	/** 创建者/拥有者 */
	private String owner;
	/** 正在共享信息 */
	private String displaying;

	public void refreshConferenceData(JSONObject json) {
		setOpentime(json.optLong(LABEL_OPEN_TIME));
		setEndtime(json.optLong(LABEL_END_TIME));
		setSubject(json.optString(LABEL_SUBJECT));
		setModeratorPassword(json.optString(LABEL_MODERATOR_PASSWORD));
		setCapacity(json.optInt(LABEL_CAPACITY));
		setPassword(json.optString(LABEL_PASSWORD));
		setState(json.optInt(LABEL_STATE));
		setName(json.optString(LABEL_NAME));
		setId(json.optString(LABEL_ID));
		setStatus(json.optInt(LABEL_STATUS));
		setDuration(json.optLong(LABEL_DURATION));
		setType(json.optString(LABEL_TYPE));
		setController(json.optString(LABEL_CONTROLLER));
		setOwner(json.optString(LABEL_OWNER));
		setDisplaying(json.optString(LABEL_DISPLAYING));
	}

	private void setOpentime(long opentime) {
		this.opentime = opentime;
	}

	private void setSubject(String subject) {
		this.subject = subject;
	}

	private void setModeratorPassword(String moderatorPassword) {
		this.moderatorPassword = moderatorPassword;
	}

	private void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	private void setPassword(String password) {
		this.password = password;
	}

	private void setState(int state) {
		this.state = state;
	}

	private void setName(String name) {
		this.name = name;
	}

	private void setId(String id) {
		this.id = id;
	}

	private void setStatus(int status) {
		this.status = status;
	}

	private void setDuration(long duration) {
		this.duration = duration;
	}

	private void setType(String type) {
		this.type = type;
	}

	private void setController(String controller) {
		if (!TextUtils.equals(this.controller, controller))
			LogUtil.i(TAG, "主讲人改变了");
		this.controller = controller;
	}

	private void setOwner(String owner) {
		this.owner = owner;
	}

	private void setDisplaying(String displaying) {
		if (!TextUtils.equals(this.displaying, displaying))
			LogUtil.i(TAG, "文档改变了");
		this.displaying = displaying;
	}

	public void setEndtime(long endtime) {
		this.endtime = endtime;
	}

	/**
	 * 结束时间
	 * 
	 * @return
	 */
	@Override
	public long getEndtime() {
		return endtime;
	}

	/**
	 * 创建时间
	 */
	@Override
	public long getOpentime() {
		return opentime;
	}

	/**
	 * 主题
	 */
	@Override
	public String getSubject() {
		return subject;
	}

	/**
	 * 主持人密码
	 */
	@Override
	public String getModeratorPassword() {
		return moderatorPassword;
	}

	/** 容量 */
	@Override
	public int getCapacity() {
		return capacity;
	}

	/** 密码 */
	@Override
	public String getPassword() {
		return password;
	}

	/**
	 * 会议开关状态
	 */
	@Override
	public int getState() {
		return state;
	}

	/**
	 * 会议名
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * 会议ID
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * 是否已上锁
	 * 
	 * @return
	 */
	@Override
	public boolean isLocked() {
		return (status & LOCKED) == LOCKED;
	}

	/**
	 * 是否正在录音
	 * 
	 * @return
	 */
	@Override
	public boolean isRecording() {
		return (status & RECORDING) == RECORDING;
	}

	/**
	 * 持续时间
	 */
	@Override
	public long getDuration() {
		return duration;
	}

	/**
	 * 会议类型：aduio、video
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public boolean isController(String username) {
		return !TextUtils.isEmpty(controller) && !TextUtils.isEmpty(username) && controller.contains(username);
	}

	@Override
	public String getController() {
		return controller;
	}

	@Override
	public boolean isOwner(String username) {
		return !TextUtils.isEmpty(owner) && !TextUtils.isEmpty(username) && owner.contains(username);
	}

	@Override
	public String getOwner() {
		return owner;
	}

	@Override
	public String getConferenceUri() {
		return "sip:conference@" + com.ncore.config.NCConstants.getSipDomain();
	}

	@Override
	public String getConferenceDTMF() {
		return name + "#" + password + "#";
	}

	@Override
	public boolean isDestroyed() {
		return state == CONF_STATE_DESTROYED;
	}

	@Override
	public String getDisplayingInfo() {
		return displaying;
	}

}
