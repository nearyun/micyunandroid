package com.ncore.model.conference;

public interface OnConferenceInitializeCallback {
	public void onStart();

	public void onCompleted();

	public void onFailure(int errorCode, String reason);

	public void onWSConnectFailure();

	public void onProgress(int progress, int total, String message);
}
