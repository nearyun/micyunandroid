package com.ncore.model.conference;

import java.util.ArrayList;

import com.ncore.model.Participant;
import com.ncore.model.sharing.SharingFile;
import com.ncore.model.sharing.SharingFilePageInfo;

public interface IConferenceDataManager {

	/**
	 * 获取会议信息对象
	 * 
	 * @return
	 */
	public IConference getConference();

	/**
	 * 获取共享文档对象列表
	 * 
	 * @return
	 */
	public ArrayList<SharingFile> getSharingFiles();

	/**
	 * 获取指定docId的文档
	 * 
	 * @param docId
	 * @return
	 */
	public SharingFile getSharingFile(String docId);

	/**
	 * 获取与会者对象列表
	 * 
	 * @return
	 */
	public ArrayList<Participant> getParticipants();

	/**
	 * 获取指定userId的人员
	 * 
	 * @return
	 */
	public Participant getParticipant(String userId);

	/**
	 * 获取指定文档的所有页数据
	 * 
	 * @param docId
	 * @return
	 */
	public ArrayList<SharingFilePageInfo> getSharingFileAllPagesInfo(String docId);

	/**
	 * 删除指定文档的所有页数据
	 * 
	 * @param docId
	 */
	public void deleteSharingFileAllPagesInfo(String docId);

	/**
	 * 清空重置所有内容
	 */
	public void reset();

}
