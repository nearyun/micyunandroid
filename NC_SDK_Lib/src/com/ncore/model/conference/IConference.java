package com.ncore.model.conference;

/**
 * 会议室接口
 * 
 * @author xiaohua
 * 
 */
public interface IConference {
	/** 结束时间 */
	public long getEndtime();
	
	/** 创建时间 */
	public long getOpentime();

	/** 主题 */
	public String getSubject();

	/** 主持人密码 */
	public String getModeratorPassword();

	/** 容量 */
	public int getCapacity();

	/** 密码 */
	public String getPassword();

	/** 会议开关状态 */
	public int getState();

	/** 会议名 */
	public String getName();

	/** 会议ID */
	public String getId();

	/**
	 * 是否已上锁
	 * 
	 * @return
	 */
	public boolean isLocked();

	/**
	 * 是否正在录音
	 * 
	 * @return
	 */
	public boolean isRecording();

	/** 持续时间 */
	public long getDuration();

	/** 会议类型：aduio、video */
	public String getType();

	/**
	 * 判断用户名是否当前会议的演讲者
	 * 
	 * @param username
	 * @return
	 */
	public boolean isController(String username);

	/**
	 * 判断用户名是否当前会议的拥有者
	 * 
	 * @param username
	 * @return
	 */
	public boolean isOwner(String username);

	/**
	 * 获取演讲者
	 * 
	 * @return
	 */
	public String getController();

	/**
	 * 获取会议拥有者
	 * 
	 * @return
	 */
	public String getOwner();

	/**
	 * 获取会议的sip uri
	 * 
	 * @return
	 */
	public String getConferenceUri();

	/**
	 * 获取会议的DTMF
	 * 
	 * @return
	 */
	public String getConferenceDTMF();

	/**
	 * 是否被销毁
	 * 
	 * @return
	 */
	public boolean isDestroyed();

	/**
	 * 获取正在共享的信息
	 * 
	 * @return
	 */
	public String getDisplayingInfo();
}
