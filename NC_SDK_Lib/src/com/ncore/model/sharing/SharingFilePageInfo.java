package com.ncore.model.sharing;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * 正在共享的某一页的信息
 * 
 * @author xiaohua
 * 
 */
public class SharingFilePageInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8363804544798350328L;
	// {
	// "doc_id":"",
	// "type":"",
	// "url":"",
	// "img_url":"",
	// "page":1,
	// "width":768,
	// "height":1024
	// }
	private String docid;
	private String type;
	private String imageUrl;
	private int pageNum;
	private int width;
	private int height;

	// private static final String LABEL_DOC_ID = "doc_id";
	private static final String LABEL_TYPE = "type";
	private static final String LABEL_IMG_URL = "img_url";
	private static final String LABEL_PAGE = "page";
	private static final String LABEL_WIDTH = "width";
	private static final String LABEL_HEIGHT = "height";

	public SharingFilePageInfo(JSONObject json) {
		// docid = json.optString(LABEL_DOC_ID);
		type = json.optString(LABEL_TYPE);
		imageUrl = json.optString(LABEL_IMG_URL);
		pageNum = json.optInt(LABEL_PAGE);
		width = json.optInt(LABEL_WIDTH);
		height = json.optInt(LABEL_HEIGHT);
	}

	/**
	 * @deprecated
	 * @return
	 */
	public String getDocId() {
		return docid;
	}

	public String getType() {
		return type;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public int getPageNum() {
		return pageNum;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	// public String getLocalPathUrl() {
	// return "/" + docid + "/page" + pageNum + "/" + width + "x" + height +
	// ".png";
	// }
}
