package com.ncore.model.sharing;

import org.json.JSONObject;

/**
 * 已上传到我方网盘的文件
 * 
 * @author xiaocui
 * 
 */
public class NetworkFileInfo extends AbsFileInfo {

	public static final int WAIT_CONVERT = 0;// 等待处理
	public static final int CONVERT_SUCCESS = 1;// 处理成功，已经全部转换为png
	public static final int CONVERTING = 2;// 正在由pdf转换为png
	public static final int CONVERT_FAILURE = 3;// 处理失败
	public static final int CONVERT_TIMEOUT = 4;// 处理超时
	public static final int PREPROCESSING = 5;// 正在由office文档转换为pdf

	private String docId;
	private int pages = 0;
	private int convertedPage = 0;
	private int state;
	private String thumbUrl;
	private long uploadTime = 0L;// 秒级别
	private int width = 0;
	private int height = 0;

	public NetworkFileInfo(JSONObject json) {
		docId = json.optString("doc_id");
		mName = json.optString("file_name");
		convertedPage = json.optInt("converted_page");
		state = json.optInt("state");
		pages = json.optInt("file_pages");
		mExtension = json.optString("file_type");
		thumbUrl = json.optString("thumb_url");
		uploadTime = json.optLong("upload_time");
		width = json.optInt("width");
		height = json.optInt("height");
	}

	// /**
	// * @deprecated
	// * @return
	// */
	// public JSONObject toJson() {
	// JSONObject json = new JSONObject();
	// try {
	// json.put("doc_id", docId);
	// json.put("file_name", mName);
	// json.put("file_pages", pages);
	// json.put("file_type", mExtension);
	// json.put("thumb_url", thumbUrl);
	// json.put("sharing", 0);
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	// return json;
	// }

	/**
	 * 获取当前网络文件的状态</p>
	 * 
	 * 0:等待处理，上传完成尚未开始处理或者已经转换为pdf等待转换为png</p> 1:处理成功，已经全部转换为png</p>
	 * 2:正在处理，正在由pdf转换为png</p> 3:处理失败</p> 4:处理超时</p>
	 * 5:正在由doc、docx、ppt、pptx转换到pdf</p>
	 * 
	 * @return
	 */
	public int getState() {
		return state;
	}

	public long getUploadTime() {
		return uploadTime;
	}

	/**
	 * 是否转换完成
	 * 
	 * @return
	 */
	public boolean isFinishConvert() {
		return convertedPage == pages;
	}

	public String getDocId() {
		return docId;
	}

	public int getPages() {
		return pages;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
