package com.ncore.model.sharing;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

/**
 * 正在共享的文件（包含所有会议中正在共享的）
 * 
 * @author xiaocui
 * 
 */
public class SharingFile implements Serializable {

	// {
	// "fileType": "jpg", // 文件类型
	// "curPage": 1, // 当前页码
	// "fileThumbnail":
	// "http://micyun.com/docshare/{docId}/page1/1200x1200.jpg", // 当前文档缩略图全路径
	// "fileOwner": "8888888", //上传者(userId)
	// "confId": "07000220-fe92-11e4-a416-00224d7c553b", // 会议id
	// "filePages": 1, // 总页数
	// "fileName": "20120531656.jpg", // 文档名
	// "displaying": 0, // 是否正在演示 0:no; 1:yes
	// "docId": "43A5C5AD-BB7E-B4F2-990A-3594DC0D66B1", // 文档docId
	// "fileStatus": 0 // 在当前会议的状态 0:正常; 1:已删除
	// }

	/**
	 * 
	 */
	private static final long serialVersionUID = 2964696298691186965L;
	private int sequenceNumber;// 在列表中的排序
	private String docId;// 文档docId
	private String fileType;// 文件类型
	private String fileThumbnail;// 当前文档缩略图全路径
	private String fileOwner;// 共享人
	private String ownerName;// 共享人名
	private int currentPage;// 当前页码
	private int filePages;// 总页数
	private String conferenceId;// 会议id
	private String fileName;// 文档名
	private int fileStatus;// 在当前会议的状态 0:正常; 1:已删除
	private String baseUri;
	private int width = 0;
	private int height = 0;
	private String sessionId;

	private static final int IS_DELETED = 1;

	private static final String LABEL_DOC_ID = "docId";
	private static final String LABEL_FILE_TYPE = "fileType";
	private static final String LABEL_FILE_THUMBNAIL = "fileThumbnail";
	private static final String LABEL_FILE_OWNER = "fileOwner";
	private static final String LABEL_OWNER_NAME = "ownerName";
	private static final String LABEL_CURRENT_PAGE = "currentPage";
	private static final String LABEL_FILE_PAGES = "filePages";
	private static final String LABEL_CONF_ID = "confId";
	private static final String LABEL_FILE_NAME = "fileName";
	private static final String LABEL_FILE_STATUS = "fileStatus";
	private static final String LABEL_BASE_URI = "baseuri";
	private static final String LABEL_WIDTH = "width";
	private static final String LABEL_HEIGHT = "height";
	private static final String LABEL_SESSION_ID = "sessionId";

	public static final SharingFile create(int seq, JSONObject json) {
		SharingFile sf = new SharingFile(seq);
		sf.docId = json.optString(LABEL_DOC_ID);
		sf.fileType = json.optString(LABEL_FILE_TYPE);
		sf.ownerName = json.optString(LABEL_OWNER_NAME);
		sf.fileThumbnail = json.optString(LABEL_FILE_THUMBNAIL);
		sf.fileOwner = json.optString(LABEL_FILE_OWNER);
		sf.currentPage = json.optInt(LABEL_CURRENT_PAGE);
		sf.filePages = json.optInt(LABEL_FILE_PAGES);
		sf.conferenceId = json.optString(LABEL_CONF_ID);
		sf.fileName = json.optString(LABEL_FILE_NAME);
		sf.fileStatus = json.optInt(LABEL_FILE_STATUS);
		sf.baseUri = json.optString(LABEL_BASE_URI);
		sf.width = json.optInt(LABEL_WIDTH);
		sf.height = json.optInt(LABEL_HEIGHT);
		sf.sessionId = json.optString(LABEL_SESSION_ID);
		return sf;
	}

	private SharingFile(int seq) {
		sequenceNumber = seq;
	}

	public static JSONObject toJson(String confId, NetworkFileInfo file, String fileOwner, String ownerName,
			String baseUri, String sessionId) {
		JSONObject json = new JSONObject();
		try {
			json.put(LABEL_DOC_ID, file.getDocId());
			json.put(LABEL_FILE_TYPE, file.getExtension());
			json.put(LABEL_FILE_THUMBNAIL, file.getThumbUrl());
			json.put(LABEL_FILE_OWNER, fileOwner);
			json.put(LABEL_OWNER_NAME, ownerName);
			json.put(LABEL_CURRENT_PAGE, 1);
			json.put(LABEL_FILE_PAGES, file.getPages());
			json.put(LABEL_CONF_ID, confId);
			json.put(LABEL_FILE_NAME, file.getName());
			json.put(LABEL_FILE_STATUS, 0);
			json.put(LABEL_BASE_URI, baseUri);
			json.put(LABEL_WIDTH, file.getWidth());
			json.put(LABEL_HEIGHT, file.getHeight());
			json.put(LABEL_SESSION_ID, sessionId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * 是否是默认文档
	 * 
	 * @param docId
	 * @return
	 */
	public static final boolean isDefaultFile(String docId) {
		return TextUtils.equals("00000000-0000-0000-0000-000000000000", docId);
	}

	/**
	 * 上传者名称
	 * 
	 * @return
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * 获取文档的序列号
	 * 
	 * @return
	 */
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocId() {
		return docId;
	}

	public String getSessionId() {
		return sessionId;
	}

	/**
	 * 
	 * @return
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * 
	 * @return
	 */
	public String getFileThumbnail() {
		return fileThumbnail;
	}

	/**
	 * 获取
	 * 
	 * @return
	 */
	public String getFileOwner() {
		return fileOwner;
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * 获取文件总页码
	 * 
	 * @return
	 */
	public int getFilePages() {
		return filePages;
	}

	/**
	 * 获取所在的会议id
	 * 
	 * @return
	 */
	public String getConferenceId() {
		return conferenceId;
	}

	/**
	 * 获取文件名
	 * 
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * 是否被删除了
	 * 
	 * @return
	 */
	public boolean isDelete() {
		return IS_DELETED == fileStatus;
	}

	public int getWidth() {
		return width;
	}

	public int getHeith() {
		return height;
	}

}
