package com.ncore.model.sharing;

public abstract class AbsFileInfo {
	/** 文件名 */
	protected String mName;
	/** 目录标识 */
	protected boolean isDirectory = false;
	/** 文件类型 */
	protected String mExtension;

	public boolean isDirectory() {
		return isDirectory;
	}

	public String getName() {
		return mName;
	}

	public String getExtension() {
		return mExtension;
	}
}
