package com.ncore.config;

import org.json.JSONObject;

import android.content.Context;

import com.ncore.sharedpreference.HostInfoSharedPreference;

public class NCConstants {

	public static final boolean BETA = false;

	// php host
	private static final String INTRA_NET = "http://192.168.1.119:88";
	private static final String OUTSIDE_NET = "http://www.micyun.com:80";
	public static String PHP_BASE_URL = BETA ? INTRA_NET : OUTSIDE_NET;

	public static final void changePhpUrl(boolean flag) {
		PHP_BASE_URL = flag ? INTRA_NET : OUTSIDE_NET;
	}

	// sip domain
	private static String SIP_DOMAIN = null;// micyun.com
	// outproxy server，外网暂时使用120.25.216.119，适当的时候使用(未知什么时候)sip.micyun.com
	private static String OUTPROXY_SERVER = null;// 120.25.216.119
	// ms server 媒体服务器
	private static String MS_ICE_SERVER = null;// stun.micyun.com
	// ws host
	private static String WS_SERVER = null;// ws.micyun.com
	// DISP服务器的ip地址
	private static String DISP_SERVER = null;// disp.micyun.com
	// 文档服务器
	private static String DOC_SERVER = null;
	// 通讯录服务器
	private static String COMPANY_SERVER = null;

	public static final void setHost(Context context, JSONObject hosts) {
		SIP_DOMAIN = hosts.optString("sipdomain");
		OUTPROXY_SERVER = hosts.optString("proxyserver");
		MS_ICE_SERVER = hosts.optString("iceserver");
		WS_SERVER = hosts.optString("wsserver");
		DISP_SERVER = hosts.optString("dispserver");
		DOC_SERVER = hosts.optString("docdomain");
		COMPANY_SERVER = hosts.optString("companyserver");
		HostInfoSharedPreference.saveHostsInformation(context, hosts.toString());
	}

	public static String getSipDomain() {
		return SIP_DOMAIN;
	}

	public static String getOutproxyServer() {
		return OUTPROXY_SERVER;
	}

	public static String getIceServer() {
		return MS_ICE_SERVER;
	}

	public static final String getWebsocketBaseUrl() {
		return "ws://" + WS_SERVER + "?confId={0}&userId={1}&uuid={2}";
	}

	public static final String getDispBaseUrl() {
		return "http://" + DISP_SERVER;
	}

	public static String getContactServer() {
		return "http://" + COMPANY_SERVER;
	}

	public static final String getDocumentBaseUrl() {
		return "http://" + DOC_SERVER;
	}

	/** 用户协议书 */
	public static final String getProtocolUrl() {
		return NCConstants.PHP_BASE_URL + "/agreement";
	}

	/** 崩溃报告链接 */
	public static final String getTraceLogUrl() {
		return NCConstants.PHP_BASE_URL + "/trace_save.php";
	}

}
