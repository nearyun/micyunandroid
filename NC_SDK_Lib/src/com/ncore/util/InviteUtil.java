package com.ncore.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.ncore.config.NCConstants;

public class InviteUtil {

	/**
	 * 生成邀请链接
	 * 
	 * @param conf
	 * @param confpwd
	 * @param nickName
	 * @return
	 */
	public static final String generateInviteUrl(String confno, String confpwd, String nickName) {
		String template = NCConstants.PHP_BASE_URL + "/j?n=%s&c=%s&p=%s";
		try {
			nickName = URLEncoder.encode(nickName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return String.format(template, nickName, confno, confpwd);
	}

	public static final String generateInviteUrl(String confno, String confpwd) {
		return generateInviteUrl(confno, confpwd, "游客");
	}

	public static final String generateInviteUrl(String confId) {
		return NCConstants.PHP_BASE_URL + "/j?confid=" + confId;
	}

}
