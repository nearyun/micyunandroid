package com.ncore.util;

import android.content.Context;

import com.ncore.config.NCConstants;
import com.ncore.util.log.trace.ExceptionHandler;
import com.tornado.util.XHLogUtil;

public class LogUtil extends XHLogUtil {

	/**
	 * 注册异常报告
	 * <p/>
	 * 报告内容会在服务器的/tmp目录下
	 * 
	 * @param context
	 */
	public static final void registerExceptionReporter(Context context) {
		ExceptionHandler.register(context, NCConstants.getTraceLogUrl());
	}

}
