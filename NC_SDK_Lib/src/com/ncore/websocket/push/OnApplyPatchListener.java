package com.ncore.websocket.push;

public interface OnApplyPatchListener {
	public void onApplyConference(String conferenceId, int etag, String patch);

	public void onApplyParticipants(String conferenceId, int etag, String patch);

	public void onApplySharingFiles(String conferenceId, int etag, String patch);

}
