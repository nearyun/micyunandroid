package com.ncore.websocket.push.impl;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;

import com.ncore.config.NCConstants;
import com.ncore.model.client.PublishQuest;
import com.ncore.model.ws.WSPublish;
import com.ncore.util.LogUtil;
import com.ncore.websocket.WebSocketClient;
import com.ncore.websocket.push.IPushWebSocket;
import com.ncore.websocket.push.OnApplyPatchListener;

/**
 * websocket接收推送的线程类
 * 
 * @author xiaohua
 * 
 */
public class PushWebSocket implements IPushWebSocket {
	private static final String TAG = PushWebSocket.class.getSimpleName();
	private WebSocketClient wsClient;
	private Handler mHandler = new Handler();
	private boolean mShutDown = false;
	private String mConferenceId = null;
	private SparseArray<PublishQuest> questArray = new SparseArray<PublishQuest>();

	public interface OnWebSocketConnectedListener {
		public void onConnected();
	}

	private Timer websocketTimer = null;
	private final int INTERVAL_TIME = 30000;

	private void startKeepAliveThread() {
		stopKeepAliveThread();
		websocketTimer = new Timer();
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				LogUtil.i(TAG, "发送websocket心跳包");
				sendMessage("{}");
			}
		};
		websocketTimer.schedule(task, 0, INTERVAL_TIME);
	}

	/**
	 * 停止keepAlive线程
	 */
	private synchronized void stopKeepAliveThread() {
		if (websocketTimer != null) {
			websocketTimer.cancel();
			websocketTimer = null;
		}
	}

	/**
	 * 构造函数
	 */
	public PushWebSocket(String conferenceId, String userId, String uuid,
			final OnWebSocketConnectedListener mOnWebSocketConnected) {
		mConferenceId = conferenceId;
		final String url = MessageFormat.format(NCConstants.getWebsocketBaseUrl(), conferenceId, userId, uuid);
		wsClient = new WebSocketClient(URI.create(url), new WebSocketClient.Listener() {

			@Override
			public void onConnect() {
				LogUtil.i(TAG, "连接成功 " + url);
				// startKeepAliveThread();
				if (mOnWebSocketConnected != null)
					mOnWebSocketConnected.onConnected();
			}

			@Override
			public void onMessage(final String message) {
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						LogUtil.i(TAG, String.format("Rx ==> %s", message));
						handleMessage(message);
					}
				});
			}

			@Override
			public void onMessage(byte[] data) {
				LogUtil.i(TAG, String.format("binary_msg ==> %s", data.toString()));
			}

			@Override
			public void onDisconnect(int code, String reason) {
				LogUtil.w(TAG, String.format("WS Disconnected! Code: %d Reason: %s", code, reason));
				stopKeepAliveThread();
				reconnectWS();
			}

			@Override
			public void onError(Exception error) {
				LogUtil.e(TAG, "Error --> " + error);
				stopKeepAliveThread();
				reconnectWS();
			}

		}, null);
	}

	/**
	 * 失去连接后，重新连接
	 */
	private void reconnectWS() {
		if (!mShutDown) {
			mHandler.postDelayed(new Runnable() {

				@Override
				public void run() {
					if (!mShutDown) {
						LogUtil.d(TAG, "restart ws now ");
						start();
					}
				}
			}, 1000);
		}
	}

	/**
	 * 开始监听
	 */
	@Override
	public synchronized void start() {
		mShutDown = false;
		if (wsClient != null && !wsClient.isConnected()) {
			LogUtil.d(TAG, "ws start to connect");
			wsClient.connect();
		} else {
			LogUtil.d(TAG, "ws still connectting");
		}
	}

	/**
	 * 停止监听
	 */
	@Override
	public void stop() {
		mShutDown = true;
		if (wsClient != null && wsClient.isConnected()) {
			sendMessage(WSPublish.getUnsubscribeString(mConferenceId, null));
			wsClient.disconnect();
		}
		stopKeepAliveThread();
	}

	public void sendMessage(String content) {
		if (wsClient != null && wsClient.isConnected()) {
			wsClient.send(content);
		} else {
			LogUtil.e(TAG, "ws send message failure");
		}
	}

	@Override
	public synchronized void sendMessage(PublishQuest quest) {
		if (wsClient != null && wsClient.isConnected()) {
			questArray.put(quest.getId(), quest);
			wsClient.send(quest.getContent());
		} else {
			LogUtil.e(TAG, "ws send message failure");
		}
	}

	/** 补丁监听器 */
	private OnApplyPatchListener mPatchListener;

	public void setOnApplyPatchListener(OnApplyPatchListener listener) {
		this.mPatchListener = listener;
	}

	private final Pattern PATTERN_CONFERENCE = Pattern
			.compile("^/conferences/[0-9a-zA-Z-]{8}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{12}$");
	private final Pattern PATTERN_MEMBERS = Pattern
			.compile("^/conferences/[0-9a-zA-Z-]{8}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{12}/members$");
	private final Pattern PATTERN_SHARINGS = Pattern
			.compile("^/conferences/[0-9a-zA-Z-]{8}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{4}-[0-9a-zA-Z-]{12}/sharings$");
	private final Pattern PATTERN_SEPARATOR = Pattern.compile("/");

	private final String LABEL_ETAG = "etag";
	private final String LABEL_CHANNEL = "channel";
	private final String LABEL_ACTION = "action";
	private final String LABEL_DATA = "data";
	private final String LABEL_NOTIFY = "notify";
	private final String LABEL_ACK = "ack";
	private final String LABEL_ID = "id";
	private final String LABEL_RESULT = "result";

	private synchronized void handleMessage(String message) {
		try {
			JSONObject root = new JSONObject(message);
			String channel = root.optString(LABEL_CHANNEL);
			String action = root.optString(LABEL_ACTION);
			String data = root.optString(LABEL_DATA);
			int etag = root.optInt(LABEL_ETAG);
			if (TextUtils.equals(action, LABEL_ACK)) {
				int id = root.optInt(LABEL_ID);
				PublishQuest pq = questArray.get(id);
				if (pq != null) {
					boolean result = root.optJSONObject(LABEL_DATA).optBoolean(LABEL_RESULT);
					pq.excuteCallback(id, result);
					questArray.remove(id);
				}
				return;
			} else if (!TextUtils.equals(action, LABEL_NOTIFY)) {
				LogUtil.d(TAG, "message action not be notify : " + action);
				return;
			}

			if (mPatchListener == null)
				return;
			String[] items = PATTERN_SEPARATOR.split(channel);
			// 0 ==> 空字符串
			// 1 ==> conference
			// 2 ==> conferenceId
			// 3 ==> members 或 sharings
			if (PATTERN_SHARINGS.matcher(channel).matches()) {
				mPatchListener.onApplySharingFiles(items[2], etag, data);
			} else if (PATTERN_MEMBERS.matcher(channel).matches()) {
				mPatchListener.onApplyParticipants(items[2], etag, data);
			} else if (PATTERN_CONFERENCE.matcher(channel).matches()) {
				mPatchListener.onApplyConference(items[2], etag, data);
			}
		} catch (Exception e) {
			LogUtil.printStackTrace(e);
		}
	}

}
