package com.ncore.websocket.push;

import com.ncore.model.client.PublishQuest;

public interface IPushWebSocket {
	/**
	 * 开始监听
	 */
	public void start();

	/**
	 * 停止监听
	 */
	public void stop();

	/**
	 * 发送消息
	 * 
	 * @param msg
	 */
	public void sendMessage(PublishQuest quest);

}
