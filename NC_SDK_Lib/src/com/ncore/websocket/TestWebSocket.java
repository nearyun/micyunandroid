package com.ncore.websocket;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.ncore.util.LogUtil;

public class TestWebSocket {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestWebSocket ws = new TestWebSocket();
		if (ws.client.isConnected())
			ws.client.send("{\"action\":\"subscribe\",\"channel\":\"/conferences/7ae4eb84-7935-11e4-a411-00224d7c553b\"}");
	}

	WebSocketClient client;

	private void startWebsocket() {
		List<BasicNameValuePair> extraHeaders = Arrays.asList(new BasicNameValuePair("Cookie", "session=abcd"));

		client = new WebSocketClient(URI.create("ws://192.168.1.119:8081"), new WebSocketClient.Listener() {
			@Override
			public void onConnect() {
				LogUtil.printf("Connected!");

			}

			@Override
			public void onMessage(String message) {
				LogUtil.printf(String.format("Got string message! %s", message));
			}

			@Override
			public void onMessage(byte[] data) {
				LogUtil.printf(String.format("Got binary message! %s", data.toString()));
			}

			@Override
			public void onDisconnect(int code, String reason) {
				LogUtil.printf(String.format("Disconnected! Code: %d Reason: %s", code, reason));
			}

			@Override
			public void onError(Exception error) {
				LogUtil.printf("Error! " + error);
			}
		}, extraHeaders);

		client.connect();

		// Later…

		// client.send(new byte[] { (byte) 0xDE, (byte) 0xAD, (byte) 0xBE,
		// (byte) 0xEF });
		// client.disconnect();
	}
}
